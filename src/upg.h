#pragma once

namespace libxwing2 {

enum class Upg {
  // *** First Edition Wave 13 ***
  // SWX74 - Saw's Renegades
  R3Astro, R4Astro, PvtWing, SmSFoils, MYarro, SGerrera, DeadmansSw, AdvSensors, TrickShot, ProtTrp,
  // SWX75 - TIE Reaper Expansion Pack
  DeathTr, DirKrennic, ISBSlicer, TactOff, Juke, SwarmTac,
  // *** Wave 1 ***
  // SWZ01 - Core Set
  R2Astro, R2D2, R5Astro, R5D8, HeightPerc, InstAim, Sense, SNReflex, Afterburn, HullUpg, ShieldUpg, Elusive, Outmaneuv, Predator,
  // SWZ04 - Lando's Millennium Falcon
  ChewieScum, L337, LCalrissianScum, Qira, SeasonNav, TBeckett, AgileGunner, HSoloScu, RigCargoCh, TactScramb, Composure, Intimidat, LandosMF,
  // SWZ06 - Rebel Conversion Kit
  ChopperA,
  HLC, IonCan, JamBeam, TracBeam,
  BMalbus, C3POReb, CAndor, ChewieReb, ChopperC, FLSlicer, Gonk, HSyndulla, Informant, JErso, KJarrus, LCalrissianReb, LOrganaReb, NNunb, NovTech, PerCPilot, R2D2C, SWren_Crew, ZOrrelios,
  Bistan, EBridger, HSoloReb, HsGunner, LSkywalkerG, SkBombard, VetTailGun, VetTurret,
  CloakDev, ContraCyb, FeedbackAr, InertDamp,
  AblatPlat, AdvSLAM, ElectBaff, EngUpg, MuniFailSa, StaDcVanes, StealthDev,
  ClustMsl, ConcusMsl, HomingMsl, IonMsl, PRockets,
  Bomblet, ConnerNet, ProtonBmb, ProxMine, SeismicCh,
  CollisDet, FCS,
  CrackShot, Daredevil, DebGambit, ExpHan, LoneWolf, Marksman, SatSalvo, Selfless, SquadLdr,
  Ghost, MilFalcon, MoldyCrow, Outrider, Phantom,
  AdvProtTrp, IonTrp,
  DorsalTrt, IonCanTrt,
  // SWZ07 - Galactic Empire Conversion Kit
  Os1ArsenalLdt, Xg1AssaultCfg,
  AdmSloane, AgentKallus, CRee, DVader, EmpPalp, GrandInq, GrandMoffTarkin, MinTua, MoffJerjerrod, SeventhSister,
  FifthBrother,
  BarRockets,
  TrajSim,
  Ruthless,
  Dauntless, ST321,
  // SWZ08 - Scum and Villainy Conversion Kit
  Genius, R5P8, R5TK,
  TripleZero, FourLOM, BFett, CBane, CVizago, IG88D, Jabba, KOnyo, LRazzi, Maul, UPlutt, Zuckuss,
  Bossk, BT1, Dengar, Greedo,
  Fearless,
  Andrasta, Havoc, HoundsTooth, IG2000, Marauder, MistHunter, PunishingOne, ShadowCaster, SlaveI, Virago,
  // *** Wave 2 ***
  // SWZ18 - First Order Conversion Kit
  CaptPhasma, GenHux, KRen, PetOffThanisson, SupLdrSnoke,
  Hate, PredictiveShot,
  SpecForGun,
  Fanatical,
  AdvOptics, BiohexCodes, HsTrackingData, PatAnalyzer, PThrusters, TargetSync,
  // SWZ19 - Resistance Conversion Kit
  BB8, BBAstro, M9G8, R2HA, R5X3,
  IntSFoils,
  C3PORes, ChewieRes, HSolo, RTico,
  Finn, PTico, Rey,
  Heroic,
  BlackOne, ReysMF,
  // SWZ22 - RZ-2 A-wing
  FPaint,
  // SWZ29 - Servants of Strife
  GrapStruts,
  EnShellCh,
  ImpervPlat,
  Kraken, TV94,
  Treacherous,
  SoullessOne,
  // SWZ30 - Sith Infiltrator
  ChanPalp, CountDooku, GenGrievous,
  BrilEva,
  DRK1ProbeDroids,
  K2B4,
  Scimitar,
  // SWZ31 - Vulture-class
  DiscordMsl,
  // SWZ32 - Guardians of the Republic
  R4PAstro, R4P17,
  CalLasTgt, Delta7B,
  BattleMed,
  SparePartsCan, SyncCon,
  Dedicated,
  // SWZ33 - ARC-170
  R4P44,
  CloneCmdrCody,
  SeventhFltGun,
  // SWZ40 - N-1
  R2A6, R2C4,
  PassiveSensors,
  PlasmaTrp,
  // SWZ41 - Hyena
  LandStruts,
  DiaBorMsl,
  DelayedFuses,
  ElectroProtBmb,
  TA175,
  // SWZ45 - Resistance Transport
  Autoblasters,
  AHoldo, GA97, KConnix, KSella, LDAcy, LOrganaRes, PZ4CO,
  AngledDef,
  // SWZ47 - Nantex-class
  TargetComp,
  Ensnare,
  GravDef,
  SnapShot,
  // SWZ48 - BTL-B Y-wing
  C110P,
  C3PORep,
  Foresight,
  PrecogReflexes,
  ATano,
  // SWZ53 - Huge Ship Conversion Kit
  AdaptiveShlds, BoostedScanners, OptPowerCore, TibannaRes,
  AdmOzzel, Azmorigan, CaptNeeda, CRieekan, JDodonna, RAntilles, StalwartCapt, StratCmdr,
  CorsairRefit,
  TFarr,
  IonCanBat, OrdTubes, PointDefBat, TargetingBat, TurbolaserBat,
  QRLocks, SabMap, ScanBaff,
  BombSpec, CommsTeam, DamageCtrlTeam, GunnerySpec, IGRMDroids, OrdnanceTeam, SensExp,
  Assailer, BloodCrow, BrightHope, BrokenHorn, Corvus, DodonnasPride, Impetuous, InsatiableWorrt, Instigator, JainasLight, Liberator, Luminous, MerchantOne, QuantumStorm, Requiem, Suppressor, TantiveIV, Thunderstrike, Vector,
  // SWZ57 - Epic Battles
  AgentotEmp, DreadHunter, FOElite, VetWingLdr,
  // SWZ62 - Major Vonreg's TIE
  MagPulseWar,
  ProudTrad,
  DeuteriumPC,
  // SWZ63 - Fireball
  R1J5,
  CoaxiumHyp,
  KazsFireball,
  // SWZ65 - Fully Loaded
  ClusterMines, IonBmb,
  // SWZ66 - Hotshots and Aces
  StabSFoils,
  K2SO,
  // SWZ67 - TIE/rb Heavy
  SyncLasCan,
  ManAssMGK300, TarAssMGK300,
  DeadeyeShot,
  IonLimOverride,
  // SWZ68 - Heralds of Hope
  R2D2_A_Res, R6D8,
  Underslung,
  OverdriveThr,
  BackwardsTail, StarbirdSlash,
  AutoTgtPri,
  // SWZ69 - Xi-class
  AgentTerex, CmdrMalarus, CmdrPyre,
  SensorBuoySuite,
  // SWZ70 - LAAT/i
  ASecura, Fives, GhostCompany, KFisto, PKoon, Wolfpack, Yoda,
  CloneCaptRex, SuppGunner,
  MultiMslPods,
  // SWZ71 - HMP
  Repulsorlift,
  ConcussionBmb,
  Kalani,
  // SWZ79 - Eta-2 Actis
  R2D2_A_Rep,
  JediCmdr,
  ExtManeuvers, Patience,
  Syliure31,
  MargSabl,
  // SWZ80 - Nimbus-class V-wing
  Q7Astro, R7A7,
  Alpha3B, Alpha3E,
  PrecIonEng,
  ThermalDet,
  // SWZ81 - Droid Tri-Fighter
  InterceptBoost,
  XX23,
  IndepCalc,
  // SWX82 - Firespray (Separatist)
  HOhnaka, JFett, ZWesell,
  BFett_G, WeaponsSysOff,
  FalseTransCodes,
  SlaveI_Sep,
  // SWX83 - Phoenix Cell
  B6Prototype_CMD, PhoenixSq,
  VectoredCanRZ1,
  SWren_Gun,
  Hopeful, TierfonBR,
  B6Prototype,
  // SWX84 - Skystrike Academy
  ShadowWing, SkystrikeAcaCla,
  SensitiveCtrls, TIEDefElite,
  Disciplined, InterloperTurn,
  // SWX85 - Fugitives and Collaborators
  R4B11,
  Bounty, InIt,
  GKey, ProtGleb,
  Cutthroat,
  // SWX86 - BTA-NR2 -Y-wing
  L4ER5, WatchfulAstro, // astro
  WartimeLoad, // config
  BFrik, OvertunedMod, // illicit
  ElecChaffMsl, // missile/payload
  // SWX87 - Fury of the First Order
  EnJamSuite,
  Compassion, Malice, ShatteringShot,
  DT798, FOOrdTech,
  FeedbackPing, // talent
  SensorScramble, // tech
  // SWX88 - Trident-class
  AVentress, GenGrievous_CMD, HOhnaka_CMD, MTuuk, RTamson, ZealousCapt,
  CorsairCrew,
  TractorTent,
  DrillBeak, EnhancedProp, ProtCanBat,
  DroidCrew, TractorTech,
  Grappler, NautolansRev, NeimoidianGrasp, Trident,
  TrackingTrp,
  // other
};

}
