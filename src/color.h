#pragma once
#include "difficulty.h"
#include "faction.h"
#include <string>
#include <vector>
#include <stdint.h>


namespace libxwing2 {

enum class Clr {
  Initiative,
  Cost,
  Attack,
  Agility,
  Hull,
  Shield,
  Charge,
  Force,
  Energy,

  Stress,
  Neutral,
  Destress,

  Rebel,
  RebelBg,
  Imperial,
  ImperialBg,
  Scum,
  ScumBg,
  Resistance,
  ResistanceBg,
  FirstOrder,
  FirstOrderBg,
  Republic,
  RepublicBg,
  Separatist,
  SeparatistBg,

  ArcRebelL,
  ArcRebelD,
  ArcImperialL,
  ArcImperialD,
  ArcScumL,
  ArcScumD,
  ArcResistanceL,
  ArcResistanceD,
  ArcFirstOrderL,
  ArcFirstOrderD,
  ArcRepublicL,
  ArcRepublicD,
  ArcSeparatistL,
  ArcSeparatistD,

  DialRebel,
  DialImperial,
  DialScum,
  DialResistance,
  DialFirstOrder,
};

class ColorNotFound : public std::runtime_error {
 public:
  ColorNotFound(Clr c);
};

class Color {
 public:
  static Color GetColor(Clr c);
  static Color GetFacFg(Fac f);
  static Color GetFacBg(Fac f);
  static Color GetDif(Dif d);
  static std::vector<Clr> GetAllClrs();

  Clr GetClr()          const;
  std::string GetName() const;
  uint8_t Red()         const;
  uint8_t Green()       const;
  uint8_t Blue()        const;
  uint8_t Gray()        const;
  std::string GetHex()  const;

 private:
  const Clr         clr;
  const std::string name;
  const uint8_t     red;
  const uint8_t     green;
  const uint8_t     blue;

  static std::vector<Color> colors;

  Color(Clr c, std::string n, uint8_t r, uint8_t g, uint8_t b);

};

}
