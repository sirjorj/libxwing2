#pragma once
#include <stdint.h>

namespace libxwing2 {

class Chargeable {
 public:
  Chargeable(int8_t cap, int8_t recur);
  int8_t GetCapacity() const;
  int8_t GetRecurring() const;

  Chargeable& operator+=(uint8_t a);
  Chargeable& operator-=(uint8_t a);

 private:
  int8_t capacity;
  int8_t recurring;
};


}
