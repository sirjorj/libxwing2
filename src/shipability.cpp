#include "shipability.h"
#include "helper.h"

namespace libxwing2 {

std::vector<ShipAbility> ShipAbility::shipAbilities = {
  { SAb::AdaptAil,   "Adaptive Ailerons",           Modifier(),                                                   "Before you reveal your dial, if you are not stressed, you must execute a white [1 {LBANK}], [1 {STRAIGHT}], or [1 {RBANK}] maneuver." },
  { SAb::AdvDrBr,    "Advanced Droid Brain",        Modifier(),                                                   "After you perform a {CALCULATE} action, gain 1 calculate token." },
  { SAb::AdvFirCtrl, "Advanced Fire Control",       Modifier(),                                                   "After you perform a {CANNON} or {MISSILE} attack, if you have a lock on the defender, you may perform a bonus primary attack against the defender." },
  { SAb::Autothr,    "Autothrusters",               Modifier(),                                                   "After you perform an action, you may perform a red {BARRELROLL} or red {BOOST} action." },
  { SAb::AdvTgtCmp,  "Advanced Targeting Computer", Modifier(),                                                   "While you perform a primary attack against a defender you have locked, roll 1 additional attack die and change 1 {HIT} result to a {CRIT} result." },
  { SAb::BroadBat,   "Broadside Batteries",         Modifier(),                                                   "You can acquire locks and perform primary attacks at range 1-4." },
  { SAb::ConcenBat,  "Concentrated Batteries",      Modifier(),                                                   "While you perform a primary, {TORPEDO}, or {MISSILE} attack, if the defender is in your {BULLSEYEARC}, roll 1 additional die." },
  { SAb::ConcFace,   "Concordia Faceoff",           Modifier(),                                                   "While you defend, if the attack range is 1 and you are in the attacker's {FRONTARC}, change 1 result to an {EVADE} result." },
  { SAb::CoPilot,    "Co-Pilot",                    Modifier(),                                                   "While you are docked, your carrier ship has your pilot ability in addition to its own." },
  { SAb::ComShuttle, "Comms Shuttle",               Modifier(),                                                   "While you are docked, your carrier ship gains {COORDINATE}.  Before your carrier ship activates, it may perform a {COORDINATE} action." },
  { SAb::DevBar,     "Devastating Barrage",         Modifier().UpgAdd({UpT::Torpedo, UpT::Missile}),              "While you perform a {TORPEDO} or {MISSILE} attack, if the defender is in your {BULLSEYEARC}, your {CRIT} results cannot be canceled by {EVADE} results." },
  { SAb::DockClamp,  "Docking Clamps",              Modifier(),                                                   "You can dock up to 4 small ships." },
  { SAb::D2Rights,   "Dead to Rights",              Modifier(),                                                   "While you perform an attack, if the defender is in your {BULLSEYEARC}, defense dice cannot be modified using green tokens." },
  { SAb::EscCraft,   "Escape Craft",                Modifier(),                                                   "[B]Setup:[/B] Requires the [B]Hound's Tooth[/B].  You [B]must[/B] begin the game docked with the [B]Hound's Tooth[/B]." },
  { SAb::ExpScan,    "Experimental Scanners",       Modifier(),                                                   "You can acquire locks beyond range 3.  You cannot acquire locks at range 1." },
  { SAb::ExpWings,   "Explosion with Wings",        Modifier(),                                                   "[B]Setup:[/B] You are dealt 1 facedown damage card.  After you perform a {SLAM} action, you may expose 1 damage card to remove 1 disarm token." },
  { SAb::FireConv,   "Fire Convergence",            Modifier(),                                                   "While a friendly ship performs a non-{SINGLETURRETARC} attack, if the defender is in your {SINGLETURRETARC}, you may spend 1 {CHARGE}.  If you do, the attacker rerolls up to 2 attack dice." },
  { SAb::FullThrot,  "Full Throttle",               Modifier(),                                                   "After you fully execute a speed 3-5 maneuver, you may perform an {EVADE} action." },
  { SAb::FinTunCtrl, "Fine-tuned Controls",         Modifier(),                                                   "After you fully execute a maneuver, you may spend 1 {FORCE} to perform a {BOOST} or {BARRELROLL} action." },
  { SAb::FinTunThr,  "Fine-Tuned Thrusters",        Modifier(),                                                   "After you fully execute a maneuver, if you are not depleted or strained, you may gain 1 deplete or strain token to perform a {LOCK} or {BARRELROLL} action." },
  { SAb::HWeapTrt,   "Heavy Weapon Turret",         Modifier(),                                                   "You can rotate your {SINGLETURRETARC} indicator only to your {FRONTARC} or {REARARC}.  You [B]must[/B] treat the {FRONTARC} requirement of your equipped {MISSILE} upgrades as {SINGLETURRETARC}." },
  { SAb::IndCalc,    "Independent Calculations",    Modifier(),                                                   "While you perform a white {CALCULATE} action, you may treat it as red to gain 1 additional calculate token.  Other ships cannot spend your calculate tokens using the Networked Calculations ship ability." },
  { SAb::IntCtrls,   "Intuitive Controls",          Modifier(),                                                   "During the System Phase, you may perform a purple {BARRELROLL} or purple {BOOST} action." },
  { SAb::IntInt,     "Intuitive Interface",         Modifier(),                                                   "After you perform an action added to your action bar by a {TALENT}, {ILLICIT}, or {MODIFICATION} upgrade, you may perform a {CALCULATE} action." },
  { SAb::LockLoad,   "Locked and Loaded",           Modifier(),                                                   "While you are docked, after your carrier ship performs a primary {FRONTARC} or {TURRET} attack, it may perform a bonus primary {REARARC} attack." },
  { SAb::LnkBat,     "Linked Battery",              Modifier(),                                                   "While you perform a {CANNON} attack, roll 1 additional die." },
  { SAb::MicroThr,   "Microthrusters",              Modifier(),                                                   "While you perform a barrel roll, you must use the {LBANK} or {RBANK} template instead of the {STRAIGHT} template." },
  { SAb::NmbBmb,     "Nimble Bomber",               Modifier(),                                                   "If you would drop a device using a {STRAIGHT} template, you may use a {LBANK} or {RBANK} template of the same speed instead." },
  { SAb::NetAim,     "Networked Aim",               Modifier(),                                                   "You cannot spend your locks to reroll attack dice.  While you perform an attack, you may reroll a number of attack dice up to the number of friendly locks on the defender." },
  { SAb::NetCalc,    "Networked Calculations",      Modifier(),                                                   "While you defend or perform an attack, you may spend 1 calculate token from a friendly ship at range 0-1 to change 1 {FOCUS} result to an {EVADE} or {HIT} result." },
  { SAb::NotchStab,  "Notched Stabilizers",         Modifier(),                                                   "While you move, you ignore asteroids." },
  { SAb::OdBurner,   "Overdrive Burners",           Modifier(),                                                   "While you defend, if your revealed maneuver is speed 3-5, roll 1 additional defense die." },
  { SAb::PursCraft,  "Pursuit Craft",               Modifier(),                                                   "After you deploy, you may acquire a lock on a ship the friendly Hound's Tooth has locked." },
  { SAb::PlatHull,   "Plated Hull",                 Modifier(),                                                   "While you defend, if you are not critically damaged, change 1 {CRIT} result to a {HIT} result." },
  { SAb::PpTracArr,  "Pinpoint Tractor Array",      Modifier(),                                                   "You cannot rotate your {SINGLETURRETARC} to your {REARARC}.  After you execute a maneuver, you may gain 1 tractor token to perform a {ROTATEARC} action." },
  { SAb::PursuitThr, "Pursuit Thrusters",           Modifier(),                                                   "During the System Phase, you may perform a {BOOST} action." },
  { SAb::ResupCraft, "Resupply Craft",              Modifier(),                                                   "After another friendly ship at range 0-1 performs an action, you may spend 1 {ENERGY}.  If you do, it removes 1 orange or red token, or recovers 1 shield." },
  { SAb::RotCan,     "Rotating Cannons",            Modifier(),                                                   "You can rotate your {SINGLETURRETARC} indicator only to your {FRONTARC} or {REARARC}.  You must treat the {FRONTARC} requirement of your equipped {CANNON} upgrades as {SINGLETURRETARC}." },
  { SAb::RigEnCells, "Rigged Energy Cells",         Modifier(),                                                   "During the System Phase, if you are not docked, lose 1 {CHARGE}.  At the end of the Activation Phase, if you have 0 {CHARGE}, you are destroyed.  Before you are removed, each ship at range 0-1 suffers 1 {CRIT} damage." },
  { SAb::RefGyro,    "Refined Gyrostabilizers",     Modifier(),                                                   "You can rotate your {SINGLETURRETARC} indicator only to your {FRONTARC} or {REARARC}.  After you perform an action, you may perform a red {BOOST} or red {ROTATEARC} action." },
  { SAb::StygArr,    "Stygium Array",               Modifier(),                                                   "After you decloak, you may perform an {EVADE} action.  At the start of the End Phase, you may spend 1 evade token to gain 1 cloak token." },
  { SAb::SensCtrls,  "Sensitive Controls",          Modifier(),                                                   "During the System Phase, you may perform a red {BARRELROLL} or red {BOOST} action." },
  { SAb::SensBlind,  "Sensor Blindspot",            Modifier(),                                                   "While you perform a primary attack at attack range 0-1, do not apply the range 0-1 bonus and roll 1 fewer attack die." },
  { SAb::StTracArr,  "Spacetug Tractor Array",      Modifier(),                                                   "[B]Action:[/B] Choose a ship in your {FRONTARC} at range 1.  That ship gains 1 tractor token, or 2 tractor tokens if it is in your {BULLSEYEARC} at range 1." },
  { SAb::TIE,        "Twin Ion Engines",            Modifier(),                                                   "Ignore the \"TIE\" ship restriction on upgrade cards." },
  { SAb::VecCannon,  "Vectored Cannons",            Modifier(),                                                   "During the System Phase, you may perform a red {BOOST} or red {ROTATEARC} action.  You can rotate your {SINGLETURRETARC} indicator only to your {FRONTARC} or {REARARC}." },
  { SAb::VecThrust,  "Vectored Thrusters",          Modifier(),                                                   "After you perform an action, you may perform a red {BOOST} action." },
  { SAb::TailGun,    "Tail Gun",                    Modifier(),                                                   "While you have a docked ship, you have a primary {REARARC} weapon with an attack value equal to your docked ship's primary {FRONTARC} attack value." },
  { SAb::TracGrasp,  "Tractor Grasp",               Modifier(),                                                   "After you perform a Tractor Tentacles attack that hits, the defender gains 1 tractor token." },
  { SAb::WpnHardpt,  "Weapon Hardpoint",            Modifier().UpgAdd({UpT::Cannon, UpT::Torpedo, UpT::Missile}), "You can equip 1 {CANNON}, {TORPEDO}, or {MISSILE} upgrade." },
};

ShipAbilityNotFound::ShipAbilityNotFound(SAb sab) : runtime_error("Ship Ability not found (enum " + std::to_string((int)sab) + ")") { }

ShipAbility ShipAbility::GetShipAbility(SAb sab) {
  for(const ShipAbility& sa : ShipAbility::shipAbilities) {
    if(sab == sa.GetSAb()) {
      return sa;
    }
  }
  throw ShipAbilityNotFound(sab);
}

std::vector<SAb> ShipAbility::GetAllSAbs() {
  std::vector<SAb> ret;
  for(const ShipAbility& shipAbility : ShipAbility::shipAbilities) {
    ret.push_back(shipAbility.GetSAb());
  }
  return ret;
}

ShipAbility::ShipAbility(SAb s, std::string n, Modifier m, std::string t)
  : sab(s), name(n), modifier(m), text(t) { }

SAb         ShipAbility::GetSAb()        const { return this->sab; }
std::string ShipAbility::GetName()       const { return this->name; }
Modifier    ShipAbility::GetModifier()   const { return this->modifier; }
Text        ShipAbility::GetText()       const { return Text(this->text); }
}
