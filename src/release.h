#pragma once
#include "condition.h"
#include "environment.h"
#include "pilot.h"
#include "ship.h"
#include "token.h"
#include "upgrade.h"
#include <string>

namespace libxwing2 {



enum class Rel {
  SWX74, // saw's renegades (1e)
  SWX75, // tie reaper (1e)
  SWZ01, // core set
  SWZ02, // saw's renegades
  SWZ03, // tie reaper
  SWZ04, // landos millennium falcon
  SWZ05, // dice pack
  SWZ06, // rebel conversion kit
  SWZ07, // empire conversion kit
  SWZ08, // scum conversion kit
  SWZ09, // rebel dial upgrade
  SWZ10, // empire dual upgrade
  SWZ11, // scum dial upgrade
  SWZ12, // t65 xwing
  SWZ13, // btla4 ywing
  SWZ14, // tie fighter
  SWZ15, // tie advanced x1
  SWZ16, // slave 1
  SWZ17, // fang fighter
  SWZ18, // first order conversion kit
  SWZ19, // resistance conversion kit
  SWZ20, // first order dial upgrade
  SWZ21, // resistance dial upgrade
  SWZ22, // rz2 awing
  SWZ23, // mining guild tie

  SWZ25, // t70 xwing
  SWZ26, // tie/fo
  SWZ27, // tie/vn silencer

  SWZ29, // servants of strife
  SWZ30, // sith infiltrator
  SWZ31, // vulture-class droid fighter
  SWZ32, // guardians of the republic
  SWZ33, // arc-170
  SWZ34, // delta-7
  SWZ35, // separatist dial upgrade
  SWZ36, // republic dial upgrade
  SWZ37, // z95 headhunter
  SWZ38, // tie striker
  SWZ39, // millennium falcon (rebel)
  SWZ40, // N-1
  SWZ41, // Hyena
  SWZ42, // b-wing
  SWZ43, // decimator
  SWZ44, // tie/sf
  SWZ45, // resistance transport
  SWZ46, // deluxe movement tools and range ruler
  SWZ47, // nantex-class
  SWZ48, // btl-b y-wing
  SWZ49, // ghost
  SWZ50, // inquisitors' tie
  SWZ51, // punishing one
  SWZ52, // m3a
  SWZ53, // huge ship conversion kit
  SWZ54, // raider
  SWZ55, // tantive iv
  SWZ56, // c-roc
  SWZ57, // epic battles multiplayer expansion
  SWZ58, // hounds tooth
  SWZ59, // tie interceptor
  SWZ60, // tie defender
  SWZ61, // a-wing
  SWZ62, // major vonreg's tie
  SWZ63, // fireball
  SWZ64, // never tell me the odds obstacles pack
  SWZ65, // fully loaded devices pack
  SWZ66, // hotshots and aces reinforcements pack
  SWZ67, // tie/rb
  SWZ68, // heralds of hope
  SWZ69, // xi-class
  SWZ70, // laat/i
  SWZ71, // hmp
  SWZ72, // damage deck - rebel alliance
  SWZ73, // damage deck - galactic empire
  SWZ74, // damage deck - scum and villainy
  SWZ75, // damage deck - resistance
  SWZ76, // damage deck - first order
  SWZ77, // damage deck - galactic republic
  SWZ78, // damage deck - separatist alliance
  SWZ79, // eta-2
  SWZ80, // vwing
  SWZ81, // trifighter
  SWZ82, // jango fetts slave 1
  SWZ83, // phoenix cell
  SWZ84, // skystrike academy
  SWZ85, // fugitives and collaborators
  SWZ86, // bta-nr2 ywing
  SWZ87, // fury of the first order
  SWZ88, // trident-class
};

class ReleaseNotFound : public std::runtime_error {
 public:
  ReleaseNotFound(Rel r);
  ReleaseNotFound(std::string r);
};

struct RelShip {
  Shp         ship;
  std::string desc; // leave blank for standaard, add text if alternate paint (ex. aces)
};

enum class RelGroup {
  FeWave14,
  CoreSet,
  Accessories,
  CardPack,
  Huge,
  Wave1,
  Wave2,
  Wave3,
  Wave4,
  Wave5,
  Wave6,
  Wave7,
  Wave8,
  Wave9,
  Wave10,
  Unknown,
};

struct RelDate {
  unsigned short year;
  unsigned char  month;
  unsigned char  date;
};
bool operator==(const RelDate& a, const RelDate& b);
bool operator!=(const RelDate& a, const RelDate& b);
bool operator>(const RelDate& a, const RelDate& b);
bool operator<(const RelDate& a, const RelDate& b);
bool operator>=(const RelDate& a, const RelDate& b);
bool operator<=(const RelDate& a, const RelDate& b);

int GetDaysBetween(const RelDate a, const RelDate b);

struct Article {
  RelDate date;
  std::string url;
};

struct RelArticles {
  Article              announcement;
  std::vector<Article> previews;
  Article              release;
};

class Release {
 public:
  static Release          GetRelease(Rel r);
  static Release          GetRelease(std::string sku);
  static std::vector<Rel> GetByPilot(Plt p);
  static std::vector<Rel> GetByUpgrade(Upg u);
  static std::vector<Rel> GetAllRels();

  Rel                  GetRel()                 const;
  std::string          GetSku()                 const;
  std::string          GetIsbn()                const;
  std::string          GetName()                const;
  RelGroup             GetGroup()               const;
  RelDate              GetAnnounceDate()        const;
  RelDate              GetReleaseDate()         const;
  Article              GetAnnouncementArticle() const;
  std::vector<Article> GetPreviewArticles()     const;
  Article              GetReleaseArticle()      const;
  std::string          GetNotes()               const;
  std::vector<RelShip> GetShips()               const;
  std::vector<Plt>     GetPilots()              const;
  std::vector<Upg>     GetUpgrades()            const;
  std::vector<Cnd>     GetConditions()          const;
  std::vector<Env>     GetEnvironments()        const;
  TokenCollection      GetTokens()              const;
  bool                 IsUnreleased()           const;

 private:
  Rel         rel;
  std::string sku;
  std::string isbn;
  std::string name;
  RelGroup group;
  RelDate announceDate;
  RelDate releaseDate;
  RelArticles articles;
  std::string notes;

  std::vector<RelShip> ships;
  std::vector<Plt>     pilots;
  std::vector<Upg>     upgrades;
  std::vector<Cnd>     conditions;
  std::vector<Env>     environments;
  TokenCollection      tokens;

  static std::vector<Release> releases;

  Release(Rel                  r,
          std::string          sku,
          std::string          isbn,
          std::string          nam,
          RelGroup             gr,
          RelDate              ad,
          RelDate              rd,
          RelArticles          art,
	  std::string          note,
	  std::vector<RelShip> shps,
          std::vector<Plt>     plts,
          std::vector<Upg>     upgs,
          std::vector<Cnd>     cond,
          std::vector<Env>     env,
          TokenCollection      tok);
};

}
