#include "adjustable.h"
#include "converter.h"
#include <fstream>
#include <stdio.h>

namespace libxwing2 {

// helpers
static std::string trim(std::string str) {
  if(str.length() == 0) return str;
  size_t first = str.find_first_not_of(' ');
  size_t last = str.find_last_not_of(' ');
  return str.substr(first, (last-first+1));
}

static std::vector<std::string> tokenize(std::string s, const char DELIMITER) {
  std::vector<std::string> ret;
  size_t start = s.find_first_not_of(DELIMITER), end=start;
  while (start != std::string::npos){
    end = s.find(DELIMITER, start);
    ret.push_back(trim(s.substr(start, end-start)));
    start = s.find_first_not_of(DELIMITER, end);
  }
  return ret;
}

// privates
std::string Adjustable::GetFilename() {
  char* root = getenv("HOME");
  std::string ret;
  if(root == 0) { ret = "./.libxwing2.adjustable"; }
  else          { ret = std::string(root) + "/.libxwing2.adjustable"; }
  return ret;
}

// publics
Adjustable::Adjustable() {
  std::ifstream adjStream(GetFilename().c_str());
  if(adjStream.good()) {
    uint16_t n = 0;
    for(std::string line; getline(adjStream, line);) {
      n++;
      std::string type;
      std::string name;
      std::string attribute;
      std::string value;
      
      {
	std::string l = trim(line);
	if(l.length() == 0) {
	  this->status.push_back({n, false, "Empty line"});
	  continue;
	}
	else if(l[0] == '#') {
	  this->status.push_back({n, false, "Comment"});
	  continue;
	}
	int split = l.find("=");
	if(split == std::string::npos) {
	  this->status.push_back({n, true, "No '='"});
	  continue;
	}
	std::string k = trim(l.substr(0, split));
	value = trim(l.substr(split+1));
	std::vector<std::string> tokens = tokenize(k, '.');
	if(tokens.size() != 3) {
	  this->status.push_back({n, true, "Key not in type.name.attribute format"});
	  continue;
	}
	type = tokens[0];
	name = tokens[1];
	attribute = tokens[2];
      }

      if(type == "plt") {
	Plt p;
	try {
	  p = converter::xws::GetPilot(name);
	}
	catch(converter::xws::PilotNotFound &pnf) {
	  this->status.push_back({n, true, "Invalid pilot '" + name + "'"});
	  continue;
	}

	if(attribute == "cost") {
	  try {
	    uint16_t c = std::stoi(value);
	    this->pilots[p].cost = c;
	  }
	  catch(std::invalid_argument &ia) {
	    this->status.push_back({n, true, "Invalid cost value"});
	    continue;
	  }
	}

	else if(attribute == "upgrades") {
	  std::vector<std::string> upgs = tokenize(value, ' ');
	  UpgradeBar uBar;
	  for(const std::string& u : upgs) {
	    try {
	      UpT ut = converter::xws::GetUpgradeType(u);
	      uBar.push_back(ut);
	    }
	    catch(converter::xws::UpgradeTypeNotFound &utnf) {
	      this->status.push_back({n, true, "Invalid upgrade type '" + u + "' on pilot '" + name + "'"});
	      continue;
	    }
	  }
	  this->pilots[p].upgrades = uBar;
	}

	else {
	  this->status.push_back({n, true, "Unknown attribute '" + attribute + "'"});
	  continue;
	}
      }

      else if(type == "upg") {
	Upg u;
	try {
	  u = converter::xws::GetUpgrade(name);
	}
	catch(converter::xws::UpgradeNotFound &unf) {
	  this->status.push_back({n, true, "Invalid upgrade '" + name + "'"});
	  continue;
	}
	if(attribute == "cost") {
	  try {
	    uint16_t c = std::stoi(value);
	    this->upgrades[u].cost = c;
	  }
	  catch(std::invalid_argument &ia) {
	    this->status.push_back({n, true, "Invalid value"});
	    continue;
	  }
	}
	else {
	  this->status.push_back({n, true, "Unknown attribute '" + attribute + "'"});
	  continue;
	}
      }
      else {
	this->status.push_back({n, true, "Unknown type '" + type + "'"});
      }
    }
  }
  adjStream.close();
}

  
std::optional<int16_t> Adjustable::GetCost(Plt pilot) {
  try        { return this->pilots[pilot].cost; }
  catch(...) { return std::nullopt;             }
}

std::optional<UpgradeBar> Adjustable::GetUpgrades(Plt pilot) {
  try        { return this->pilots[pilot].upgrades; }
  catch(...) { return std::nullopt;                 }
}

std::optional<int16_t> Adjustable::GetCost(Upg upgrade) {
  try        { return this->upgrades[upgrade].cost; }
  catch(...) { return std::nullopt;                 }
}

std::vector<AdjStatus> Adjustable::GetStatus() {
  return this->status;
}
  

  
}
