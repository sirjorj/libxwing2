#include "token.h"
#include <algorithm>
#include <optional>

namespace libxwing2 {

// token
std::vector<Token> Token::tokens = {
  // ship
  { Tok::Dial,                  "Dial",                                             "Dial"          },
  { Tok::DialId,                "Dial ID",                                          "Dial ID"       },
  { Tok::Ship,                  "Ship",                                             "Ship"          },
				    // game effects
  { Tok::Calculate,             "Calculate",                                        "Calculate"     },
  { Tok::Cloak,                 "Cloak",                                            "Cloak"         },
  { Tok::Crit,                  "Critical Hit",                                     "Crit"          },
  { Tok::Deplete,               "Deplete",                                          "Deplete"       },
  { Tok::Disarm,                "Disarm",                                           "Disarm"        },
  { Tok::Evade,                 "Evade",                                            "Evade"         },
  { Tok::Focus,                 "Focus",                                            "Focus"         },
  { Tok::ForceCharge,           "Force Charge",                                     "Force"         },
  { Tok::Fuse,                  "Fuse Marker",                                      "Fuse"          },
  { Tok::ID,                    "ID",                                               "ID"            },
  { Tok::Ion,                   "Ion",                                              "Ion"           },
  { Tok::Jam,                   "Jam",                                              "Jam"           },
  { Tok::Lock,                  "Lock",                                             "Lock"          },
  { Tok::Reinforce,             "Reinforce",                                        "Reinforce"     },
  { Tok::Shield,                "Shield",                                           "Shield"        },
  { Tok::StandardCharge,        "Standard Charge",                                  "Charge"        },
  { Tok::Strain,                "Strain",                                           "Strain"        },
  { Tok::Stress,                "Stress",                                           "Stress"        },
  { Tok::Tractor,               "Tractor",                                          "Tractor"       },
  { Tok::TurretMountMarker,     "Turret Mount Marker",                              "TurretMount"   },

  // turrets
  { Tok::SmallSingleTurret,     "Small Single Turret",                              "1TurretS"      },
  { Tok::SmallDoubleTurret,     "Small Double Turret",                              "2TurretS"      },
  { Tok::LargeSingleTurret,     "Large Single Turret",                              "1TurretL"      },
  { Tok::LargeDoubleTurret,     "Large Double Turret",                              "2TurretL"      },
  { Tok::HugeSingleTurret,      "Huge Single Turret",                               "1TurretH"      },
  { Tok::HugeDoubleTurret,      "Huge Double Turret",                               "2TurretH"      },

  // obstacles
  { Tok::Asteroid,              "Asteroid",                                         "Asteroid"      },
  { Tok::DebrisCloud,           "Debris Cloud",                                     "Debris"        },
  { Tok::GasCloud,              "Gas Cloud",                                        "Gas"           },
  { Tok::CargoDrift,            "Cargo Drift",                                      "Cargo"         },

  // devices
  { Tok::Bomblet,               "Bomblet",                                          "Bomblet"       },
  { Tok::ClusterMineSet,        "Cluster Mine Set",                                 "Cluster Mines" },
  { Tok::ConcussionBomb,        "Concussion Bomb",                                  "Conc Bomb"     },
  { Tok::ConnerNet,             "Conner Net",                                       "Conner Net"    },
  { Tok::ElectroProtBomb,       "Electro-Proton Bomb",                              "ElecProt Bomb" },
  { Tok::IonBomb,               "Ion Bomb",                                         "Ion Bomb"      },
  { Tok::LooseCargo,            "Loose Cargo",                                      "Loose Cargo"   },
  { Tok::ProtonBomb,            "Proton Bomb",                                      "Proton Bomb"   },
  { Tok::ProximityMine,         "Proximity Mine",                                   "ProxMine"      },
  { Tok::SeismicCharge,         "Seismic Charge",                                   "SeismicCh"     },
  { Tok::SpareParts,            "Spare Parts",                                      "Spare Parts"   },
  { Tok::ThermalDetonator,      "Thermal Detonator",                                "Thermal Det"   },

  // conditions
  { Tok::CompIntel,             "Compromising Intel",                               "Comp Intel"    },
  { Tok::Decoyed,               "Decoyed",                                          "Decoyed"       },
  { Tok::FearfulPrey,           "Fearful Prey",                                     "Fearful Prey"  },
  { Tok::Hunted,                "Hunted",                                           "Hunted"        },
  { Tok::ISYTDS,                "I'll Show You the Dark Size",                      "ISYTDS"        },
  { Tok::ITR,                   "It's the Resistance",                              "ITR"           },
  { Tok::ListeningDevice,       "Listening Device",                                 "Listening Dev" },
  { Tok::OptimizedPrototype,    "Optimized Prototype",                              "Opt Proto"     },
  { Tok::Rattled,               "Rattled",                                          "Rattled"       },
  { Tok::SuppressiveFire,       "Suppressive Fire",                                 "Supp. Fire"    },
  { Tok::YBMBYSTM,              "You'd Better Mean Business / You Should Thank Me", "YBMB/YSTM"     },

  // remote
  { Tok::BuzzDroidSwarm,        "Buzz Droid Swarm",                                 "Buzz Droids"   },
  { Tok::DRK1ProbeDroid,        "DRK-1 Probe Droid",                                "DRK-1 Probe"   },
  { Tok::SensorBuoyRed,         "Sensor Buoy (Blue)",                               "Sens Buoy B"   },
  { Tok::SensorBuoyBlue,        "Sensor Buoy (Red)",                                "Sens Buoy R"   },
  { Tok::TrackTorp,             "Tracking Torpedo",                                 "Track Torp"    },
  { Tok::TrackTorpLock,         "Tracking Torpedo Lock",                            "TrackTrp Lock" },

  // misc
  { Tok::FirstPlayerMarker,     "First Player Marker",                              "First Player"  },
  { Tok::HyperspaceMarker,      "Hyperspace Marker",                                "Hyperspace"    },
  { Tok::StrategicMarker,       "Strategic Marker",                                 "Strat Mark"    },
  { Tok::TridentAggMarker,      "Trident Aggression Marker",                        "Trident Agg"   },
  { Tok::VictoryCounter,        "Victory Counter",                                  "Victory"       },
};

TokenNotFound::TokenNotFound(Tok t) : runtime_error("Token not found (enum " + std::to_string((int)t) + ")") { }

Token Token::GetToken(Tok tok) {
  for(const Token& token : Token::tokens) {
    if(token.GetTok() == tok) {
      return token;
    }
  }
  throw TokenNotFound(tok);
}

std::vector<Tok> Token::GetAllToks() {
  std::vector<Tok> ret;
  for(const Token& token : Token::tokens) {
    ret.push_back(token.GetTok());
  }
  return ret;
}
Tok         Token::GetTok()       const { return this->tok; }
std::string Token::GetName()      const { return this->name; }
std::string Token::GetShortName() const { return this->shortName; }

Token::Token(Tok         t,
             std::string n,
             std::string s)
  : tok(t), name(n), shortName(s) { }



//
// tokens
//

Tokens::Tokens(Tok t) : tok(t),count(0) { }
Tokens::~Tokens() { }
Tok      Tokens::GetTok()   const { return this->tok;  }
void     Tokens::Add(int c)       { this->count += c; }
uint16_t Tokens::GetCount() const { return this->count; }

Dials::Dials() : Tokens(Tok::Dial) { }
void Dials::Add(Shp s, Fac f) { Tokens::Add(1); this->dials.push_back({s,f}); }
std::vector<std::pair<Shp,Fac>> Dials::GetDials() const { return this->dials; }

DialIds::DialIds() : Tokens(Tok::DialId) { }
void DialIds::Add(Shp s) { Tokens::Add(1); this->dialIds[s] += 1; }
std::map<const Shp,int> DialIds::GetDialIds() const { return this->dialIds; }

ShTokens::ShTokens() : Tokens(Tok::Ship) { }
void ShTokens::Add(Plt p1, Plt p2) { Tokens::Add(1); this->pilots.push_back({p1,p2}); }
std::vector<std::pair<Plt,Plt>> ShTokens::GetPilots() const { return this->pilots; }

IDTokens::IDTokens(Tok t) : Tokens(t) { }
void IDTokens::Add(int c, int id) { Tokens::Add(c); this->ids[id] += c; }
std::map<int,int> IDTokens::GetIDCounts() const { return this->ids; }

StrategicMarkers::StrategicMarkers() : Tokens(Tok::StrategicMarker) { }
void StrategicMarkers::Add(int id) { Tokens::Add(1); this->ids[id] += 1; }
std::map<int,int> StrategicMarkers::GetIDCounts() const { return this->ids; }

TrackingTorpedoTokens::TrackingTorpedoTokens() : Tokens(Tok::TrackTorp) { }
void TrackingTorpedoTokens::Add(std::string id) { Tokens::Add(1); this->ids[id] += 1; }
std::map<std::string,int> TrackingTorpedoTokens::GetIDCounts() const { return this->ids; }

TrackingTorpedoLocks::TrackingTorpedoLocks() : Tokens(Tok::TrackTorpLock) { }
void TrackingTorpedoLocks::Add(std::string id) { Tokens::Add(1); this->ids[id] += 1; }
std::map<std::string,int> TrackingTorpedoLocks::GetIDCounts() const { return this->ids; }

TurretTokens::TurretTokens(Tok t) : Tokens(t) { }
void TurretTokens::Add(Tok t, int c, std::optional<Fac> f) { Tokens::Add(c); this->fac[f] += c; }
std::map<std::optional<Fac>,int> TurretTokens::GetFacCounts() const { return this->fac; }



//
// tokencollection
//

TokenCollection::TokenCollection() { }

TokenCollection& TokenCollection::operator+=(const TokenCollection& rhs) {
  std::map<Tok,std::shared_ptr<Tokens>> tc = rhs.GetAllTokens();
  for(const std::pair<Tok,std::shared_ptr<Tokens>> t : tc) {
    Tok tok = t.first;
    std::shared_ptr<Tokens> tokensSP = t.second;
    if(ShTokens *cp = dynamic_cast<ShTokens*>(tokensSP.get())) {
      const std::vector<std::pair<Plt,Plt>> &p = cp->GetPilots();
      for(const std::pair<Plt,Plt> &pp : p) {
	this->AddShip(pp.first, pp.second);
      }
    }
    else if(IDTokens *cp = dynamic_cast<IDTokens*>(tokensSP.get())) {
      const std::map<int,int> &i = cp->GetIDCounts();
      for(const std::pair<const int,int> &ii : i) {
	int id = ii.first;
	int count = ii.second;
	this->AddIds(tok, count, id);
      }
    }
    else if(TurretTokens *cp = dynamic_cast<TurretTokens*>(tokensSP.get())) {
      const std::map<std::optional<Fac>,int> &t = cp->GetFacCounts();
      for(const std::pair<const std::optional<Fac>,int> &tt : t) {
	this->AddTurret(tok, tt.second, tt.first);
      }
    }
    else {
      this->AddTokens(tok, tokensSP->GetCount());
    }
  }
  return *this;
}

void TokenCollection::AddDial(Shp s, Fac f) {
  if(this->tokens.find(Tok::Dial) == this->tokens.end()) {
    this->tokens[Tok::Dial] = std::make_shared<Dials>();
  }
  dynamic_cast<Dials*>(this->tokens[Tok::Dial].get())->Add(s, f);
}

void TokenCollection::AddDialId(Shp s) {
  if(this->tokens.find(Tok::DialId) == this->tokens.end()) {
    this->tokens[Tok::DialId] = std::make_shared<DialIds>();
  }
  dynamic_cast<DialIds*>(this->tokens[Tok::DialId].get())->Add(s);
}

void TokenCollection::AddShip(Plt p1, Plt p2) {
  if(this->tokens.find(Tok::Ship) == this->tokens.end()) {
    this->tokens[Tok::Ship] = std::make_shared<ShTokens>();
  }
  dynamic_cast<ShTokens*>(this->tokens[Tok::Ship].get())->Add(p1, p2);
}

void TokenCollection::AddStrategicMarkers(int i) {
  if(this->tokens.find(Tok::StrategicMarker) == this->tokens.end()) {
    this->tokens[Tok::StrategicMarker] = std::make_shared<StrategicMarkers>();
  }
  dynamic_cast<StrategicMarkers*>(this->tokens[Tok::StrategicMarker].get())->Add(i);
}

void TokenCollection::AddTrackingTorpedo(std::string i) {
  if(this->tokens.find(Tok::TrackTorp) == this->tokens.end()) {
    this->tokens[Tok::TrackTorp] = std::make_shared<TrackingTorpedoTokens>();
  }
  dynamic_cast<TrackingTorpedoTokens*>(this->tokens[Tok::TrackTorp].get())->Add(i);
}

void TokenCollection::AddTrackingTorpedoLock(std::string i) {
  if(this->tokens.find(Tok::TrackTorpLock) == this->tokens.end()) {
    this->tokens[Tok::TrackTorpLock] = std::make_shared<TrackingTorpedoLocks>();
  }
  dynamic_cast<TrackingTorpedoLocks*>(this->tokens[Tok::TrackTorpLock].get())->Add(i);
}

void TokenCollection::AddIds(Tok t, int c, int id) {
  if(this->tokens.find(t) == this->tokens.end()) {
    this->tokens[t] = std::make_shared<IDTokens>(t);
  }
  dynamic_cast<IDTokens*>(this->tokens[t].get())->Add(c, id);
}

  void TokenCollection::AddTurret(Tok t, int c, std::optional<Fac> f) {
  if(this->tokens.find(t) == this->tokens.end()) {
    this->tokens[t] = std::make_shared<TurretTokens>(t);
  }
  dynamic_cast<TurretTokens*>(this->tokens[t].get())->Add(t, c, f);
}

void TokenCollection::AddTokens(Tok t, int c) {
  if(this->tokens.find(t) == this->tokens.end()) {
    this->tokens[t] = std::make_shared<Tokens>(t);
  }
  this->tokens[t]->Add(c);
}

TokenCollection& TokenCollection::Dial(Shp s, Fac f)                     { this->AddDial(s,f);                            return *this; }
TokenCollection& TokenCollection::DialId(Shp s)                          { this->AddDialId(s);                            return *this; }
TokenCollection& TokenCollection::Ship(Plt p1, Plt p2)                   { this->AddShip(p1, p2);                         return *this; }
TokenCollection& TokenCollection::Calc(int c)                            { this->AddTokens(Tok::Calculate, c);            return *this; }
TokenCollection& TokenCollection::Cloak(int c)                           { this->AddTokens(Tok::Cloak, c);                return *this; }
TokenCollection& TokenCollection::Crit(int c)                            { this->AddTokens(Tok::Crit, c);                 return *this; }
TokenCollection& TokenCollection::Deplete(int c)                         { this->AddTokens(Tok::Deplete, c);              return *this; }
TokenCollection& TokenCollection::Disarm(int c)                          { this->AddTokens(Tok::Disarm, c);               return *this; }
TokenCollection& TokenCollection::Evade(int c)                           { this->AddTokens(Tok::Evade, c);                return *this; }
TokenCollection& TokenCollection::Focus(int c)                           { this->AddTokens(Tok::Focus, c);                return *this; }
TokenCollection& TokenCollection::ForceCharge(int c)                     { this->AddTokens(Tok::ForceCharge, c);          return *this; }
TokenCollection& TokenCollection::Fuse(int c)                            { this->AddTokens(Tok::Fuse, c);                 return *this; }
TokenCollection& TokenCollection::ID(int c,int id)                       { this->AddIds(Tok::ID, c, id);                  return *this; }
TokenCollection& TokenCollection::Ion(int c)                             { this->AddTokens(Tok::Ion, c);                  return *this; }
TokenCollection& TokenCollection::Jam(int c)                             { this->AddTokens(Tok::Jam, c);                  return *this; }
TokenCollection& TokenCollection::Lock(int c,int id)                     { this->AddIds(Tok::Lock, c, id);                return *this; }
TokenCollection& TokenCollection::Reinforce(int c)                       { this->AddTokens(Tok::Reinforce, c);            return *this; }
TokenCollection& TokenCollection::Shield(int c)                          { this->AddTokens(Tok::Shield, c);               return *this; }
TokenCollection& TokenCollection::StandardCharge(int c)                  { this->AddTokens(Tok::StandardCharge, c);       return *this; }
TokenCollection& TokenCollection::Strain(int c)                          { this->AddTokens(Tok::Strain, c);               return *this; }
TokenCollection& TokenCollection::Stress(int c)                          { this->AddTokens(Tok::Stress, c);               return *this; }
TokenCollection& TokenCollection::Tractor(int c)                         { this->AddTokens(Tok::Tractor, c);              return *this; }
TokenCollection& TokenCollection::TurretMount(int c)                     { this->AddTokens(Tok::TurretMountMarker, c);    return *this; }
TokenCollection& TokenCollection::SmSiTurret(int c,std::optional<Fac> f) { this->AddTurret(Tok::SmallSingleTurret, c, f); return *this; }
TokenCollection& TokenCollection::SmDoTurret(int c,std::optional<Fac> f) { this->AddTurret(Tok::SmallDoubleTurret, c, f); return *this; }
TokenCollection& TokenCollection::LaSiTurret(int c,std::optional<Fac> f) { this->AddTurret(Tok::LargeSingleTurret, c, f); return *this; }
TokenCollection& TokenCollection::LaDoTurret(int c,std::optional<Fac> f) { this->AddTurret(Tok::LargeDoubleTurret, c, f); return *this; }
TokenCollection& TokenCollection::HuSiTurret(int c,std::optional<Fac> f) { this->AddTurret(Tok::HugeSingleTurret, c, f);  return *this; }
TokenCollection& TokenCollection::HuDoTurret(int c,std::optional<Fac> f) { this->AddTurret(Tok::HugeDoubleTurret, c, f);  return *this; }
TokenCollection& TokenCollection::Asteroid(int c)                        { this->AddTokens(Tok::Asteroid, c);             return *this; }
TokenCollection& TokenCollection::Debris(int c)                          { this->AddTokens(Tok::DebrisCloud, c);          return *this; }
TokenCollection& TokenCollection::Gas(int c)                             { this->AddTokens(Tok::GasCloud, c);             return *this; }
TokenCollection& TokenCollection::Cargo(int c)                           { this->AddTokens(Tok::CargoDrift, c);           return *this; }
TokenCollection& TokenCollection::Bomblet(int c)                         { this->AddTokens(Tok::Bomblet, c);              return *this; }
TokenCollection& TokenCollection::ClusterMine(int c)                     { this->AddTokens(Tok::ClusterMineSet, c);       return *this; }
TokenCollection& TokenCollection::ConcussionBomb(int c)                  { this->AddTokens(Tok::ConcussionBomb, c);       return *this; }
TokenCollection& TokenCollection::ConnerNet(int c)                       { this->AddTokens(Tok::ConnerNet, c);            return *this; }
TokenCollection& TokenCollection::ElectroChaff(int c)                    { this->AddTokens(Tok::ElectroProtBomb, c);      return *this; }
TokenCollection& TokenCollection::ElectroProtBomb(int c)                 { this->AddTokens(Tok::ElectroProtBomb, c);      return *this; }
TokenCollection& TokenCollection::IonBomb(int c)                         { this->AddTokens(Tok::IonBomb, c);              return *this; }
TokenCollection& TokenCollection::LooseCargo(int c)                      { this->AddTokens(Tok::LooseCargo, c);           return *this; }
TokenCollection& TokenCollection::ProtonBomb(int c)                      { this->AddTokens(Tok::ProtonBomb, c);           return *this; }
TokenCollection& TokenCollection::ProxMine(int c)                        { this->AddTokens(Tok::ProximityMine, c);        return *this; }
TokenCollection& TokenCollection::SeismicCharge(int c)                   { this->AddTokens(Tok::SeismicCharge, c);        return *this; }
TokenCollection& TokenCollection::SpareParts(int c)                      { this->AddTokens(Tok::SpareParts, c);           return *this; }
TokenCollection& TokenCollection::ThermalDet(int c)                      { this->AddTokens(Tok::ThermalDetonator, c);     return *this; }
TokenCollection& TokenCollection::CompIntel(int c)                       { this->AddTokens(Tok::CompIntel, c);            return *this; }
TokenCollection& TokenCollection::Decoyed(int c)                         { this->AddTokens(Tok::Decoyed, c);              return *this; }
TokenCollection& TokenCollection::FearfulPrey(int c)                     { this->AddTokens(Tok::FearfulPrey, c);          return *this; }
TokenCollection& TokenCollection::Hunted(int c)                          { this->AddTokens(Tok::Hunted, c);               return *this; }
TokenCollection& TokenCollection::ISYTDS(int c)                          { this->AddTokens(Tok::ISYTDS, c);               return *this; }
TokenCollection& TokenCollection::ITR(int c)                             { this->AddTokens(Tok::ITR, c);                  return *this; }
TokenCollection& TokenCollection::ListeningDev(int c)                    { this->AddTokens(Tok::ListeningDevice, c);      return *this; }
TokenCollection& TokenCollection::OptPrototype(int c)                    { this->AddTokens(Tok::OptimizedPrototype, c);   return *this; }
TokenCollection& TokenCollection::Rattled(int c)                         { this->AddTokens(Tok::Rattled, c);              return *this; }
TokenCollection& TokenCollection::SuppFire(int c)                        { this->AddTokens(Tok::SuppressiveFire, c);      return *this; }
TokenCollection& TokenCollection::BuzzDroid(int c)                       { this->AddTokens(Tok::BuzzDroidSwarm, c);       return *this; }
TokenCollection& TokenCollection::DRK1ProbeDroid(int c)                  { this->AddTokens(Tok::DRK1ProbeDroid, c);       return *this; }
TokenCollection& TokenCollection::SensorBuoyBlue(int c)                  { this->AddTokens(Tok::SensorBuoyBlue, c);       return *this; }
TokenCollection& TokenCollection::SensorBuoyRed(int c)                   { this->AddTokens(Tok::SensorBuoyRed, c);        return *this; }
TokenCollection& TokenCollection::TrackTorp(std::string id)              { this->AddTrackingTorpedo(id);                  return *this; }
TokenCollection& TokenCollection::TrackTorpLock(std::string id)          { this->AddTrackingTorpedoLock(id);              return *this; }
TokenCollection& TokenCollection::TridentAgg(int c)                      { this->AddTokens(Tok::TridentAggMarker, c);     return *this; }
TokenCollection& TokenCollection::FirstPlayer(int c)                     { this->AddTokens(Tok::FirstPlayerMarker, c);    return *this; }
TokenCollection& TokenCollection::Hyperspace(int c)                      { this->AddTokens(Tok::HyperspaceMarker, c);     return *this; }
TokenCollection& TokenCollection::StratMark(int i)                       { this->AddStrategicMarkers(i);                  return *this; }
TokenCollection& TokenCollection::Victory(int c)                         { this->AddTokens(Tok::VictoryCounter, c);       return *this; }
TokenCollection& TokenCollection::YBMBYSTM(int c)                        { this->AddTokens(Tok::YBMBYSTM, c);             return *this; }

std::map<Tok,std::shared_ptr<Tokens>> TokenCollection::GetAllTokens() const { return this->tokens; }

Dials TokenCollection::GetDials() const {
  if(this->tokens.find(Tok::Dial) == this->tokens.end()) { return Dials(); }
  return *dynamic_cast<Dials*>(this->tokens.at(Tok::Dial).get());
}

DialIds TokenCollection::GetDialIds() const {
  if(this->tokens.find(Tok::DialId) == this->tokens.end()) { return DialIds(); }
  return *dynamic_cast<DialIds*>(this->tokens.at(Tok::DialId).get());
}

ShTokens TokenCollection::GetShipTokens() const {
  if(this->tokens.find(Tok::Ship) == this->tokens.end()) { return ShTokens(); }
  return *dynamic_cast<ShTokens*>(this->tokens.at(Tok::Ship).get());
}

StrategicMarkers TokenCollection::GetStrategicMarkers() const {
  if(this->tokens.find(Tok::StrategicMarker) == this->tokens.end()) { return StrategicMarkers(); }
  return *dynamic_cast<StrategicMarkers*>(this->tokens.at(Tok::StrategicMarker).get());
}

TrackingTorpedoTokens TokenCollection::GetTrackingTorpedoes() const {
  if(this->tokens.find(Tok::TrackTorp) == this->tokens.end()) { return TrackingTorpedoTokens(); }
  return *dynamic_cast<TrackingTorpedoTokens*>(this->tokens.at(Tok::TrackTorp).get());
}

TrackingTorpedoLocks TokenCollection::GetTrackingTorpedoLocks() const {
  if(this->tokens.find(Tok::TrackTorpLock) == this->tokens.end()) { return TrackingTorpedoLocks(); }
  return *dynamic_cast<TrackingTorpedoLocks*>(this->tokens.at(Tok::TrackTorp).get());
}

IDTokens TokenCollection::GetIDTokens(Tok tok) const {
  if(this->tokens.find(tok) == this->tokens.end()) { return IDTokens(tok); }
  return *dynamic_cast<IDTokens*>(this->tokens.at(tok).get());
}

TurretTokens TokenCollection::GetTurretTokens(Tok tok) const {
  if(this->tokens.find(tok) == this->tokens.end()) { return TurretTokens(tok); }
  return *dynamic_cast<TurretTokens*>(this->tokens.at(tok).get());
}

Tokens TokenCollection::GetTokens(Tok tok) const {
  if(this->tokens.find(tok) == this->tokens.end()) { return Tokens(tok); }
  return *this->tokens.at(tok).get();
}

}
