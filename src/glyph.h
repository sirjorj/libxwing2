#pragma once
#include "action.h"
#include "attack.h"
#include "dice.h"
#include "faction.h"
#include "shared.h"
#include "ship.h"
#include "upgradetype.h"

// mappings for https://github.com/geordanr/xwing-miniatures-font

namespace libxwing2 {

class ShipGlyphNotFound : public std::runtime_error {
 public:
  ShipGlyphNotFound(Shp s);
};

class ShipGlyph {
 public:
  static ShipGlyph GetShipGlyph(Shp s);
  static std::string GetGlyph(Shp s);
  //static std::vector<ShipGlyph> GetAllShipGlyphs();

  Shp         GetShp()      const;
  std::string GetScssName() const;
  std::string GetGlyph()    const;

 private:
  Shp         shp;
  std::string scssName;
  std::string glyph;

  static std::vector<ShipGlyph> shipGlyphs;

  ShipGlyph(Shp t,
	    std::string scss,
	    std::string gly);
};



enum class Ico {
  agility,
  astromech,
  attack,
  bankleft,
  bankright,
  barrelroll,
  base_all,
  base_small,
  base_medium,
  base_large,
  boost,
  bullseyearc,
  calculate,
  cannon,
  cargo,
  charge,
  cloak,
  command,
  condition_outline,
  condition,
  config,
  coordinate,
  crew,
  crit,
  dalan_bankleft,
  dalan_bankright,
  deplete,
  device,
  doublerecurring,
  doubleturretarc,
  empire,
  energy,
  epic,
  evade,
  firstorder,
  focus,
  forcecharge,
  forcepower,
  frontarc,
  fullfrontarc,
  fullreararc,
  gunner,
  hardpoint,
  helmet_imperial,
  helmet_rebel,
  helmet_scum,
  hit,
  hull,
  ig88d_sloopleft,
  ig88d_sloopright,
  illicit,
  jam,
  kturn,
  leftarc,
  linked,
  lock,
  missile,
  modification,
  negativerecurring,
  obstacle_core2asteroid0_outline,
  obstacle_core2asteroid0,
  obstacle_core2asteroid1_outline,
  obstacle_core2asteroid1,
  obstacle_core2asteroid2_outline,
  obstacle_core2asteroid2,
  obstacle_core2asteroid3_outline,
  obstacle_core2asteroid3,
  obstacle_core2asteroid4_outline,
  obstacle_core2asteroid4,
  obstacle_core2asteroid5_outline,
  obstacle_core2asteroid5,
  obstacle_coreasteroid0_outline,
  obstacle_coreasteroid0,
  obstacle_coreasteroid1_outline,
  obstacle_coreasteroid1,
  obstacle_coreasteroid2_outline,
  obstacle_coreasteroid2,
  obstacle_coreasteroid3_outline,
  obstacle_coreasteroid3,
  obstacle_coreasteroid4_outline,
  obstacle_coreasteroid4,
  obstacle_coreasteroid5_outline,
  obstacle_coreasteroid5,
  obstacle_vt49decimatordebris0_outline,
  obstacle_vt49decimatordebris0,
  obstacle_vt49decimatordebris1_outline,
  obstacle_vt49decimatordebris1,
  obstacle_vt49decimatordebris2_outline,
  obstacle_vt49decimatordebris2,
  obstacle_yt2400debris0_outline,
  obstacle_yt2400debris0,
  obstacle_yt2400debris1_outline,
  obstacle_yt2400debris1,
  obstacle_yt2400debris2_outline,
  obstacle_yt2400debris2,
  point,
  rangebonusindicator,
  reararc,
  rebel_outline,
  rebel,
  recover,
  recurring,
  reinforce,
  reload,
  republic,
  reversebankleft,
  reversebankright,
  reversestraight,
  rightarc,
  rotatearc,
  scum,
  separatists,
  sensor,
  shield,
  singleturretarc,
  slam,
  sloopleft,
  sloopright,
  squad_point_cost,
  stop,
  straight,
  strain,
  tacticalrelay,
  talent,
  team,
  tech,
  title,
  token_calculate_outline,
  token_calculate,
  token_charge_outline,
  token_charge,
  token_crit_outline,
  token_crit,
  token_cloak_outline,
  token_cloak,
  token_deplete_outline,
  token_deplete,
  token_evade_outline,
  token_evade,
  token_focus_outline,
  token_focus,
  token_force_outline,
  token_force,
  token_disarm_outline,
  token_disarm,
  token_ion_outline,
  token_ion,
  token_jam_outline,
  token_jam,
  token_lock_outline,
  token_lock,
  token_point_1_outline,
  token_point_1,
  token_point_3_outline,
  token_point_3,
  token_reinforce_outline,
  token_reinforce,
  token_reinforceaft_outline,
  token_reinforceaft,
  token_shield_outline,
  token_shield,
  token_strain_outline,
  token_strain,
  token_stress_outline,
  token_stress,
  token_tractor_outline,
  token_tractor,
  torpedo,
  triplerecurring,
  trollleft,
  trollright,
  turnleft,
  turnright,
  turret,
  unique_outline,
  unique,
  // added:
  blank,
};



class IconGlyphNotFound : public std::runtime_error {
 public:
  IconGlyphNotFound(Ico i);
};

class IconGlyph {
 public:
  static IconGlyph   GetIconGlyph(Ico i);
  static std::string GetGlyph(Ico i);
  static std::string GetGlyph(std::string ctph);

  static IconGlyph   GetIconGlyph(Act a);
  static std::string GetGlyph(Act a);

  static IconGlyph   GetIconGlyph(Fac f);
  static std::string GetGlyph(Fac f);

  static IconGlyph   GetIconGlyph(UpT u);
  static std::string GetGlyph(UpT u);

  static IconGlyph   GetIconGlyphEx(UpT u);
  static std::string GetGlyphEx(UpT u);

  static IconGlyph   GetIconGlyph(AtD u);
  static std::string GetGlyph(AtD u);
  static IconGlyph   GetIconGlyph(DeD u);
  static std::string GetGlyph(DeD u);

  static IconGlyph   GetIconGlyph(Arc a);
  static std::string GetGlyph(Arc a);

  static IconGlyph   GetIconGlyph(Brn b);
  static std::string GetGlyph(Brn b);


  static std::vector<Ico> GetAllIcos();

  Ico         GetIco()                 const;
  std::string GetScssName()            const;
  std::string GetCardTextPlaceholder() const;
  std::string GetGlyph()               const;


 private:
  Ico         ico;
  std::string scssName;
  std::string cardTextPlaceholder;
  std::string glyph;

  static Ico ActToIco(Act a);
  static Ico FacToIco(Fac f);
  static Ico UpgToIco(UpT u);
  //static Ico ExToIco(UpT u);
  static Ico AtDToIco(AtD a);
  static Ico DeDToIco(DeD d);
  static Ico ArcToIco(Arc a);
  static Ico BrnToIco(Brn b);

  static std::vector<IconGlyph> iconGlyphs;

  IconGlyph(Ico i,
	    std::string scss,
	    std::string ctph,
	    std::string gly);
};

}
