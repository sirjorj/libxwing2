#pragma once
#include "action.h"
#include "attack.h"
#include "force.h"
#include "maneuver.h"
#include "upgradetype.h"
#include <functional>
#include <optional>

namespace libxwing2 {

struct AttackMod {
  Arc arc;
  uint16_t val;
};

class Modifier {
 public:
  struct ManeuverDifficulty {
    std::optional<int8_t> speed;
    Brn    bearing;
  };

  Modifier();

  Modifier& ActAdd(ActionBar a);
  Modifier& Agility(int16_t a);
  Modifier& AttackAdd(PriAttacks a);
  Modifier& Attack(AttackMod a);
  Modifier& Energy(int16_t e);
  Modifier& Force(FAf f);
  Modifier& Hull(int16_t h);
  Modifier& Shield(int16_t s);
  Modifier& UpgAdd(UpgradeBar u);
  Modifier& UpgRem(UpgradeBar u);
  Modifier& ManDec(std::vector<ManeuverDifficulty> m);
  Modifier& ManInc(std::vector<ManeuverDifficulty> m);

  ActionBar GetActAdd() const;
  int16_t GetAgility() const;
  PriAttacks GetAttackAdd() const;
  AttackMod GetAttackMod() const;
  int16_t GetEnergy() const;
  FAf GetForceAf() const;
  int16_t GetHull() const;
  int16_t GetShield() const;
  UpgradeBar GetUpgAdd() const;
  UpgradeBar GetUpgRem() const;
  std::vector<ManeuverDifficulty> GetManDec() const;
  std::vector<ManeuverDifficulty> GetManInc() const;

private:
  std::optional<ActionBar>                       _actAdd;
  std::optional<int16_t>                         _agility;
  std::optional<PriAttacks>                      _attackAdd;
  std::optional<AttackMod>                       _attackMod;
  std::optional<int16_t>                         _energy;
  std::optional<FAf>                             _forceAf;
  std::optional<int16_t>                         _hull;
  std::optional<std::vector<ManeuverDifficulty>> _manDec;
  std::optional<std::vector<ManeuverDifficulty>> _manInc;
  std::optional<int16_t>                         _shield;
  std::optional<UpgradeBar>                      _upgAdd;
  std::optional<UpgradeBar>                      _upgRem;

};

}
