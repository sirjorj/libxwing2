#pragma once
#include "shared.h"
#include "difficulty.h"
#include <stdexcept>
#include <vector>

namespace libxwing2 {

// Bearing
enum class Brn {
  LTurn,
  LBank,
  Straight,
  RBank,
  RTurn,
  KTurn,
  Stationary,
  LSloop,
  RSloop,
  LTroll,
  RTroll,
  RevLBank,
  RevStraight,
  RevRBank,
};

class BearingNotFound : public std::runtime_error {
 public:
  BearingNotFound(Brn a);
  BearingNotFound(std::string txt);
};

class Bearing {
 public:
  static Bearing GetBearing(Brn b);
  static Bearing GetBearing(std::string text);
  static std::vector<Brn> GetAllBrns();

  Brn GetBrn()               const;
  std::string GetName()      const;
  std::string GetShortName() const;
  std::string GetCharacter() const;
  std::string GetText()      const;

 private:
  Brn         brn;
  std::string name;
  std::string shortName;
  std::string character;
  std::string text;

  static std::vector<Bearing> bearings;

  Bearing(Brn         b,
          std::string n,
          std::string x,
          std::string c,
          std::string t);
};



struct Maneuver {
  int8_t speed;
  Brn    bearing;
  Dif    difficulty;
};

typedef std::vector<Maneuver> Maneuvers;

bool FindManeuver(Maneuvers ms, uint8_t s, Brn b, Maneuver &m);

}
