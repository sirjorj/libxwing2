#pragma once
#include "faction.h"
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// UpgradeType
enum class UpT {
  Astro,
  Cannon,
  Cargo,
  Command,
  Config,
  Crew,
  Force,
  Gunner,
  Hardpoint,
  Hyperdr,
  Illicit,
  Missile,
  Mod,
  Payload,
  Sensor,
  TactRel,
  Talent,
  Team,
  Tech,
  Title,
  Torpedo,
  Turret,
};



class UpgradeTypeNotFound : public std::runtime_error {
 public:
  UpgradeTypeNotFound(UpT u);
  UpgradeTypeNotFound(std::string xws);
};



class UpgradeType {
 public:
  static UpgradeType GetUpgradeType(UpT typ);
  static std::vector<UpT> GetAllUpTs();
  UpT         GetUpT()       const;
  std::string GetName()       const;
  std::string GetShortName()  const;

 private:
  UpT upt;
  std::string name;
  std::string shortName;

  static std::vector<UpgradeType> upgradeTypes;

  UpgradeType(UpT         t,
              std::string n,
              std::string s);
};

UpT operator|(UpT a, UpT b);
UpT operator&(UpT a, UpT b);



class UpgradeSlot {
 public:
  UpgradeSlot(UpT t);
  UpgradeSlot(std::vector<UpT> t);

  std::vector<UpT> GetTypes() const;
  bool IsMulti()              const;
  bool CanEquip(UpT t)        const;

  bool operator==(UpgradeSlot o);
  bool operator==(UpgradeSlot o) const;

 private:
  std::vector<UpT> types;
};



typedef std::vector<UpgradeSlot> UpgradeBar;

}
