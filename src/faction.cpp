#include "faction.h"

namespace libxwing2 {

// Faction
std::vector<Faction> Faction::factions = {
  //{ Fac::None,       "None",                "None"        },
  { Fac::Rebel,      "Rebel Alliance",      "Rebel"       },
  { Fac::Imperial,   "Galactic Empire",     "Imperial"    },
  { Fac::Scum,       "Scum and Villainy",   "Scum"        },
  { Fac::Resistance, "Resistance",          "Resistance"  },
  { Fac::FirstOrder, "First Order",         "First Order" },
  { Fac::Republic,   "Galactic Republic",   "Republic"    },
  { Fac::Separatist, "Separatist Alliance", "Separatist"  },
  //{ Fac::All,        "All",                 "All"         },
};

FactionNotFound::FactionNotFound(Fac f)           : runtime_error("Faction not found (enum " + std::to_string((int)f) + ")") { }
FactionNotFound::FactionNotFound(std::string xws) : runtime_error("Faction not found (" + xws + ")") { }

Faction Faction::GetFaction(Fac f) {
  for(const Faction& faction : Faction::factions) {
    if(faction.GetFac() == f) {
      return faction;
    }
  }
  throw FactionNotFound(f);
}

std::vector<Fac> Faction::GetAllFacs() {
  std::vector<Fac> ret;
  for(const Faction& faction : Faction::factions) {
    ret.push_back(faction.GetFac());
  }
  return ret;
}

Fac         Faction::GetFac()     const { return this->fac; }
std::string Faction::GetName()    const { return this->name; }
std::string Faction::GetShort()   const { return this->shortName; }
std::string Faction::Get3Letter() const { return this->shortName.substr(0,3); }

Faction::Faction(Fac         f,
                 std::string n,
		 std::string s)
  : fac(f), name(n), shortName(s) { }

}
