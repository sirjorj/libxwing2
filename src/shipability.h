#pragma once
#include "modifier.h"
#include "text.h"

namespace libxwing2 {

enum class SAb {
  AdaptAil,
  CoPilot,
  RigEnCells,
  LockLoad,
  ExpScan,
  VecThrust,
  ComShuttle,
  TailGun,
  SensBlind,
  AdvTgtCmp,
  FullThrot,
  Autothr,
  StygArr,
  NmbBmb,
  AdvDrBr,
  ConcFace,
  D2Rights,
  WpnHardpt,
  StTracArr,
  MicroThr,
  EscCraft,
  PursCraft,
  HWeapTrt,
  LnkBat,
  RefGyro,
  NotchStab,
  NetCalc,
  FinTunCtrl,
  PpTracArr,
  PlatHull,
  BroadBat,
  OdBurner,
  DockClamp,
  ResupCraft,
  ConcenBat,
  FinTunThr,
  ExpWings,
  RotCan,
  FireConv,
  NetAim,
  IntCtrls,
  TIE,
  IndCalc,
  VecCannon,
  SensCtrls,
  AdvFirCtrl,
  TracGrasp,
  //
  IntInt,
  PursuitThr,
  DevBar,
};



class ShipAbilityNotFound : public std::runtime_error {
 public:
  ShipAbilityNotFound(SAb sab);
  //ShipAbilityNotFound(std::string pltstr);
};



class ShipAbility {
 public:
  static ShipAbility GetShipAbility(SAb sab);
  static std::vector<SAb> GetAllSAbs();

  SAb         GetSAb()        const;
  std::string GetName()       const;
  Modifier    GetModifier()   const;
  Text        GetText()       const;

 private:
  SAb           sab;
  std::string   name;
  Modifier      modifier;
  std::string   text;

  static std::vector<ShipAbility> shipAbilities;

  ShipAbility(SAb s, std::string n, Modifier m, std::string t);
};


}
