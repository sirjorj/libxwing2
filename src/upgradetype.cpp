#include "upgradetype.h"
#include "pilot.h"
#include <map>

namespace libxwing2 {

// *** UpgradeType ***
UpgradeTypeNotFound::UpgradeTypeNotFound(UpT u)           : runtime_error("UpgradeType not found: (enum " + std::to_string((int)u) + ")") { }
UpgradeTypeNotFound::UpgradeTypeNotFound(std::string xws) : runtime_error("UpgradeType not found: '" + xws + "'") { }

std::vector<UpgradeType> UpgradeType::upgradeTypes = {
  { UpT::Astro,     "Astromech",      "Ast" },
  { UpT::Cannon,    "Cannon",         "Can" },
  { UpT::Cargo,     "Cargo",          "Car" },
  { UpT::Command,   "Command",        "Cmd" },
  { UpT::Config,    "Configuration",  "Cfg" },
  { UpT::Crew,      "Crew",           "Crw" },
  { UpT::Force,     "Force Power",    "Frc" },
  { UpT::Gunner,    "Gunner",         "Gun" },
  { UpT::Hardpoint, "Hardpoint",      "HaP" },
  { UpT::Hyperdr,   "Hyperdrive",     "Hyp" },
  { UpT::Illicit,   "Illicit",        "Ill" },
  { UpT::Missile,   "Missile",        "Msl" },
  { UpT::Mod,       "Modification",   "Mod" },
  { UpT::Payload,   "Payload",        "Pld" },
  { UpT::Sensor,    "Sensor",         "Sns" },
  { UpT::TactRel,   "Tactical Relay", "Tac" },
  { UpT::Talent,    "Talent",         "Tal" },
  { UpT::Team,      "Team",           "Tea" },
  { UpT::Tech,      "Tech",           "Tec" },
  { UpT::Title,     "Title",          "Ttl" },
  { UpT::Torpedo,   "Torpedo",        "Trp" },
  { UpT::Turret,    "Turret",         "Tur" },
};

UpgradeType UpgradeType::GetUpgradeType(UpT upt) {
  for(const UpgradeType& upgradeType : UpgradeType::upgradeTypes) {
    if(upgradeType.GetUpT() == upt) {
      return upgradeType;
    }
  }
  throw UpgradeTypeNotFound(upt);
}

std::vector<UpT> UpgradeType::GetAllUpTs() {
  std::vector<UpT> ret;
  for(const UpgradeType& upgradeType : UpgradeType::upgradeTypes) {
    ret.push_back(upgradeType.GetUpT());
  }
  return ret;
}

UpT         UpgradeType::GetUpT()        const { return this->upt; }
std::string UpgradeType::GetName()       const { return this->name; }
std::string UpgradeType::GetShortName()  const { return this->shortName; }

UpgradeType::UpgradeType(UpT         t,
                         std::string n,
                         std::string s)
  : upt(t), name(n), shortName(s) { }

UpT operator|(UpT a, UpT b) {
  return static_cast<UpT>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

UpT operator&(UpT a, UpT b) {
  return static_cast<UpT>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}

UpgradeSlot::UpgradeSlot(UpT t) : types({t}){
}

UpgradeSlot::UpgradeSlot(std::vector<UpT> t) : types(t) {
}

std::vector<UpT> UpgradeSlot::GetTypes()      const { return this->types; }
bool             UpgradeSlot::IsMulti()       const { return (this->types.size() > 1); }
bool             UpgradeSlot::CanEquip(UpT t) const {
  for(UpT s : this->types) {
    if(s == t) {
      return true;
    }
  }
  return false;
}

bool UpgradeSlot::operator==(UpgradeSlot o)       { return this->GetTypes() == o.GetTypes(); }
bool UpgradeSlot::operator==(UpgradeSlot o) const { return this->GetTypes() == o.GetTypes(); }






}
