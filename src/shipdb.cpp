#include "ship.h"

namespace libxwing2 {

typedef Maneuvers  M;

const Brn LT = Brn::LTurn;
const Brn LB = Brn::LBank;
const Brn ST = Brn::Straight;
const Brn RB = Brn::RBank;
const Brn RT = Brn::RTurn;
const Brn KT = Brn::KTurn;
const Brn SY = Brn::Stationary;
const Brn LS = Brn::LSloop;
const Brn RS = Brn::RSloop;
const Brn LR = Brn::LTroll;
const Brn RR = Brn::RTroll;
const Brn RLB= Brn::RevLBank;
const Brn RST= Brn::RevStraight;
const Brn RRB= Brn::RevRBank;

const Dif B = Dif::Blue;
const Dif W = Dif::White;
const Dif R = Dif::Red;
const Dif P = Dif::Purple;

// maneuvers          5                   4                             3 Standard                          3 Special                         2 Standard                               2 Special                              1 Standard                         1 Special          0
//           ------------------  ------------------  ------------------------------------------------  ------------------  ------------------------------------------------  ----------------------------  ------------------------------------------------  ------------------  --------
M mAGGRE = {                     {4,ST,W}, {4,KT,R},           {3,LB,B}, {3,ST,B}, {3,RB,B}, {3,LS,R}, {3,RS,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}                                                   };
M mALPHA = {                     {4,ST,R},           {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W}                                                             };
M mARC17 = {                     {4,ST,R}, {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                                             };
M mASF01 = {                     {4,ST,R},                     {3,LB,R}, {3,ST,B}, {3,RB,R},                               {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W}, {2,KT,R},                     {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}, {1,LR,R}, {1,RR,R}                               };
M mATTSH = {                     {4,ST,W}, {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                                                   };
M mAUZIT = {                     {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R}                     };
M mBEL22 = { {5,ST,W},           {4,ST,W},           {3,LT,R}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,R}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W}, {1,LB,W},           {1,RB,W}, {1,RT,W}                                                   };
M mBTANR = {           {5,KT,R}, {4,ST,R},           {3,LT,R}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                                             };
M mBTLA4 = {                     {4,ST,R}, {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                                             };
M mBTLBY = {                     {4,ST,R}, {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W}                                                             };
M mBTLS8 = {                                                   {3,LB,W}, {3,ST,W}, {3,RB,W},                               {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                                             };
M mCR90C = { {5,ST,R},           {4,ST,R},                     {3,LB,R}, {3,ST,B}, {3,RB,R},                                         {2,LB,B}, {2,ST,B}, {2,RB,B},                                                   {1,LB,W}, {1,ST,W}, {1,RB,W},                               {0,SY,R}, {0,LB,R}, {0,RB,R} };
M mCROCC = { {5,ST,R},           {4,ST,R},                     {3,LB,R}, {3,ST,W}, {3,RB,R},                                         {2,LB,W}, {2,ST,B}, {2,RB,W},                                                   {1,LB,W}, {1,ST,B}, {1,RB,W},                               {0,SY,R}, {0,LB,R}, {0,RB,R} };
M mCYT13 = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                                             };
M mDELTA = { {5,ST,W}, {5,KT,R}, {4,ST,W}, {4,KT,R},           {3,LB,W}, {3,ST,B}, {3,RB,W},                               {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LS,R}, {2,RS,R},           {1,LT,W}, {1,LB,B},           {1,RB,B}, {1,RT,W}                                                   };
M mDTRIF = { {5,ST,W}, {5,KT,R}, {4,ST,B},           {3,LT,B}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,B}, {3,KT,R},           {2,LT,B}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,B},                               {1,LT,W},                               {1,RT,W}, {1,LR,R}, {1,RR,R}                               };
M mESCCR = {                                                   {3,LB,W}, {3,ST,W}, {3,RB,W},           {3,KT,R},           {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                     {0,SY,R}                               };
M mETA2A = { {5,ST,W},           {4,ST,B}, {4,KT,R}, {3,LT,W}, {3,LB,B}, {3,ST,B}, {3,RB,B}, {3,RT,W},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LR,P}, {2,RR,P},           {1,LT,W},                               {1,RT,W}                                                   };
M mEWING = { {5,ST,W},           {4,ST,B}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                                                   };
M mFANGF = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B}, {2,LR,R}, {2,RR,R},           {1,LT,W},                               {1,RT,W}                                                   };
M mFIREB = {                     {4,ST,R},           {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}                                                   };
M mFIRES = {                     {4,ST,W}, {4,KT,R},           {3,LB,W}, {3,ST,B}, {3,RB,W},           {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}                                                   };
M mG1ASF = {                     {4,ST,R}, {4,KT,R},           {3,LB,R}, {3,ST,W}, {3,RB,R},                               {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W}, {2,KT,R},                     {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R},                     {0,SY,R}                     };
M mGOZAN = {                     {4,ST,R},                               {3,ST,B},                                                   {2,LB,R}, {2,ST,B}, {2,RB,R},                                                   {1,LB,W}, {1,ST,B}, {1,RB,W},                               {0,SY,R}, {0,LB,R}, {0,RB,R} };
M mGR75M = {                     {4,ST,R},                               {3,ST,R},                                                   {2,LB,W}, {2,ST,W}, {2,RB,W},                                                   {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R}, {0,LB,R}, {0,RB,R} };
M mHMPDG = { {5,ST,R},           {4,ST,R},           {3,LT,W}, {3,LB,R}, {3,ST,W}, {3,RB,R}, {3,RT,W},                     {2,LT,B}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,B},                                         {1,LB,R}, {1,ST,B}, {1,RB,R},                               {0,SY,R}                     };
M mHWK29 = {                     {4,ST,W},           {3,LT,R}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R}                     };
M mHYENA = { {5,ST,R},           {4,ST,W},           {3,LT,W},           {3,ST,B},           {3,RT,W},                     {2,LT,B}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,B}, {2,KT,R}, {2,LR,R}, {2,RR,R}, {1,LT,W}, {1,LB,R}, {1,ST,W}, {1,RB,R}, {1,RT,W}                                                   };
M mJUM5K = {                     {4,ST,W}, {4,KT,R},           {3,LB,B}, {3,ST,B}, {3,RB,W},           {3,LS,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,W}, {2,RT,R},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,W}, {1,RT,R}                                                   };
M mKIHRA = {                     {4,ST,W}, {4,KT,R},           {3,LB,W}, {3,ST,B}, {3,RB,W},                               {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LR,R}, {2,RR,R},           {1,LT,W}, {1,LB,B},           {1,RB,B}, {1,RT,W}                                                   };
M mLAATG = {                     {4,ST,R},           {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W},                               {0,SY,R}                     };
M mLAMBD = {                                                   {3,LB,R}, {3,ST,W}, {3,RB,R},                               {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R}                     };
M mLACPC = { {5,ST,W}, {5,KT,R}, {4,ST,B},           {3,LT,B}, {3,LB,B}, {3,ST,B}, {3,RB,B}, {3,RT,B},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,W}, {1,ST,W}, {1,RB,W}                                                             };
M mM12LK = {                               {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,R}, {1,LB,W}, {1,ST,B}, {1,RB,W}, {1,RT,R}                                                   };
M mM3AIN = { {5,ST,W}, {5,KT,R}, {4,ST,W},                     {3,LB,W}, {3,ST,B}, {3,RB,W},           {3,KT,R},           {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,W}, {1,LB,B},           {1,RB,B}, {1,RT,W}                                                   };
M mMG100 = {                                                   {3,LB,R}, {3,ST,W}, {3,RB,R},                               {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R},                     {0,SY,R}                     };
M mMGTIE = { {5,ST,R},           {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W},                               {1,RT,W}                                                   };
M mMYT13 = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                                   {1,LB,W}, {1,ST,B}, {1,RB,W}                                                   };
M mNABN1 = { {5,ST,W},           {4,ST,W},           {3,LT,W}, {3,LB,B}, {3,ST,B}, {3,RB,B}, {3,RT,W}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,W}, {1,RB,W}                                                             };
M mNANTX = { {5,ST,W}, {5,KT,R}, {4,ST,W},           {3,LT,W}, {3,LB,B}, {3,ST,B}, {3,RB,B}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W}, {1,LB,B},           {1,RB,B}, {1,RT,W},                                                  };
M mNIMBV = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B}, {2,KT,R},                     {1,LT,W}, {1,LB,R},           {1,RB,R}, {1,RT,W},                                                  };
M mQUADJ = {                                                   {3,LB,W}, {3,ST,B}, {3,RB,W},                               {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LS,R}, {2,RS,R},{2,RST,R}, {1,LT,W}, {1,LB,W}, {1,ST,W}, {1,RB,W}, {1,RT,W}, {1,RLB,R},{1,RRB,R}                              };
M mRAIDR = { {5,ST,R},           {4,ST,W},                     {3,LB,R}, {3,ST,B}, {3,RB,R},                                         {2,LB,W}, {2,ST,B}, {2,RB,W},                                                   {1,LB,B}, {1,ST,W}, {1,RB,B},                               {0,SY,R}, {0,LB,R}, {0,RB,R} };
M mRTRAN = {                     {4,ST,R},                     {3,LB,R}, {3,ST,W}, {3,RB,R},                               {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}, {1,RLB,R},{1,RRB,R},{0,SY,R}                     };
M mRTPOD = {                     {4,ST,R},                     {3,LB,R}, {3,ST,W}, {3,RB,R},           {3,KT,R},           {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                                                   };
M mRZ1AW = { {5,ST,B}, {5,KT,R}, {4,ST,B},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B},                               {1,LT,W},                               {1,RT,W}                                                   };
M mRZ2AW = { {5,ST,B}, {5,KT,R}, {4,ST,B},           {3,LT,W}, {3,LB,B}, {3,ST,B}, {3,RB,B}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B},                               {1,LT,W},                               {1,RT,W}                                                   };
M mSYT13 = {                     {4,ST,R},           {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                                   {1,LB,W}, {1,ST,B}, {1,RB,W}                                                   };
M mSCURR = {                     {4,ST,R},           {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                                             };
M mSHEAT = {                     {4,ST,R},           {3,LT,R}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,R}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W},           {1,RST,R}                                        };
M mSITHI = {           {5,KT,R}, {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LS,R}, {2,RS,R},           {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                                                   };
M mSTARV = {                     {4,ST,W},                     {3,LB,W}, {3,ST,B}, {3,RB,W},           {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}                                                   };
M mSYLIU = {                                                                                                                                                                                                                                                                                                  };
M mT65XW = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                                             };
M mT70XW = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B}                                                             };
M mTIEAv = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W}, {2,LR,R}, {2,RR,R},           {1,LT,B}, {1,LB,B},           {1,RB,B}, {1,RT,B}                                                   };
M mTIEAx = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,B}, {1,ST,W}, {1,RB,B}                                                             };
M mTIEag = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W}                                                             };
M mTIEba = { {5,ST,W}, {5,KT,R}, {4,ST,B},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LS,R}, {2,RS,R},           {1,LT,B}, {1,LB,B},           {1,RB,B}, {1,RT,B}                                                   };
M mTIEca = {                               {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R}                     };
M mTIEdD = { {5,ST,B},           {4,ST,B}, {4,KT,W}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R}, {2,KT,R},                     {1,LT,R}, {1,LB,B},           {1,RB,B}, {1,RT,R}                                                   };
M mTIEfo = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B}, {2,LS,R}, {2,RS,R},           {1,LT,W},                               {1,RT,W}                                                   };
M mTIEin = { {5,ST,W},           {4,ST,B}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B},                               {1,LT,W},                               {1,RT,W}                                                   };
M mTIEln = { {5,ST,W},           {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W},                               {1,RT,W}                                                   };
M mTIEph = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W}, {1,LB,W},           {1,RB,W}, {1,RT,W}                                                   };
M mTIErb = {                     {4,ST,W},           {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R}, {3,LR,R}, {3,RR,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,R}, {1,LB,W}, {1,ST,B}, {1,RB,W}, {1,RT,R},                                                  };
M mTIERe = {                                                   {3,LB,W}, {3,ST,B}, {3,RB,W},                               {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}, {1,LS,R}, {1,RS,R}, {0,SY,R}                     };
M mTIEsa = {           {5,KT,R}, {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W}                                                             };
M mTIEse = {                     {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,W}, {1,ST,B}, {1,RB,W}, {1,RT,R}                                                   };
M mTIEsf = { {5,ST,W},           {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                                                   };
M mTIEsk = {                                                   {3,LB,W}, {3,ST,B}, {3,RB,W},                               {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W}, {2,LS,R}, {2,RS,R},           {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}, {1,KT,R}                                         };
M mTIEvn = { {5,ST,B},           {4,ST,B}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,LR,R}, {3,RR,R}, {2,LT,B}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,B},                               {1,LT,W},                               {1,RT,W}                                                   };
M mTIEwi = { {5,ST,B}, {5,KT,R}, {4,ST,B}, {4,KT,R}, {3,LT,W}, {3,LB,B}, {3,ST,B}, {3,RB,B}, {3,RT,W}, {3,LS,R}, {3,RS,R}, {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,W},                               {1,RT,W}                                                   };
M mTRIDE = { {5,ST,W},           {4,ST,B}, {4,KT,R},           {3,LB,W}, {3,ST,B}, {3,RB,W},                                         {2,LB,B}, {2,ST,B}, {2,RB,B},                              {2,RST,R},                                                   {1,RST,W},          {0,SY,R}, {0,LB,R}, {0,RB,R} };
M mUPSIL = {                                         {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,R}, {1,LB,W}, {1,ST,B}, {1,RB,W}, {1,RT,R},                     {0,SY,R}                     };
M mUWING = {                     {4,ST,W},                     {3,LB,W}, {3,ST,W}, {3,RB,W},                               {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                                   {1,LB,B}, {1,ST,B}, {1,RB,B},                     {0,SY,R}                     };
M mV19TO = {                     {4,ST,W},                     {3,LB,R}, {3,ST,B}, {3,RB,R},           {3,KT,R},           {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W}, {2,LR,R}, {2,RR,R},           {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                                                   };
M mVCX10 = {                     {4,ST,W}, {4,KT,R}, {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                               {1,LT,R}, {1,LB,W}, {1,ST,B}, {1,RB,W}, {1,RT,R}                                                   };
M mVT49D = {                     {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,R}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,R}                                                   };
M mVULTU = { {5,ST,W},           {4,ST,B},           {3,LT,W}, {3,LB,R}, {3,ST,B}, {3,RB,R}, {3,RT,W},                     {2,LT,B}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,B}, {2,LR,R}, {2,RR,R},           {1,LT,W},                               {1,RT,W}, {1,KT,R}                                         };
M mXICLA = {                     {4,ST,W},           {3,LT,R}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,R},                     {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R}                     };
M mYT240 = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,W}, {3,RB,W}, {3,RT,W},                     {2,LT,W}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,W},                               {1,LT,W}, {1,LB,B}, {1,ST,B}, {1,RB,B}, {1,RT,W}                                                   };
M mYV666 = {                     {4,ST,W},           {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W},                     {2,LT,R}, {2,LB,W}, {2,ST,B}, {2,RB,W}, {2,RT,R},                                         {1,LB,B}, {1,ST,B}, {1,RB,B},                               {0,SY,R}                     };
M mZ95HH = {                     {4,ST,W}, {4,KT,R}, {3,LT,W}, {3,LB,W}, {3,ST,B}, {3,RB,W}, {3,RT,W}, {3,KT,R},           {2,LT,W}, {2,LB,B}, {2,ST,B}, {2,RB,B}, {2,RT,W},                                         {1,LB,W}, {1,ST,B}, {1,RB,W}                                                             };



std::vector<Ship> Ship::ships = {
  { Shp::Aggressor,        "Aggressor Assault Fighter",           "Aggressor",      "AgF",  BSz::Medium, mAGGRE },
  { Shp::AlphaClass,       "Alpha-class Star Wing",               "Star Wing",      "SW",   BSz::Small,  mALPHA },
  { Shp::ARC170,           "ARC-170 Starfighter",                 "ARC-170",        "AR",   BSz::Medium, mARC17 },
  { Shp::ASF01BWing,       "A/SF-01 B-wing",                      "B-wing",         "BW",   BSz::Small,  mASF01 },
  { Shp::AttackShuttle,    "Attack Shuttle",                      "Attack Shuttle", "AS",   BSz::Small,  mATTSH },
  { Shp::Auzituck,         "Auzituck Gunship",                    "Auzituck",       "Auz",  BSz::Small,  mAUZIT },
  { Shp::Belbullab22,      "Belbullab-22 Starfighter",            "Belbullab-22",   "B22",  BSz::Small,  mBEL22 },
  { Shp::BTANR2YWing,      "BTA-NR2 Y-wing",                      "BTA-NR2 Y-Wing", "NR2",  BSz::Small,  mBTANR },
  { Shp::BTLA4YWing,       "BTL-A4 Y-wing",                       "BTL-A4 Y-Wing",  "YW",   BSz::Small,  mBTLA4 },
  { Shp::BTLBYWing,        "BTL-B Y-wing",                        "BTL-B Y-Wing",   "YwB",  BSz::Small,  mBTLBY },
  { Shp::BTLS8KWing,       "BTL-S8 K-wing",                       "K-wing",         "KW",   BSz::Medium, mBTLS8 },
  { Shp::CR90,             "CR90 Corellian Corvette",             "CR90",           "C90",  BSz::Huge,   mCR90C },
  { Shp::CROC,             "C-ROC Cruiser",                       "C-ROC",          "CRC",  BSz::Huge,   mCROCC },
  { Shp::CustYT1300,       "Customized YT-1300 Light Freighter",  "Cst YT-1300",    "CY",   BSz::Large,  mCYT13 },
  { Shp::Delta7,           "Delta-7 Aethersprite",                "Delta-7",        "D7A",  BSz::Small,  mDELTA },
  { Shp::DroidTriFighter,  "Droid Tri-Fighter",                   "Tri-Fighter",    "DTF",  BSz::Small,  mDTRIF },
  { Shp::EscCraft,         "Escape Craft",                        "Escape Craft",   "ES",   BSz::Small,  mESCCR },
  { Shp::Eta2Actis,        "Eta-2 Actis",                         "Eta-2 Actis",    "E2A",  BSz::Small,  mETA2A },
  { Shp::EWing,            "E-wing",                              "E-wing",         "EW",   BSz::Small,  mEWING },
  { Shp::FangFighter,      "Fang Fighter",                        "Fang Fighter",   "FNG",  BSz::Small,  mFANGF },
  { Shp::Fireball,         "Fireball",                            "Fireball",       "FB",   BSz::Small,  mFIREB },
  { Shp::Firespray,        "Firespray-class Patrol Craft",        "Firespray",      "FPC",  BSz::Medium, mFIRES },
  { Shp::G1A,              "G-1A Starfighter",                    "G-1A",           "G1A",  BSz::Medium, mG1ASF },
  { Shp::Gozanti,          "Gozanti-class Cruiser",               "Gozanti",        "GOZ",  BSz::Huge,   mGOZAN },
  { Shp::GR75,             "GR-75 Medium Transport",              "GR-75",          "G75",  BSz::Huge,   mGR75M },
  { Shp::HMP,              "HMP Droid Gunship",                   "HMP",            "HMP",  BSz::Small,  mHMPDG },
  { Shp::HWK290,           "HWK-290 Light Freighter",             "HWK-290",        "HK",   BSz::Small,  mHWK29 },
  { Shp::Hyena,            "Hyena-class Droid Bomber",            "Hyena-class",    "HDB",  BSz::Small,  mHYENA },
  { Shp::JM5K,             "JumpMaster 5000",                     "JumpMaster 5k",  "JM5",  BSz::Large,  mJUM5K },
  { Shp::Kihraxz,          "Kihraxz Fighter",                     "Kihraxz",        "KXZ",  BSz::Small,  mKIHRA },
  { Shp::LAAT,             "LAAT/i Gunship",                      "LAAT/i",         "LAT",  BSz::Medium, mLAATG },
  { Shp::Lambda,           "Lambda-class T-4a Shuttle",           "Lambda-class",   "LS",   BSz::Large,  mLAMBD },
  { Shp::LancerClass,      "Lancer-class Pursuit Craft",          "Lancer-class",   "LPC",  BSz::Large,  mLACPC },
  { Shp::M12LKimogila,     "M12-L Kimogila Fighter",              "Kimogila",       "M12",  BSz::Medium, mM12LK },
  { Shp::M3A,              "M3-A Interceptor",                    "M3-A",           "M3A",  BSz::Small,  mM3AIN },
  { Shp::MG100,            "MG-100 StarFortress",                 "MG-100",         "MG1",  BSz::Large,  mMG100 },
  { Shp::ModifiedTIEln,    "Modified TIE/ln Fighter",             "MG TIE",         "Tmg",  BSz::Small,  mMGTIE },
  { Shp::ModYT1300,        "Modified YT-1300 Light Freighter",    "Mod YT-1300",    "YT13", BSz::Large,  mMYT13 },
  { Shp::N1,               "Naboo Royal N-1 Starfighter",         "N-1",            "N1",   BSz::Small,  mNABN1 },
  { Shp::Nantex,           "Nantex-class Starfighter",            "Nantex-class",   "NTX",  BSz::Small,  mNANTX },
  { Shp::NimbusClass,      "Nimbus-class V-wing",                 "V-wing",         "A3N",  BSz::Small,  mNIMBV },
  { Shp::Quadjumper,       "Quadrijet Transfer Spacetug",         "Quadjumper",     "QUA",  BSz::Small,  mQUADJ },
  { Shp::Raider,           "Raider-class Corvette",               "Raider",         "RDR",  BSz::Huge,   mRAIDR },
  { Shp::ResTransport,     "Resistance Transport",                "Res Xport",      "RT",   BSz::Small,  mRTRAN },
  { Shp::ResTransportPod,  "Resistance Transport Pod",            "Res Xport Pod",  "RTP",  BSz::Small,  mRTPOD },
  { Shp::RZ1AWing,         "RZ-1 A-wing",                         "RZ-1 A-wing",    "AW",   BSz::Small,  mRZ1AW },
  { Shp::RZ2AWing,         "RZ-2 A-wing",                         "RZ-2 A-wing",    "RZ2",  BSz::Small,  mRZ2AW },
  { Shp::ScaYT1300,        "Scavenged YT-1300",                   "Sca YT-1300",    "sYT",  BSz::Large,  mSYT13 },
  { Shp::Scurrg,           "Scurrg H-6 bomber",                   "Scurrg",         "SRG",  BSz::Medium, mSCURR },
  { Shp::Sheathipede,      "Sheathipede-class Shuttle",           "Sheathipede",    "ShS",  BSz::Small,  mSHEAT },
  { Shp::SithInf,          "Sith Infiltrator",                    "Sith Inf",       "SIn",  BSz::Large,  mSITHI },
  { Shp::StarViper,        "StarViper-class Attack Platform",     "StarViper",      "SV",   BSz::Small,  mSTARV },
  { Shp::Syliure,          "Syliure-class Hyperspace Ring",       "Hype Ring",      "ScH",  BSz::Small,  mSYLIU },
  { Shp::T65XWing,         "T-65 X-wing",                         "T-65 X-wing",    "T65",  BSz::Small,  mT65XW },
  { Shp::T70XWing,         "T-70 X-wing",                         "T-70 X-wing",    "T70",  BSz::Small,  mT70XW },
  { Shp::TIEAdvV1,         "TIE Advanced v1",                     "TIE Adv v1",     "TAv",  BSz::Small,  mTIEAv },
  { Shp::TIEAdvX1,         "TIE Advanced x1",                     "TIE Adv x1",     "TAx",  BSz::Small,  mTIEAx },
  { Shp::TIEagAggressor,   "TIE/ag Aggressor",                    "TIE Aggressor",  "TAg",  BSz::Small,  mTIEag },
  { Shp::TIEbaInterceptor, "TIE/ba Interceptor",                  "TIE/ba Int",     "VT",   BSz::Small,  mTIEba },
  { Shp::TIEcaPunisher,    "TIE/ca Punisher",                     "TIE Punisher",   "TPu",  BSz::Medium, mTIEca },
  { Shp::TIEdDefender,     "TIE/D Defender",                      "TIE Defender",   "TD",   BSz::Small,  mTIEdD },
  { Shp::TIEfoFighter,     "TIE/fo Fighter",                      "TIE/fo Fighter", "Tfo",  BSz::Small,  mTIEfo },
  { Shp::TIEinInterceptor, "TIE/in Interceptor",                  "TIE/in Int",     "TI",   BSz::Small,  mTIEin },
  { Shp::TIElnFighter,     "TIE/ln Fighter",                      "TIE Fighter",    "TF",   BSz::Small,  mTIEln },
  { Shp::TIEphPhantom,     "TIE/ph Phantom",                      "TIE Phantom",    "TPh",  BSz::Small,  mTIEph },
  { Shp::TIErbHeavy,       "TIE/rb Heavy",                        "TIE/rb Heavy",   "Trb",  BSz::Medium, mTIErb },
  { Shp::TIEReaper,        "TIE Reaper",                          "TIE Reaper",     "TR",   BSz::Medium, mTIERe },
  { Shp::TIEsaBomber,      "TIE/sa Bomber",                       "TIE Bomber",     "TB",   BSz::Small,  mTIEsa },
  { Shp::TIEseBomber,      "TIE/se Bomber",                       "TIE/se Bomber",  "Tse",  BSz::Small,  mTIEse },
  { Shp::TIEsfFighter,     "TIE/sf Fighter",                      "TIE/sf Fighter", "Tsf",  BSz::Small,  mTIEsf },
  { Shp::TIEskStriker,     "TIE/sk Striker",                      "TIE Striker",    "TS",   BSz::Small,  mTIEsk },
  { Shp::TIEvnSilencer,    "TIE/vn Silencer",                     "TIE Silencer",   "Tvn",  BSz::Small,  mTIEvn },
  { Shp::TIEwiWhisperMod,  "TIE/wi Whisper Modified Interceptor", "TIE Whisper",    "Twi",  BSz::Small,  mTIEwi },
  { Shp::Trident,          "Trident-class Assault Ship",          "Trident-class",  "TAS",  BSz::Huge,   mTRIDE },
  { Shp::Upsilon,          "Upsilon-class command shuttle",       "Upsilon-class",  "Ups",  BSz::Large,  mUPSIL },
  { Shp::UWing,            "UT-60D U-wing",                       "U-Wing",         "UE",   BSz::Medium, mUWING },
  { Shp::V19,              "V-19 Torrent Starfighter",            "V-19 Torrent",   "V19*", BSz::Small,  mV19TO },
  { Shp::VCX100,           "VCX-100 Light Freighter",             "VCX-100",        "VCX",  BSz::Large,  mVCX10 },
  { Shp::VT49,             "VT-49 Decimator",                     "Decimator",      "Dec",  BSz::Large,  mVT49D },
  { Shp::Vulture,          "Vulture-class Droid Fighter",         "Vulture-class",  "VDF",  BSz::Small,  mVULTU },
  { Shp::Xi,               "Xi-class Light Shuttle",              "Xi-Class",       "Xi",   BSz::Medium, mXICLA },
  { Shp::YT2400,           "YT-2400 Light Freighter",             "YT-2400",        "YT24", BSz::Large,  mYT240 },
  { Shp::YV666,            "YV-666 Light Freighter",              "YV-666",         "YV6",  BSz::Large,  mYV666 },
  { Shp::Z95,              "Z-95-AF4 Headhunter",                 "Z-95",           "Z95",  BSz::Small,  mZ95HH },
};

namespace ShipGroup {
  const std::vector<Shp> AWING = {Shp::RZ1AWing,Shp::RZ2AWing};
  const std::vector<Shp> TIE   = {Shp::TIEAdvV1,Shp::TIEAdvX1,Shp::TIEagAggressor,Shp::TIEbaInterceptor,Shp::TIEcaPunisher,Shp::TIEdDefender,Shp::TIEfoFighter,Shp::TIEinInterceptor,Shp::TIElnFighter,Shp::TIEphPhantom,Shp::TIErbHeavy,Shp::TIEReaper,Shp::TIEsaBomber,Shp::TIEsfFighter,Shp::TIEskStriker,Shp::TIEvnSilencer};
  const std::vector<Shp> XWING = {Shp::T65XWing,Shp::T70XWing};
  const std::vector<Shp> YWING = {Shp::BTLA4YWing,Shp::BTLBYWing};
}


}
