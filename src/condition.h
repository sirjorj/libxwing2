#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

enum class Cnd {
  CompIntel,
  Decoyed,
  FearfulPrey,
  Hunted,
  ISYTDS,
  ITR,
  LstnDev,
  MercilessPur,
  OptProt,
  Rattled,
  SupFire,
  YBMB,
  YSTM
};

class ConditionNotFound : public std::runtime_error {
 public:
  ConditionNotFound(Cnd cnd);
  ConditionNotFound(std::string cndstr);
};

class Condition {
 public:
  static Condition GetCondition(Cnd cnd);
  static Condition GetCondition(std::string cndstr);
  static std::vector<Cnd> GetAllCnds();

  Cnd         GetCnd()       const;
  std::string GetCndStr()    const;
  uint8_t     GetLimited()   const;
  std::string GetName()      const;
  std::string GetShortName() const;
  std::string GetText()      const;
  bool        IsUnreleased() const;

 private:
  Cnd         cnd;
  uint8_t     limited;
  std::string name;
  std::string nameShort;
  std::string text;

  static std::vector<Condition> conditions;

  Condition(Cnd         c,
	    uint8_t     l,
	    std::string n,
	    std::string s,
	    std::string txt);
};

}
