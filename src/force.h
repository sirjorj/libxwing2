#pragma once
#include "chargeable.h"
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

enum class FAf {
  None  = 0,
  Dark  = 1,
  Light = 2
};

FAf operator|(FAf a, FAf b);
FAf operator&(FAf a, FAf b);

class ForceAffiliationNotFound : public std::runtime_error {
 public:
  ForceAffiliationNotFound(FAf);
};

class ForceAffiliation {
 public:
  static ForceAffiliation GetForceAffiliation(FAf f);
  FAf         GetFAf()  const;
  std::string GetName() const;

 private:
  FAf faf;
  std::string name;

  static std::vector<ForceAffiliation> forceAffiliations;

  ForceAffiliation(FAf         f,
		   std::string n);
};

class Force : public Chargeable {
 public:
   Force(int8_t cap, int8_t recur, FAf af);
   FAf GetFAf() const;
   ForceAffiliation GetForceAffiliation() const;

 private:
   FAf faf;

};

}
