#pragma once
#include "pilot.h"
#include <optional>
#include <string>
#include <vector>

namespace libxwing2 {

class Squad {
 public:
  Squad();

  void                   SetName(std::string n);
  std::string            GetName() const;
  void                   SetDescription(std::string d);
  std::string            GetDescription() const;
  void                   SetFac(Fac f);
  std::optional<Fac>     GetFac();
  void                   AddPilot(Pilot p);
  std::vector<Pilot>&    GetPilots();
  std::optional<int16_t> GetCost(PtL ptl=PtL::Current);

  std::vector<std::string> Validate();
  
 private:
  std::string name;
  std::string description;
  std::optional<Fac> fac;
  std::vector<Pilot> pilots;
};

}
