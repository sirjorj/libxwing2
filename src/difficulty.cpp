#include "difficulty.h"

namespace libxwing2 {

// difficulty
std::vector<Difficulty> Difficulty::difficulties = {
  { Dif::Blue,   "Blue" },
  { Dif::White,  "White" },
  { Dif::Red,    "Red" },
  { Dif::Purple, "Purple" },
  { Dif::All,    "All" },
};

Dif& operator++(Dif& d) {
  switch(d) {
  case Dif::Blue:   d = Dif::White;  break;
  case Dif::White:  d = Dif::Red;    break;
  case Dif::Red:    d = Dif::Red;    break;
  case Dif::Purple: d = Dif::Purple; break;
  case Dif::All:    d = Dif::All;    break;
  }
  return d;
}

Dif& operator--(Dif& d) {
  switch(d) {
  case Dif::Blue:   d = Dif::Blue;   break;
  case Dif::White:  d = Dif::Blue;   break;
  case Dif::Red:    d = Dif::White;  break;
  case Dif::Purple: d = Dif::Purple; break;
  case Dif::All:    d = Dif::All;    break;
  }
  return d;
}

Dif operator++(Dif& d, int) {
  Dif r = d;
  ++d;
  return r;
}

Dif operator--(Dif& d,int) {
  Dif r = d;
  --d;
  return r;
}
  
DifficultyNotFound::DifficultyNotFound(Dif d) : runtime_error("Difficulty not found (enum " + std::to_string((int)d) + ")") { }

Difficulty Difficulty::GetDifficulty(Dif dif) {
  for(const Difficulty& difficulty : Difficulty::difficulties) {
    if(difficulty.GetDif() == dif) {
      return difficulty;
    }
  }
  throw DifficultyNotFound(dif);
}

std::vector<Dif> Difficulty::GetAllDifs() {
  std::vector<Dif> ret;
  for(const Difficulty& difficulty : Difficulty::difficulties) {
    ret.push_back(difficulty.GetDif());
  }
  return ret;
}
  
Dif         Difficulty::GetDif()  const { return this->dif; }
std::string Difficulty::GetName() const { return this->name; }

Difficulty::Difficulty(Dif         d,
		       std::string n)
  : dif(d), name(n) { }

}
