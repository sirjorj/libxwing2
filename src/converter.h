#pragma once
#include "condition.h"
#include "plt.h"
#include "ship.h"
#include "squad.h"
#include "upg.h"
#include <stdexcept>

namespace libxwing2 { namespace converter {

std::vector<Fac> GetMissingFactions();
std::vector<Shp> GetMissingShips();
std::vector<Plt> GetMissingPilots();
std::vector<UpT> GetMissingUpgradeTypes();
std::vector<Upg> GetMissingUpgrades();
//std::vector<Cnd> GetMissingConditions();

namespace ffg {
  class PilotNotFound : public std::runtime_error {
  public:
    PilotNotFound(Plt     p);
    PilotNotFound(int16_t f);
  };
  int16_t GetPilot(Plt     p);
  Plt     GetPilot(int16_t f);
  std::string GetArt(Plt p);
  std::string GetCard(Plt p);

  class UpgradeNotFound : public std::runtime_error {
  public:
    UpgradeNotFound(Upg     u);
    UpgradeNotFound(int16_t f);
  };
  int16_t GetUpgrade(Upg     u, uint16_t side=0);
  Upg     GetUpgrade(int16_t f, uint16_t side=0);
  std::string GetArt(Upg u, uint16_t side=0);
  std::string GetCard(Upg u, uint16_t side=0);
}



namespace xws {
  class FactionNotFound : public std::runtime_error {
  public:
    FactionNotFound(Fac         f);
    FactionNotFound(std::string x);
  };
  std::string GetFaction(Fac         f);
  Fac         GetFaction(std::string x);

  class ShipNotFound : public std::runtime_error {
  public:
    ShipNotFound(Shp         s);
    ShipNotFound(std::string x);
  };
  std::string GetShip(Shp         s);
  Shp         GetShip(std::string x);

  class PilotNotFound : public std::runtime_error {
  public:
    PilotNotFound(Plt         s);
    PilotNotFound(std::string x);
  };
  std::string GetPilot(Plt         p);
  Plt         GetPilot(std::string x);

  class UpgradeTypeNotFound : public std::runtime_error {
  public:
    UpgradeTypeNotFound(UpT         u);
    UpgradeTypeNotFound(std::string x);
  };
  std::string GetUpgradeType(UpT         u);
  UpT         GetUpgradeType(std::string x);

  class UpgradeNotFound : public std::runtime_error {
  public:
    UpgradeNotFound(Upg         u);
    UpgradeNotFound(std::string x);
  };
  std::string GetUpgrade(Upg         u);
  Upg         GetUpgrade(std::string x);

  class ConditionNotFound : public std::runtime_error {
  public:
    ConditionNotFound(Cnd         c);
    ConditionNotFound(std::string x);
  };
  std::string GetCondition(Cnd         c);
  Cnd         GetCondition(std::string x);

  class SquadNotFound : public std::runtime_error {
  public:
    SquadNotFound(std::string f);
  };
  Squad       GetSquad(std::istream &x);
  Squad       GetSquad(std::string xwsFile);
  std::string GetSquad(Squad s);
}

namespace yasb {
  // https://github.com/raithos/xwing/blob/master/coffeescripts/cards-common.coffee
  class FactionNotFound : public std::runtime_error {
  public:
    FactionNotFound(Fac         f);
    FactionNotFound(std::string x);
  };
  std::string GetFaction(Fac         f);
  Fac         GetFaction(std::string x);

  class PilotNotFound : public std::runtime_error {
  public:
    PilotNotFound(Plt     s);
    PilotNotFound(int32_t i);
  };
  int16_t GetPilot(Plt     p);
  Plt     GetPilot(int32_t i);

  class UpgradeNotFound : public std::runtime_error {
  public:
    UpgradeNotFound(Upg      u);
    UpgradeNotFound(uint16_t i);
  };
  uint16_t GetUpgrade(Upg      u);
  Upg      GetUpgrade(uint16_t i);

  class InvalidUrl : public std::runtime_error {
  public:
    InvalidUrl(std::string f);
  };
  Squad       GetSquad(std::string url);
  std::string GetSquad(Squad s);
}

}}
