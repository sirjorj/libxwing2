#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

enum class Arc {
  None,
  Front,
  Rear,
  Left,
  Right,
  Bullseye,
  FullFront,
  FullRear,
  SingleTurret,
  DoubleTurret,
};

class FiringArcNotFound : public std::runtime_error {
 public:
  FiringArcNotFound(Arc a);
};

class FiringArc {
 public:
  static FiringArc GetFiringArc(Arc a);
  static std::vector<Arc> GetAllArcs();
  Arc         GetArc()  const;
  std::string GetName() const;

 private:
  Arc arc;
  std::string name;

  static std::vector<FiringArc> firingArcs;

  FiringArc(Arc         a,
            std::string n);
};


// primary attack
struct PriAttack {
  Arc     arc;
  uint8_t value;
};
typedef std::vector<PriAttack> PriAttacks;


// secondary attack
struct SecAttack {
  Arc     arc;
  uint8_t value;
  bool    ordnance;
  uint8_t minRange;
  uint8_t maxRange;
};
typedef std::vector<SecAttack> SecAttacks;
 
}
