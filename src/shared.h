#pragma once
#include <algorithm>
#include <functional>
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// misc helpers
std::string ToLower(std::string s);

}
