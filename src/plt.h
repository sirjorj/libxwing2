#pragma once

namespace libxwing2 {

enum class Plt {
  // *** First Edition Wave 13 ***
  // SWX74 - Saw's Renegades
  KSperado, LTenza, ETwoTubes, CAZealot, SGerrera, MYarro, BTwoTubes, PartRenegade,
  // SWX75 - TIE Reaper Expansion Pack
  MajVermeil, CaptFeroph, Vizier, ScarifBsPlt,
  // *** Wave 1 ***
  // SWZ01 - Core Set
  LSkywalker, JPorkins, RedSqVet, BlueSqEsc,
  IVersio, BlackSqAce_Imp, VRudor, NightBeast, ObsidianSqPlt, AcademyPlt,
  // SWZ04 - Lando's Millennium Falcon
  HSoloScum, LCalrissianScum, L337C, FreighterCapt, LCalrissian_EC, ORPioneer, L337E, AutoPltDrone,
  // SWZ06 - Rebel Conversion Kit
  NWexley_ARC, GDreis_ARC, SBey_ARC, Ibtisam,                     // ARC-170
  BStramm, TNumb, BladeSqVet, BlueSqPlt,                          // A/SF-01 B-wing
  HSyndulla_AS, EBridger_AS, SWren_AS, ZOrrelios_AS,              // Attack Shuttle
  Wullffwarro, Lowhhrick, KashyyykDef,                            // Auzituck Gunship
  NWexley_YW, DVander, HSalm, EVerlaine, GoldSqVet, GraySqBomber, // BTL-A4 Y-wing
  MDoni, ETuketu, WardenSqPlt,                                    // BTL-S8 K-wing
  CHorn, GDarklighter, RogueSqEsc, KnaveSqEsc,                    // E-wing
  JOrs, RGarnet, KKatarn, RebelScout,                             // HWK290
  HSoloReb, LCalrissianReb, Chewbacca, OuterRimSmug,              // Modified YT-1300
  JFarrell, ACrynyd, GreenSqPlt, PhoenixSqPlt,                    // RZ1Awing
  FRau_Sh, EBridger_Sh, ZOrrelios_Sh, AP5,                        // Sheathipede
  WAntilles_T65, TKyrell, GDreis_XW, BDarklighter,                // T-65 X-wing
  EBridger_TF, SWren_TF, ZOrrelios_TF, CaptRex,                   // TIE/ln Fighter
  BRook, CAndor, HTobber, BlueSqScout,                            // UT-60D U-wing
  HSyndulla_VCX, KJarrus_VCX, Chopper, LothalRebel,               // VCX-100
  DRendar, Leebo, WildSpaceFrin,                                  // YT-2400
  ACracken, LtBlount, TalaSqPlt, BanditSqPlt,                     // Z-95
  // SWZ07 - Galactic Empire Conversion Kit
  MajVynder, LtKarsabi, RhoSqPlt, NuSqPlt,                           // Alpha-class Star Wing
  CaptKagi, ColJendon, LtSai, OmicronGrpPlt,                         // Lambda
  GrandInq, SeventhSister, BaronOfEmp, Inquisitor,                   // TIE Advanced v1
  DVader_x1, MStele, VFoslo, ZStrom, StormSqAce, TempestSqPlt,       // TIE Advanced x1
  LtKestal, OnyxSqScout, DblEdge, SienSpecialist,                    // TIE/ag Aggressor
  Redline, Deathrain, CutlassSqPlt,                                  // TIE/ca Punisher
  RBrath, ColVessery, CountessRyad, OnyxSqAce, DeltaSqPlt,           // TIE/D Defender
  SFel, SaberSqAce, TPhennir,  AlphaSqPlt,                           // TIE/in Interceptor
  Howlrunner, MMithel, SSkutu, DMeeko, GHask_TIEln, SMarana, Wampa,  // TIE/ln Fighter
  Whisper, Echo, SigmaSqAce, ImdaarTestPlt,                          // TIE/ph Phantom
  TBren, CaptJonus, MajRhymer, GammaSqAce, Deathfire, ScimitarSqPlt, // TIE/sa Bomber
  Duchess, Countdown, PureSabacc, BlackSqScout, PlanetarySent,       // TIE/sk Striker
  RAdmChiraneau, CaptOicunn, PatrolLdr,                              // VT-49 Decimator
  // SWZ08 - Scum and Villainy Conversion Kit
  IG88A, IG88B, IG88C, IG88D,                                                    // Aggressor
  Kavil, DRenthal, HiredGun, CrymorahGoon,                                       // BTL-A4 Y-wing
  FRau_FF, OTeroch, JRekkoff, KSolus, SkullSqPlt, ZealousRecruit,                // Fang Fighter
  BFett, EAzzameen, KScarlet, KFrost, KTrelix, BountyHunter,                     // Firespray
  FourLOM, Zuckuss, GandFindsman,                                                // G1A
  DBonearm, PGodalhi, TMux, SpiceRunner,                                         // HWK290
  Dengar, TTrevura, Manaroo, ContractedSc,                                       // JM5K
  TCobra, Graz, VHel, BlackSunAce, CaptJostero, CartMarauder,                    // Kihraxz
  KOnyo, AVentress, SWren_LC, ShadowportHun,                                     // Lancer-Class
  TKulda, DOberos_KG, CartExecution,                                             // M12-L Kimogila
  Serissu, GRed, LAshera, QJast, TansariiPtVet, Inaldra, CartelSpacer, SBounder, // M3A
  ConstZuvio, SPlank, UPlutt, JGunrunner,                                        // Quadjumper
  CaptNym, SSixxa, LRevenant,                                                    // Scurrg
  Guri, DOberos_SV, PXizor, BSAssassin, BSEnforcer,                              // StarViper
  Bossk_YV, MEval, LRazzi, TSlaver,                                              // YV666
  NSuhlak, BSSoldier, KLeeachos, BinayrePirate, NashtahPup,                      // Z-95
  // *** Wave 2 ***
  // SWZ18 - First Order Conversion Kit
  Midnight, CmdrMalarus, Scorch_FO, Static, Longshot, OmegaSqAce, Muse, TN3465, ZetaSqPlt, EpsilonSqCadet, LtRivas, Null,   // TIE/fo Fighter
  Quickdraw, Backdraft, OmegaSqExpert, ZetaSqSurvivor,                                                                      // TIE/sf
  Blackout, KRen_VN, FOTestPlt, Recoil, Avenger, SJEngineer,                                                                // TIE/vn Silencer
  CaptCardinal, MajStridan, LtTavson, LtDormitz, StarkillerBsPlt, PetOffThanisson,                                          // Upsilon
  // SWZ19 - Resistance Conversion Kit
  FDallow, BTeene, EKappehl, Vennie, Cat, CobaltSqBomber,                                                                   // MG-100
  HSoloRes, Rey, ChewieRes, ResSymp,                                                                                        // Scavenged YT1300
  PDameron, EAsty, NNunb, BlackSqAce_Res, KKun, TWexley, JPava, JSeastriker, RedSqExpert, LtBastian, BlueSqRookie, JTubbs,  // T-70 X-wing
  // SWZ22 - RZ-2 A-wing
  LLampar, TLintra, GSonnel, GreenSqExp, ZBangel, BlueSqRecruit,
  // SWZ23 - Mining Guild TIE
  ForemanProach, Ahhav, CaptSeevor, OverseerYushyn, MGSurveyor, MGSentry,
  // SWZ29 - Servants of Strife
  GenGrievous, SkakoanAce, WTambor, CaptSear, FOAutopilot,     // Belbullab-22
  DFS081, PrecHunter, SepDrone, HaorChallProto, TradeFedDrone, // Vulture-class
  // SWZ30 - Sith Infiltrator
  DMaul, CDooku, O66, DarkCourier,
  // SWZ31 - Vulture-class
  DFS311,
  // SWZ32 - Guardians of the Republic
  OKenobi, PKoon, MWindu, STiin, JediKnight,                                 // Delta-7
  OddBall_V19, Kickback, Axe, BlueSqProtector, Swoop, GoldSqTrooper, Tucker, // V19 Torrent (Republic)
  // SWZ33 - ARC-170
  OddBall_ARC, Wolffe, Jag, Sinker, SquadSevenVet, OneOhFourthBP,
  // SWZ34 - Delta-7 Aethersprite
  ASkywalker_D7, BOffee, LUnduli, ATano_D7,
  // SWZ40 - N-1
  ROlie, ASkywalker_N1, PAmidala, DEllberger, BravoFlightOff, NabooHandmaiden,
  // SWZ41 - Hyena
  DBS404, DBS32C, BombardmentDrn, SepBomber, BaktoidProto, TUBomber,
  // SWZ45 - Resistance Transport
  CNell, PNGoode, NChavdri, LogDivPlt, // Resistance Transport
  BB8, RTico, Finn, VMoradi,           // Resistance Transport Pod
  // SWZ47 - Nantex-class
  SFac, BKret, Chertek, PetArenaAce, StalgasinHG, Gorgol,
  // SWZ48 - BTL-B Y-wing
  ASkywalker_BTLB, OddBall_BTLB, Matchstick, Broadside, ShadowSqVet, Goji, R2D2, RedSqBomber,
  // SWZ53 - Huge Ship Conversion Kit
  SynSmugglers, SepPrivateers, // C-ROC (Scum, Separatist)
  AlderaanGuard, RepJudiciary, // CR90 (Rebel, Republic)
  OuterRimGar, FOSymp,         // Gozanti-class Cruiser (Imperial, First Order)
  EchoBaseEvac, NewRepVol,     // GR-75 (Rebel, Resistance)
  OuterRimPat, FOCollab,       // Raider (Imperial, First Order)
  // SWZ58 - Hound's Tooth
  Bossk_Z95,
  // SWZ62 - Major Vonreg's TIE
  MajVonreg, Holo, Ember, FOProvocateur,
  // SWZ63 - Fireball
  JYeager, KXiono, ColStationMech, R1J5,
  // SWZ66 - Hotshots and Aces
  GMoonsong,            // A/SF-01 B-wing (Rebel)
  NLumb,                // JM5K (Scum)
  G4RGORVM,             // M3A (Scum)
  PTico,                // MG-100 (Resistance)
  LOrgana,              // Modified YT-1300 (Rebel)
  FifthBrother,         // TIE Advanced v1 (Imperial)
  Vagabond,             // TIE/sk Striker (Imperial)
  K2SO,                 // UT-60D U-wing (Rebel)
  AKallus,              // VCX100 (Rebel)
  MKee,                 // VT-49 Decimator (Imperial)
  ZTlo, RBlario,        // RZ-2 Awing (Resistance)
  LtLeHuse, CaptPhasma, // TIE/sf (First Order)
  Rush,                 // TIE/vn Silencer (FirstOrder)
  // SWZ67 - TIE/rb Heavy
  Rampage, LDree, OnyxSqSentry, CaridaAcaCadet,
  // SWZ68 - Heralds of Hope
  PDameron_RC, CThrenalli_T70, TWexley_BT, NChireen, // T-70 X-wing
  SVanik, WTyce, SJavos, MCobben,                    // RZ2Awing
  // SWZ69 - Xi-class
  CmdrMalarus_Xi, GHask_Xi, AgentTerex, FOCourier,
  // SWZ70 - LAAT/i
  Hawk, Warthog, Hound, TwoOneTwoBatPlt,
  // SWZ71 - HMP
  OnderonOpp, DGS286, SepPredator, GeonosianProto, DGS047, BaktoidDrone,
  // SWZ79 - Eta-2 Actis
  ASkywalker_Eta2, OKenobi_Eta2, ASecura, STi, JediGen, Yoda,
  TransGalMegCL, // Syliure-class Hyperspace Ring
  // SWZ80 - Nimbus-class V-wing
  Contrail, OddBall_Vw, Klick, WTarkin, ShadowSqEsc, LoyalistVol,
  // SWZ81 - Droid Tri-Fighter
  PhlacArpProto, DIST81, FearsomePred, DIS347, SepInt, ColInt,
  // SWZ82 - Firespray (Separatist)
  JFett, ZWesell, BFett_Sep, SepRacketeer,
  // SWZ83 - Phoenix Cell
  HSyndulla_ASF01, NPollard,                                              // A/SF-01 B-wing
  HSyndulla_RZ1, ATano_RZ1, SBey_RZ1, WAntilles_RZ1, DKlivian, SWren_RZ1, // RZ-1 Awing
  // SWZ84 - Skystrike Academy
  DVader_d, VSkerris_d, CaptDobbs,                                    // TIE/D Defender
  CRee, VSkerris_TIEin, CmdtGoran, GHask_TIEin, LtLorrir, NWindrider, // TIE/in Interceptor
  // SWZ85 - Fugitives and Collaborators
  Tapusk, GKey, KJarrus_HWK,                            // HWK290
  LKai, AHadrassian, AmaxWarrior, Padric, JinataSecOff, // BTL-A4 Y-wing
  // SWZ86 - BTA-NR2 -Y-wing
  ZBliss, TNasz, WTeshlo, LFossang, NewRepPatrol, SZaro, AAckbar, CThrenalli_BTA, KijimiSR, CKapellim,
  // SWZ87 - Fury of the First Order
  KRen_WI, Wrath, Nightfall, SevOhNineLegAce, Whirlwind, RedFuryZealot, // TIE/wi Whisper Modified Interceptor
  Breach, Scorch_SE, Dread, FOCadet, Grudge, SJTestPlt,                 // TIE/se Bomber (First Order)
  // SWZ88 - Trident-class
  LawlessPirates, ColicoidDest,
};

}
