#pragma once
#include "shared.h"
#include "difficulty.h"
#include <functional>
#include <list>
#include <stdexcept>
#include <vector>
#include <stdint.h>

namespace libxwing2 {


// Actions
enum class Act : uint32_t {
  //None       = 0x0000,
  BarrelRoll = 0x0001,
  Boost      = 0x0002,
  Calculate  = 0x0004,
  Cloak      = 0x0008,
  Coordinate = 0x0010,
  Evade      = 0x0020,
  Focus      = 0x0040,
  Jam        = 0x0080,
  Lock       = 0x0100,
  Reinforce  = 0x0200,
  Reload     = 0x0400,
  RotateArc  = 0x0800,
  SLAM       = 0x1000,
};

class ActionNotFound : public std::runtime_error {
 public:
  ActionNotFound(Act a);
};

class Action {
 public:
  static Action GetAction(Act s);
  static std::vector<Act> GetAllActs();
  Act         GetAct()       const;
  std::string GetName()      const;
  std::string GetShortName() const;

 private:
  Act act;
  std::string name;
  std::string shortName;

  static std::vector<Action> actions;

  Action(Act         a,
         std::string n,
         std::string s);
};

struct SAct { // skilled action
  Dif difficulty;
  Act action;
};

bool operator ==(const SAct a, const SAct b);


typedef std::vector<std::list<SAct>> ActionBar;

}
