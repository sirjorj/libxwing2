#include "dice.h"
#include <algorithm>

namespace libxwing2 {

// attack dic
AttackDieNotFound::AttackDieNotFound(AtD a)         : runtime_error("Attack Die not found (enum " + std::to_string((int)a) + ")") { }
AttackDieNotFound::AttackDieNotFound(std::string t) : runtime_error("Attack Die not found '" + t + "'") { }

std::vector<AttackDie> AttackDie::attackDie = { 
  {AtD::Blank, "Blank",        "Blank", "{BLANK}" },
  {AtD::Focus, "Focus",        "Focus", "{FOCUS}" },
  {AtD::Hit,   "Hit",          "Hit",   "{HIT}"   },
  {AtD::Crit,  "Critical Hit", "Crit",  "{CRIT}"  },
};

AttackDie AttackDie::GetAttackDie(AtD atd) {
  for(const AttackDie& die : AttackDie::attackDie) {
    if(die.GetAtD() == atd) {
      return die;
    }
  }
  throw AttackDieNotFound(atd);
}

AttackDie AttackDie::GetAttackDie(std::string atdstr) {
  for(const AttackDie& die : AttackDie::attackDie) {
    if(die.GetText() == atdstr) {
      return die;
    }
  }
  throw AttackDieNotFound(atdstr);
}

AttackDie::AttackDie(AtD a,
		     std::string n,
		     std::string s,
		     std::string t)
  : atd(a), name(n), shortName(s), text(t) { }

AtD         AttackDie::GetAtD()       const { return this->atd; }
std::string AttackDie::GetName()      const { return this->name; }
std::string AttackDie::GetShortName() const { return this->shortName; }
std::string AttackDie::GetText()      const { return this->text; }

AttackDice::AttackDice() { }

AttackDice::AttackDice(std::vector<AtD> d)
  : dice(d) { }

void AttackDice::Add(AtD a) {
  this->dice.push_back(a);
}

std::vector<AttackDie> AttackDice::GetDice() const {
  std::vector<AttackDie> ret;
  for(AtD d : this->dice) {
    ret.push_back(AttackDie::GetAttackDie(d));
  }
  return ret;
}

std::vector<AttackDie> AttackDice::GetSortedDice() const {
  std::vector<AttackDie> ret;
  std::vector<AtD> sorted = this->dice;
  std::sort(sorted.begin(), sorted.end(), [](AtD i, AtD j){ return i>j; });
  for(AtD d : sorted) {
    ret.push_back(AttackDie::GetAttackDie(d));
  }
  return ret;
}

void AttackDice::Focus() {
  for(AtD &a : this->dice) {
    if(a == AtD::Focus) { a = AtD::Hit; }
  }
}



// defense dice
DefenseDieNotFound::DefenseDieNotFound(DeD d)         : runtime_error("Defense Die not found (enum " + std::to_string((int)d) + ")") { }
DefenseDieNotFound::DefenseDieNotFound(std::string t) : runtime_error("Defense Die not found '" + t + "'") { }

std::vector<DefenseDie> DefenseDie::defenseDie = { 
  {DeD::Blank, "Blank", "Blank", "{BLANK}" },
  {DeD::Focus, "Focus", "Focus", "{FOCUS}" },
  {DeD::Evade, "Evade", "Evade", "{EVADE}" },
};

DefenseDie DefenseDie::GetDefenseDie(DeD ded) {
  for(const DefenseDie& die : DefenseDie::defenseDie) {
    if(die.GetDeD() == ded) {
      return die;
    }
  }
  throw DefenseDieNotFound(ded);
}

DefenseDie DefenseDie::GetDefenseDie(std::string t) {
  for(const DefenseDie& d : DefenseDie::defenseDie) {
    if(d.GetText() == t) {
      return d;
    }
  }
  throw DefenseDieNotFound(t);
}

DefenseDie::DefenseDie(DeD         d,
                       std::string n,
                       std::string s,
                       std::string t)
  : ded(d), name(n), shortName(s), text(t) { }

DeD         DefenseDie::GetDeD()       const { return this->ded; }
std::string DefenseDie::GetName()      const { return this->name; }
std::string DefenseDie::GetShortName() const { return this->shortName; }
std::string DefenseDie::GetText()      const { return this->text; }

DefenseDice::DefenseDice() { }

DefenseDice::DefenseDice(std::vector<DeD> d)
  : dice(d) { }

void DefenseDice::Add(DeD a) {
  this->dice.push_back(a);
}

std::vector<DefenseDie> DefenseDice::GetDice() const {
  std::vector<DefenseDie> ret;
  for(DeD d : this->dice) {
    ret.push_back(DefenseDie::GetDefenseDie(d));
  }
  return ret;
}

std::vector<DefenseDie> DefenseDice::GetSortedDice() const {
  std::vector<DefenseDie> ret;
  std::vector<DeD> sorted = this->dice;
  std::sort(sorted.begin(), sorted.end(), [](DeD i, DeD j){ return i>j; });
  for(DeD d : sorted) {
    ret.push_back(DefenseDie::GetDefenseDie(d));
  }
  return ret;
}

void DefenseDice::Focus() {
  for(DeD d : this->dice) {
    if(d == DeD::Focus) { d = DeD::Evade; }
  }
}

}
