#include "xwi.h"
#include "./json/json.h"
#include "converter.h"
#include <algorithm>
#include <fstream>

namespace libxwing2 {

XWI::XWI() {
  this->Init();
}

XWI::XWI(std::istream& s) {
  this->Init();
  this->Load(s);
}

XWI::XWI(std::string f) {
  this->Init();
  std::ifstream is(f);
  if(!is.good()) { throw std::runtime_error("Error loading xwi file"); }
  this->Load(is);
  is.close();
}

std::string XWI::Serialize(bool shrink) const {
  std::string ret;
  if(shrink) {
    Json::Value xwi;
    {
      int c = 0;
      for(const std::pair<const Rel,int>& exp : this->expansions) {
	if(exp.second) {
	  xwi["expansions"][c]["sku"]   = Release::GetRelease(exp.first).GetSku();
	  xwi["expansions"][c]["count"] = exp.second;
	  c++;
	}
      }
    }
    {
      int c = 0;
      for(const std::pair<const Shp,int>& exp : this->ships) {
	if(exp.second) {
	  xwi["ships"][c]["ship"]  = converter::xws::GetShip(exp.first);
	  xwi["ships"][c]["count"] = exp.second;
	  c++;
	}
      }
    }
    {
      int c = 0;
      for(const std::pair<const Plt,int>& exp : this->pilots) {
	if(exp.second) {
	  xwi["pilots"][c]["pilot"] = converter::xws::GetPilot(exp.first);
	  xwi["pilots"][c]["count"] = exp.second;
	  c++;
	}
      }
    }
    {
      int c = 0;
      for(const std::pair<const Upg,int>& exp : this->upgrades) {
	if(exp.second) {
	  xwi["upgrades"][c]["upgrade"] = converter::xws::GetUpgrade(exp.first);
	  xwi["upgrades"][c]["count"]   = exp.second;
	}
      }
    }
    ret = Json::FastWriter().write(xwi);
  } else {
    // manually write this one
    std::stringstream ss;
    ss << "{" << std::endl;

    {
      ss << "  \"expansions\" : [" << std::endl;
      int max=0;
      std::vector<std::pair<std::string,int>> expansions;
      for(std::map<Rel,int>::const_iterator it=this->expansions.begin(); it!=this->expansions.end(); ++it) {
	std::string k = Release::GetRelease(it->first).GetSku();
	int v = it->second;
	int s = k.size();
	max = std::max(max,s);
	expansions.push_back({k,v});
      }
      for(int i=0; i<expansions.size(); i++) {
	std::string k = expansions[i].first;
	int v = expansions[i].second;
	ss << "    { \"sku\": \"" << k <<"\"," << std::string(max - k.size()+1, ' ') << "\"count\": " << std::to_string(v) << "}" << ((i < expansions.size()-1) ? "," : "") << std::endl;
      }
      ss << "  ]," << std::endl;
    }

    {
      ss << "  \"ships\" : [" << std::endl;
      int max=0;
      std::vector<std::pair<std::string,int>> ships;
      for(std::map<Shp,int>::const_iterator it=this->ships.begin(); it!=this->ships.end(); ++it) {
	std::string k = converter::xws::GetShip(it->first);
	int v = it->second;
	int s = k.size();
	max = std::max(max,s);
	ships.push_back({k,v});
      }
      for(int i=0; i<ships.size(); i++) {
	std::string k = ships[i].first;
	int v = ships[i].second;
	ss << "    { \"ship\": \"" << k <<"\"," << std::string(max - k.size()+1, ' ') << "\"count\": " << std::to_string(v) << "}" << ((i < ships.size()-1) ? "," : "") << std::endl;
      }
      ss << "  ]," << std::endl;
    }

    {
      ss << "  \"pilots\" : [" << std::endl;
      int max=0;
      std::vector<std::pair<std::string,int>> pilots;
      for(std::map<Plt,int>::const_iterator it=this->pilots.begin(); it!=this->pilots.end(); ++it) {
	std::string k = converter::xws::GetPilot(it->first);
	int v = it->second;
	int s = k.size();
	max = std::max(max,s);
	pilots.push_back({k,v});
      }
      for(int i=0; i<pilots.size(); i++) {
	std::string k = pilots[i].first;
	int v = pilots[i].second;
	ss << "    { \"pilot\": \"" << k <<"\"," << std::string(max - k.size()+1, ' ') << "\"count\": " << std::to_string(v) << "}" << ((i < pilots.size()-1) ? "," : "") << std::endl;
      }
      ss << "  ]," << std::endl;
    }

    {
      ss << "  \"upgrades\" : [" << std::endl;
      int max=0;
      std::vector<std::pair<std::string,int>> upgrades;
      for(std::map<Upg,int>::const_iterator it=this->upgrades.begin(); it!=this->upgrades.end(); ++it) {
	std::string k = converter::xws::GetUpgrade(it->first);
	int v = it->second;
	int s = k.size();
	max = std::max(max,s);
	upgrades.push_back({k,v});
      }
      for(int i=0; i<upgrades.size(); i++) {
	std::string k = upgrades[i].first;
	int v = upgrades[i].second;
	ss << "    { \"upgrade\": \"" << k <<"\"," << std::string(max - k.size()+1, ' ') << "\"count\": " << std::to_string(v) << "}" << ((i < upgrades.size()-1) ? "," : "") << std::endl;
      }
      ss << "  ]" << std::endl;
    }

    ss << "}" << std::endl;
    ret = ss.str();
  }
  return ret;
}

void XWI::Save(std::string f, bool shrink) const {
  std::ofstream o;
  o.open(f);
  o << this->Serialize(shrink);
  o.close();
}

void XWI::Set(Rel rel, int val) { this->expansions[rel] = val; }
void XWI::Set(Shp shp, int val) { this->ships[shp]      = val; }
void XWI::Set(Plt plt, int val) { this->pilots[plt]     = val; }
void XWI::Set(Upg upg, int val) { this->upgrades[upg]   = val; }

void XWI::Add(Rel rel, int val) { this->expansions[rel] += val; }
void XWI::Add(Shp shp, int val) { this->ships[shp]      += val; }
void XWI::Add(Plt plt, int val) { this->pilots[plt]     += val; }
void XWI::Add(Upg upg, int val) { this->upgrades[upg]   += val; }

int XWI::Get(Rel rel) const { return this->expansions.at(rel); }
int XWI::Get(Shp shp) const { return this->ships.at(shp); }
int XWI::Get(Plt plt) const { return this->pilots.at(plt); }
int XWI::Get(Upg upg) const { return this->upgrades.at(upg); }

bool XWI::IsEmpty() const {
  for(const std::pair<const Rel,int>& rel : this->expansions) {
    if(rel.second) { return false; }
  }
  for(const std::pair<const Shp,int>& shp : this->ships) {
    if(shp.second) { return false; }
  }
  for(const std::pair<const Plt,int>& plt : this->pilots) {
    if(plt.second) { return false; }
  }
  for(const std::pair<const Upg,int>& upg : this->upgrades) {
    if(upg.second) { return false; }
  }
  return true;  
}

XWI XWI::GetResolved() {
  XWI ret = *this;
  for(Rel rel : Release::GetAllRels()) {
    Release release = Release::GetRelease(rel);
    int rc = ret.Get(rel);
    for(int i=0; i<rc; i++) {
      // ships
      for(const RelShip &rs : release.GetShips()) {
	ret.Add(rs.ship,1);
      }
      // pilots
      for(const Plt &p : release.GetPilots()) {
	ret.Add(p,1);
      }
      // upgrades
      for(const Upg &u : release.GetUpgrades()) {
	ret.Add(u,1);
      }

    }
    ret.Set(rel,0);
  }
  return ret;
}

void XWI::Init() {
  for(Rel rel : Release::GetAllRels()) {
    this->expansions[rel] = 0;
  }
  for(Shp shp : Ship::GetAllShps()) {
    this->ships[shp] = 0;
  }
  for(Plt plt : Pilot::GetAllPlts()) {
    this->pilots[plt] = 0;
  }
  for(Upg upg : Upgrade::GetAllUpgs()) {
    this->upgrades[upg] = 0;
  }
}

void XWI::Load(std::istream& s) {
  if(s.peek() == EOF) {
    return;
  }
  try{
    Json::Value root;
    s >> root;
    for(const Json::Value& jv : root["expansions"]) {
      std::string sku = jv["sku"].asString();
      int count = jv["count"].asInt();
      Rel rel = Release::GetRelease(sku).GetRel();
      this->expansions[rel] = count;
    }
    for(const Json::Value& jv : root["ships"]) {
      std::string name = jv["ship"].asString();
      int count = jv["count"].asInt();
      Shp shp = Ship::GetShip(converter::xws::GetShip(name)).GetShp();
      this->ships[shp] = count;
    }
    for(const Json::Value& jv : root["pilots"]) {
      std::string name = jv["pilot"].asString();
      int count = jv["count"].asInt();
      Plt plt = Pilot::GetPilot(converter::xws::GetPilot(name)).GetPlt();
      this->pilots[plt] = count;
    }
    for(const Json::Value& jv : root["upgrades"]) {
      std::string name = jv["upgrade"].asString();
      int count = jv["count"].asInt();
      Upg upg = Upgrade::GetUpgrade(converter::xws::GetUpgrade(name)).GetUpg();
      this->upgrades[upg] = count;
    }
  }
  catch(Json::RuntimeError& err) {
    throw std::runtime_error(std::string("Json error: ") + err.what());
  }
}

}
