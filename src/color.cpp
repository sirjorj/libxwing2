#include "color.h"
#include <stdexcept>
namespace libxwing2 {

std::vector<Color> Color::colors = {
  // stats (from official squad builder)
  { Clr::Initiative,     "Initiative",    0xe7, 0x7e, 0x29 },
  { Clr::Cost,           "Cost",          0x41, 0xbe, 0xf0 },
  { Clr::Attack,         "Attack",         237,   54,   56 },
  { Clr::Agility,        "Agility",        106,  190,   70 },
  { Clr::Hull,           "Hull",           240,  229,   49 },
  { Clr::Shield,         "Shield",         130,  209,  225 },
  { Clr::Charge,         "Charge",         253,  191,   16 },
  { Clr::Force,          "Force",          196,  160,  202 },
  { Clr::Energy,         "Energy",         231,   21,  131 },

  // difficulty (from official squad builder)
  { Clr::Stress,         "Stress",         236,   31,   33 },
  { Clr::Neutral,        "Neutral",        255,  255,  255 },
  { Clr::Destress,       "Destress",      0x1e, 0x90, 0xff }, // this one is css 'dodger blue'

  // faction colors (fron product images on website)
  { Clr::Rebel,          "Rebel (Fore)",       203,   18,   14 },
  { Clr::RebelBg,        "Rebel (Back)",       179,  181,  173 },
  { Clr::Imperial,       "Imperial (Fore)",    214,  214,  221 },
  { Clr::ImperialBg,     "Imperial (Back)",     32,   78,  120 },
  { Clr::Scum,           "Scum (Fore)",        179,  181,  173 },
  { Clr::ScumBg,         "Scum (Back)",         37,   58,   33 },
  { Clr::Resistance,     "Resistance (Fore)",  216,  115,   37 },
  { Clr::ResistanceBg,   "Resistance (Back)",   64,   64,   64 },
  { Clr::FirstOrder,     "FirstOrder (Fore)",  180,   40,   40 },
  { Clr::FirstOrderBg,   "FirstOrder (Back)",   57,   58,   57 },
  { Clr::Republic,       "Republic (Fore)",    239,  243,  243 },
  { Clr::RepublicBg,     "Republic (Back)",    108,   22,   15 },
  { Clr::Separatist,     "Separatist (Fore)",   32,   48,  141 },
  { Clr::SeparatistBg,   "Separatist (Back)",  214,  216,  215 },

  // firing arcs (from website articles)
  { Clr::ArcRebelL,      "Rebel Arc (Light)",       223,   30,   37 },
  { Clr::ArcRebelD,      "Rebel Arc (Dark)",         58,   18,   18 },
  { Clr::ArcImperialL,   "Imperial Arc (Light)",    107,  185,   69 },
  { Clr::ArcImperialD,   "Imperial Arc (Dark)",      18,   57,   29 },
  { Clr::ArcScumL,       "Scum Arc (Light)",        244,  209,   24 },
  { Clr::ArcScumD,       "Scum Arc (Dark)",          62,   52,   21 },
  { Clr::ArcResistanceL, "Resistance Arc (Light)",  224,   32,   39 },
  { Clr::ArcResistanceD, "Resistance Arc (Dark)",    51,   17,   17 },
  { Clr::ArcFirstOrderL, "FirstOrder Arc (Light)",  100,  182,   70 },
  { Clr::ArcFirstOrderD, "FirstOrder Arc (Dark)",    18,   55,   28 },
  { Clr::ArcRepublicL,   "Republic Arc (Light)",     79,  173,  221 },
  { Clr::ArcRepublicD,   "Republic Arc (Dark)",      23,   70,   97 },
  { Clr::ArcSeparatistL, "Separatist Arc (Light)",  245,  157,   41 },
  { Clr::ArcSeparatistD, "Separatist Arc (Dark)",   121,   61,   26 },

  // dials
  { Clr::DialRebel,      "Rebel Dial",              130,   40,   25 },
  { Clr::DialImperial,   "Imperial Dial",            53,   57,   61 },
  { Clr::DialScum,       "Scum Dial",                63,   58,   39 },
  { Clr::DialResistance, "Resistance Dial",         229,  101,   58 },
  { Clr::DialFirstOrder, "First Order Dial",         50,   41,   31 },
};

ColorNotFound::ColorNotFound(Clr c) : runtime_error("Color not found (enum " + std::to_string((int)c) + ")") { }

Color Color::GetColor(Clr clr) {
  for(const Color& color : Color::colors) {
    if(color.GetClr() == clr) {
      return color;
    }
  }
  throw ColorNotFound(clr);
}

Color Color::GetFacFg(Fac f) {
  switch(f) {
  case Fac::Rebel:      return Color::GetColor(Clr::Rebel);
  case Fac::Imperial:   return Color::GetColor(Clr::Imperial);
  case Fac::Scum:       return Color::GetColor(Clr::Scum);
  case Fac::Resistance: return Color::GetColor(Clr::Resistance);
  case Fac::FirstOrder: return Color::GetColor(Clr::FirstOrder);
  case Fac::Republic:   return Color::GetColor(Clr::Republic);
  case Fac::Separatist: return Color::GetColor(Clr::Separatist);
  default:              throw FactionNotFound(f);
  }
}
  
Color Color::GetFacBg(Fac f) {
  switch(f) {
  case Fac::Rebel:      return Color::GetColor(Clr::RebelBg);
  case Fac::Imperial:   return Color::GetColor(Clr::ImperialBg);
  case Fac::Scum:       return Color::GetColor(Clr::ScumBg);
  case Fac::Resistance: return Color::GetColor(Clr::ResistanceBg);
  case Fac::FirstOrder: return Color::GetColor(Clr::FirstOrderBg);
  case Fac::Republic:   return Color::GetColor(Clr::RepublicBg);
  case Fac::Separatist: return Color::GetColor(Clr::SeparatistBg);
  default:              throw FactionNotFound(f);
  }
}

Color Color::GetDif(Dif d) {
  switch(d) {
  case Dif::Red:    return Color::GetColor(Clr::Stress);
  case Dif::White:  return Color::GetColor(Clr::Neutral);
  case Dif::Blue:   return Color::GetColor(Clr::Destress);
  case Dif::Purple: return Color::GetColor(Clr::Force);
  default:          throw DifficultyNotFound(d);
  }
}

std::vector<Clr> Color::GetAllClrs() {
  std::vector<Clr> ret;
  for(const Color& color : Color::colors) {
    ret.push_back(color.GetClr());
  }
  return ret;
}

Clr Color::GetClr()          const { return this->clr; }
std::string Color::GetName() const { return this->name; }
uint8_t Color::Red()         const { return this->red; }
uint8_t Color::Green()       const { return this->green; }
uint8_t Color::Blue()        const { return this->blue; }
uint8_t Color::Gray()        const {
  double ret = (0.3 * this->red) + (0.59 * this->green) + (0.11 * this->blue);
  return (uint8_t) (ret + 0.5);
}
std::string Color::GetHex() const {
  char ret[7];
  snprintf(ret, sizeof(ret), "%02x%02x%02x", this->red, this->green, this->blue);
  return ret;
}

Color:: Color(Clr c, std::string n, uint8_t r, uint8_t g, uint8_t b)
  : clr(c), name(n), red(r), green(g), blue(b) { }

}
