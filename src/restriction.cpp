#include "restriction.h"
#include "pilot.h"
#include "shared.h"
#include <sstream>

namespace libxwing2 {

// helpers

static std::vector<Fac> GetFactionsIncludingSomeone(FacIncluding f) {
  std::vector<Fac> ret = f.f;
  for(const Plt& plt : Pilot::GetAllPlts()) {
    Pilot pilot = Pilot::GetPilot(plt);
    if(pilot.GetName() == f.i) {
      if(std::find(ret.begin(), ret.end(), pilot.GetFac()) != ret.end()) {
	//ret = ret | pilot.GetFaction().GetFac();
	ret.push_back(pilot.GetFac());
      }
    }
  }
  return ret;
}

// ctors
Restriction::Restriction() { }
Restriction::Restriction(std::string t) : _text(t) { }

Restriction& Restriction::ByAct(SAct a) { this->_act = a; return *this; }
Restriction& Restriction::ByAgi(int a) { this->_agi = a; return *this; }
Restriction& Restriction::ByArc(Arc a) { this->_arc = a; return *this; }
Restriction& Restriction::ByBase(std::vector<BSz> b) { this->_baseSizes = b; return *this; }
Restriction& Restriction::ByEnergyMin(int8_t e) { this->_energyMin = e; return *this; }
Restriction& Restriction::ByEquippedUpg(UpT u) { this->_equippedUpgrade = u; return *this; }
Restriction& Restriction::ByFac(std::vector<Fac> f) { this->_factions = f; return *this; }
Restriction& Restriction::ByFacInc(std::vector<Fac> f, std::string i) { this->_factionsIncluding = {f, i}; return *this; }
Restriction& Restriction::ByForce(FAf f) { this->_forceAf = f; return *this; }
Restriction& Restriction::ByInitMax(int8_t i) { this->_initMax = i; return *this; }
Restriction& Restriction::ByInitMin(int8_t i) { this->_initMin = i; return *this; }
Restriction& Restriction::ByLimit(int8_t l) { this->_limit = l; return *this; }
Restriction& Restriction::ByShipAbility(SAb s) { this->_sab = s; return *this; }
Restriction& Restriction::ByShips(std::vector<Shp> s) { this->_ships = s; return *this; }
Restriction& Restriction::ByShieldMin(int8_t s) { this->_shieldMin = s; return *this; }
Restriction& Restriction::IsSolitary() {this->_solitary = true; return *this; }
Restriction& Restriction::IsStandardized() {this->_standardized = true; return *this; }

std::string        Restriction::GetText()            const { return (this->_text) ? *(this->_text) : ""; }
std::vector<Fac>   Restriction::GetFactions()        const { return (this->_factions) ? *(this->_factions) : std::vector<Fac>(); }
std::vector<Fac>   Restriction::GetAllowedFactions() const { return (this->_factionsIncluding) ? GetFactionsIncludingSomeone(*(this->_factionsIncluding)) : this->GetFactions() ; }
std::vector<BSz>   Restriction::GetBaseSizes()       const { return (this->_baseSizes) ? *(this->_baseSizes) : std::vector<BSz>(); }
std::optional<UpT> Restriction::GetEquippedUpgrade() const { return this->_equippedUpgrade; }
FAf                Restriction::GetFAf()             const { return (this->_forceAf) ? *(this->_forceAf) : FAf::None; }
bool Restriction::GetSolitary() const {
  if(this->_solitary) {
    return *this->_solitary;
  }
  return false;
}

std::vector<std::string> Restriction::CheckRestrictions(const Pilot& p, const Upgrade& u, const std::vector<Pilot>& ps) const {
  std::vector<std::string> ret;

  // action
  if(this->_act) {
    SAct a = *this->_act;
    bool hasAct = false;
    for(const std::list<SAct>& ac : p.GetModActions()) {
      auto it = ac.front();
      if((it.action == a.action) && ((a.difficulty == it.difficulty) || (a.difficulty == Dif::All)))   {
	hasAct = true;
	break;
      }
    }
    if(!hasAct) {
      std::string err = "Invalid Action - requires ";
      if(a.difficulty != Dif::All) {
	err += Difficulty::GetDifficulty(a.difficulty).GetName();
	err += " ";
      }
      err += Action::GetAction(a.action).GetName();
      ret.push_back(err);
    }
  }

  // agi
  if(this->_agi) {
    if(p.GetNatAgility() != *this->_agi) {
      ret.push_back("Invalid Agility - requires " + std::to_string(*this->_agi) +  " but has " + std::to_string(p.GetNatAgility()));
    }
  }
  
  // arc
  if(this->_arc) {
    Arc arc = *this->_arc;
    bool foundIt = false;
    std::string hasStr;
    bool first = true;
    for(const PriAttack& pa : p.GetModAttacks()) {
      if(first) { first = false; }
      else      { hasStr += " / "; }
      hasStr += FiringArc::GetFiringArc(pa.arc).GetName();
      if(pa.arc == arc) {
	foundIt = true;
      }
    }
    if(!foundIt) {
      ret.push_back("Invalid Arc - requires " + FiringArc::GetFiringArc(arc).GetName() + " but contains " + hasStr);
    }
  }

  // basesize
  if(this->_baseSizes) {
    bool foundIt = false;
    std::ostringstream bszstr;
    bool first = true;
    std::vector<BSz> bszs = *this->_baseSizes;
    for(BSz bsz : bszs) {
      if(first) { first = false; }
      else      { bszstr << ","; }
      bszstr << BaseSize::GetBaseSize(bsz).GetName();
      if(bsz == p.GetShip().GetBSz()) {
	foundIt = true;
	break;
      }      
    }
    if(!foundIt) {
      std::ostringstream oss;
      oss << "Invalid Base Size - requires "
	  <<  bszstr.str()
	  << " but played on "
	  << p.GetShip().GetBaseSize().GetName();
      ret.push_back(oss.str());
    }
  }

  // energy (min)
  if(this->_energyMin) {
    int8_t em = *this->_energyMin;
    int8_t e = p.GetModEnergy().GetCapacity();
    if(e < em) {
      ret.push_back("Invalid Energy - requires " + std::to_string(em) + " or higher but has " + std::to_string(e));
    }
  }

  // equipped upgrades
  if(this->_equippedUpgrade) {
    UpT eu = *this->_equippedUpgrade;
    bool foundIt = false;
    for(const Upgrade& u : p.GetAppliedUpgrades()) {
      if(u.GetUpT() == eu) {
	foundIt = true;
	break;
      }
    }
    if(!foundIt) {
      ret.push_back("Invalid Equipped Upgrade - requires " + UpgradeType::GetUpgradeType(eu).GetName());
    }
    return ret;
  }

  // factions
  if(this->_factions) {
    bool foundIt = false;
    std::ostringstream facstr;
    bool first = true;
    std::vector<Fac> facs = *this->_factions;
    for(Fac fac : facs) {
      if(first) { first = false; }
      else      { facstr << ","; }
      facstr << Faction::GetFaction(fac).GetName();
      if(fac == p.GetFac()) {
	foundIt = true;
	break;
      }
    }
    if(!foundIt) {
      std::ostringstream oss;
      oss << "Invalid Faction - requires "
	  <<  facstr.str()
	  << " but played on "
	  << p.GetFaction().GetName();
      ret.push_back(oss.str());
    }
  }

  // factions including someone
  if(this->_factionsIncluding) {
    bool foundIt = false;
    std::ostringstream facstr;
    bool first = true;
    FacIncluding fi = *this->_factionsIncluding;
    for(Fac fac : fi.f) {
      if(first) { first = false; }
      else      { facstr << ","; }
      facstr << Faction::GetFaction(fac).GetName();
      if(fac == p.GetFac()) {
	foundIt = true;
	break;
      }
    }
    if(!foundIt) {
      for(const Pilot& plt : ps) {
	if(plt.GetName() == fi.i) {
	  foundIt = true;
	  break;
	}
	for(const Upgrade& upg : plt.GetAppliedUpgrades()) {
	  if(upg.GetName() == fi.i) {
	    foundIt = true;
	    break;
	  }
	}
      }
    }
    if(!foundIt) {
      std::ostringstream oss;
      oss << "Invalid Faction - requires faction "
	  <<  facstr.str()
	  << " or squad including '"
	  << fi.i
	  << "'";
    }
  }

  // force affinity
  if(this->_forceAf) {
    FAf faf = *this->_forceAf;
    if(p.GetModForce().GetFAf() != faf) {
      std::string reqStr = ForceAffiliation::GetForceAffiliation(faf).GetName();
      std::string hasStr = ForceAffiliation::GetForceAffiliation(p.GetModForce().GetFAf()).GetName();
      ret.push_back("Invalid Force Affiliation: requires " + reqStr + " but played on " + hasStr);
    }
  }

  // initiative (max)
  if(this->_initMax) {
    int8_t im = *this->_initMax;
    int8_t i = p.GetNatInitiative();
    if(i >= im) {
      ret.push_back("Invalid Energy - must be " + std::to_string(im) + " or lower but has " + std::to_string(i));
    }
  }

  // initiative (min)
  if(this->_initMin) {
    int8_t im = *this->_initMin;
    int8_t i = p.GetNatInitiative();
    if(i <= im) {
      ret.push_back("Invalid Energy - must be " + std::to_string(im) + " or higher but has " + std::to_string(i));
    }
  }

  // limit
  if(this->_limit) {
    int8_t lim = *this->_limit;
    int8_t l = p.GetLimited();
    if(lim != l) {
      ret.push_back("Invalid Limit - must be " + std::to_string(lim) + " but played on " + std::to_string(l));
    }
  }

  // ship ability
  if(this->_sab) {
    SAb sab = *this->_sab;
    if(!p.GetSAb() || (*p.GetSAb() != sab)) {
      ret.push_back("Invalid ship ability - requres " + ShipAbility::GetShipAbility(sab).GetName() + " but has " + (p.GetSAb() ? p.GetShipAbility()->GetName() : "none"));
    }
  }

  // ships
  if(this->_ships) {
    Ships ships = *this->_ships;
    bool foundIt = false;
    bool first = true;
    std::string shipsStr;
    for(Shp shp : ships) {
      if(first) { first = false; }
      else      { shipsStr += " / "; }
      if(p.GetShp() == shp) {
	foundIt = true;
      }
      shipsStr += Ship::GetShip(shp).GetName();
    }
    if(!foundIt) {
      ret.push_back("Invalid Ship - requires " + shipsStr + " but played on " + p.GetShip().GetName());
    }
  }

  // shield (min)
  if(this->_shieldMin) {
    int8_t sm = *this->_shieldMin;
    int8_t s = p.GetNatShield().GetCapacity();
    if(s < sm) {
      ret.push_back("Invalid Shield - must be at least " + std::to_string(sm) + " or higher but has " + std::to_string(s));
    }
  }

  // solitary
  if(this->_solitary && *this->_solitary) {
    // silly optional bools...
    for(const Pilot& plt : ps) {
      if(&plt != &p) {
	for(const Upgrade& upg : plt.GetAppliedUpgrades()) {
	  if(upg.GetRestriction().GetSolitary()) {
	    ret.push_back("Multiple Solitary - " + upg.GetName());
	  }
	}
      }
    }
  }

  // standardized
  if(this->_standardized && *this->_standardized) {
    Shp reqShp = p.GetShp();
    Upg reqUpg = u.GetUpg();
    for(const Pilot& pilot : ps) {
      if(pilot.GetShp() == reqShp) {
	bool foundIt = false;
	for(const Upgrade& upgrade : pilot.GetAppliedUpgrades()) {
	  if(upgrade.GetUpg() == reqUpg) {
	    foundIt = true;
	    break;
	  }
	}
	if(!foundIt) {
	  ret.push_back("Standardized error - " + Ship::GetShip(reqShp).GetName() + " requires " + Upgrade::GetUpgrade(reqUpg).GetName());
	}
      }
    }
  }
  return ret;
}

}
