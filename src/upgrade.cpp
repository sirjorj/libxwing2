#include "upgrade.h"
#include "helper.h"
#include "release.h"
#include "shared.h"
#include <algorithm>
#include <sstream>

namespace libxwing2 {



static void ReplaceShortString(std::string& str, std::string src, std::string dst) {
  while(1) {
    size_t loc = str.find(src);
    if(loc == std::string::npos) {
      return;
    } else {
      str.replace(loc, src.size(), dst);
    }
  }
}

std::string GetShortString(std::string s) {
  if(false) { }
  // 1-off mappings
  else if(s == "7th Fleet Gunner")           { return "7th Fleet Gun"; }
  else if(s == "104th Battalion Pilot")      { return "104th Batt Plt"; }
  else if(s == "Automated Target Priority")  { return "Auto Tgt Priority"; }
  else if(s == "B6 Blade Wing Prototype")    { return "B6 Blade Wing Proto"; }
  else if(s == "Battle Meditation")          { return "Battle Med"; }
  else if(s == "Ben Teene")                  { return "B Teene"; }
  else if(s == "Biohexacrypt Codes")         { return "Biohex Codes"; }
  else if(s == "Bombardment Drone")          { return "Bombard Drone"; }
  else if(s == "Bombardment Specialists")    { return "Bombardment Spec"; }
  else if(s == "Bomblet Generator")          { return "Bomblet Gen"; }
  else if(s == "Calibrated Laser Targeting") { return "Cal Laser Tgt"; }
  else if(s == "Carida Academy Cadet")       { return "Carida Acad Cadet"; }
  else if(s == "Cavern Angels Zealot")       { return "Cavern Angels Zlt"; }
  else if(s == "Collision Detector")         { return "Collision Detect"; }
  else if(s == "Colossus Station Mechanic")  { return "Colossus Sta Mech"; }
  else if(s == "Contraband Cybernetics")     { return "Contraband Cyber"; }
  else if(s == "Deuterium Power Cells")      { return "Deut Power Cells"; }
  else if(s == "Diamond-Boron Missiles")     { return "Dia-Bor Msls"; }
  else if(s == "DRK-1 Probe Droids")         { return "Probe Droids"; }
  else if(s == "Edon Kappehl")               { return "E Kappehl"; }
  else if(s == "Electro-Proton Bomb")        { return "Electro-Prot Bomb"; }
  else if(s == "Emperor Palpatine")          { return "Palpatine"; }
  else if(s == "Energy-Shell Charges")       { return "Energy-Shell Ch"; }
  else if(s == "False Transponder Codes")    { return "False Tr Codes"; }
  else if(s == "Feethan Ottraw Autopilot")   { return "Feethan Ottraw Ap"; }
  else if(s == "Finch Dallow")               { return "F Dallow"; }
  else if(s == "First Order Collaborators")  { return "FO Collaborators"; }
  else if(s == "First Order Provocateur")    { return "FO Provocateur"; }
  else if(s == "First Order Sympathizers")   { return "FO Sympathizers"; }
  else if(s == "First Order Test Pilot")     { return "FO Test Plt"; }
  else if(s == "Geonosian Prototype")        { return "Geonosian Proto"; }
  else if(s == "GNK Gonk Droid")             { return "Gonk"; }
  else if(s == "Haor Chall Prototype")       { return "Haor Chall Proto"; }
  else if(s == "Heightened Perception")      { return "Heightened Percept"; }
  else if(s == "Hyperspace Tracking Data")   { return "Hypersp Track Data"; }
  else if(s == "Independent Calculations")   { return "Independent Calc"; }
  else if(s == "Jabba the Hutt")             { return "Jabba"; }
  else if(s == "Lando Calrissian")           { return "Lando"; }
  else if(s == "Mag-Pulse Warheads")         { return "Mag-Pulse"; }
  else if(s == "Maneuver-Assist MGK-300")    { return "Man-Assist MGK-300"; }
  else if(s == "Multi-Missile Pods")         { return "Multi-Msl Pods"; }
  else if(s == "New Republic Volunteers")    { return "New Rep Volunteers"; }
  else if(s == "Os-1 Arsenal Loadout")       { return "Os-1 Arsenal"; }
  else if(s == "Pammich Nerro Goode")        { return "P N Goode"; }
  else if(s == "Petty Officer Thanisson")    { return "P O Thanisson"; }
  else if(s == "Phlac-Arphocc Prototype")    { return "Phlac-Arphocc Pr"; }
  else if(s == "Precision Ion Engines")      { return "Prec Ion Eng"; }
  else if(s == "Precognitive Reflexes")      { return "Precog Reflexes"; }
  else if(s == "Quick-Release Locks")        { return "Quick-Rel Locks"; }
  else if(s == "Repulsorlift Stabilizers")   { return "Repulsorlift Stab"; }
  else if(s == "Resistance Sympathizer")     { return "Res Sympathizer"; }
  else if(s == "Rigged Cargo Chute")         { return "Rigged Cargo Ch"; }
  else if(s == "Separatist Interceptor")     { return "Sep Interceptor"; }
  else if(s == "Seventh Fleet Gunner")       { return "7th Fleet Gunner"; }
  else if(s == "Sienar-Jaemus Engineer")     { return "SJ Engineer"; }
  else if(s == "Skystrike Academy Class")    { return "Skystrike Academy"; }
  else if(s == "Spare Parts Canisters")      { return "Spare Parts Can"; }
  else if(s == "Special Forces Gunner")      { return "Spec Forces Gunner"; }
  else if(s == "Stalgasin Hive Guard")       { return "Stalgasin Hive Grd"; }
  else if(s == "Starkiller Base Pilot")      { return "StarkillerBase Plt"; }
  else if(s == "Static Discharge Vanes")     { return "Static Discharge Va"; }
  else if(s == "Supernatural Reflexes")      { return "Supernat Reflexes"; }
  else if(s == "Synchronized Console")       { return "Sync Console"; }
  else if(s == "Tallissan Lintra")           { return "Talli Lintra"; }
  else if(s == "Target-Assist MGK-300")      { return " Tar-Assist MGK-300"; }
  else if(s == "Targeting Synchronizer")     { return "Targeting Sync"; }
  else if(s == "Techno Union Bomber")        { return "Tech Union Bomber"; }
  else if(s == "Thermal Detonators")         { return "Thermal Det"; }
  else if(s == "Trade Federation Drone")     { return "Trade Fed Drone"; }
  else if(s == "Trajectory Simulator")       { return "Trajectory Sim"; }
  else if(s == "TransGalMeg Control Link")   { return "TGM Control Link"; }
  else if(s == "Underslung Blaster Cannon")  { return "Underslung Blaster"; }
  else if(s == "Vectored Cannons (RZ-1)")    { return "Vectored Cannons"; }
  else if(s == "Xg-1 Assault Configuration") { return "Xg-1 Assault"; }
  else if(s == "XX-23 S-Thread Tracers")     { return "XX-23"; }
  // abbreviated terms
  else {
    ReplaceShortString(s, "\"",                         "");
    ReplaceShortString(s, "Admiral",                    "Adm");
    ReplaceShortString(s, "Adv.",                       "Adv");
    ReplaceShortString(s, "Bombs",                      "Bmbs");
    ReplaceShortString(s, "Captain",                    "Capt");
    ReplaceShortString(s, "Colonel",                    "Col");
    ReplaceShortString(s, "Commander",                  "Cmdr");
    ReplaceShortString(s, "Concussion",                 "Concuss");
    ReplaceShortString(s, "Director",                   "Dir");
    ReplaceShortString(s, "Division",                   "Div");
    ReplaceShortString(s, "Escort",                     "Esc");
    ReplaceShortString(s, "General",                    "Gen");
    ReplaceShortString(s, "Grand Moff",                 "G Moff");
    ReplaceShortString(s, "Group",                      "Grp");
    ReplaceShortString(s, "Lieutenant",                 "Lt");
    ReplaceShortString(s, "Major",                      "Maj");
    ReplaceShortString(s, "s Millennium",               "s M");
    ReplaceShortString(s, "Mining Guild",               "MG");
    ReplaceShortString(s, "Missiles",                   "Msls");
    ReplaceShortString(s, "Officer",                    "Off");
    ReplaceShortString(s, "Pilot",                      "Plt");
    ReplaceShortString(s, "Rear Admiral",               "RAdm");
    ReplaceShortString(s, "Rockets",                    "Rkts");
    ReplaceShortString(s, "Squadron",                   "Sq");
    ReplaceShortString(s, "Supreme Leader",             "Sup Ldr");
    ReplaceShortString(s, "Torpedoes",                  "Torp");
    ReplaceShortString(s, "Veteran",                    "Vet");
  }
  return s;
}



// *** Upgrade ***
UpgradeNotFound::UpgradeNotFound(Upg upg)            : runtime_error("Upgrade not found (enum: " + std::to_string((int)upg) + ")") { }
UpgradeNotFound::UpgradeNotFound(std::string upgstr) : runtime_error("Upgrade not found " + upgstr + ")") { }

// factories
Upgrade Upgrade::GetUpgrade(Upg upg) {
  for(const Upgrade& upgrade : Upgrade::upgrades) {
    if(upgrade.GetUpg() == upg) {
      return upgrade;
    }
  }
  throw UpgradeNotFound(upg);
}

Upgrade Upgrade::GetUpgrade(std::string upgstr) {
  for(const Upgrade& upgrade : Upgrade::upgrades) {
    if(upgrade.GetUpgStr() == upgstr) {
      return upgrade;
    }
  }
  throw UpgradeNotFound(upgstr);
}

std::vector<Upg> Upgrade::GetAllUpgs() {
  std::vector<Upg> ret;
  for(const Upgrade& upgrade : Upgrade::upgrades) {
    ret.push_back(upgrade.GetUpg());
  }
  return ret;
}

std::vector<Upg> Upgrade::GetAllUpgs(UpT type) {
  std::vector<Upg> ret;
  for(const Upgrade& upgrade : Upgrade::upgrades) {
    if(upgrade.GetUpT() == type) {
      ret.push_back(upgrade.GetUpg());
    }
  }
  return ret;
}

std::vector<Upg> Upgrade::FindUpg(std::string u, std::vector<Upgrade> src) {
  std::vector<Upg> ret;
  std::string ss = ToLower(u); // searchString
  for(const Upgrade& upgrade : src) {
    if((ToLower(upgrade.GetName()).find(ss)       != std::string::npos) ||
       (ToLower(upgrade.GetTitle()).find(ss)      != std::string::npos) ||
       (ToLower(upgrade.GetShortTitle()).find(ss) != std::string::npos)) {
      ret.push_back(upgrade.GetUpg());
    }
  }
  return ret;
}

std::vector<Upg> Upgrade::Search(std::string s) {
  std::vector<Upg> ret;
  std::string ss = ToLower(s);
  for(Upg upg : Upgrade::GetAllUpgs()) {
    Upgrade upgrade = Upgrade::GetUpgrade(upg);
    if((ToLower(upgrade.GetName()).find(ss)                  != std::string::npos) ||
       (ToLower(upgrade.GetUpgradeType().GetName()).find(ss) != std::string::npos) ||
       (ToLower(upgrade.GetTitle()).find(ss)                 != std::string::npos) ||
       (ToLower(upgrade.GetShortTitle()).find(ss)            != std::string::npos) ||
       (ToLower(upgrade.GetText1().GetCleanText()).find(ss)  != std::string::npos) ||
       (ToLower(upgrade.GetText2().GetCleanText()).find(ss)  != std::string::npos)) {
      ret.push_back(upg);
    }
  }
  return ret;
}

// maintenance
void Upgrade::SanityCheck() {
  std::vector<std::string> entries;
  std::vector<std::string> dupes;
  int counter=0;
  printf("Checking Upgrades");
  for(const Upgrade& u : upgrades) {
    counter++;
    std::string newOne = u.GetUpgStr();
    if(std::find(entries.begin(), entries.end(), newOne) == entries.end()) {
      printf("."); fflush(stdout);
    } else {
      dupes.push_back(newOne);
      printf("X"); fflush(stdout);
    }
    entries.push_back(newOne);
  }
  printf("%zu\n", entries.size());
  if(dupes.size()) {
    printf("Dupes: %zu\n", dupes.size());
    for(const std::string& d : dupes) {
      printf("    %s\n", d.c_str());
    }
  }
}

// info (card)
Upg         Upgrade::GetUpg()       const { return this->upg; }
std::string Upgrade::GetUpgStr()    const {
  std::stringstream ss;
  ss << ToStringHelper(this->GetUpgradeType().GetShortName());
  ss << ToStringHelper(this->GetShortName());
  //Fac allowed = this->GetRestriction().GetAllowedFactions();
  std::vector<Fac> allowed = this->sides[0].restriction.GetAllowedFactions(); // this is if a card has a restiction on front but not back (C-110P)
  if(allowed.size()) {
    for(const Fac fac : allowed) {
      ss << ToStringHelper(Faction::GetFaction(fac).Get3Letter());
    }
  }
  return ss.str();
}
std::string Upgrade::GetName()      const { return (this->name=="") ? this->sides[0].title  : this->name; }
std::string Upgrade::GetShortName() const { return (this->name=="") ? this->GetShortTitle() : this->name; }
uint8_t     Upgrade::GetLimited()   const { return this->limited; }
bool        Upgrade::IsUnreleased() const {
  for(Rel rel : Release::GetAllRels()) {
    Release release = Release::GetRelease(rel);
    if(!release.IsUnreleased()) {
      for(Upg u : release.GetUpgrades()) {
	if(u == this->GetUpg()) {
	  return false;
	}
      }
    }
  }
  return true;
}

std::optional<Cost>        Upgrade::GetCost(PtL ptl)       const { return PointList::GetCost(this->GetUpg(), ptl); }
std::optional<bool>        Upgrade::GetHyperspace(PtL ptl) const { return PointList::GetHyperspace(this->GetUpg(), ptl); }
std::optional<std::string> Upgrade::GetEffect() const { return this->effect; }
std::optional<Rem>         Upgrade::GetRem()    const { return this->rem; }
std::optional<Remote>      Upgrade::GetRemote() const {
  if(this->rem) { return Remote::GetRemote(*this->rem); }
  else          { return std::nullopt; }
}

// info (side)
UpT              Upgrade::GetUpT()         const { return this->sides[this->curSide].upgradeType; }
UpgradeType      Upgrade::GetUpgradeType() const { return UpgradeType::GetUpgradeType(this->sides[this->curSide].upgradeType); }
std::vector<UpT> Upgrade::GetSlots()       const { return this->sides[this->curSide].slots; }
std::string      Upgrade::GetTitle()       const { return this->sides[this->curSide].title; }
std::string      Upgrade::GetShortTitle()  const { return GetShortString(this->GetName()); }
Chargeable       Upgrade::GetCharge()      const { return this->sides[this->curSide].charge; }
Chargeable       Upgrade::GetForce()       const { return this->sides[this->curSide].force; }
std::optional<SecAttack> Upgrade::GetAttackStats()    const { return this->sides[this->curSide].attackStats; }
std::optional<SAb>         Upgrade::GetSAb()   const { return this->sides[this->curSide].sab;  }
std::optional<ShipAbility> Upgrade::GetShipAbility() const {
  if(this->sides[this->curSide].sab) { return ShipAbility::GetShipAbility(*this->sides[this->curSide].sab); }
  else                               { return std::nullopt; }

}


Modifier         Upgrade::GetModifier()        const { return this->sides[this->curSide].modifier; }
Restriction      Upgrade::GetRestriction()     const { return this->sides[this->curSide].restriction; }
bool             Upgrade::HasAbility()         const { return this->sides[this->curSide].hasAbility; }

Text Upgrade::GetText()  const { return Text(this->sides[this->curSide].text); }
Text Upgrade::GetText1() const { return Text(this->sides[0].text); }
Text Upgrade::GetText2() const { return Text(this->IsDualSided() ? this->sides[1].text : ""); }

bool             Upgrade::IsEpicOnly()     const {
  if(this->GetUpT() == UpT::Cargo ||
     this->GetUpT() == UpT::Command ||
     this->GetUpT() == UpT::Hardpoint ||
     this->GetUpT() == UpT::Team) {
    return true;
  }

  for(BSz bsz : this->GetRestriction().GetBaseSizes()) {
    if(bsz == libxwing2::BSz::Huge) {
      return true;
    }
  }
  return false;
}

//std::optional<AttackStats> Upgrade::GetAttackStats()    const {return this->sides[this->curSide].attackStats; }
//std::optional<uint8_t>     Upgrade::GetEnergyLimit()    const {return this->sides[this->curSide].energyLimit; }
//std::optional<int8_t>      Upgrade::GetEnergyModifier() const {return this->sides[this->curSide].energyModifier; }

// stats
/*
std::vector<Upg> Upgrade::GetReqSlots()         const { return this->slots; }
  //StatModifiers    Upgrade::GetStatModifier()     const { return this->sides[this->curSide].statModifier; }
  //ManModifiers     Upgrade::GetManModifier()      const { return this->sides[this->curSide].manModifier; }
RestrictionCheck Upgrade::GetRestrictionCheck() const { return this->restrictionCheck; }
uint8_t          Upgrade::GetExtras()           const { return this->extras; }
void             Upgrade::SetExtras(uint8_t x)        { this->extras = x; }
void             Upgrade::ExtraUp()                   { if(this->extras < 255) { this->extras++; } }
void             Upgrade::ExtraDn()                   { if(this->extras >   0) { this->extras--; } }
*/

// state
void Upgrade::Setup(Upgrade& u) { this->setup(u); }
bool Upgrade::IsDualSided() const  { return this->sides.size() > 1; }
void Upgrade::Flip() {
  if(this->sides.size() > 1) {
    this->curSide = (this->curSide+1) % 2;
  }
}


Upgrade::GameState& Upgrade::GS() {
  if(!this->gameState) {
    this->gameState = Upgrade::GameState(this);
  }
  return *this->gameState;
}

// constructor
Upgrade::Upgrade(Upg         u,
		 std::string n,
		 uint8_t     l,
		 SetupF      s,
		 UpgradeSide s1)
  : upg(u), name(n), limited(l), setup(s), curSide(0), sides({{s1}}) { }

Upgrade::Upgrade(Upg         u,
		 std::string n,
		 uint8_t     l,
		 SetupF      s,
		 UpgradeSide s1,
		 std::string e)
  : upg(u), name(n), limited(l), effect(e), setup(s), curSide(0), sides({{s1}}) { }

Upgrade::Upgrade(Upg         u,
		 std::string n,
		 uint8_t     l,
		 SetupF      s,
		 UpgradeSide s1,
		 Rem         r)
  : upg(u), name(n), limited(l), rem(r), setup(s), curSide(0), sides({{s1}}) { }

Upgrade::Upgrade(Upg         u,
		 std::string n,
		 uint8_t     l,
		 SetupF      s,
		 UpgradeSide s1,
		 UpgradeSide s2)
  : upg(u), name(n), limited(l), setup(s), curSide(0), sides({{s1,s2}}) { }

Upgrade::Upgrade(Upg         u,
		 std::string n,
		 uint8_t     l,
		 SetupF      s,
		 UpgradeSide s1,
		 UpgradeSide s2,
		 std::string e)
  : upg(u), name(n), limited(l), effect(e), setup(s), curSide(0), sides({{s1,s2}}) { }

Upgrade::Upgrade(Upg         u,
		 std::string n,
		 uint8_t     l,
		 SetupF      s,
		 UpgradeSide s1,
		 UpgradeSide s2,
		 Rem         r)
  : upg(u), name(n), limited(l), rem(r), setup(s), curSide(0), sides({{s1,s2}}) { }

  // GameState stuff
  Upgrade::GameState::GameState(Upgrade *u)
    : upgrade(u)
    , isEnabled(1)
    , charge(u->GetCharge().GetCapacity())
    , force(u->GetForce().GetCapacity())
  { }

  bool Upgrade::GameState::IsEnabled() const { return this->isEnabled;  }
  void Upgrade::GameState::Enable()          { this->isEnabled = true;  }
  void Upgrade::GameState::Disable()         { this->isEnabled = false; }

  int8_t Upgrade::GameState::GetCurCharge() const { return this->charge;}
  void   Upgrade::GameState::ChargeUp()           { if(this->charge < this->upgrade->GetCharge().GetCapacity()) { this->charge++; } }
  void   Upgrade::GameState::ChargeDn()           { if(this->charge > 0) { this->charge--; } };

  int8_t Upgrade::GameState::GetCurForce() const { return this->force;}
  void   Upgrade::GameState::ForceUp()           { if(this->force < this->upgrade->GetForce().GetCapacity()) { this->force++; } }
  void   Upgrade::GameState::ForceDn()           { if(this->force > 0) { this->force--; } };
}
