#include "pointlist.h"
#include "helper.h"
#include "pilot.h"
#include <sstream>

namespace libxwing2 {

// cost

Cost Cost::ByCost(int16_t c) {
  return Cost(CsT::Const, {c});
}

Cost Cost::ByAgi(int16_t a0, int16_t a1, int16_t a2, int16_t a3) {
  return Cost(CsT::Agi, {a0, a1, a2, a3});
}

Cost Cost::ByBase(int16_t s, int16_t m, int16_t l) {
  return Cost(CsT::Base, {s, m, l});
}

Cost Cost::ByBase(int16_t s, int16_t m, int16_t l, int16_t h) {
  return Cost(CsT::Base, {s, m, l, h});
}

Cost Cost::ByInit(int16_t i0, int16_t i1, int16_t i2, int16_t i3, int16_t i4, int16_t i5, int16_t i6) {
  return Cost(CsT::Init, {i0, i1, i2, i3, i4, i5, i6});
}

Cost Cost::ByInit(int16_t i0, int16_t i1, int16_t i2, int16_t i3, int16_t i4, int16_t i5, int16_t i6, int16_t i7, int16_t i8) {
  return Cost(CsT::Init, {i0, i1, i2, i3, i4, i5, i6, i7, i8});
}



Cost::Cost(CsT t, std::vector<int16_t> c) : costType(t), costs(c) { }

CsT                  Cost::GetCsT()     const { return this->costType; }
std::vector<int16_t> Cost::GetCosts()   const { return this->costs; }
std::string          Cost::GetCostStr() const {
  std::stringstream ss;
  switch(this->GetCsT()) {
    case CsT::Const: {
      ss << std::to_string(this->GetCosts()[0]);
      break;
    }
    case CsT::Base: {
      ss <<  "S:" << std::to_string(this->GetCosts()[0])
	 << " M:" << std::to_string(this->GetCosts()[1])
	 << " L:" << std::to_string(this->GetCosts()[2]);
      break;
    }
    case CsT::Agi: {
      ss <<  "0:" << std::to_string(this->GetCosts()[0])
	 << " 1:" << std::to_string(this->GetCosts()[1])
	 << " 2:" << std::to_string(this->GetCosts()[2])
	 << " 3:" << std::to_string(this->GetCosts()[3]);
      break;
    }
    case CsT::Init: {
      ss <<  "0:" << std::to_string(this->GetCosts()[0])
	 << " 1:" << std::to_string(this->GetCosts()[1])
	 << " 2:" << std::to_string(this->GetCosts()[2])
	 << " 3:" << std::to_string(this->GetCosts()[3])
	 << " 4:" << std::to_string(this->GetCosts()[4])
	 << " 5:" << std::to_string(this->GetCosts()[5])
	 << " 6:" << std::to_string(this->GetCosts()[6]);
      break;
    }
  }
  return ss.str();
}

// operators
bool Cost::operator==(const Cost& u) const {
  //only true if they are the same type and ALL costs match
  if(this->costType != u.costType)         { return false; }
  if(this->costs.size() != u.costs.size()) { return false; }
  for(int i=0; i<this->costs.size(); i++) {
    if(this->costs[i] != u.costs[i]) {
      return false;
    }
  }
  return true;
}

bool Cost::operator!=(const Cost& u) const {
  return !(*this == u);
}

bool Cost::operator>(const Cost& u) const {
  if(this->costType != u.costType) { return false; }
  for(int i=0; i<this->costs.size(); i++) {
    if(this->costs[i] < u.costs[i]) {
      return false;
    }
  }
  return true;
}

bool Cost::operator<(const Cost& u) const {
  if(this->costType != u.costType) { return false; }
  for(int i=0; i<this->costs.size(); i++) {
    if(this->costs[i] > u.costs[i]) {
      return false;
    }
  }
  return true;
}

bool operator==(const PLPilot& a, const PLPilot& b) {
  return (a.plt == b.plt) && (a.hyp == b.hyp) && (a.cost == b.cost) && (a.upgBar == b.upgBar);
}

bool operator!=(const PLPilot& a, const PLPilot& b) {
  return !(a == b);
}  

bool operator==(const PLUpgrade& a, const PLUpgrade& b) {
  return (a.upg == b.upg) && (a.hyp == b.hyp) && (a.cost == b.cost);
}

bool operator!=(const PLUpgrade& a, const PLUpgrade& b) {
  return !(a == b);
}  



// pointlist
PointListNotFound::PointListNotFound(PtL ptl)         : runtime_error("PointList found (enum " + std::to_string((int)ptl) + ")") { }
PointListNotFound::PointListNotFound(std::string ptl) : runtime_error("PointList found (" + ptl + ")") { }

std::string PointList::GetName(PtL ptl) {
  for(const PLVersion& plv : PointList::versions) {
    if(plv.ptl == ptl) {
      return plv.name;
    }
  }
  throw PointListNotFound(ptl);
}

PLDate PointList::GetAnnounceDate(PtL ptl) {
  for(const PLVersion& plv : PointList::versions) {
    if(plv.ptl == ptl) {
      return plv.announceDate;
    }
  }
  throw PointListNotFound(ptl);
}

PLDate PointList::GetEffectDate(PtL ptl) {
  for(const PLVersion& plv : PointList::versions) {
    if(plv.ptl == ptl) {
      return plv.effectDate;
    }
  }
  throw PointListNotFound(ptl);
}

std::optional<PLPilot> PointList::Get(Plt plt, PtL ptl) {
  bool foundVersion = false;
  for(std::vector<PLVersion>::reverse_iterator plv = PointList::versions.rbegin(); plv != PointList::versions.rend(); ++plv) {
    if(!foundVersion && (plv->ptl == ptl)) {
      foundVersion = true;
    }
    if(foundVersion) {
      for(const PLPilot& plpilot : plv->pilots) {
	if(plpilot.plt == plt) {
	  return plpilot;
	}
      }
    }
  }
  if(foundVersion) {
    return std::nullopt;
  }
  throw PointListNotFound(ptl);
}

std::optional<PLUpgrade> PointList::Get(Upg upg, PtL ptl) {
  bool foundVersion = false;
  for(std::vector<PLVersion>::reverse_iterator plv = PointList::versions.rbegin(); plv != PointList::versions.rend(); ++plv) {
    if(!foundVersion && (plv->ptl == ptl)) {
      foundVersion = true;
    }
    if(foundVersion) {
      for(const PLUpgrade& plupgrade : plv->upgrades) {
	if(plupgrade.upg == upg) {
	  return plupgrade;
	}
      }
    }
  }
  if(foundVersion) {
    return std::nullopt;
  }
  throw PointListNotFound(ptl);
}

std::optional<bool> PointList::GetHyperspace(Plt plt, PtL ptl) {
  std::optional<PLPilot> plp = Get(plt, ptl);
  if(plp) { return plp->hyp;     }
  else    { return std::nullopt; }
}

std::optional<Cost> PointList::GetCost(Plt plt, PtL ptl) {
  std::optional<PLPilot> plp = Get(plt, ptl);
  if(plp) { return plp->cost;    }
  else    { return std::nullopt; }
}

std::optional<UpgradeBar> PointList::GetUpgradeBar(Plt plt, PtL ptl) {
  std::optional<PLPilot> plp = Get(plt, ptl);
  if(plp) { return plp->upgBar;  }
  else    { return std::nullopt; }
}

std::optional<KeywordList> PointList::GetKeywordList(Plt plt, PtL ptl) {
  std::optional<PLPilot> plp = Get(plt, ptl);
  if(plp) { return plp->keywords; }
  else    { return std::nullopt;  }
}

std::optional<bool> PointList::GetHyperspace(Upg upg, PtL ptl) {
  std::optional<PLUpgrade> plu = Get(upg, ptl);
  if(plu) { return plu->hyp;     }
  else    { return std::nullopt; }
}

std::optional<Cost> PointList::GetCost(Upg upg, PtL ptl) {
  std::optional<PLUpgrade> plu = Get(upg, ptl);
  if(plu) { return plu->cost;    }
  else    { return std::nullopt; }
}

std::vector<PtL> PointList::GetPtLs() {
  std::vector<PtL> ret;
  for(const PLVersion& plv : PointList::versions) {
    ret.push_back(plv.ptl);
  }
  return ret;
}

std::string PointList::GetPtLStr(PtL ptl) {
  return ToStringHelper(PointList::GetName(ptl));
}
  
PtL PointList::GetPtL(std::string p) {
  for(PtL ptl : PointList::GetPtLs()) {
    if(PointList::GetPtLStr(ptl) == p) {
      return ptl;
    }
  }
  throw new PointListNotFound(p);
}
  
std::map<PtL, std::vector<std::string>> PointList::GetUnchangedEntries() {
  std::map<PtL, std::vector<std::string>> dupes;

  const std::vector<PtL> ptls = PointList::GetPtLs();

  for(Plt& plt : Pilot::GetAllPlts()) {
    Pilot pilot = Pilot::GetPilot(plt);
    std::optional<PLPilot> lastPilot;    
    for(const PtL ptl : ptls) {
      std::optional<PLPilot> curPilot = PointList::Get(pilot.GetPlt(), ptl);
      if(curPilot) {
	if(lastPilot) {
	  if(*curPilot == *lastPilot) {
	    std::stringstream ss;
	    ss << PointList::GetName(ptl) << " - [" << pilot.GetShip().GetName() << "] " << pilot.GetName();
	    dupes[ptl].push_back(ss.str());
	  }
	}
	lastPilot = curPilot;
      }
    }
  }

  for(Upg upg : Upgrade::GetAllUpgs()) {
    Upgrade upgrade = Upgrade::GetUpgrade(upg);
    std::optional<PLUpgrade> lastUpgrade;
    for(const PtL ptl : ptls) {
      std::optional<PLUpgrade> curUpgrade = PointList::Get(upg, ptl);
      if(curUpgrade) {
	if(lastUpgrade) {
	  if(*curUpgrade == *lastUpgrade) {
	    std::stringstream ss;
	    ss << PointList::GetName(ptl) << " - " << upgrade.GetName();
	    dupes[ptl].push_back(ss.str());
	  }
	}
	lastUpgrade = curUpgrade;
      }
    }
  }

  return dupes;
}

}
