#include "pilot.h"
#include "adjustable.h"
#include "helper.h"
#include "pointlist.h"
#include "release.h"
#include <functional>
#include <sstream>

namespace libxwing2 {

// pilot
PilotNotFound::PilotNotFound(Plt plt)            : runtime_error("Pilot not found (enum " + std::to_string((int)plt) + ")") { }
PilotNotFound::PilotNotFound(std::string pltstr) : runtime_error("Pilot not found: '" + pltstr + "'") { }

// factories
Pilot Pilot::GetPilot(Plt plt) {
  for(const Pilot& pilot : Pilot::pilots) {
    if(pilot.GetPlt() == plt) {
      return pilot;
    }
  }
  throw PilotNotFound(plt);
}

Pilot Pilot::GetPilot(std::string pltstr) {
  for(const Pilot& pilot : Pilot::pilots) {
    if(pilot.GetPltStr() == pltstr) {
      return pilot;
    }
  }
  throw PilotNotFound(pltstr);
}

std::vector<Plt> Pilot::GetAllPlts(std::vector<Fac> facs) {
  std::vector<Plt> ret;
  for(const Pilot& pilot : Pilot::pilots) {
    if( !facs.size() || (std::find(facs.begin(), facs.end(), pilot.GetFac()) != facs.end()) ) {
      ret.push_back(pilot.GetPlt());
    }
  }
  return ret;
}

std::vector<Plt> Pilot::FindPlt(std::string p) {
  return Pilot::FindPlt(p, Pilot::GetAllPlts());
}

std::vector<Plt> Pilot::FindPlt(std::string p, std::vector<Plt> src) {
  std::vector<Plt> ret;
  std::string ss = ToLower(p); // searchString
  for(const Plt& plt : src) {
    Pilot pilot = Pilot::GetPilot(plt);
    if((ToLower(pilot.GetPltStr()).find(ss)    != std::string::npos) ||
       (ToLower(pilot.GetName()).find(ss)      != std::string::npos) ||
       (ToLower(pilot.GetShortName()).find(ss) != std::string::npos)
       ) {
      ret.push_back(pilot.GetPlt());
    }
  }
  return ret;
}

std::vector<Plt> Pilot::Search(std::string s) {
  std::vector<Plt>  ret;
  std::string ss = ToLower(s);
  for(const Plt& plt : Pilot::GetAllPlts()) {
    Pilot pilot = Pilot::GetPilot(plt);
    if((ToLower(pilot.GetPltStr()).find(ss)              != std::string::npos) ||
       (ToLower(pilot.GetName()).find(ss)                != std::string::npos) ||
       (ToLower(pilot.GetShortName()).find(ss)           != std::string::npos) ||
       (ToLower(pilot.GetSubtitle()).find(ss)            != std::string::npos) ||
       (ToLower(pilot.GetText().GetCleanText()).find(ss) != std::string::npos) ||
       (pilot.GetShipAbility() && ToLower(pilot.GetShipAbility()->GetName()).find(ss) != std::string::npos) ||
       (pilot.GetShipAbility() && ToLower(pilot.GetShipAbility()->GetText().GetCleanText()).find(ss) != std::string::npos)
       ) {
      ret.push_back(plt);
    }
  }
  return ret;
}

// maintenance
void Pilot::SanityCheck() {
  std::vector<std::string> entries;
  std::vector<std::string> dupes;
  int counter=0;
  printf("Checking Pilots");
  for(const Pilot& p : Pilot::pilots) {
    counter++;
    std::string newOne = p.GetPltStr();
    if(std::find(entries.begin(), entries.end(), newOne) == entries.end()) {
      printf("."); fflush(stdout);
    } else {
      dupes.push_back(newOne);
      printf("X"); fflush(stdout);
    }
    entries.push_back(newOne);
  }
  printf("%zu\n", entries.size());
  if(dupes.size()) {
    printf("Dupes: %zu\n", dupes.size());
    for(const std::string& d : dupes) {
      printf("  %s\n", d.c_str());
    }
  }
}

// plt/shp/fac
Plt         Pilot::GetPlt()        const { return this->plt; }
std::string Pilot::GetPltStr()     const {
  std::stringstream ss;
  ss << ToStringHelper(this->GetShip().GetDialId());
  ss << ToStringHelper(this->GetFaction().Get3Letter());
  ss << ToStringHelper(this->GetShortName());
  ss << ToStringHelper(this->GetSubtitle());
  return ss.str();
}
Fac         Pilot::GetFac()        const { return this->fac; }
Faction     Pilot::GetFaction()    const { return Faction::GetFaction(this->fac); }
Shp         Pilot::GetShp()        const { return this->shp; }
Ship        Pilot::GetShip()       const { return Ship::GetShip(this->shp); }

// stats
int8_t Pilot::GetNatInitiative() const { return this->initiative; }
int8_t Pilot::GetModInitiative() const {
  int8_t init = this->GetNatInitiative();
  return init;
}
int8_t Pilot::GetNatEngagement() const { return this->engagement; }
int8_t Pilot::GetModEngagement() const {
  int8_t init = this->GetNatEngagement();
  return init;
}

uint8_t     Pilot::GetLimited()    const { return this->limited; }

std::string Pilot::GetName()       const { return this->pilotName; }
std::string Pilot::GetShortName()  const { return GetShortString(this->GetName()); }
std::string Pilot::GetSubtitle()   const { return this->pilotSubtitle; }

PriAttacks Pilot::GetNatAttacks() const { return this->attacks; }
PriAttacks Pilot::GetModAttacks() const {
  PriAttacks pa = this->GetNatAttacks();
  for(const Upgrade& u : this->GetAppliedUpgrades()) {
    for(const PriAttack& a : u.GetModifier().GetAttackAdd()) {
      pa.push_back(a);
    }
  }
  return pa;
}

int8_t Pilot::GetNatAgility() const { return this->agility; }
int8_t Pilot::GetModAgility() const {
  int8_t agility = this->GetNatAgility();
  return agility;
}

int8_t Pilot::GetNatHull() const { return this->hull; }
int8_t Pilot::GetModHull() const {
  int8_t hull = this->GetNatHull();
  for(const Upgrade& u : this->GetAppliedUpgrades()) {
    hull += u.GetModifier().GetHull();
  }
  return hull;
}

Chargeable Pilot::GetNatShield() const { return this->shield; }
Chargeable Pilot::GetModShield() const {
  int8_t shield = this->GetNatShield().GetCapacity();
  for(const Upgrade& u : this->GetAppliedUpgrades()) {
    shield += u.GetModifier().GetShield();
  }
  return { shield, this->GetNatShield().GetRecurring() };
}

Chargeable Pilot::GetNatEnergy() const { return this->energy; }
Chargeable Pilot::GetModEnergy() const {
  int8_t energy = this->GetNatEnergy().GetCapacity();
  for(const Upgrade& u : this->GetAppliedUpgrades()) {
    energy += u.GetModifier().GetEnergy();
  }
  return { energy, this->GetNatEnergy().GetRecurring() };
}

Chargeable Pilot::GetNatCharge() const { return this->charge; }
Chargeable Pilot::GetModCharge() const {
  Chargeable charge = this->GetNatCharge();
  //throw std::runtime_error("not implemented");
  return charge;
}

Force Pilot::GetNatForce() const { return this->force; }
Force Pilot::GetModForce() const {
  int8_t cap = this->GetNatForce().GetCapacity();
  bool   rec = this->GetNatForce().GetRecurring();
  FAf    faf = this->GetNatForce().GetFAf();
  for(const Upgrade& u : this->GetAppliedUpgrades()) {
    cap += u.GetForce().GetCapacity();
    rec |= u.GetForce().GetRecurring();
  }
  return Force(cap, rec, faf);
}

std::optional<SAb>         Pilot::GetSAb()         const { return this->sab; }
std::optional<ShipAbility> Pilot::GetShipAbility() const {
  if(this->sab) { return ShipAbility::GetShipAbility(*this->sab); }
  else          { return std::nullopt; }
}

ActionBar Pilot::GetNatActions() const { return this->actions; }
ActionBar Pilot::GetModActions() const {
  ActionBar a = this->GetNatActions();
  for(const Upgrade& u : this->GetAppliedUpgrades()) {
    ActionBar aa = u.GetModifier().GetActAdd();
    for(const std::list<SAct>& sa : aa) {
      a.push_back(sa);
    }
  }
  return a;
}

bool Pilot::HasAbility()    const { return this->hasAbility; }

Text Pilot::GetText() const { return Text(this->text); }

// adjustables
// assume all pilots have constant cost to hide the Cost object and just return the uint16_t
// if that ever changes, this will need to be updated
std::optional<int16_t> Pilot::GetNatCost(PtL ptl) const {
  Adjustable adj;
  std::optional<int16_t> acost = adj.GetCost(this->GetPlt());
  if(acost) { return *acost; }
  else {
    std::optional<Cost> cost = PointList::GetCost(this->GetPlt(), ptl);
    if(!cost) { return std::nullopt; }
    else {
      return cost->GetCosts()[0]; // assume just 1 and grab it
    }
  }
}

std::optional<int16_t> Pilot::GetModCost(PtL ptl) const {
  if(!this->GetNatCost(ptl)) {
    return std::nullopt;
  }
  int16_t ret = *this->GetNatCost(ptl);
  for(const Upgrade& u : this->GetAppliedUpgrades()) {
    std::optional<int16_t> cost = this->GetUpgCost(u.GetUpg(), ptl);
    if(!cost) {
      return std::nullopt;
    }
    else {
      ret += *cost;
    }
  }
  return ret;
}

std::optional<int16_t> Pilot::GetUpgCost(Upg upg, PtL ptl) const {
  std::optional<Cost> uCost = Upgrade::GetUpgrade(upg).GetCost(ptl);
  if(!uCost) {
    return std::nullopt;
  }
  switch(uCost->GetCsT()) {
  case CsT::Const: return uCost->GetCosts()[0]; break;

  case CsT::Base:
    switch(this->GetShip().GetBSz()) {
    case BSz::Small:  return uCost->GetCosts()[0]; break;
    case BSz::Medium: return uCost->GetCosts()[1]; break;
    case BSz::Large:  return uCost->GetCosts()[2]; break;
    default:          return std::nullopt;
    }

  case CsT::Agi: return uCost->GetCosts()[this->GetNatAgility()]; break;

  case CsT::Init: return uCost->GetCosts()[this->GetNatInitiative()]; break;
  }

  return std::nullopt;
}

std::optional<bool> Pilot::GetHyperspace(PtL ptl) const { return PointList::GetHyperspace(this->GetPlt(), ptl); }

std::optional<UpgradeBar> Pilot::GetNatUpgradeBar(PtL ptl) const { return PointList::GetUpgradeBar(this->GetPlt(), ptl); }
std::optional<UpgradeBar> Pilot::GetModUpgradeBar(PtL ptl) const {
  if(!this->GetNatUpgradeBar(ptl)) {
    return std::nullopt;
  }
  UpgradeBar ub = *this->GetNatUpgradeBar();
  if(this->GetShipAbility()) {
    for(const UpgradeSlot& s : this->GetShipAbility()->GetModifier().GetUpgAdd()) {
      ub.push_back(s);
    }
    for(const UpgradeSlot& s : this->GetShipAbility()->GetModifier().GetUpgRem()) {
      auto it = std::find(ub.rbegin(), ub.rend(), s);
      if(it != ub.rend()) {
	ub.erase(std::next(it).base());
      }
    }
  }
  for(const Upgrade& u : this->GetAppliedUpgrades()) {
    for(const UpgradeSlot& s : u.GetModifier().GetUpgAdd()) { ub.push_back(s); }
    for(const UpgradeSlot& s : u.GetModifier().GetUpgRem()) {
      auto it = std::find(ub.rbegin(), ub.rend(), s);
      if(it != ub.rend()) {
	ub.erase(std::next(it).base());
      }
    }
  }
  return ub;
}

std::optional<KeywordList> Pilot::GetKeywordList(PtL ptl) const {
  std::optional<KeywordList> keywords = PointList::GetKeywordList(this->GetPlt(), ptl);
  if(!keywords) {
    return std::nullopt;
  } else {
    return keywords;
  }
}

// other
bool Pilot::IsUnreleased()  const {
  for(Rel rel : Release::GetAllRels()) {
    Release release = Release::GetRelease(rel);
    if(!release.IsUnreleased()) {
      for(Plt p : release.GetPilots()) {
	if(p == this->GetPlt()) {
	  return false;
	}
      }
    }
  }
  return true;
}

Maneuvers Pilot::GetNatManeuvers() const { return this->GetShip().GetManeuvers(); }
Maneuvers Pilot::GetModManeuvers() const {
  Maneuvers mans = this->GetNatManeuvers();
  for(const Upgrade& u : this->GetAppliedUpgrades()) {
    std::vector<Modifier::ManeuverDifficulty> dec = u.GetModifier().GetManDec();
    for(const Modifier::ManeuverDifficulty &d : dec) {
      for(Maneuver& m : mans) {
	if((!d.speed || (*d.speed == m.speed)) && (d.bearing == m.bearing)) {
	  m.difficulty--;
	}
      }
    }
    std::vector<Modifier::ManeuverDifficulty> inc = u.GetModifier().GetManInc();
    for(const Modifier::ManeuverDifficulty &i : inc) {
      for(Maneuver& m : mans) {
	if((!i.speed || (*i.speed == m.speed)) && (i.bearing == m.bearing)) {
	  m.difficulty++;
	}
      }
    }

  }
  return mans;
}

std::vector<Upgrade>& Pilot::GetAppliedUpgrades() { return this->appliedUpgrades; }

std::vector<Upgrade> Pilot::GetAppliedUpgrades() const { return this->appliedUpgrades; }

void Pilot::ApplyUpgrade(Upgrade u) { this->appliedUpgrades.push_back(u); }

Pilot::GameState& Pilot::GS() {
  if(!this->gameState) {
    this->gameState = Pilot::GameState(this);
  }
  return *this->gameState;
}

Pilot::Pilot(Plt         p,
	     Fac         f,
	     Shp         s,
	     int8_t      init,
	     uint8_t     lim,
	     std::string name,
	     std::string subt,
	     PriAttacks  att,
	     int8_t      agi,
	     int8_t      hul,
	     int8_t      shi,
	     Chargeable  chr,
	     Force       frc,
	     std::optional<SAb> sa,
	     ActionBar   act,
	     bool        abi,
	     std::string txt
	     )
  : plt(p), fac(f), shp(s), initiative(init), engagement(init), limited(lim), pilotName(name), pilotSubtitle(subt),
    attacks(att), agility(agi), hull(hul), shield({shi,0}), energy({0,0}), charge(chr), force(frc),
    sab(sa), actions(act), hasAbility(abi), text(txt) { }

Pilot::Pilot(Plt         p,
	     Fac         f,
	     Shp         s,
	     int8_t      init,
	     int8_t      enga,
	     std::string name,
	     PriAttacks  att,
	     int8_t      agi,
	     int8_t      hul,
	     Chargeable  shi,
	     Chargeable  enr,
	     Chargeable  chr,
	     Force       frc,
	     std::optional<SAb> sa,
	     ActionBar   act,
	     bool        abi,
	     std::string txt
	     )
  : plt(p), fac(f), shp(s), initiative(init), engagement(enga), limited(0), pilotName(name), pilotSubtitle(""),
    attacks(att), agility(agi), hull(hul), shield(shi), energy(enr), charge(chr), force(frc),
    sab(sa), actions(act), hasAbility(abi), text(txt) { }



// GameState stuff
Pilot::GameState::GameState(Pilot *p)
  : pilot(p)
  , isEnabled(1)
  , shieldHits(0)
  , hullHits(0)
  , charge(p->GetModCharge().GetCapacity())
  , force(p->GetModForce().GetCapacity())
{ }
  bool Pilot::GameState::IsEnabled() const { return this->isEnabled;  }
  void Pilot::GameState::Enable()          { this->isEnabled = true;  }
  void Pilot::GameState::Disable()         { this->isEnabled = false; }

  int8_t Pilot::GameState::GetCurShield()  const { return this->pilot->GetModShield().GetCapacity() - this->shieldHits; }
  int8_t Pilot::GameState::GetShieldHits() const { return this->shieldHits; }
  void   Pilot::GameState::ShieldUp()            { if(this->shieldHits > 0) { this->shieldHits--; } }
  void   Pilot::GameState::ShieldDn()            { if(this->shieldHits < this->pilot->GetModShield().GetCapacity()) { this->shieldHits++; } }

  int8_t Pilot::GameState::GetCurHull()  const { return this->pilot->GetModHull() - this->hullHits; }
  int8_t Pilot::GameState::GetHullHits() const { return this->hullHits; }
  void   Pilot::GameState::HullUp()            { if(this->hullHits > 0) { this->hullHits--; } }
  void   Pilot::GameState::HullDn()            { if(this->hullHits < this->pilot->GetModHull()) { this->hullHits++; } }

  int8_t Pilot::GameState::GetCurCharge() const { return this->charge;}
  void   Pilot::GameState::ChargeUp()           { if(this->charge < this->pilot->GetModCharge().GetCapacity()) { this->charge++; } }
  void   Pilot::GameState::ChargeDn()           { if(this->charge > 0) { this->charge--; } };

  int8_t Pilot::GameState::GetCurForce() const { return this->force;}
  void   Pilot::GameState::ForceUp()           { if(this->force < this->pilot->GetModForce().GetCapacity()) { this->force++; } }
  void   Pilot::GameState::ForceDn()           { if(this->force > 0) { this->force--; } };
}
