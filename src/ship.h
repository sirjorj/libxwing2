#pragma once
#include "basesize.h"
#include "maneuver.h"
#include "shared.h"
#include <optional>
#include <stdexcept>
#include <vector>


namespace libxwing2 {




// ship
enum class Shp {
  // *** First Edition Wave 13 ***
  T65XWing, UWing, // Saws Renegades
  TIEReaper,       // TIE Reaper
  // *** Wave 1 ***
  TIElnFighter,         // Core Set
  CustYT1300, EscCraft, // Lando's Millenium Falcon
  // Rebel Conversion Kit
  ARC170, ASF01BWing, AttackShuttle, Auzituck, BTLA4YWing, BTLS8KWing,
  EWing, HWK290, ModYT1300, RZ1AWing, Sheathipede, VCX100, YT2400, Z95,
  // Imperial Conversion Kit
  AlphaClass, Lambda, TIEAdvV1, TIEAdvX1, TIEagAggressor, TIEcaPunisher,
  TIEdDefender, TIEinInterceptor, TIEphPhantom, TIEsaBomber, TIEskStriker, VT49,
  // Scum Conversion Kit
  Aggressor, FangFighter, Firespray, G1A, JM5K, Kihraxz, LancerClass,
  M12LKimogila, M3A, Quadjumper, Scurrg, StarViper, YV666,
  // *** Wave 2 ***
  TIEfoFighter, TIEsfFighter, TIEvnSilencer, Upsilon, // First Order Conversion Kit
  MG100, ScaYT1300, T70XWing,                         // Resistance Conversion Kit
  RZ2AWing,                                // RZ2 AWing
  ModifiedTIEln,                           // Mining Guild TIE
  // *** Wave 3 ***
  Belbullab22, Vulture, // Servants of Strife
  SithInf,              // Sith Infiltrator
  Delta7, V19,          // Guardians of the Republic
  // *** Wave 4 ***
  N1,
  Hyena,
  ResTransport, ResTransportPod,
  // *** Wave 5 ***
  Nantex,
  BTLBYWing,
  // *** Huge Ships ***
  CR90, CROC, Gozanti, GR75, Raider,
  // *** Wave 6 ***
  TIEbaInterceptor,
  Fireball,
  // *** Wave 7 ***
  TIErbHeavy,
  Xi,
  LAAT,
  HMP,
  // *** Wave 8 ***
  Eta2Actis, Syliure,
  NimbusClass,
  DroidTriFighter,
  // *** Huge Ship ***
  Trident,
  // *** Wave 9 ***
  BTANR2YWing,
  TIEseBomber, TIEwiWhisperMod,
};

namespace ShipGroup {
  extern const std::vector<Shp> AWING;
  extern const std::vector<Shp> TIE;
  extern const std::vector<Shp> XWING;
  extern const std::vector<Shp> YWING;
}

class ShipNotFound : public std::runtime_error {
 public:
  ShipNotFound(Shp shp);
  ShipNotFound(std::string shpstr);
};

class Ship {
 public:
  static Ship GetShip(Shp shp);
  static Ship GetShip(std::string shpstr);
  static std::vector<Shp> GetAllShps();
  static std::vector<Shp> FindShp(std::string s);
  static std::vector<Shp> Search(std::string s);

  static void SanityCheck();

  Shp         GetShp()       const;
  std::string GetShpStr()    const;
  std::string GetName()      const;
  std::string GetShortName() const;
  std::string GetDialId()    const;
  BSz         GetBSz()       const;
  BaseSize    GetBaseSize()  const;
  Maneuvers   GetManeuvers() const;

 private:
  Shp shp;
  std::string name;
  std::string shortName;
  std::string dialId;
  BSz         bsz;
  Maneuvers   maneuvers;

  static std::vector<Ship> ships;

  Ship(Shp         t,
       std::string n,
       std::string s,
       std::string d,
       BSz         b,
       Maneuvers   m);
};

}
