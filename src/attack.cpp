#include "attack.h"

namespace libxwing2 {

FiringArcNotFound::FiringArcNotFound(Arc a) : runtime_error("Firing Arc not found (enum " + std::to_string((int)a) + ")") { }

std::vector<FiringArc> FiringArc::firingArcs = {
  { Arc::None,         "None"         },
  { Arc::Front,        "Front"        },
  { Arc::Rear,         "Rear"         },
  { Arc::Left,         "Left"         },
  { Arc::Right,        "Right"        },
  { Arc::Bullseye,     "Bullseye"     },
  { Arc::FullFront,    "FullFront"    },
  { Arc::FullRear,     "FullRear"     },
  { Arc::SingleTurret, "SingleTurret" },
  { Arc::DoubleTurret, "DoubleTurret" },

};

FiringArc FiringArc::GetFiringArc(Arc arc) {
  for(const FiringArc& firingarc : FiringArc::firingArcs) {
    if(firingarc.GetArc() == arc) {
      return firingarc;
    }
  }
  throw FiringArcNotFound(arc);
}

std::vector<Arc> FiringArc::GetAllArcs() {
  std::vector<Arc> ret;
  for(const FiringArc& firingArc : FiringArc::firingArcs) {
    ret.push_back(firingArc.GetArc());
  }
  return ret;
}

Arc         FiringArc::GetArc()  const { return this->arc; }
std::string FiringArc::GetName() const { return this->name; }

FiringArc::FiringArc(Arc         a,
		     std::string n)
  : arc(a), name(n) { }
}
