#pragma once

namespace libxwing2 {

enum class QB {
  // Aggressor (Scum)
  IG88A_AsIg_IG88D_AsIg, IG88B_FcIcIg_IG88C_FcIcIg,
  // Alpha-class Star Wing (Imperial)
  LtKarsabi_OmAlAsXg, NuSqPlt_FcPtAsOs, MajVynder_SsBrAsOs, RhoSqPlt_FcIcHmAsXg,
  // ARC170 (Rebel)
  NWexley_EhSnVtR3ApHu, GDreis_EhPtPcVt, SBey_EhPtPcR3, Ibtisam_El,
  // ARC170 (Republic)
  OddBall_ElSnCcAf, Jag_PrItR2SfSu, SquadSevenVet_EhPtR4NtSf, Wolffe_OuR4VtPc, Sinker_Eh, OneOhFourthBP_DeR4,
  // A/SF-01 B-wing (Rebel)
  BStramm_TsJb, BladeSqVet_ElTb, TNumb_SlAsHlSu, BlueSqPlt_As,
  BStramm_SlJbHlEb, BladeSqVet_PrJbIc, TNumb_ElEb, BlueSqPlt_FcAp,
  GMoonsong_ElAfPsSsAu, BladeSqVet_SsAdAuSsPt, BlueSqPlt_PsSsPt,
  HSyndulla_B6DsPsSw, BlueSqPlt_PsAbSs, NPollart_DgAbSs, BladeSqVet_SsPsSlPtSuSs,
  // AttackShuttle (Rebel)
  HSyndulla_ElIcPh, SWren_OmDtPh,
  EBridger_AS, ZOrrelios_AS,
  // Auzituck (Rebel)
  Wullffwarro_SeGnNtHu, Lowhhrick, KashyyykDef_Nt,
  // Belbullab-22 (Separatist)
  GenGrievous_TrIpSoTv, WTambor_InKrSu, SkakoanAce_CsAf, CaptSear_DaKrIpSd, FOAutopilot_TvIp,
  // BTL-A4 Y-wing (Rebel)
  NWexley_EhIcVtR3, EVerlaine_EhIc, GoldSqVet_EhPtR3, HSalm_OuIcVtR5, DVander_PtR3, GraySqBomber_IcPbR5,
  // BTL-A4 Y-wing (Scum)
  Kavil_EhDtGePbAf, HiredGun_CsIcVtR3Cn, DRenthal_EhIcHg, CrymorahGoon_DtItR3IdPm,
  LKai_MsSsTsIcPtWsR4Cm, JinataSecOff_AtPtWsR3, AHadrassian_PrSgR5Td, Padric_DtGeTd, AmaxBomber_CtR5Td,
  // BTL-B Y-wing (Republic)
  ASkywalker_PrIcPtAtR2Pb, R2D2_IcC3Pb, Matchstick_ElIcR2PbDfSu, OddBall_PrIcPtR3Hu, Broadside_SsIcR5Hu, ShadowSqVet_SsIcPtR5, Goji_EpAf, RedSqBomber_IcR2PbDf,
  // BTL-S8 K-wing (Rebel)
  ETuketu_TsImPcCnPbAs, MDoni_PbAs, WardenSqPlt_BrBg,
  // Customized YT-1300 (Scum)
  HSolo_ElChAgRcTsLm_ORP_Tb, LCalrissian_HsQiLm_L337, L337_OuHsQiHuLm_LCalrissian_ElSu, FC_Lm_APD_Af,
  // Delta-7 (Republic)
  MWindu_SrR2D7AfSu, PKoon_BmSeR4Su, JediKnight_D7, OKenobi_PsR4SpCl, STiin_SrR4D7, ASkywalker_SrCl, ATano_BmR4, LUnduli_BeR4SuD7, BOffee_SeR2,
  // Droid Tri-Fighter (Separatist)
  DIST81_MsDm, PhlacArpProto_XxIb, SepInt_OuDmIb, DIS347_ElFcIc, FearsomePred_FcIb, ColInt_FcHuIc,
  // Eta-2 Actis (Republic)
  ASkywalker_MsEmR2AfSu, Yoda_HpPaSeR2, ASecura_PrSrR3, STi_ElPa, JediGen_MsEm, OKenobi_MsPaR5Hu_Ring,
  // E-wing (Rebel)
  CHorn_OmFcPtR2Af, RogueSqEsc_PrCdPtR3, GDarklighter_CsFcItR4, KnaveSqEsc,
  // FangFighter (Scum)
  FRau_DaAfHu, KSolus_Fe, ZealousRecruit_Pt, JRekkoff_PrItAfHu, OTeroch, SkullSqPlt_Fe,
  SkullSqPlt_MaApAf,
  // Fireball (Resistance)
  JYeager_ElAsMpTc, R1J5_MpAfChTc, KXiono_OuAsR1KfCh, ColStationMech_Ss,
  // Firespray (Scum)
  BFett_LwPcIdScSl, KScarlet_Ma, KTrelix_Cm, EAzzameen_ElPcIdPmScAn, KFrost_Pc, BountyHunter_PcIdSc,
  // Firespray (Separatist)
  JFett_ZwBfFtSi, BFett_DgHoSi, ZWesell_DsJfHgFtSi, SepRacket_JbSgTdAp,
  // G1A (Scum)
  FourLOM_ElAs00ZuBtMh, Zuckuss_LwTb4LMh, GandFindsman_FcFsDsEb,
  // HMP (Separatist)
  DGS286, OnderonOpp, DGS047, SepPredator, GeonosianProto, BaktoidDrone,
  // HWK290 (Rebel)
  JOrs_TsPcScCdEuMc, RGarnet_ElScHuSu, KKatarn_Mc, RebelScout_PbScSwEu,
  // HWK290 (Scum)
  DBonearm_FaCnSd, PGodalhi_DgJuCcSd, TMux_CdPm, SpiceRunner_DsPbEb_x2,
  GKey_ElFtEu,KJarrus_PaTd,Tapusk_CtGk,SpiceRunner_HoFtCbEu,
  // Hyena-class
  DBS404_PrPtAfLs, BaktoidProto_PsBrHu, TUBomber_PtEpDf, DBS32C_TaSdLs, BombardmentDrn_TsDfBg, SepBomber_PsDb,
  // JM5K (Scum)
  TTrevura_EhGnPtDs, ContractedSc_ItId, Dengar_EhPtR4CcPo, Manaroo_InPcPtFaSd,
  Dengar_PrHuPtSuR5PoCc, Manaroo_ElLrApDeSu, TTrevura_Lw00ApIn, ContractedSc_Pc,
  NLumb_SsDsPtAfDe, ContractedSc_Pt,
  // Kihraxz (Scum)
  TCobra_OuCmIdAfEbSu, VHel_CsSd, Graz_OuCmCcAf, BlackSunAce_PrSu, CaptJostero_ImMf, CartMarauder_CmHuMf,
  // LAAT/i (Republic)
  Hawk, Warthog, Hound, TwoOneTwoBatPlt,
  // Lambda (Imperial)
  ColJendon_CdIcDvFsSt, CaptKagi_CdTbEpSuSd, LtSai_CrGnAsJb, OmicronGrpPlt_AsJb,
  // LancerClass (Scum)
  KOnyo_OmRcSuSc, SWren_FeKoSc, AVentress_SeVtDsId, ShadowportHun_MaCc,
  // M3A (Scum)
  Serissu_Sd, GRed_JuCmMf, QJast_CsApAf, LAshera_JuCmMfSd, Inaldra_IcHuSu, TansariiPtVet_CsHl_x2, SBounder_PrCmAf, CartelSpacer_ItMf_x2,
  Serissu_ElHuJb, LAshera_OuAfIc, GRed_OuPt, TansariiPtVet_PrHuPt, QJast_ElAfIt, Inaldra_PrAfIt, SBounder_InSuIt, CartelSpacer_InJbIcSu, G4RGORVM_InDsAuSu, TansariiPtVet_OuMfPt, CartelSpacer_SsSuAu,
  // MG-100 (Resistance)
  FDallow_AoPtPbApHu, Cat_SbCnEb, CobaltSqBomber_TsPbAp, EKappehl_PaSnSbCnPb, Vennie_AoRtFiSu, BTeene_CnPb,
  PTico_RtPbSb, CobaltSqBomber_PsCnAgHu,
  // M12-L Kimogila (Scum)
  TKulda_SsPtCmR4IdSu, DOberos_EhR5Id, CartExecution_CsR5Cc,
  // Modified TIE/ln Fighter
  Ahhav_ElAfHu, MGSurveyor_StTsSuSd, OverseerYushyn, CaptSeevor_OuSu, ForemanProach_PrStHu, MGSentry,
  ForemanProach_FeCs,
  // Modified YT1300 (Rebel)
  HSolo_LwChMf, Chewbacca_PrC3LoR2HsLsUeMf, LCalrissian_StCmNnEuMf, OuterRimSmug_HmNtVtFaSd,
  Chewbacca_C3LoR2HsLsMf, HSolo_C3LoChRcMf, LCalrissian_StHmNnR3MfEuHu, OuterRimSmug_InLcRcEu,
  LOrgana_ChLcR2MfEu, OuterRimSmug_HmSu,
  // Naboo N-1
  ASkywalker_OuHpCdPtR2, ROlie_DaR2, NabooHandmaiden_PtR5, PAmidala_ElCd, DEllberger_PsPtR3, BravoFlightOff_PsPtR2,
  // Nantex-class Starfighter
  SFac_EnPrAfSu, Chertek_GdJuTc, PetArenaAce_EnSsSd, BKret_EnSsHu, Gorgol_GdSuSd, StalgasinHG_EnGdTc,
  // Nimbus-class V-wing (Republic)
  Contrail_IlR7Ri, Klick_R3TdAb, ShadowSqEsc_OuR2Ae, Oddball_ElIlQ7Pi, WTarkin_R3Ae, LoyalistVol,
  // Quadjumper (Scum)
  ConstZuvio_OuRcCnSu, JGunrunner_NtPmEb_x2, SPlank_UpFaScHuSu, UPlutt_NtCcPmAf,
  // Resistance Transport
  CNell_CoLoKs, PNGoode_AuR5KcSp, NChavdri_PtAdR2, LogDivPlt_PtLdAh,
  // Resistance Transport Pod
  BB8_AuAf, VMoradi_Ga, RTico_Pz, Finn_Pr,
  // RZ1Awing (Rebel)
  ACrynyd_InPrHu, GreenSqPlt_DaCmSu, JFarrell_OuCm, PhoenixSqPlt_Pr_x2,
  JFarrell_DaJuPr, GreenSqPlt_JuOuHu, ACrynyd_InJuCm, PhoenixSqPlt_DaPrAf,
  HSyndulla_Ho, SWren_Ou, PhoenixSqPlt_Ho, WAntilles_DsXx, DKlivian_ElMp, PhoenixSqPlt_El, ATano_MsPaSe, PhoenixSqPlt_Ms, SBey_MsSsVc, GreenSqPlt_SsMpVc,
  // RZ-2 A-wing (Resistance)
  LLampar_PtHm, GSonnel_ElAf, GreenSqExp_HePtHu, TLintra_PrFp, ZBangel_OuPr, BlueSqRecruit_PtHmSu, GSonnel_PtMa, ZTlo_ElAoSsAf, RBlario_SsPaMp, BlueSqRecruit_CoPtSs,
  SVanik_DdSsHu, WTyce_ElSsCm, BlueSqRecruit_SsAtCm, SJavos_SsSsImSu, MCobben_PrDsAtSu, GreenSqExp_SsPtAf,
  // Scavenged YT1300 (Resistance)
  HSolo_Ch, Rey_FiBbIdEuRm, Chewie_ReEuRm, ResSymp_DgC3ChHs,
  // Scurrg H-6 Bomber (Scum)
  CaptNym_SlTsR4BgHa, SSixxa_IcSbCnPm, LRevenant_DtBg,
  // Sheathipede (Rebel)
  FRau_OuSnR4SdPh, EBridger_HpChAfPh,
  FRau_ElR2KjPh, AP5_ChSuAfPh, EBridger_HaMaR5HuPh, ZOrrelios_OuHuHsPh,
  // Sith Infiltrator
  DMaul_HaHlPcDrSuSc, CDooku_BePsItGgHuSc, O66_CpSuSc, DarkCourier_CdGgK2Sc,
  // StarViper (Scum)
  PXizor_PrFcSuVi, BSEnforcer_Cd, Guri_DaAsAp, DOberos_OuFcPtCc, BSAssassin_Fe,
  // T65Xwing (Rebel)
  LSkywalker_IaPtR2Ss, RedSqVet_PrR5Ss, JPorkins_OuR5AfHuSs, BlueSqEsc_PtR3Ss, WAntilles_OuPtR4SuSs, BDarklighter_SeSs, TKyrell_ElItR2AfHuSs, GDreis_Ss,
  KSperado_OnR2DsAfHuSs, ETwoTubes_TsR4SdSs, LTenza_ElR2DsAfSuSs, CAZealot_R2DsSs,
  TKyrell_SsSdOu,
  // T70Xwing (Resistance)
  PDameron_PtBbBoIsAf, JPava_R5IsHu, BlackSqAce_PtM9Is, EAsty_ElIsAf, JSeastriker_R2IsSu, JTubbs, NNunb_ElIsAf, LtBastian_TsPtR3Is, RedSqExpert, TWexley_OuPtIs, KKun_PrIsAf, BlueSqRookie_Bb,
  PDameron_BtDdR2OtBoIs, BlackSqAce_DsPtPtR3AfIs, TWexley_SsR6UbHuIs, BlueSqRookie_AtR2Is, CThrenalli_BtPtPtR4Is, NChireen_AutoTgtPri, RedSqExp_PrUbIs,
  // TIEAdvV1 (Imperial)
  GrandInq_SrCm, Inquisitor_IaCmMf, SeventhSister_Hm, BaronOfEmp_CsPrSd,
  GrandInq_HaSuPr, BaronOfEmp_ElAfOu, SeventhSister_PsAfPrHu, Inquisitor_HpPr,
  FifthBrother_Fo, BaronOfEmp_SsMp, Inquisitor_BeAf,
  // TIEAdvX1 (Imperial)
  DVader_SrFcCmAfSu, MStele_RuFcCmSu, StormSqAce_Fc, VFoslo_OuFcCmSu, ZStrom_SlFcCmSu, TempestSqPlt_Cm,
  // TIE/ag Aggressor (Imperial)
  LtKestal_ElBrSu, OnyxSqScout_OuDtPr, DblEdge_IcCmHg, SienSpecialist_IcHmVtHu,
  // TIE/ba Interceptor
  Holo_PtMfMpHu, Ember_ElCmPrAf, MajVonreg_OuMpDp, FOProvocateur_Ss,
  // TIE/ca Punisher (Imperial)
  Deathrain_TsHmBgAp, CutlassSqPlt_TsImSbPb, Redline_DgCm, CutlassSqPlt_AsPrCn,
  // TIE/d Defender (Imperial)
  CountessRyad_OuAf, OnyxSqAce, RBrath_JuCdCm, DeltaSqPlt_Ic, ColVessery_JuFcCm, OnyxSqAce_ElAsPr,
  RBrath_ElImAs, ColVessery_OuHuTb, CountessRyad_PrTbAsIm, OnyxSqAce_OuSuAs, DeltaSqPlt_TbIm,
  DVader_SeMpTd, OnyxSqAce_ItTbTd, VSkerris_d_DdPrMpId, CaptDobbs_DiTbTd, DeltaSqPlt_AbTd,
  // TIE/fo Fighter (First Order)
  Midnight_Af, Static_Ou, OmegaSqAce_ElFaHu, Scorch_FaHu, Longshot_ElPr, ZetaSqPlt_AoSu, Muse_SlAo, Null_StAfSu, EpsilonSqCadet_TsAf, CmdrMalarus_Ao, TN3465_TsSu, LtRivas,
  // TIE/in Interceptor (Imperial)
  SFel_OuAfSd, AlphaSqPlt_Ap_x2, TPhennir_DaEb, SaberSqAce_PrSd,
  SFel_DaAfPrSu, SaberSqAce_PrSu, TPhennir_Ou, AlphaSqPlt_HuSu,
  VSkerris_DdElPrSc, CmdtGoran_DiMsSc, AlphaSqPlt_DiSdSc, GHask_DiElSuTcSc, LtLorrir_OuSc, SaberSqAce_DdTcSc, CRee_DiSc, NWindrider_DdSuSc, AlphaSqPlt_SuTcSc,
  // TIE/ln Fighter (Imperial)
  VRudor_ElSu, BlackSqAce_OuAfSu, AcademyPlt, IVersio_OuSu, NightBeast_PrHuSu, ObsidianSqPlt, SSkutu_PrSu, Wampa_CsHuSd, BlackSqAce_CsAfSu, GHask_CsSu, DMeeko_JuSd, Howlrunner_JuSu, SMarana_MaAf, BlackSqAce_JuSd_x2, ObsidianSqPlt_OuSu_x2, AcademyPlt_x2,
  IVersio_PtLw,
  // TIE/ln Fighter (Rebel)
  CaptRex_ElJuSd, SWren_OuCnHu, EBridger_SrZoHu, ZOrrelios,
  // TIE/ph Phantom (Imperial)
  Whisper_JuAsAkSd, SigmaSqAce_PrAsGi, Echo_LwCdPcSd, ImdaarTestPlt_Mj,
  // TIE/rb Heavy
  Rampage_PrSlApAbTa, OnyxSqSentry_IlHlSuTa, LDree_DsHlIoMa, CaridaAcaCadet_SsSlMa,
  // TIEReaper (Imperial)
  Vizier_Dk, ScarifBsPlt_DtSu, MajVermeil_StTo, CaptFeroph_StIs,
  // TIE/sa Bomber (Imperial)
  TBren_CsPtPb, Deathfire_CmSbScPmEb, MajRhymer_InApCm, ScimitarSqPlt_ImPb_x2, CaptJonus_PtSu, GammaSqAce_CmSbBgSu,
  // TIE/sf (First Order)
  Quickdraw_JuCdHgAfSu, ZetaSqSurvivor_PaImSf, Backdraft_PaCdImSfSu, OmegaSqExpert_JuSf,
  CaptPhasma_OuAoImSfSu, LtLeHuse_ElMpAd, ZetaSqSurvivor_PtAoPs,
  // TIE/sk Striker (Imperial)
  Duchess_TsSu, BlackSqScout_SbPmHu, Countdown_Su, PlanetarySent_Cn_x2, PureSabacc_Sd, Duchess_OuCnAfHu, Countdown_ElPbSbSu, PureSabacc_TsSu, BlackSqScout_PrCn, PlanetarySent_PbSbHu,
  Vagabond_OuSbPbAf, BlackSqScout_SsTc, PlanetarySent_TsCn,
  // TIE/vn Silencer (FirstOrder)
  KRen_HaPsPtAp, Recoil_PrPt, FOTestPlt_OuHu, Blackout_ElAf, Avenger_PtAp, SJEngineer,
  Blackout_Ts,
  Rush_PtPtAd, FOTestPlt_PtPsPt, SJEngineer_SsMpPsAf,
  // Upsilon (FirstOrder)
  LtTavson_AsIcKrSlSu, LtDormitz_BcHtTb, StarkillerBsPlt, MajStridan_BcPaTbCpGh, CaptCardinal_IcPo, PetOffThanisson_CpTs,
  // UT-60D U-wing (Rebel)
  CAndor_FcJeBmPw, BRook_CaPw, HTobber_FcIcBiPcPw, BlueSqScout_AsToPw,
  MYarro_ElSgAsSuPw, SGerrera_MyPw, BTwoTubes_AsPw, PartRenegade_AsDsPw,
  K2SO_TsPsAdHuPw, PartRenegade_MySgDsPw,
  // V19 Torrent (Republic)
  OddBall_SsCmAfMf, Kickback_CsSc, Axe_JuHm, Swoop_CoPrSc, BlueSqProtector_DeSc, Tucker_CmMf, GoldSqTrooper_CmAf,
  // VCX100 (Rebel)
  KJarrus_IcHsChEbGh_Zeb_Ph, HSyndulla_ElDtKjGh_Ezra_SrDtPh, Chopper_IcZoGh_AP5_R4Ph, LR_DtLcGh_Zeb_R5Ph,
  HSyndulla_OuCdDtChKjGh, Chopper_HsZoGh, KJarrus_SeDtHuTsGh, LothalRebel_DtAg, AKallus_PsPtZoGhSu, LothalRebel_PsPt,
  // VT-49 Decimator (Imperial)
  CaptOicunn_InGmDa, RAdmChiraneau_StMtTo, PatrolLdr_InSsFb,
  RAdmChiraneau_PtVtGnPm, PatrolLdr_00BtDv, PatrolLdr_LwSsGiFb, CaptOicunn_AkDaTs,
  MKee_PtPmGnSu, PatrolLdr_HuTs,
  // Vulture-class (Separatist)
  HaorChallProto_EsAfSd, TradeFedDrone_Es, DFS081_PrGsHu, PrecHunter_CmAfSu, SepDrone_EsGsSu, DFS311_Gs, HaorChallProto_DmEsSd,
  // Xi (First Order)
  AgentTerex, CmdrMalarus, GHask_Xi, FOCourier,
  // YT2400 (Rebel)
  DRendar_EhTsPcRcOu, Leebo_OuId, WildSpaceFrin_CmVtCc,
  // YV666 (Scum)
  MEval_OuCmLrDeCc, LRazzi_BfBoDeFaSd, TSlaver_HgJhCcRc, Bossk_MaGrHt_NashtahPup, TSlaver_DsHt_NP_Pr,
  Bossk_PrBtHmHtGn_NP_PrFaHm, MEval_SlFaTbApTo, LRazzi_PrFaBo, TSlaver_HmGrCvFaFs,
  // Z95 (Rebel)
  ACracken_StCmHu, BanditSqPlt, LtBlount_OuPrSu, TalaSqPlt_Se,
  // Z95 (Scum)
  NSuhlak_LwHmCdHu, BSSoldier_EhCmDsHu_x2, KLeeachos_EhCmCcAf, BinayrePirate_Ds, NSuhlak_CsOuHuSd, KLeeachos_SsCmCmDsMf, BSSoldier_CsCmAfSu,
  Bossk_PrFaHmAf,
  Bossk_PrAfSs, BinayrePirate_SsAfCm, BSSoldier_Co,
};

enum class QBC {
  PDF_Rebel, PDF_Imperial, PDF_Scum, PDF_Resistance, PDF_FirstOrder, PDF_Republic, PDF_Separatist,
  // Aggressor (Scum)
  // Alpha-class Star Wing (Imperial)
  // ARC170 (Rebel)
  // ARC170 (Republic)
  ARC170_033_1, ARC170_033_2,
  // A/SF-01 B-wing (Rebel)
  ASF01Bwing_042_1, ASF01Bwing_042_2, ASF01Bwing_066,
  ASF01Bwing_083_1, ASF01Bwing_083_2,
  // AttackShuttle (Rebel)
  // Auzituck (Rebel)
  // Belbullab-22 (Separatist)
  Belbullab22_029_1, Belbullab22_029_2,
  // BTL-A4 Y-wing (Rebel)
  BTLA4Ywing_013_1, BTLA4Ywing_013_2,
  BTLA4Ywing_085_1, BTLA4Ywing_085_2,
  // BTL-A4 Y-wing (Scum)
  // BTL-B Y-wing (Republic)
  BTLBYwing_048_1, BTLBYwing_048_2, BTLBYwing_048_3, BTLBYwing_048_4,
  // Customized YT-1300 (Scum)
  CustYT1300_004_1, CustYT1300_004_2,
  // EscapeCraft (Scum)
  // Delta-7 (Republic)
  Delta7_032_1, Delta7_032_2, Delta7_034_1, Delta7_034_2,
  // Droid Tri-Fighter
  DroidTriFighter_081_1, DroidTriFighter_081_2,
  // EWing (Rebel)
  // Eta-2 Actis (Republic)
  Eta2Actis_079_1, Eta2Actis_079_2, Eta2Actis_079_3,
  // Fang Fighter (Scum)
  Fang_017_1, Fang_017_2, Fang_W1QBP,
  // Fireball (Resistance)
  Fireball_063_1, Fireball_063_2,
  // Firespray (Scum)
  Firespray_016_1, Firespray_016_2,
  // Firespray (Separatist)
  Firespray_082_1, Firespray_082_2,
  // G1A (Scum)
  // HMP (Separatist)
  HMP_071_1, HMP_071_2, HMP_071_3,
  // HWK290 (Rebel)
  // HWK290 (Scum)
  HWK290_085_1, HWK290_085_2,
  // Hyena-class
  Hyena_041_1, Hyena_041_2,
  // JM5K (Scum)
  JM5K_051_1, JM5K_051_2, JM5K_066,
  // Kihraxz (Scum)
  // Kimogila (Scum)
  // KWing (Rebel)
  // LAATi (Republic)
  LAATi_070_1, LAATi_070_2,
  // Lambda (Imperial)
  // LancerClass (Scum)
  // M3A (Scum)
  M3A_052_1, M3A_052_2, M3A_052_3, M3A_052_4, M3A_066,
  // MG-100 (Resistance)
  MG100_066,
  // Modified TIE/ln Fighter
  ModTIEln_023_1, ModTIEln_023_2,
  ModTIEln_W2QBP,
  // Modified YT1300 (Rebel)
  ModYT1300_039_1, ModYT1300_039_2, ModYT1300_066,
  // Naboo N-1
  NabooN1_040_1, NabooN1_040_2,
  // Nantex-class Starfighter
  Nantex_047_1, Nantex_047_2,
  // Nimbus-class V-wing (Republic)
  NimbusClass_080_1, NimbusClass_080_2,
  // Quadjumper (Scum)
  // Resistance Transport
  ResTrans_045_1, ResTrans_045_2,
  // Resistance Transport Pod
  ResTransPod_045_1, ResTransPod_045_2,
  // RZ1Awing (Rebel)
  RZ1Awing_061_1, RZ1Awing_061_2,
  RZ1Awing_083_1, RZ1Awing_083_2, RZ1Awing_083_3, RZ1Awing_083_4,
  // RZ2Awing (Resistance)
  RZ2Awing_022_1, RZ2Awing_022_2,
  RZ2Awing_066,
  RZ2Awing_W2QBP,
  RZ2Awing_068_1, RZ2Awing_068_2,
  // Scavenged YT1300 (Resistance)
  // Scurrg (Scum)
  // Sheathipede (Rebel)
  Sheathipede_049_1, Sheathipede_049_2,
  // Sith Infiltrator
  SithInf_030_1, SithInf_030_2,
  // StarViper (Scum)
  // T-65 X-wing (Rebel)
  T65Xwing_001_1, T65Xwing_001_2, T65Xwing_012_1, T65Xwing_012_2, T65Xwing_W1QBP,
  // T-70 X-wing (Resistance)
  T70Xwing_025_1, T70Xwing_025_2, T70Xwing_025_3, T70Xwing_025_4, T70XWing_068_1, T70XWing_068_2, T70XWing_068_3,
  // TIE Advanced v1 (Imperial)
  TIEAdvv1_050_1, TIEAdvv1_050_2, TIEAdvv1_066,
  // TIE Advanced x1 (Imperial)
  TIEAdvx1_015_1, TIEAdvx1_015_2,
  // TIE/ag Aggressor (Imperial)
  // TIE/ba Interceptor (First Order)
  TIEbaInt_062_1, TIEbaInt_062_2,
  // TIE/ca Punisher (Imperial)
  // TIE/d Defender (Imperial)
  TIEdDef_060_1, TIEdDef_060_2,
  TIEdDef_084_1, TIEdDef_084_2,
  // TIE/fo Fighter (First Order)
  TIEfoFi_026_1, TIEfoFi_026_2, TIEfoFi_026_3, TIEfoFi_026_4,
  // TIE/in Interceptor (Imperial)
  TIEinInt_059_1, TIEinInt_059_2,
  TIEinInt_084_1, TIEinInt_084_2, TIEinInt_084_3,
  // TIE/ln Fighter (Imperial)
  TIElnFi_001_1, TIElnFi_001_2, TIElnFi_014_1, TIElnFi_014_2, TIElnFi_W1QBP,
  // TIE/ln Fighter (Rebel)
  // TIEPhantom (Imperial)
  // TIE/rb Heavy
  TIErbHea_067_1, TIErbHea_067_2,
  // TIEReaper (Imperial)
  // TIE/sa Bomber (Imperial)
  // TIEsf (First Order)
  TIEsfFi_044_1, TIEsfFi_044_2, TIEsfFi_066,
  // TIE/sk Striker (Imperial)
  TIEskSt_038_1, TIEskSt_038_2, TIEskSt_066,
  // TIE/vn Silencer (FirstOrder)
  TIEvnSil_027_1, TIEvnSil_027_2, TIEvnSil_066,
  TIEvnSil_W2QBP,
  // Upsilon (FirstOrder)
  // UT-60D U-wing (Rebel)
  UT60DUwing_066,
  // V19 Torrent (Republic)
  V19Torrent_032_1, V19Torrent_032_2, V19Torrent_032_3,
  // VCX100 (Rebel)
  VCX100_049_1, VCX100_049_2, VCX100_066,
  // VT-49 Decimator (Imperial)
  VT49Deci_043_1, VT49Deci_043_2, VT49Deci_066,
  // Vulture-class (Separatist)
  Vulture_029_1, Vulture_029_2, Vulture_031_1, Vulture_031_2,
  // Xi (First Order)
  Xi_069_1, Xi_069_2,
  // YT2400 (Rebel)
  // YV666 (Scum)
  YV666_058_1, YV666_058_2,
  // Z95 (Rebel)
  // Z95 (Scum)
  Z95_037_1, Z95_037_2,
  Z95_058_1,
  Z95_066,
};

}
