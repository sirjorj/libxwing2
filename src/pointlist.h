#pragma once
#include "plt.h"
#include "keyword.h"
#include "upg.h"
#include "upgradetype.h"
#include <map>
#include <optional>
#include <stdint.h>
#include <vector>

namespace libxwing2 {

enum PtL {
  Launch,      // https://www.fantasyflightgames.com/en/news/2018/7/27/man-your-ships-1/
  Wave2,
  January2019, // https://www.fantasyflightgames.com/en/news/2019/1/28/balance-in-the-force/
  UpsNerf,     // https://www.fantasyflightgames.com/en/news/2019/2/28/the-battle-of-toronto/
  Wave3,
  July2019,
  Wave5,
  HugeShips,
  January2020,
  July2020,
  Wave7,
  Wave7b,
  Wave8,
  Wave9,
  June2021,
  Sept2021,
  Current = Sept2021
};

// Cost Type
enum class CsT {
  Const,
  Base,
  Agi,
  Init
};

// cost
class Cost {
 public:
  static Cost ByCost(int16_t c);
  static Cost ByAgi(int16_t a0, int16_t a1, int16_t a2, int16_t a3);
  static Cost ByBase(int16_t s, int16_t m, int16_t l);
  static Cost ByBase(int16_t s, int16_t m, int16_t l, int16_t h);
  static Cost ByInit(int16_t i0, int16_t i1, int16_t i2, int16_t i3, int16_t i4, int16_t i5, int16_t i6);
  static Cost ByInit(int16_t i0, int16_t i1, int16_t i2, int16_t i3, int16_t i4, int16_t i5, int16_t i6, int16_t i7, int16_t i8);

  CsT                  GetCsT()     const;
  std::vector<int16_t> GetCosts()   const;
  std::string          GetCostStr() const;

  bool operator==(const Cost& c) const;
  bool operator!=(const Cost& c) const;
  bool operator>(const Cost& c)  const;
  bool operator<(const Cost& c)  const;

 private:
  Cost(CsT t, std::vector<int16_t> c);
  CsT costType;
  std::vector<int16_t> costs;
};

struct PLPilot {
  Plt plt;
  bool hyp;
  Cost cost;
  UpgradeBar upgBar;
  KeywordList keywords;
};
bool operator==(const PLPilot& a, const PLPilot& b);
bool operator!=(const PLPilot& a, const PLPilot& b);

struct PLUpgrade {
  Upg upg;
  bool hyp;
  Cost cost;
  KeywordList keywords;
};
bool operator==(const PLUpgrade& a, const PLUpgrade& b);
bool operator!=(const PLUpgrade& a, const PLUpgrade& b);

struct PLDate {
  unsigned short year;
  unsigned char  month;
  unsigned char  date;
};



class PointListNotFound : public std::runtime_error {
 public:
  PointListNotFound(PtL plt);
  PointListNotFound(std::string plt);
};

// pointlist
class PointList {
 public:
  static std::vector<PtL> GetPtLs();
  static std::string      GetPtLStr(PtL);
  static PtL              GetPtL(std::string);

  static std::string GetName(PtL ptl);

  static PLDate GetAnnounceDate(PtL ptl);
  static PLDate GetEffectDate(PtL ptl);

  static std::optional<PLPilot>   Get(Plt plt, PtL ptl=PtL::Current);
  static std::optional<PLUpgrade> Get(Upg upg, PtL ptl=PtL::Current);

  static std::optional<bool>        GetHyperspace(Plt plt, PtL ptl=PtL::Current);
  static std::optional<Cost>        GetCost(Plt plt, PtL ptl=PtL::Current);
  static std::optional<UpgradeBar>  GetUpgradeBar(Plt plt, PtL ptl=PtL::Current);
  static std::optional<KeywordList> GetKeywordList(Plt plt, PtL ptl=PtL::Current);

  static std::optional<bool>       GetHyperspace(Upg upg, PtL ptl=PtL::Current);
  static std::optional<Cost>       GetCost(Upg upg, PtL ptl=PtL::Current);

  static std::map<PtL, std::vector<std::string>> GetUnchangedEntries();

 private:
  struct PLVersion {
    PtL ptl;
    std::string name;
    PLDate announceDate;
    PLDate effectDate;
    std::vector<PLPilot> pilots;
    std::vector<PLUpgrade> upgrades;
  };

  static std::vector<PLVersion> versions;

};

}
