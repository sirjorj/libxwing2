#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

enum class KWd {
  Assault,
  Awing,
  Bwing,
  BountyHunter,
  Clone,
  DarkSide,
  Droid,
  Freighter,
  Jedi,
  LightSide,
  Mandalorian,
  Partisan,
  Sith,
  Spectre,
  TIE,
  Xwing,
  YT1300,
  Ywing,
};

class KeywordNotFound : public std::runtime_error {
 public:
  KeywordNotFound(KWd k);
  KeywordNotFound(std::string xws);
};

class Keyword {
 public:
  static Keyword GetKeyword(KWd k);
  static std::vector<KWd> GetAllKWds();
  KWd         GetKWd()     const;
  std::string GetName()    const;

 private:
  KWd         kwd;
  std::string name;

  static std::vector<Keyword> keywords;

  Keyword(KWd         k,
          std::string n);
};

typedef std::vector<KWd> KeywordList;

}
