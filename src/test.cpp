#include <libxwing2/converter.h>
#include <libxwing2/glyph.h>
#include <stdio.h>



int main(int argc, char* argv[]) {
  { // faction
    printf("Factions - ");
    {
      std::vector<libxwing2::Fac> fac = libxwing2::converter::GetMissingFactions();
      if(fac.size()) {
	printf("MISSING:\n");
	for(libxwing2::Fac f : fac) {
	  printf("  %s\n", libxwing2::Faction::GetFaction(f).GetName().c_str());
	}
      } else {
	printf("OK\n");
      }
    }
  }

  { // ship
    printf("Ships - ");
    {
      std::vector<libxwing2::Shp> shp = libxwing2::converter::GetMissingShips();
      if(shp.size()) {
	printf("MISSING:\n");
	for(libxwing2::Shp s : shp) {
	  printf("  %s\n", libxwing2::Ship::GetShip(s).GetName().c_str());
	}
      } else {
	printf("OK\n");
      }
      // glyphs
      for(const libxwing2::Shp& shp : libxwing2::Ship::GetAllShps()) {
	try {
	  libxwing2::ShipGlyph sg = libxwing2::ShipGlyph::GetShipGlyph(shp);
	}
	catch(libxwing2::ShipGlyphNotFound &sgnf) {
	  printf("  Missing  glyph for %s\n", libxwing2::Ship::GetShip(shp).GetName().c_str());
	}
      }


    }
  }

  { // pilot
    printf("Pilots - ");
    {
      std::vector<libxwing2::Plt> plt = libxwing2::converter::GetMissingPilots();
      if(plt.size()) {
	printf("MISSING:\n");
	for(libxwing2::Plt p : plt) {
	  printf("  [%s] %s\n",
		 libxwing2::Pilot::GetPilot(p).GetShip().GetName().c_str(),
		 libxwing2::Pilot::GetPilot(p).GetName().c_str());
	}
      } else {
	printf("OK\n");
      }
    }
  }  
  ;
  { // upgradetype
    printf("Upgrade Types - ");
    {
      std::vector<libxwing2::UpT> upt = libxwing2::converter::GetMissingUpgradeTypes();
      if(upt.size()) {
	printf("MISSING:\n");
	for(libxwing2::UpT u : upt) {
	  printf("  %s\n", libxwing2::UpgradeType::GetUpgradeType(u).GetName().c_str());
	}
      } else {
      printf("OK\n");
      }
    }
  }  

  { // upgrade
    printf("Upgrades - ");
    {
      std::vector<libxwing2::Upg> upg = libxwing2::converter::GetMissingUpgrades();
      if(upg.size()) {
	printf("MISSING:\n");
	for(libxwing2::Upg u : upg) {
	  printf("  %s\n", libxwing2::Upgrade::GetUpgrade(u).GetName().c_str());
	}
      } else {
	printf("OK\n");
      }
    }
  }
  /*
  { // Points
    printf("PointLists - ");
    std::map<libxwing2::PtL, std::vector<std::string>> unchanged = libxwing2::PointList::GetUnchangedEntries();
    if(unchanged.size()) {
      printf("Unchanged Entries:\n");
      for(const std::pair<libxwing2::PtL, std::vector<std::string>>& unc : unchanged) {
	printf("  [%s]\n", libxwing2::PointList::GetPtLStr(unc.first).c_str());
	for(const std::string& name : unc.second) {
	  printf("    %s\n", name.c_str());
	}
      }
    } else {
      printf("OK\n");
    }
  }
  */
  return 0;
}
