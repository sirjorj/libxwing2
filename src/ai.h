#pragma once
#include "action.h"
#include "ship.h"

namespace libxwing2 {














enum class AiC {
 Tri_Lunge, // 2
 Tri_Retreat,
 Tri_Reposition,
 //
 Tri_Approach, // 5
 Tri_Sweep     // 6
};

struct AiAction {
  Act act;
  std::string instruction;
};

class AiCard {
 public:
  static AiCard              GetAiCard(AiC aic);
  static std::vector<AiCard> GetAllAiCards(Shp sh);



 private:
  AiC aic;
  std::string name;
  std::string maneuver;
  std::vector<AiAction> actions;
  Shp shp;
  uint8_t cardNum;
  uint8_t numOfCards;

  static std::vector<AiCard> aiCards;

  AiCard(AiC                   a,
	 std::string           n,
	 std::string           m,
	 std::vector<AiAction> ac,
	 Shp                   s,
	 uint8_t               c,
	 uint8_t               cs);
};

}
