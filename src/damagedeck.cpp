#include "damagedeck.h"
#include "helper.h"

namespace libxwing2 {

// deck
DeckNotFound::DeckNotFound(Dck d) : runtime_error("Deck not found (enum " + std::to_string((int)d) + ")") { }
DeckNotFound::DeckNotFound(std::string d) : runtime_error("Deck not found '" + d + "'") { }

std::vector<Deck> Deck::deck = {
  { Dck::CoreSet,  "Core Set" },
  { Dck::HugeShip, "Huge Ship" },
};

Deck Deck::GetDeck(Dck dck) {
  for(const Deck& deck : Deck::deck) {
    if(deck.GetDck() == dck) {
      return deck;
    }
  }
  throw DeckNotFound(dck);
}

Deck Deck::GetDeck(std::string dckstr) {
  for(const Deck& deck : Deck::deck) {
    if(deck.GetDckStr() == dckstr) {
      return deck;
    }
  }
  throw DeckNotFound(dckstr);
}

std::vector<Dck> Deck::GetAllDcks() {
  std::vector<Dck> ret;
  for(const Deck& deck : Deck::deck) {
    ret.push_back(deck.GetDck());
  }
  return ret;
}

Dck         Deck::GetDck()    const { return this->dck; }
std::string Deck::GetDckStr() const { return ToStringHelper(this->name); }
std::string Deck::GetName()   const { return this->name; }

Deck::Deck(Dck         d,
           std::string n)
  : dck(d), name(n) { }



// trait
TraitNotFound::TraitNotFound(Trt t) : runtime_error("Trait not found (enum " + std::to_string((int)t) + ")") { }

std::vector<Trait> Trait::trait = {
  { Trt::Crew,  "Crew" },
  { Trt::Pilot, "Pilot" },
  { Trt::Ship,  "Ship" },
};

Trait Trait::GetTrait(Trt trt) {
  for(const Trait& trait : Trait::trait) {
    if(trait.GetTrt() == trt) {
      return trait;
    }
  }
  throw TraitNotFound(trt);
}

Trt         Trait::GetTrt()  const { return this->trt; }
std::string Trait::GetName() const { return this->name; }

Trait::Trait(Trt         t,
             std::string n)
  : trt(t), name(n) { }



// precision shot
PrecisionShotNotFound::PrecisionShotNotFound(PrS p) : runtime_error("PrecisionShot not found (enum " + std::to_string((int)p) + ")") { }

std::vector<PrecisionShot> PrecisionShot::precisionShots = {
  { PrS::BridgeHit,         "Bridge Hit!",        {Arc::Bullseye},        "Lose all {CHARGE} from each of your {CREW} upgrades.  Lose all of your {FORCE}." },
  { PrS::DriveDamaged,      "Drive Damaged",      {Arc::FullRear},        "Gain 2 ion tokens." },
  { PrS::ControlsDisrupted, "Controls Disrupted", {Arc::FullFront},       "Gain 1 jam token." },
  { PrS::TurretDisabled,    "Turret Disabled",    {Arc::Left,Arc::Right}, "The attacker chooses 1 of your {HARDPOINT} upgrades that is not Offline.  Flip that card." },
};

PrecisionShot PrecisionShot::GetPrecisionShot(PrS p) {
  for(const PrecisionShot& ps : PrecisionShot::precisionShots) {
    if(ps.GetPrS() == p) {
      return ps;
    }
  }
  throw PrecisionShotNotFound(p);
}

PrS              PrecisionShot::GetPrS()  const { return this->prs; }
std::string      PrecisionShot::GetName() const { return this->name; }
std::vector<Arc> PrecisionShot::GetArcs() const { return this->arcs; }
std::string      PrecisionShot::GetText() const { return this->text; }

PrecisionShot::PrecisionShot(PrS p,
			     std::string n,
			     std::vector<Arc> a,
			     std::string t)
  : prs(p), name(n), arcs(a), text(t) {}

// damage card
DamageCardNotFound::DamageCardNotFound(DmgCrd dc) : runtime_error("Card not found '" + std::to_string((int)dc.first) + "/" + Deck::GetDeck(dc.second).GetName() + "'") { }

std::vector<DamageCard> DamageCard::damageCards = {
  // core set
  { Dmg::PanickedPilot,          {  1 },       2, Dck::CoreSet,  Trt::Pilot, {},                      "Panicked Pilot",           "Gain 2 stress tokens.  Then repair this card." },
  { Dmg::BlindedPilot,           {  2 },       2, Dck::CoreSet,  Trt::Pilot, {},                      "Blinded Pilot",            "While you perform an attack, you can modify your dice only by spending {FORCE} for their default effect.  Action: Repair this card." },
  { Dmg::WoundedPilot,           {  3 },       2, Dck::CoreSet,  Trt::Pilot, {},                      "Wounded Pilot",            "After you perform an action, roll 1 attack die.  On a {HIT} or {CRIT} result, gain 1 stress token.  Action: Repair this card." },
  { Dmg::StunnedPilot,           {  4 },       2, Dck::CoreSet,  Trt::Pilot, {},                      "Stunned Pilot",            "After you execute a maneuver, if you moved through or overlapped an obstacle, suffer 1 {HIT} damage." },
  { Dmg::ConsoleFire,            {  5 },       2, Dck::CoreSet,  Trt::Ship,  {},                      "Console Fire",             "Before you engage, roll 1 attack die.  On a {HIT} result, suffer 1 {HIT} damage.  Action: Repair this card." },
  { Dmg::DamagedEngine,          {  6 },       2, Dck::CoreSet,  Trt::Ship,  {},                      "Damaged Engine",           "Increase the difficulty of your turn maneuvers ({LTURN} and {RTURN})." },
  { Dmg::WeaponsFailure,         {  7 },       2, Dck::CoreSet,  Trt::Ship,  {},                      "Weapons Failure",          "While you perform an attack, roll 1 fewer attack die.  Action: Repair this card." },
  { Dmg::HullBreach,             {  8 },       2, Dck::CoreSet,  Trt::Ship,  {},                      "Hull Breach",              "Before you would suffer 1 or more {HIT} damage, suffer that much {CRIT} damage instead.  Action: Repair this card." },
  { Dmg::StructuralDamage,       {  9 },       2, Dck::CoreSet,  Trt::Ship,  {},                      "Structural Damage",        "While you defend, roll 1 fewer defense die." },
  { Dmg::DamagedSensorArray,     { 10 },       2, Dck::CoreSet,  Trt::Ship,  {},                      "Damaged Sensor Array",     "You cannot perform any actions except the {FOCUS} action and actions from damage cards.  Action: Repair this card." },
  { Dmg::LooseStabilizer,        { 11 },       2, Dck::CoreSet,  Trt::Ship,  {},                      "Loost Stabilizer",         "After you execute a non-straight maneuver ({STRAIGHT}), suffer 1 {HIT} damage and repair this card.  Action: Repair this card." },
  { Dmg::DisabledPowerRegulator, { 12 },       2, Dck::CoreSet,  Trt::Ship,  {},                      "Disabled Power Regulator", "Before you engage, gain 1 ion token.  After you execute an ion maneuver, repair this card." },
  { Dmg::FuelLeak,               { 13 },       4, Dck::CoreSet,  Trt::Ship,  {},                      "Fuel Leak",                "After you suffer 1 {CRIT} damage, suffer 1 {HIT} damage and repair this card.  Action: Repair this card." },
  { Dmg::DirectHit,              { 14 },       5, Dck::CoreSet,  Trt::Ship,  {},                      "Direct Hit!",              "Suffer 1 {HIT} damage.  Then repair this card." },

  // huge ship
  { Dmg::DamagedCircuitry,      {1,2,3,4,5},   5, Dck::HugeShip, Trt::Ship, {PrS::TurretDisabled},    "Damaged Circuitry",        "When you recover {ENERGY} during the End Phase, recover 1 fewer {ENERGY} to a minimum of 0.  Action: Repair this card." },
  { Dmg::ScrambledOrders,       {6,7,8},       3, Dck::HugeShip, Trt::Crew, {PrS::TurretDisabled},    "Scrambled Orders",         "Before you engage, gain 2 jam tokens.  Then repair this card.", },
  { Dmg::HullRupture,           {9,10},        2, Dck::HugeShip, Trt::Ship, {PrS::TurretDisabled},    "Hull Rupture",             "After defending, if you suffered {HIT}/{CRIT} damage, suffer 1 additional {HIT} damage for each of your faceup damage cards.  Then repair this card.  Action: Repair this card." },
  { Dmg::StabilizersDamaged,    {11,12,13},    3, Dck::HugeShip, Trt::Ship, {PrS::DriveDamaged},      "Stabilizers Damaged",      "Increase the difficulty of your left and right maneuvers." },
  { Dmg::PowerSurge,            {14,15,16,17}, 4, Dck::HugeShip, Trt::Ship, {PrS::DriveDamaged},      "Power Surge",              "After you execute a maneuver, lose 1 {ENERGY}, plus 1 {ENERGY} per ion token you have.  Then repair this card." },
  { Dmg::EngineCoolantLeak,     {18,19,20},    3, Dck::HugeShip, Trt::Ship, {PrS::DriveDamaged},      "Engine Coolant Leak",      "After you execute a sped 3-5 maneuver, roll 1 attack die.  On a {HIT}/{CRIT} result, suffer 1 matching damage, then repair this card.  Action: Repair this card." },
  { Dmg::PanickedCrew,          {21,22,23},    3, Dck::HugeShip, Trt::Crew, {PrS::ControlsDisrupted}, "Panicked Crew",            "You cannot gain green tokens or perform the {COORDINATE} or {JAM} action." },
  { Dmg::ExplodingColsoles,     {24,25},       2, Dck::HugeShip, Trt::Ship, {PrS::ControlsDisrupted}, "Exploding Consoles",       "After you perform an action, roll 1 attack die.  On a {HIT}/{CRIT} result, suffer 1 {HIT} damage, then repair this card." },
  { Dmg::CrewShaken,            {26,27,28},    3, Dck::HugeShip, Trt::Crew, {PrS::ControlsDisrupted}, "Crew Shaken",              "After you overlap a ship or obstacle, suffer 2 {CRIT} damage, then repair this card." },
  { Dmg::AtmosphereLeak,        {29,30},       2, Dck::HugeShip, Trt::Crew, {PrS::BridgeHit},         "Atmosphere Leak",          "You cannot perform actions on your upgrade cards.  Action: Repair this card." },
};

DamageCard DamageCard::GetDamageCard(DmgCrd dc) {
  for(const DamageCard& d : DamageCard::damageCards) {
    if((d.GetDmg() == dc.first) && (d.GetDck() == dc.second)) {
      return d;
    }
  }
  throw DamageCardNotFound(dc);
}

std::vector<Dmg> DamageCard::GetAllDmgs(Dck d) {
  std::vector<Dmg> ret;
  for(const DamageCard& dc : DamageCard::damageCards) {
    if(dc.GetDck() == d) {
      ret.push_back(dc.GetDmg());
    }
  }
  return ret;
}

Dmg                          DamageCard::GetDmg()          const { return this->dmg; }
std::vector<uint8_t>         DamageCard::GetIds()           const { return this->ids; }
uint8_t                      DamageCard::GetCount()         const { return this->count; }
Dck                          DamageCard::GetDck()           const { return this->dck; }
Deck                         DamageCard::GetDeck()          const { return Deck::GetDeck(this->dck); }
Trt                          DamageCard::GetTrt()           const { return this->trt; }
Trait                        DamageCard::GetTrait()         const { return Trait::GetTrait(this->trt); }
std::optional<PrS>           DamageCard::GetPrS()           const { return this->precShot; }
std::optional<PrecisionShot> DamageCard::GetPrecisionShot() const {
  if(this->precShot) { return PrecisionShot::GetPrecisionShot(*this->precShot); }
  else               { return std::nullopt; }
}
std::string                  DamageCard::GetName()          const { return this->name; }
std::string                  DamageCard::GetText()          const { return this->text; }

DamageCard::DamageCard(Dmg                  dm,
		       std::vector<uint8_t> id,
		       uint8_t              c,
                       Dck                  dc,
                       Trt                  tr,
		       std::optional<PrS>   ps,
		       std::string          na,
                       std::string          tx)
  : dmg(dm), ids(id), count(c), dck(dc), trt(tr), precShot(ps), name(na), text(tx) { }
}
