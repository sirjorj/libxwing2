#pragma once
#include "release.h"
#include <map>
#include <string>

namespace libxwing2 {

class XWI {
 public:
  XWI();
  XWI(std::istream& s);
  XWI(std::string f);
  std::string Serialize(bool shrink=false) const;
  void Save(std::string f, bool shrink=false) const;
  void Set(Rel rel, int val);
  void Set(Shp shp, int val);
  void Set(Plt plt, int val);
  void Set(Upg upg, int val);
  void Add(Rel rel, int val);
  void Add(Shp shp, int val);
  void Add(Plt plt, int val);
  void Add(Upg upg, int val);
  int Get(Rel rel) const;
  int Get(Shp shp) const;
  int Get(Plt plt) const;
  int Get(Upg upg) const;
  bool IsEmpty() const;
  XWI GetResolved();

 private:
  void Init();
  void Load(std::istream& s);
  std::map<Rel, int> expansions;
  std::map<Shp, int> ships;
  std::map<Plt, int> pilots;
  std::map<Upg, int> upgrades;
};

}
