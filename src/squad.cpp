#include "pointlist.h"
#include "squad.h"
#include <map>
#include <sstream>

namespace libxwing2 {

Squad::Squad()
  : name("")
  , description("")
  , fac({})
{
}

void                   Squad::SetName(std::string n)        { this->name = n; }
std::string            Squad::GetName() const               { return this->name; }
void                   Squad::SetDescription(std::string d) { this->description = d; }
std::string            Squad::GetDescription() const        { return this->description; }
void                   Squad::SetFac(Fac f)                 { this->fac = f; }
std::optional<Fac>     Squad::GetFac()                      { return this->fac; }
void                   Squad::AddPilot(Pilot p)             { this->pilots.push_back(p); }
std::vector<Pilot>&    Squad::GetPilots()                   { return this->pilots; }
std::optional<int16_t> Squad::GetCost(PtL ptl) {
  int ret = 0;
  for(const Pilot& p : this->GetPilots()) {
    std::optional<int16_t> pCost = p.GetModCost(ptl);
    if(!pCost) { return std::nullopt; }
    else { ret += *pCost; }
  }
  return ret;
}

std::vector<std::string> Squad::Validate() {
  std::vector<std::string> ret;
  int16_t cost = 0;
  std::map<std::string, std::pair<uint8_t, uint8_t>> limited;

  for(const Pilot& p : this->GetPilots()) {
    if(p.GetModCost()) {
      cost += *p.GetModCost();
    }

    // faction
    if(this->GetFac() && (*this->GetFac() == p.GetFaction().GetFac())) {
      std::ostringstream oss;
      oss << "Squad is " << (this->GetFac() ? Faction::GetFaction(*this->GetFac()).GetName() : "*none*")
	  << " but pilot " << p.GetName() << " is " << p.GetFaction().GetName();
      ret.push_back(oss.str());
    }

    // limited
    if(p.GetLimited() > 0) {
      std::string n = p.GetName();
      if(limited.find(n) == limited.end()) {
    	limited[n] = {0, p.GetLimited() };
      }
      if(++limited[n].first > p.GetLimited()) {
	ret.push_back("There are " + std::to_string(limited[n].first) + " " + n + " cards in this squad but the limit is " + std::to_string(limited[n].second));
      }
    }

    // upgrades...
    std::map<std::string, std::pair<uint8_t, uint8_t>> perPilots;
    std::optional<UpgradeBar> openSlots = p.GetModUpgradeBar();
    if(openSlots) {
      for(const Upgrade &u : p.GetAppliedUpgrades()) {

	// limited
	if(u.GetLimited() > 0) {
	  std::string n = u.GetName();
	  if(limited.find(n) == limited.end()) {
	    limited[n] = {0, u.GetLimited() };
	  }
	  if(++limited[n].first > limited[n].second) {
	    ret.push_back("There are " + std::to_string(limited[n].first) + " " + n + " cards in this squad but the limit is " + std::to_string(limited[n].second));
	  }
	}

	// no duplicate upgrades per pilot
	std::string n = u.GetName();
	if(perPilots.find(n) == perPilots.end()) {
	  perPilots[n] = {0, 1};
	}
	if(++perPilots[n].first > perPilots[n].second) {
	  ret.push_back(p.GetName() + " contains " + std::to_string(perPilots[n].first) + " " + n + "upgrades");
	}

	// have slot
	if(std::find(openSlots->begin(), openSlots->end(), u.GetUpT()) == openSlots->end()) {
	  ret.push_back("No " + u.GetUpgradeType().GetName() + " slot for upgrade '" + u.GetName() + "'");
	} else {
	  auto it = std::find(openSlots->rbegin(), openSlots->rend(), u.GetUpT());
	  if(it != openSlots->rend()) {
	    openSlots->erase(std::next(it).base());
	  }
	}

	// upgrade restrictions
	std::vector<std::string> restIssues = u.GetRestriction().CheckRestrictions(p, u, this->GetPilots());
	for(const std::string& ri : restIssues) {
	  ret.push_back( u.GetName() + ": " + ri);
	}
      }
    }
  }

  return ret;
}




}
