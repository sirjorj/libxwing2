#pragma once
#include "action.h"
#include "chargeable.h"
#include "maneuver.h"
#include "modifier.h"
#include "pointlist.h"
#include "remote.h"
#include "restriction.h"
#include "shared.h"
#include "shipability.h"
#include "text.h"
#include "upg.h"
#include "upgradetype.h"
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// Misc
class Pilot;
class Upgrade;
typedef std::function<void(Upgrade&)> SetupF;



// Upgrade
std::string GetShortString(std::string s);

class UpgradeNotFound : public std::runtime_error {
 public:
  UpgradeNotFound(Upg         upg);
  UpgradeNotFound(std::string upgstr);
};

struct UpgradeSide {
  const UpT                      upgradeType;
  const std::vector<UpT>         slots;
  const std::string              title;
  const Chargeable               charge;
  const Chargeable               force;
  const std::optional<SecAttack> attackStats;
  const std::optional<SAb>       sab;
  const Modifier                 modifier;
  const Restriction              restriction;
  const bool                     hasAbility;
  const std::string              text;
};



class Upgrade {
 public:
  // factories
  static Upgrade GetUpgrade(Upg upg);
  static Upgrade GetUpgrade(std::string upgstr);
  static std::vector<Upg> GetAllUpgs();
  static std::vector<Upg> GetAllUpgs(UpT type);
  static std::vector<Upg> FindUpg(std::string u, std::vector<Upgrade> src=Upgrade::upgrades);
  static std::vector<Upg> Search(std::string s);

  // maintenance
  static void SanityCheck();

  // info (card)
  Upg         GetUpg()        const;
  std::string GetUpgStr()     const;
  std::string GetName()       const;
  std::string GetShortName()  const;
  uint8_t     GetLimited()    const;
  bool        IsUnreleased()  const;
  std::optional<Cost>        GetCost(PtL ptl=PtL::Current)       const;
  std::optional<bool>        GetHyperspace(PtL ptl=PtL::Current) const;
  std::optional<std::string> GetEffect() const;
  std::optional<Rem>         GetRem()    const;
  std::optional<Remote>      GetRemote() const;

  // info (side)
  UpT              GetUpT()         const;
  UpgradeType      GetUpgradeType() const;
  std::vector<UpT> GetSlots()       const;
  std::string      GetTitle()       const;
  std::string      GetShortTitle()  const;
  Chargeable       GetCharge()      const;
  Chargeable       GetForce()       const;
  std::optional<SecAttack>   GetAttackStats() const;
  std::optional<SAb>         GetSAb()         const;
  std::optional<ShipAbility> GetShipAbility() const;
  Modifier         GetModifier()    const;
  Restriction      GetRestriction() const;
  bool             HasAbility()     const;
  Text             GetText()        const;
  Text             GetText1()       const;
  Text             GetText2()       const;
  bool             IsEpicOnly()     const;

  // gamestate
  void Setup(Upgrade&);
  bool IsDualSided() const;
  void Flip();

 private:
  const Upg         upg;
  const std::string name;
  const uint8_t     limited;
  const std::optional<std::string> effect;
  const std::optional<Rem> rem;
  const SetupF      setup;

  uint8_t curSide;
  std::vector<UpgradeSide> sides;

  static std::vector<Upgrade> upgrades;

  Upgrade(Upg         u,
	  std::string n,
	  uint8_t     l,
	  SetupF      s,
	  UpgradeSide s1);

  Upgrade(Upg         u,
	  std::string n,
	  uint8_t     l,
	  SetupF      s,
	  UpgradeSide s1,
	  std::string e);

  Upgrade(Upg         u,
	  std::string n,
	  uint8_t     l,
	  SetupF      s,
	  UpgradeSide s1,
	  Rem         r);

  Upgrade(Upg         u,
	  std::string n,
	  uint8_t     l,
	  SetupF      s,
	  UpgradeSide s1,
	  UpgradeSide s2);

  Upgrade(Upg         u,
	  std::string n,
	  uint8_t     l,
	  SetupF      s,
	  UpgradeSide s1,
	  UpgradeSide s2,
	  std::string e);

  Upgrade(Upg         u,
	  std::string n,
	  uint8_t     l,
	  SetupF      s,
	  UpgradeSide s1,
	  UpgradeSide s2,
	  Rem         r);

  // The (now optional) GameState Object
  class GameState {
  public:
    GameState(Upgrade *u);

    bool IsEnabled() const;
    void Enable();
    void Disable();
    int8_t GetCurCharge() const;
    void ChargeUp();
    void ChargeDn();
    int8_t GetCurForce() const;
    void ForceUp();
    void ForceDn();

  private:
    Upgrade *upgrade;
    bool isEnabled;
    int8_t charge;
    int8_t force;
  };
  std::optional<GameState> gameState;

 public:
  GameState& GS();

};
}
