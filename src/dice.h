#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// attack dice
enum class AtD {
  Blank = 0,
  Focus = 1,
  Hit   = 2,
  Crit  = 3
};

class AttackDieNotFound : public std::runtime_error {
 public:
  AttackDieNotFound(AtD a);
  AttackDieNotFound(std::string t);
};

class AttackDie {
 public:
  static AttackDie GetAttackDie(AtD a);
  static AttackDie GetAttackDie(std::string t);

  AtD GetAtD()               const;
  std::string GetName()      const;
  std::string GetShortName() const;
  std::string GetText()      const;

 private:
  AtD atd;
  std::string name;
  std::string shortName;
  std::string text;

  static std::vector<AttackDie> attackDie;

  AttackDie(AtD atd,
            std::string n,
            std::string s,
            std::string t);
};

class AttackDice {
 public:
  AttackDice();
  AttackDice(std::vector<AtD> dice);

  void Add(AtD d);
  std::vector<AttackDie> GetDice()       const;
  std::vector<AttackDie> GetSortedDice() const;
  void Focus();
  
 private:
  std::vector<AtD> dice;
};



// defense dice
enum class DeD {
  Blank = 0,
  Focus = 1,
  Evade = 4
};

class DefenseDieNotFound : public std::runtime_error {
 public:
  DefenseDieNotFound(DeD d);
  DefenseDieNotFound(std::string t);
};

class DefenseDie {
 public:
  static DefenseDie GetDefenseDie(DeD ded);
  static DefenseDie GetDefenseDie(std::string dedstr);

  DeD GetDeD()               const;
  std::string GetName()      const;
  std::string GetShortName() const;
  std::string GetText()      const;

 private:
  DeD         ded;
  std::string name;
  std::string shortName;
  std::string text;

  static std::vector<DefenseDie> defenseDie;

  DefenseDie(DeD         d,
             std::string n,
             std::string s,
             std::string t);
};



class DefenseDice {
 public:
  DefenseDice();
  DefenseDice(std::vector<DeD> dice);

  void Add(DeD d);
  std::vector<DefenseDie> GetDice()       const;
  std::vector<DefenseDie> GetSortedDice() const;
  void Focus();

 private:
  std::vector<DeD> dice;
};

}
