#include "remote.h"
#include "helper.h"

namespace libxwing2 {

std::vector<Remote> Remote::remotes = {
//  Remote             in name                    faction         ag hu  text
  { Rem::BuzzDroids,   0, "Buzz Droid Swarm",     Fac::Separatist, 3, 1, "After an enemy ship moves through or overlaps you, relocate to its front or rear guides (you are at range 0 of this ship).  You cannot overlap an object this way.  If you cannot be placed at either set of guides, you and that ship each suffer 1 {HIT} damage.[P][B]Engagement Phase:[/B] At your initiative, each enemy ship at range 0 suffers 1 {CRIT} damage." },
  { Rem::DRK1,         0, "DRK-1 Probe Droid",    Fac::Separatist, 3, 1, "While a friendly ship locks an object or jams an enemy ship, it may measure range from you.[P]After an enemy ship overlaps you, that ship rolls 1 attack die.  On a {FOCUS} result, you suffer 1 {HIT} damage.[P][B]System Phase:[/B] At your initiative, you may relocate using a [2 {LBANK}], [2 {STRAIGHT}], or [2 {RBANK}] template." },
  { Rem::SensBuoyBlue, 0, "Sensor Buoy (Blue)",   Fac::FirstOrder, 3, 2, "First Order buoys are often placed to warn of enemies approaching their hidden fortresses." },
  { Rem::SensBuoyRed,  0, "Sensor Buoy (Red)",    Fac::FirstOrder, 3, 2, "First Order buoys are often placed to warn of enemies approaching their hidden fortresses." },
  { Rem::Shuttle,      0, "Shuttle",              {},              1, 8, "While this remote defends, if a friendly ship is in the attack arc, add 1 {EVADE} result." },
  { Rem::TrackingTrp,  0, "Tracking Torpedo (A)", {},              3, 4, "[B]System Phase:[/B] Relocate this remote forward using the [3 {LBANK}], [3 {RBANK}], or [4 {STRAIGHT}] template.[P][B]Engagement Phase:[/B] If an object on which this remote has a lock is in its {FRONTARC} at range 0-1, this remote detonates.[P][B]End Phase:[/B] If this remote does not have a lock on any object, it [B]must[/B] acquire a lock on an object in its {FRONTARC}, if able.[P]After this remote is destroyed, roll 1 attack die; on a {HIT} or {CRIT} result, it detonates.  When this remote detonates, each ship, remote, and structure at range or in its {FRONTARC} at range 1 rolls 4 attack dice and suffers 1 damage for each matching {HIT} and {CRIT} result." },
};

RemoteNotFound::RemoteNotFound(Rem rem) : runtime_error("Remote not found (enum: " + std::to_string((int)rem) + ")") { }

Remote Remote::GetRemote(Rem rem) {
  for(const Remote& remote : Remote::remotes) {
    if(remote.GetRem() == rem) {
      return remote;
    }
  }
  throw RemoteNotFound(rem);
}

std::vector<Rem> Remote::GetAllRems() {
  std::vector<Rem> ret;
  for(const Remote& remote : Remote::remotes) {
    ret.push_back(remote.GetRem());
  }
  return ret;
}

Rem                Remote::GetRem()        const { return this->rem; }
uint8_t            Remote::GetInitiative() const { return this->initiative; }
std::string        Remote::GetName()       const { return this->name; }
std::optional<Fac> Remote::GetFac()        const { return this->fac; }
Text               Remote::GetText()       const { return Text(this->text); }
uint8_t            Remote::GetAgility()    const { return this->agility; }
uint8_t            Remote::GetHull()       const { return this->hull; }

  Remote::Remote(Rem r, uint8_t i, std::string n, std::optional<Fac> f, uint8_t a, uint8_t h, std::string t)
  : rem(r), initiative(i), name(n), fac(f), agility(a), hull(h), text(t) {
}

}
