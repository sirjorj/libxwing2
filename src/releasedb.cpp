#include "release.h"

namespace libxwing2 {

#if 0
// Ship
#define SH(a,b)  std::make_shared<ShTokens>(Plt::a,Plt::b)
// ToKen
#define TK(t)    std::make_shared<Tokens>(Tok::t,1)
// TokenS
#define TS(t,c)  std::make_shared<Tokens>(Tok::t,c)
// LocK
#define LK(n)    std::make_shared<LkTokens>(n,1)
#define LS(n,c)  std::make_shared<LkTokens>(n,c)
// ID
#define ID(n)    std::make_shared<IDTokens>(n,3)
#define IS(n,c)  std::make_shared<IDTokens>(n,c)
// Turrets
#define SST(f,c) std::make_shared<SSTTokens>(c,f)
#define SDT(f,c) std::make_shared<SDTTokens>(c,f)
#define LST(f,c) std::make_shared<LSTTokens>(c,f)
#define LDT(f,c) std::make_shared<LDTTokens>(c,f)
#define HST(f,c) std::make_shared<HSTTokens>(c,f)
#define HDT(f,c) std::make_shared<HDTTokens>(c,f)
#endif

std::vector<Release> Release::releases = {

// [Info]
// Rel  - enum key
// SKU  - product SKU
// ISBN - product ISBN
// Name - expansion name
// Type - expansion type (wave#, coreset, aces, epic)

// [Dates] (year, month, date)
// Announced - the date this expansion was announced
// Released  - the date this expansion was released

// [URLs]
// AnnouncementURL - url to expansion announcement article
// PreviewURLS     - url(s) to expansion preview article(s)
// ReleaseURL      - url to expansion release article

// [Items]

// [Ships] (ships included in the expansion)
// Ship - the ship
// Desc - a description (for alternate paint jobs)

// [Pilots] (pilots included in the expansion)
// XwsName - pilot name per xws format
// Faction - pilot faction
// Ship    - pilot ship

// [Upgrades]
// Type    - upgrade type
// XwsName - upgrade name per xws format

// [Conditions] (condition cards included in the expansion)
// XwsName - xws-formatted name of the condition card

// [Tokens] (tokens included in the expansion)
// TK(type)        - single token
// TS(type, count) - multiple tokens
// ID(num)         - id token set (3 of number 'num')
// TL(a,b)         - target lock set (red and blue with letters 'a' on one side and 'b' on the other)

//{ Rel, SKU, ISBN, Name, Type,
//  {Announced}, {Released},
//  { AnnouncementURL,
//    {PreviewURLs},
//    ReleaseURL},
//  { {Ship, Desc}, ... },
//  { Pilot, ... },
//  { Upgrade, ... },
//  { Condition, ... },
//  { Token, ...}
//},
  { Rel::SWX74, "SWX74", "841333105303", "Saw's Renegades Expansion Pack", RelGroup::FeWave14,
    {2018, 2,13}, {2018, 6,21},
    {
      { {2018, 2,13}, "https://www.fantasyflightgames.com/en/news/2018/2/13/save-the-dream/" },
    { { {2018, 6, 5}, "https://www.fantasyflightgames.com/en/news/2018/6/5/extreme-action/" } },
      { {2018, 6,21}, "https://www.fantasyflightgames.com/en/news/2018/6/21/built-on-hope/"}
    },
    "",
    { {Shp::T65XWing,"Blank & White"}, {Shp::UWing,"Blank & White"} },
    { Plt::KSperado, Plt::LTenza, Plt::ETwoTubes, Plt::CAZealot, Plt::CAZealot, Plt::CAZealot, Plt::SGerrera, Plt::MYarro, Plt::BTwoTubes, Plt::PartRenegade },
    { Upg::R3Astro, Upg::R4Astro, Upg::PvtWing, Upg::SmSFoils, Upg::MYarro, Upg::SGerrera, Upg::DeadmansSw, Upg::DeadmansSw, Upg::AdvSensors, Upg::TrickShot, Upg::TrickShot, Upg::ProtTrp },
    {},
    {},
      TokenCollection()
      .Dial(Shp::T65XWing,Fac::Rebel).Dial(Shp::UWing,Fac::Rebel)
      .Ship(Plt::KSperado,Plt::CAZealot).Ship(Plt::LTenza,Plt::CAZealot).Ship(Plt::ETwoTubes,Plt::CAZealot)
      .Ship(Plt::SGerrera,Plt::BTwoTubes).Ship(Plt::MYarro,Plt::PartRenegade)
  },

  { Rel::SWX75, "SWX75", "841333105310", "TIE Reaper Expansion Pack", RelGroup::FeWave14,
    {2018, 2,13}, {2018, 6,21},
    {
      { {2018, 2,13}, "https://www.fantasyflightgames.com/en/news/2018/2/13/save-the-dream/" },
    { { {2018, 6,15}, "https://www.fantasyflightgames.com/en/news/2018/6/15/transporting-terror/" } },
      { {2018, 6,21}, "https://www.fantasyflightgames.com/en/news/2018/6/21/built-on-hope/" }
    },
    "",
    { {Shp::TIEReaper,""} },
    { Plt::MajVermeil, Plt::CaptFeroph, Plt::Vizier, Plt::ScarifBsPlt },
    { Upg::DeathTr, Upg::DirKrennic, Upg::ISBSlicer, Upg::ISBSlicer, Upg::TactOff, Upg::TactOff, Upg::Juke, Upg::Juke, Upg::SwarmTac, Upg::SwarmTac },
    { Cnd::OptProt },
    {},
    TokenCollection()
    .Dial(Shp::TIEReaper,Fac::Imperial)
    .Ship(Plt::MajVermeil,Plt::Vizier).Ship(Plt::CaptFeroph,Plt::ScarifBsPlt)
  },

  { Rel::SWZ01, "SWZ01", "841333105587", "X-Wing Second Edition", RelGroup::CoreSet,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/x-wing-second-edition/" },
    { { {2018, 5, 9}, "https://www.fantasyflightgames.com/en/news/2018/5/9/a-few-maneuvers/" },
      { {2018, 5,21}, "https://www.fantasyflightgames.com/en/news/2018/5/21/take-action-1/" },
      { {2018, 5,29}, "https://www.fantasyflightgames.com/en/news/2018/5/29/begin-engagement/" },
      { {2018, 6, 4}, "https://www.fantasyflightgames.com/en/news/2018/6/4/squad-selection/"} },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" },
    },
    "",
    { {Shp::T65XWing,""}, {Shp::TIElnFighter,""}, {Shp::TIElnFighter,""} },
    {
      Plt::LSkywalker, Plt::JPorkins, Plt::RedSqVet, Plt::BlueSqEsc,
      Plt::IVersio, Plt::BlackSqAce_Imp, Plt::BlackSqAce_Imp, Plt::VRudor, Plt::NightBeast, Plt::ObsidianSqPlt, Plt::ObsidianSqPlt, Plt::AcademyPlt, Plt::AcademyPlt
    },
    {
      Upg::R2Astro, Upg::R2D2, Upg::R3Astro, Upg::R5Astro, Upg::R5D8,
      Upg::SmSFoils,
      Upg::HeightPerc, Upg::HeightPerc, Upg::InstAim, Upg::Sense, Upg::Sense, Upg::SNReflex, Upg::SNReflex,
      Upg::Afterburn, Upg::HullUpg, Upg::ShieldUpg,
      Upg::Elusive, Upg::Outmaneuv, Upg::Predator,
      Upg::ProtTrp },
    {},
    {},
    {
      TokenCollection()
      .Dial(Shp::T65XWing,Fac::Rebel).Dial(Shp::TIElnFighter,Fac::Imperial).Dial(Shp::TIElnFighter,Fac::Imperial)
      .DialId(Shp::T65XWing).DialId(Shp::TIElnFighter).DialId(Shp::TIElnFighter)
      .Ship(Plt::LSkywalker,Plt::RedSqVet).Ship(Plt::JPorkins,Plt::BlueSqEsc)
      .Ship(Plt::IVersio,Plt::BlackSqAce_Imp).Ship(Plt::IVersio,Plt::BlackSqAce_Imp).Ship(Plt::VRudor,Plt::ObsidianSqPlt).Ship(Plt::VRudor,Plt::ObsidianSqPlt).Ship(Plt::NightBeast,Plt::AcademyPlt).Ship(Plt::NightBeast,Plt::AcademyPlt)
      .Crit(3).Disarm(1).Evade(3).Focus(4).ForceCharge(2).ID(3,1).ID(3,2).ID(3,3).ID(3,4).ID(3,5).ID(3,6).Ion(3).Lock(1,1).Lock(1,2).Lock(1,3).Lock(1,4).Lock(1,5).Lock(1,6).Shield(4).StandardCharge(6).Stress(5)
      .Asteroid(3).Debris(3)
      .FirstPlayer(1).Hyperspace(2)
    }
  },

/*
***
2019-11-12
Cancelled Products
We had previously announced that we were releasing the Saw's Renegades
Expansion Pack and the TIE Reaper Expansion Pack in "black boxes"—i.e. matching
the rest of the second edition product, no first edition cards included within. The
business team took a look at our stock, and it just didn't make sense to create
those. The "grey boxes" released at the end of first edition included both first
edition and second edition content, and there are still a ton of the grey boxes in
stores.

Because of that, we're pulling the black boxes from the release schedule, and will
not be producing those. Anyone interested in that content should just get the grey
boxes, which as I mentioned, should be readily available.

Let me know if there are any questions! Hopefully this shouldn't ruffle too many
feathers, but I'm sorry about the miscommunication!
***
  { Rel::SWZ02, "SWZ02", "841333105594", "Saw's Renegades Expansion Pack", RelGroup::Wave6,
    {2019, 8, 1}, {0000,00,00},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/echoes-of-war/" },
      {},
      { {0000,00,00}, ""}
    },
    { {Shp::T65XWing,"Blank & White"}, {Shp::UWing,"Blank & White"} },
    { Plt::KSperado, Plt::LTenza, Plt::ETwoTubes, Plt::CAZealot, Plt::CAZealot, Plt::CAZealot, Plt::SGerrera, Plt::MYarro, Plt::BTwoTubes, Plt::PartRenegade },
    { Upg::R3Astro, Upg::R4Astro, Upg::PvtWing, Upg::SmSFoils, Upg::MYarro, Upg::SGerrera, Upg::DeadmansSw, Upg::DeadmansSw, Upg::AdvSensors, Upg::TrickShot, Upg::TrickShot, Upg::ProtTrp },
    {},
    {},
    {
      //SH(KSperado,CAZealot), SH(LTenza,CAZealot), SH(ETwoTubes,CAZealot), SH(SGerrera,BTwoTubes), SH(MYarro,PartRenegade)
      TK(Evade), TS(Focus,2), TS(Shield,5), TS(StandardCharge,2), TS(Stress,2)
    }
  },

  { Rel::SWZ03, "SWZ03", "841333105600", "TIE Reaper Expansion Pack", RelGroup::Wave6,
    {2019, 8, 1}, {0000,00,00},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/echoes-of-war/" },
      {},
      { {0000,00,00}, "" }
    },
    { {Shp::TIEReaper,""} },
    { Plt::MajVermeil, Plt::CaptFeroph, Plt::Vizier, Plt::ScarifBsPlt },
    { Upg::DeathTr, Upg::DirKrennic, Upg::ISBSlicer, Upg::ISBSlicer, Upg::TactOff, Upg::TactOff, Upg::Juke, Upg::Juke, Upg::SwarmTac, Upg::SwarmTac },
    { Cnd::OptProt },
    {},
    {
      //SH(MajVermeil,Vizier), SH(CaptFeroph,ScarifBsPlt)
      ID(0), TS(Jam,2), LK(0), TS(Shield,2), TS(StandardCharge,2),
      TK(OptimizedPrototype)
    }
  },
*/

  { Rel::SWZ04, "SWZ04", "", "Lando's Millennium Falcon Expansion Pack", RelGroup::Wave1,
    {2018, 6, 7}, {2018, 9,13},
    {
      { {2018, 6, 7}, "https://www.fantasyflightgames.com/en/news/2018/6/7/fast-enough/" },
    { { {2018, 6,25}, "https://www.fantasyflightgames.com/en/news/2018/6/25/got-it-where-it-counts/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    { {Shp::CustYT1300,""}, {Shp::EscCraft,""} },
    {
      Plt::HSoloScum, Plt::LCalrissianScum, Plt::L337C, Plt::FreighterCapt,
      Plt::LCalrissian_EC, Plt::ORPioneer, Plt::L337E, Plt::AutoPltDrone
    },
    {
      Upg::ChewieScum, Upg::L337, Upg::LCalrissianScum, Upg::Qira, Upg::SeasonNav, Upg::TBeckett,
      Upg::AgileGunner, Upg::HSoloScu,
      Upg::RigCargoCh,
      Upg::TactScramb,
      Upg::Composure, Upg::Intimidat,
      Upg::LandosMF
    },
    {},
    {},
    {
      TokenCollection()
      .Dial(Shp::CustYT1300, Fac::Scum).Dial(Shp::EscCraft, Fac::Scum)
      .DialId(Shp::CustYT1300).DialId(Shp::EscCraft)
      .Ship(Plt::HSoloScum,Plt::L337C).Ship(Plt::LCalrissianScum,Plt::FreighterCapt)
      .Ship(Plt::LCalrissian_EC,Plt::L337E).Ship(Plt::ORPioneer,Plt::AutoPltDrone)
      .Calc(1).ID(3,9).Lock(2,9).Shield(5).StandardCharge(3)
      .LaDoTurret(1,Fac::Scum)
      .LooseCargo(1)
    }
  },

  { Rel::SWZ05, "SWZ05", "841333105624", "Dice Pack", RelGroup::Accessories,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/x-wing-second-edition/" },
      {},
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ06, "SWZ06", "841333105631", "Rebel Alliance Conversion Kit", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/all-wings-report-in-1/" },
    { { {2018, 8,13}, "https://www.fantasyflightgames.com/en/news/2018/8/13/fly-for-freedom/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    { },
    {
      Plt::NWexley_ARC, Plt::GDreis_ARC, Plt::SBey_ARC, Plt::Ibtisam,
      Plt::BStramm, Plt::TNumb, Plt::BladeSqVet, Plt::BladeSqVet, Plt::BlueSqPlt, Plt::BlueSqPlt,
      Plt::HSyndulla_AS, Plt::EBridger_AS, Plt::SWren_AS, Plt::ZOrrelios_AS,
      Plt::Wullffwarro, Plt::Lowhhrick, Plt::KashyyykDef, Plt::KashyyykDef,
      Plt::NWexley_YW, Plt::DVander, Plt::HSalm, Plt::EVerlaine, Plt::GoldSqVet, Plt::GoldSqVet, Plt::GraySqBomber, Plt::GraySqBomber,
      Plt::MDoni, Plt::ETuketu, Plt::WardenSqPlt, Plt::WardenSqPlt,
      Plt::CHorn, Plt::GDarklighter, Plt::RogueSqEsc, Plt::RogueSqEsc, Plt::KnaveSqEsc, Plt::KnaveSqEsc,
      Plt::JOrs, Plt::RGarnet, Plt::KKatarn, Plt::RebelScout, Plt::RebelScout,
      Plt::HSoloReb, Plt::LCalrissianReb, Plt::Chewbacca, Plt::OuterRimSmug,
      Plt::JFarrell, Plt::ACrynyd, Plt::GreenSqPlt, Plt::GreenSqPlt, Plt::PhoenixSqPlt, Plt::PhoenixSqPlt,
      Plt::FRau_Sh, Plt::EBridger_Sh, Plt::ZOrrelios_Sh, Plt::AP5,
      Plt::WAntilles_T65, Plt::TKyrell, Plt::GDreis_XW, Plt::BDarklighter, Plt::RedSqVet, Plt::RedSqVet, Plt::BlueSqEsc, Plt::BlueSqEsc,
      Plt::EBridger_TF, Plt::SWren_TF, Plt::CaptRex, Plt::ZOrrelios_TF,
      Plt::BRook, Plt::CAndor, Plt::HTobber, Plt::BlueSqScout,
      Plt::HSyndulla_VCX, Plt::KJarrus_VCX, Plt::Chopper, Plt::LothalRebel,
      Plt::DRendar, Plt::Leebo, Plt::WildSpaceFrin, Plt::WildSpaceFrin,
      Plt::ACracken, Plt::LtBlount, Plt::TalaSqPlt, Plt::TalaSqPlt, Plt::TalaSqPlt, Plt::BanditSqPlt, Plt::BanditSqPlt, Plt::BanditSqPlt
    },
    {
      Upg::ChopperA, Upg::R2Astro, Upg::R2Astro, Upg::R3Astro, Upg::R3Astro, Upg::R4Astro, Upg::R4Astro, Upg::R5Astro, Upg::R5Astro,
      Upg::HLC, Upg::HLC, Upg::IonCan, Upg::IonCan, Upg::JamBeam, Upg::JamBeam, Upg::TracBeam, Upg::TracBeam,
      Upg::PvtWing, Upg::PvtWing, Upg::SmSFoils, Upg::SmSFoils,
      Upg::BMalbus, Upg::C3POReb, Upg::CAndor, Upg::ChewieReb, Upg::ChopperC, Upg::FLSlicer, Upg::FLSlicer, Upg::Gonk, Upg::Gonk, Upg::HSyndulla, Upg::Informant, Upg::JErso, Upg::KJarrus, Upg::LCalrissianReb, Upg::LOrganaReb, Upg::NNunb, Upg::NovTech, Upg::NovTech, Upg::PerCPilot, Upg::PerCPilot, Upg::R2D2C, Upg::SWren_Crew, Upg::SeasonNav, Upg::SeasonNav, Upg::TactOff, Upg::TactOff, Upg::ZOrrelios,
      Upg::Bistan, Upg::EBridger, Upg::HSoloReb, Upg::HsGunner, Upg::HsGunner, Upg::LSkywalkerG, Upg::SkBombard, Upg::SkBombard, Upg::VetTailGun, Upg::VetTailGun, Upg::VetTurret, Upg::VetTurret,
      Upg::CloakDev, Upg::ContraCyb, Upg::ContraCyb, Upg::DeadmansSw, Upg::DeadmansSw, Upg::FeedbackAr, Upg::FeedbackAr, Upg::InertDamp, Upg::InertDamp, Upg::RigCargoCh, Upg::RigCargoCh,
      Upg::AblatPlat, Upg::AblatPlat, Upg::AdvSLAM, Upg::AdvSLAM, Upg::ElectBaff, Upg::ElectBaff, Upg::EngUpg, Upg::EngUpg, Upg::HullUpg, Upg::HullUpg, Upg::MuniFailSa, Upg::MuniFailSa, Upg::ShieldUpg, Upg::ShieldUpg, Upg::StaDcVanes, Upg::StaDcVanes, Upg::StealthDev, Upg::StealthDev, Upg::TactScramb, Upg::TactScramb,
      Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::IonMsl, Upg::IonMsl, Upg::PRockets, Upg::PRockets,
      Upg::Bomblet, Upg::ConnerNet, Upg::ConnerNet, Upg::ProtonBmb, Upg::ProtonBmb, Upg::ProxMine, Upg::ProxMine, Upg::SeismicCh, Upg::SeismicCh,
      Upg::AdvSensors, Upg::AdvSensors, Upg::CollisDet, Upg::CollisDet, Upg::FCS, Upg::FCS,
      Upg::CrackShot, Upg::CrackShot, Upg::Daredevil, Upg::Daredevil, Upg::DebGambit, Upg::DebGambit, Upg::Elusive, Upg::Elusive, Upg::ExpHan, Upg::ExpHan, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Marksman, Upg::Marksman, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Predator, Upg::Predator, Upg::SatSalvo, Upg::SatSalvo, Upg::Selfless, Upg::Selfless, Upg::Selfless, Upg::SquadLdr, Upg::SwarmTac, Upg::SwarmTac, Upg::TrickShot, Upg::TrickShot,
      Upg::Ghost, Upg::MilFalcon, Upg::MoldyCrow, Upg::Outrider, Upg::Phantom,
      Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::IonTrp, Upg::IonTrp, Upg::ProtTrp, Upg::ProtTrp,
      Upg::DorsalTrt, Upg::DorsalTrt, Upg::IonCanTrt, Upg::IonCanTrt
    },
    { Cnd::LstnDev, Cnd::SupFire },
    {},
    TokenCollection()
    .Dial(Shp::ARC170, Fac::Rebel).Dial(Shp::ARC170, Fac::Rebel)
    .Dial(Shp::AttackShuttle, Fac::Rebel).Dial(Shp::AttackShuttle, Fac::Rebel)
    .Dial(Shp::ASF01BWing, Fac::Rebel).Dial(Shp::ASF01BWing, Fac::Rebel)
    .Dial(Shp::Auzituck, Fac::Rebel).Dial(Shp::Auzituck, Fac::Rebel)
    .Dial(Shp::BTLA4YWing, Fac::Rebel).Dial(Shp::BTLA4YWing, Fac::Rebel)
    .Dial(Shp::BTLS8KWing, Fac::Rebel).Dial(Shp::BTLS8KWing, Fac::Rebel)
    .Dial(Shp::EWing, Fac::Rebel).Dial(Shp::EWing, Fac::Rebel)
    .Dial(Shp::HWK290, Fac::Rebel).Dial(Shp::HWK290, Fac::Rebel)
    .Dial(Shp::ModYT1300, Fac::Rebel).Dial(Shp::ModYT1300, Fac::Rebel)
    .Dial(Shp::RZ1AWing, Fac::Rebel).Dial(Shp::RZ1AWing, Fac::Rebel).Dial(Shp::RZ1AWing, Fac::Rebel)
    .Dial(Shp::Sheathipede, Fac::Rebel).Dial(Shp::Sheathipede, Fac::Rebel)
    .Dial(Shp::T65XWing, Fac::Rebel).Dial(Shp::T65XWing, Fac::Rebel)
    .Dial(Shp::TIElnFighter, Fac::Rebel).Dial(Shp::TIElnFighter, Fac::Rebel)
    .Dial(Shp::UWing, Fac::Rebel).Dial(Shp::UWing, Fac::Rebel)
    .Dial(Shp::VCX100, Fac::Rebel).Dial(Shp::VCX100, Fac::Rebel)
    .Dial(Shp::YT2400, Fac::Rebel).Dial(Shp::YT2400, Fac::Rebel)
    .Dial(Shp::Z95, Fac::Rebel).Dial(Shp::Z95, Fac::Rebel).Dial(Shp::Z95, Fac::Rebel).Dial(Shp::Z95, Fac::Rebel)
    .DialId(Shp::ASF01BWing).DialId(Shp::ASF01BWing)
    .DialId(Shp::BTLA4YWing).DialId(Shp::BTLA4YWing)
    .DialId(Shp::BTLS8KWing).DialId(Shp::BTLS8KWing)
    .DialId(Shp::EWing).DialId(Shp::EWing)
    .DialId(Shp::HWK290).DialId(Shp::HWK290)
    .DialId(Shp::ModYT1300).DialId(Shp::ModYT1300)
    .DialId(Shp::RZ1AWing).DialId(Shp::RZ1AWing).DialId(Shp::RZ1AWing)
    .DialId(Shp::T65XWing).DialId(Shp::T65XWing)
    .DialId(Shp::YT2400).DialId(Shp::YT2400)
    .DialId(Shp::Z95).DialId(Shp::Z95).DialId(Shp::Z95).DialId(Shp::Z95)
    .Ship(Plt::NWexley_ARC,Plt::GDreis_ARC).Ship(Plt::SBey_ARC,Plt::Ibtisam)
    .Ship(Plt::HSyndulla_AS,Plt::EBridger_AS).Ship(Plt::SWren_AS,Plt::ZOrrelios_AS)
    .Ship(Plt::Wullffwarro,Plt::KashyyykDef).Ship(Plt::Lowhhrick,Plt::KashyyykDef)
    .Ship(Plt::BStramm,Plt::BladeSqVet).Ship(Plt::TNumb,Plt::BlueSqPlt).Ship(Plt::BladeSqVet,Plt::BlueSqPlt)
    .Ship(Plt::CHorn,Plt::RogueSqEsc).Ship(Plt::GDarklighter,Plt::KnaveSqEsc).Ship(Plt::RogueSqEsc,Plt::KnaveSqEsc)
    .Ship(Plt::JOrs,Plt::RebelScout).Ship(Plt::RGarnet,Plt::RebelScout).Ship(Plt::KKatarn,Plt::RebelScout)
    .Ship(Plt::MDoni,Plt::WardenSqPlt).Ship(Plt::ETuketu,Plt::WardenSqPlt)
    .Ship(Plt::JFarrell,Plt::GreenSqPlt).Ship(Plt::ACrynyd,Plt::PhoenixSqPlt).Ship(Plt::GreenSqPlt,Plt::PhoenixSqPlt)
    .Ship(Plt::FRau_Sh,Plt::EBridger_Sh).Ship(Plt::ZOrrelios_Sh,Plt::AP5)
    .Ship(Plt::WAntilles_T65,Plt::RedSqVet).Ship(Plt::GDreis_XW,Plt::BlueSqEsc).Ship(Plt::BDarklighter,Plt::BlueSqEsc).Ship(Plt::RedSqVet,Plt::BlueSqEsc).Ship(Plt::TKyrell,Plt::TKyrell)
    .Ship(Plt::SWren_TF,Plt::EBridger_TF).Ship(Plt::CaptRex,Plt::ZOrrelios_TF)
    .Ship(Plt::BRook,Plt::HTobber).Ship(Plt::CAndor,Plt::BlueSqScout)
    .Ship(Plt::HSyndulla_VCX,Plt::Chopper).Ship(Plt::KJarrus_VCX,Plt::LothalRebel)
    .Ship(Plt::HSoloReb,Plt::Chewbacca).Ship(Plt::LCalrissianReb,Plt::OuterRimSmug)
    .Ship(Plt::DRendar,Plt::WildSpaceFrin).Ship(Plt::Leebo,Plt::WildSpaceFrin)
    .Ship(Plt::DVander,Plt::GoldSqVet).Ship(Plt::HSalm,Plt::GraySqBomber).Ship(Plt::GoldSqVet,Plt::GraySqBomber).Ship(Plt::NWexley_YW,Plt::NWexley_YW).Ship(Plt::EVerlaine,Plt::EVerlaine)
    .Ship(Plt::ACracken,Plt::TalaSqPlt).Ship(Plt::LtBlount,Plt::BanditSqPlt).Ship(Plt::TalaSqPlt,Plt::BanditSqPlt).Ship(Plt::TalaSqPlt,Plt::BanditSqPlt)
    .Calc(3).Cloak(1).Disarm(2).ForceCharge(4).Jam(2).Reinforce(2).StandardCharge(5).Tractor(2)
    .SmSiTurret(4,Fac::Rebel).SmDoTurret(2,Fac::Rebel).LaDoTurret(2,Fac::Rebel)
    .ListeningDev(1).SuppFire(1)
  },

  { Rel::SWZ07, "SWZ07", "841333105648", "Galactic Empire Conversion Kit", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/crush-the-rebellion-1/"},
    { { {2018, 8,27}, "https://www.fantasyflightgames.com/en/news/2018/8/27/pushing-the-envelope/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    { },
    {
      Plt::MajVynder, Plt::LtKarsabi, Plt::RhoSqPlt, Plt::RhoSqPlt, Plt::NuSqPlt, Plt::NuSqPlt,
      Plt::CaptKagi, Plt::ColJendon, Plt::LtSai, Plt::OmicronGrpPlt,
      Plt::GrandInq, Plt::SeventhSister, Plt::BaronOfEmp, Plt::BaronOfEmp, Plt::BaronOfEmp, Plt::Inquisitor, Plt::Inquisitor, Plt::Inquisitor,
      Plt::DVader_x1, Plt::MStele, Plt::VFoslo, Plt::ZStrom, Plt::StormSqAce, Plt::StormSqAce, Plt::TempestSqPlt, Plt::TempestSqPlt,
      Plt::LtKestal, Plt::OnyxSqScout, Plt::OnyxSqScout, Plt::DblEdge, Plt::SienSpecialist, Plt::SienSpecialist,
      Plt::TBren,  Plt::CaptJonus, Plt::MajRhymer, Plt::GammaSqAce, Plt::GammaSqAce, Plt::GammaSqAce, Plt::Deathfire, Plt::ScimitarSqPlt, Plt::ScimitarSqPlt, Plt::ScimitarSqPlt,
      Plt::RBrath, Plt::ColVessery, Plt::CountessRyad, Plt::OnyxSqAce, Plt::OnyxSqAce, Plt::DeltaSqPlt, Plt::DeltaSqPlt,
      Plt::Howlrunner, Plt::MMithel, Plt::SSkutu, Plt::DMeeko, Plt::GHask_TIEln, Plt::SMarana, Plt::BlackSqAce_Imp, Plt::BlackSqAce_Imp, Plt::BlackSqAce_Imp, Plt::BlackSqAce_Imp, Plt::ObsidianSqPlt, Plt::ObsidianSqPlt, Plt::ObsidianSqPlt, Plt::ObsidianSqPlt, Plt::AcademyPlt, Plt::AcademyPlt, Plt::AcademyPlt, Plt::AcademyPlt, Plt::Wampa,
      Plt::SFel, Plt::SaberSqAce, Plt::SaberSqAce, Plt::TPhennir, Plt::AlphaSqPlt, Plt::AlphaSqPlt,
      Plt::Whisper, Plt::Echo, Plt::SigmaSqAce, Plt::SigmaSqAce, Plt::ImdaarTestPlt, Plt::ImdaarTestPlt,
      Plt::Redline, Plt::Deathrain, Plt::CutlassSqPlt, Plt::CutlassSqPlt,
      Plt::Duchess, Plt::Countdown, Plt::PureSabacc, Plt::BlackSqScout, Plt::BlackSqScout, Plt::BlackSqScout, Plt::PlanetarySent, Plt::PlanetarySent, Plt::PlanetarySent,
      Plt::RAdmChiraneau, Plt::CaptOicunn, Plt::PatrolLdr, Plt::PatrolLdr
    },
    {
      Upg::HLC, Upg::HLC, Upg::IonCan, Upg::IonCan, Upg::JamBeam, Upg::JamBeam, Upg::TracBeam, Upg::TracBeam,
      Upg::Os1ArsenalLdt, Upg::Os1ArsenalLdt, Upg::Os1ArsenalLdt, Upg::Xg1AssaultCfg, Upg::Xg1AssaultCfg, Upg::Xg1AssaultCfg,
      Upg::AdmSloane, Upg::AgentKallus, Upg::CRee, Upg::DVader, Upg::EmpPalp, Upg::FLSlicer, Upg::FLSlicer, Upg::Gonk, Upg::Gonk, Upg::GrandInq, Upg::GrandMoffTarkin, Upg::Informant, Upg::MinTua, Upg::MoffJerjerrod, Upg::NovTech, Upg::NovTech, Upg::PerCPilot, Upg::PerCPilot, Upg::SeasonNav, Upg::SeasonNav, Upg::SeventhSister, Upg::TactOff, Upg::TactOff,
      Upg::Bomblet, Upg::ConnerNet, Upg::ConnerNet, Upg::ConnerNet, Upg::ProtonBmb, Upg::ProtonBmb, Upg::ProtonBmb, Upg::ProxMine, Upg::ProxMine, Upg::ProxMine, Upg::SeismicCh, Upg::SeismicCh, Upg::SeismicCh,
      Upg::FifthBrother, Upg::HsGunner, Upg::HsGunner, Upg::SkBombard, Upg::SkBombard, Upg::VetTurret, Upg::VetTurret,
      Upg::BarRockets, Upg::BarRockets, Upg::BarRockets, Upg::ClustMsl, Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::IonMsl, Upg::IonMsl, Upg::IonMsl, Upg::PRockets, Upg::PRockets, Upg::PRockets,
      Upg::AblatPlat, Upg::AblatPlat, Upg::AdvSLAM, Upg::AdvSLAM, Upg::ElectBaff, Upg::ElectBaff, Upg::HullUpg, Upg::HullUpg, Upg::MuniFailSa, Upg::MuniFailSa, Upg::ShieldUpg, Upg::ShieldUpg, Upg::StaDcVanes, Upg::StaDcVanes, Upg::StealthDev, Upg::StealthDev, Upg::TactScramb, Upg::TactScramb,
      Upg::AdvSensors, Upg::AdvSensors, Upg::CollisDet, Upg::CollisDet, Upg::FCS, Upg::FCS, Upg::TrajSim, Upg::TrajSim,
      Upg::CrackShot, Upg::CrackShot, Upg::CrackShot, Upg::Daredevil, Upg::Daredevil, Upg::DebGambit, Upg::DebGambit, Upg::Elusive, Upg::Elusive, Upg::ExpHan, Upg::ExpHan, Upg::Intimidat, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Marksman, Upg::Marksman, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Predator, Upg::Predator, Upg::Predator, Upg::Ruthless, Upg::Ruthless, Upg::Ruthless, Upg::SatSalvo, Upg::SatSalvo, Upg::SquadLdr, Upg::SwarmTac, Upg::SwarmTac, Upg::TrickShot, Upg::TrickShot, Upg::TrickShot,
      Upg::Dauntless, Upg::ST321,
      Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::IonTrp, Upg::IonTrp, Upg::IonTrp, Upg::ProtTrp, Upg::ProtTrp, Upg::ProtTrp,
      Upg::DorsalTrt, Upg::DorsalTrt, Upg::IonCanTrt, Upg::IonCanTrt
    },
    { Cnd::Hunted, Cnd::LstnDev },
    {},
    TokenCollection()
    .Dial(Shp::AlphaClass, Fac::Imperial).Dial(Shp::AlphaClass, Fac::Imperial)
    .Dial(Shp::Lambda, Fac::Imperial).Dial(Shp::Lambda, Fac::Imperial)
    .Dial(Shp::TIEAdvV1, Fac::Imperial).Dial(Shp::TIEAdvV1, Fac::Imperial).Dial(Shp::TIEAdvV1, Fac::Imperial)
    .Dial(Shp::TIEAdvX1, Fac::Imperial).Dial(Shp::TIEAdvX1, Fac::Imperial)
    .Dial(Shp::TIEagAggressor, Fac::Imperial).Dial(Shp::TIEagAggressor, Fac::Imperial).Dial(Shp::TIEagAggressor, Fac::Imperial)
    .Dial(Shp::TIEcaPunisher, Fac::Imperial).Dial(Shp::TIEcaPunisher, Fac::Imperial)
    .Dial(Shp::TIEdDefender, Fac::Imperial).Dial(Shp::TIEdDefender, Fac::Imperial)
    .Dial(Shp::TIEinInterceptor, Fac::Imperial).Dial(Shp::TIEinInterceptor, Fac::Imperial).Dial(Shp::TIEinInterceptor, Fac::Imperial)
    .Dial(Shp::TIElnFighter, Fac::Imperial).Dial(Shp::TIElnFighter, Fac::Imperial).Dial(Shp::TIElnFighter, Fac::Imperial).Dial(Shp::TIElnFighter, Fac::Imperial)
    .Dial(Shp::TIEphPhantom, Fac::Imperial).Dial(Shp::TIEphPhantom, Fac::Imperial)
    .Dial(Shp::TIEsaBomber, Fac::Imperial).Dial(Shp::TIEsaBomber, Fac::Imperial).Dial(Shp::TIEsaBomber, Fac::Imperial)
    .Dial(Shp::TIEskStriker, Fac::Imperial).Dial(Shp::TIEskStriker, Fac::Imperial).Dial(Shp::TIEskStriker, Fac::Imperial)
    .Dial(Shp::VT49, Fac::Imperial).Dial(Shp::VT49, Fac::Imperial).Dial(Shp::VT49, Fac::Imperial)
    .DialId(Shp::Lambda).DialId(Shp::Lambda)
    .DialId(Shp::TIEAdvX1).DialId(Shp::TIEAdvX1)
    .DialId(Shp::TIEcaPunisher).DialId(Shp::TIEcaPunisher)
    .DialId(Shp::TIEdDefender).DialId(Shp::TIEdDefender)
    .DialId(Shp::TIEinInterceptor).DialId(Shp::TIEinInterceptor).DialId(Shp::TIEinInterceptor)
    .DialId(Shp::TIElnFighter).DialId(Shp::TIElnFighter).DialId(Shp::TIElnFighter).DialId(Shp::TIElnFighter)
    .DialId(Shp::TIEphPhantom).DialId(Shp::TIEphPhantom)
    .DialId(Shp::TIEsaBomber).DialId(Shp::TIEsaBomber)
    .DialId(Shp::VT49).DialId(Shp::VT49)
    .Ship(Plt::MajVynder,Plt::RhoSqPlt).Ship(Plt::LtKarsabi,Plt::NuSqPlt).Ship(Plt::RhoSqPlt,Plt::NuSqPlt)
    .Ship(Plt::CaptKagi,Plt::ColJendon).Ship(Plt::LtSai,Plt::OmicronGrpPlt)
    .Ship(Plt::GrandInq,Plt::BaronOfEmp).Ship(Plt::SeventhSister,Plt::Inquisitor).Ship(Plt::Inquisitor,Plt::BaronOfEmp).Ship(Plt::Inquisitor,Plt::BaronOfEmp)
    .Ship(Plt::DVader_x1,Plt::StormSqAce).Ship(Plt::MStele,Plt::TempestSqPlt).Ship(Plt::ZStrom,Plt::TempestSqPlt).Ship(Plt::StormSqAce,Plt::TempestSqPlt).Ship(Plt::VFoslo,Plt::VFoslo)
    .Ship(Plt::LtKestal,Plt::OnyxSqScout).Ship(Plt::OnyxSqScout,Plt::SienSpecialist).Ship(Plt::DblEdge,Plt::SienSpecialist)
    .Ship(Plt::TBren,Plt::GammaSqAce).Ship(Plt::CaptJonus,Plt::GammaSqAce).Ship(Plt::MajRhymer,Plt::ScimitarSqPlt).Ship(Plt::GammaSqAce,Plt::ScimitarSqPlt).Ship(Plt::Deathfire,Plt::ScimitarSqPlt)
    .Ship(Plt::RBrath,Plt::OnyxSqAce).Ship(Plt::ColVessery,Plt::DeltaSqPlt).Ship(Plt::CountessRyad,Plt::DeltaSqPlt).Ship(Plt::OnyxSqAce,Plt::DeltaSqPlt)
    .Ship(Plt::Howlrunner,Plt::BlackSqAce_Imp).Ship(Plt::MMithel,Plt::BlackSqAce_Imp).Ship(Plt::SSkutu,Plt::ObsidianSqPlt).Ship(Plt::BlackSqAce_Imp,Plt::ObsidianSqPlt).Ship(Plt::BlackSqAce_Imp,Plt::AcademyPlt).Ship(Plt::ObsidianSqPlt,Plt::AcademyPlt).Ship(Plt::ObsidianSqPlt,Plt::AcademyPlt).Ship(Plt::Wampa,Plt::AcademyPlt).Ship(Plt::DMeeko,Plt::DMeeko).Ship(Plt::GHask_TIEln,Plt::GHask_TIEln).Ship(Plt::SMarana,Plt::SMarana)
    .Ship(Plt::SFel,Plt::SaberSqAce).Ship(Plt::TPhennir,Plt::AlphaSqPlt).Ship(Plt::SaberSqAce,Plt::AlphaSqPlt)
    .Ship(Plt::Whisper,Plt::SigmaSqAce).Ship(Plt::Echo,Plt::ImdaarTestPlt).Ship(Plt::SigmaSqAce,Plt::ImdaarTestPlt)
    .Ship(Plt::Redline,Plt::CutlassSqPlt).Ship(Plt::Deathrain,Plt:: CutlassSqPlt)
    .Ship(Plt::Duchess,Plt::BlackSqScout).Ship(Plt::Countdown,Plt::PlanetarySent).Ship(Plt::PureSabacc,Plt::PlanetarySent).Ship(Plt::BlackSqScout,Plt::PlanetarySent).Ship(Plt::BlackSqScout,Plt::PlanetarySent)
    .Ship(Plt::RAdmChiraneau,Plt::PatrolLdr).Ship(Plt::CaptOicunn,Plt::PatrolLdr)
    .Calc(1).Cloak(4).Disarm(3).Focus(4).ForceCharge(6).Jam(2).Reinforce(2).StandardCharge(10).Stress(4).Tractor(3)
    .SmSiTurret(4,Fac::Imperial).LaDoTurret(2,Fac::Imperial)
    .Hunted(1).ListeningDev(1)
  },

  { Rel::SWZ08, "SWZ08", "841333105655", "Scum and Villainy Conversion Kit", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/become-infamous/" },
    { { {2018, 9,10}, "https://www.fantasyflightgames.com/en/news/2018/9/10/deadly-calculations/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    { },
    {
      Plt::IG88A, Plt::IG88B, Plt::IG88C, Plt::IG88D,
      Plt::FRau_FF, Plt::OTeroch, Plt::JRekkoff, Plt::KSolus, Plt::SkullSqPlt, Plt::SkullSqPlt, Plt::ZealousRecruit, Plt::ZealousRecruit, Plt::ZealousRecruit,
      Plt::BFett, Plt::EAzzameen, Plt::KScarlet, Plt::KFrost, Plt::KTrelix, Plt::BountyHunter, Plt::BountyHunter,
      Plt::FourLOM, Plt::Zuckuss, Plt::GandFindsman, Plt::GandFindsman,
      Plt::DBonearm, Plt::PGodalhi, Plt::TMux, Plt::SpiceRunner, Plt::SpiceRunner,
      Plt::Dengar, Plt::TTrevura, Plt::Manaroo, Plt::ContractedSc,
      Plt::TCobra, Plt::Graz, Plt::VHel, Plt::BlackSunAce, Plt::BlackSunAce, Plt::BlackSunAce, Plt::CaptJostero, Plt::CartMarauder, Plt::CartMarauder, Plt::CartMarauder,
      Plt::TKulda, Plt::DOberos_KG, Plt::CartExecution, Plt::CartExecution,
      Plt::KOnyo, Plt::AVentress, Plt::SWren_LC, Plt::ShadowportHun,
      Plt::Serissu, Plt::GRed, Plt::LAshera, Plt::QJast, Plt::TansariiPtVet, Plt::TansariiPtVet, Plt::TansariiPtVet, Plt::TansariiPtVet, Plt::Inaldra, Plt::CartelSpacer, Plt::CartelSpacer, Plt::CartelSpacer, Plt::CartelSpacer, Plt::SBounder,
      Plt::ConstZuvio, Plt::SPlank, Plt::UPlutt, Plt::JGunrunner, Plt::JGunrunner, Plt::JGunrunner,
      Plt::CaptNym, Plt::SSixxa, Plt::LRevenant, Plt::LRevenant,
      Plt::Guri, Plt::DOberos_SV, Plt::PXizor, Plt::BSAssassin, Plt::BSAssassin, Plt::BSEnforcer, Plt::BSEnforcer,
      Plt::Bossk_YV, Plt::MEval, Plt::LRazzi, Plt::TSlaver,
      Plt::Kavil, Plt::DRenthal, Plt::HiredGun, Plt::HiredGun, Plt::CrymorahGoon, Plt::CrymorahGoon,
      Plt::NSuhlak, Plt::BSSoldier, Plt::BSSoldier, Plt::BSSoldier, Plt::KLeeachos, Plt::BinayrePirate, Plt::BinayrePirate, Plt::BinayrePirate, Plt::NashtahPup,
     },
    {
      Upg::Genius, Upg::R2Astro, Upg::R2Astro, Upg::R3Astro, Upg::R3Astro, Upg::R4Astro, Upg::R4Astro, Upg::R5Astro, Upg::R5Astro, Upg::R5P8, Upg::R5TK,
      Upg::Bomblet, Upg::ConnerNet, Upg::ConnerNet, Upg::ProtonBmb, Upg::ProtonBmb, Upg::ProxMine, Upg::ProxMine, Upg::SeismicCh, Upg::SeismicCh,
      Upg::HLC, Upg::HLC, Upg::IonCan, Upg::IonCan, Upg::JamBeam, Upg::JamBeam, Upg::TracBeam, Upg::TracBeam,
      Upg::TripleZero, Upg::FourLOM, Upg::BFett, Upg::CBane, Upg::CVizago, Upg::FLSlicer, Upg::FLSlicer, Upg::Gonk, Upg::Gonk, Upg::IG88D, Upg::Informant, Upg::Jabba, Upg::KOnyo, Upg::LRazzi, Upg::Maul, Upg::NovTech, Upg::NovTech, Upg::PerCPilot, Upg::PerCPilot, Upg::SeasonNav, Upg::SeasonNav, Upg::TactOff, Upg::TactOff, Upg::UPlutt, Upg::Zuckuss,
      Upg::Bossk, Upg::BT1, Upg::Dengar, Upg::Greedo, Upg::HsGunner, Upg::HsGunner, Upg::SkBombard, Upg::SkBombard, Upg::VetTailGun, Upg::VetTailGun, Upg::VetTurret, Upg::VetTurret,
      Upg::CloakDev, Upg::ContraCyb, Upg::ContraCyb, Upg::DeadmansSw, Upg::DeadmansSw, Upg::DeadmansSw, Upg::FeedbackAr, Upg::FeedbackAr, Upg::FeedbackAr, Upg::InertDamp, Upg::InertDamp, Upg::RigCargoCh, Upg::RigCargoCh,
      Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::IonMsl, Upg::IonMsl, Upg::PRockets, Upg::PRockets,
      Upg::AblatPlat, Upg::AblatPlat, Upg::ElectBaff, Upg::ElectBaff, Upg::EngUpg, Upg::EngUpg, Upg::HullUpg, Upg::HullUpg, Upg::MuniFailSa, Upg::MuniFailSa, Upg::ShieldUpg, Upg::ShieldUpg, Upg::StaDcVanes, Upg::StaDcVanes, Upg::StealthDev, Upg::StealthDev, Upg::TactScramb, Upg::TactScramb,
      Upg::AdvSensors, Upg::AdvSensors, Upg::CollisDet, Upg::CollisDet, Upg::FCS, Upg::FCS, Upg::TrajSim, Upg::TrajSim,
      Upg::CrackShot, Upg::CrackShot, Upg::CrackShot, Upg::Daredevil, Upg::Daredevil, Upg::DebGambit, Upg::DebGambit, Upg::Elusive, Upg::Elusive, Upg::ExpHan, Upg::ExpHan, Upg::Fearless, Upg::Fearless, Upg::Fearless, Upg::Intimidat, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Marksman, Upg::Marksman, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Predator, Upg::Predator, Upg::SatSalvo, Upg::SatSalvo, Upg::SquadLdr, Upg::SwarmTac, Upg::SwarmTac, Upg::TrickShot, Upg::TrickShot,
      Upg::Andrasta, Upg::Havoc, Upg::HoundsTooth, Upg::IG2000, Upg::IG2000, Upg::Marauder, Upg::MistHunter, Upg::PunishingOne, Upg::ShadowCaster, Upg::SlaveI, Upg::Virago,
      Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::IonTrp, Upg::IonTrp, Upg::ProtTrp, Upg::ProtTrp,
      Upg::DorsalTrt, Upg::DorsalTrt, Upg::IonCanTrt, Upg::IonCanTrt,
    },
    { Cnd::LstnDev },
    {},
    TokenCollection()
    .Dial(Shp::Aggressor, Fac::Scum).Dial(Shp::Aggressor, Fac::Scum)
    .Dial(Shp::BTLA4YWing, Fac::Scum).Dial(Shp::BTLA4YWing, Fac::Scum)
    .Dial(Shp::FangFighter, Fac::Scum).Dial(Shp::FangFighter, Fac::Scum).Dial(Shp::FangFighter, Fac::Scum)
    .Dial(Shp::Firespray, Fac::Scum).Dial(Shp::Firespray, Fac::Scum)
    .Dial(Shp::G1A, Fac::Scum).Dial(Shp::G1A, Fac::Scum)
    .Dial(Shp::HWK290, Fac::Scum).Dial(Shp::HWK290, Fac::Scum)
    .Dial(Shp::JM5K, Fac::Scum).Dial(Shp::JM5K, Fac::Scum)
    .Dial(Shp::Kihraxz, Fac::Scum).Dial(Shp::Kihraxz, Fac::Scum).Dial(Shp::Kihraxz, Fac::Scum)
    .Dial(Shp::LancerClass, Fac::Scum).Dial(Shp::LancerClass, Fac::Scum)
    .Dial(Shp::M12LKimogila, Fac::Scum).Dial(Shp::M12LKimogila, Fac::Scum)
    .Dial(Shp::M3A, Fac::Scum).Dial(Shp::M3A, Fac::Scum).Dial(Shp::M3A, Fac::Scum).Dial(Shp::M3A, Fac::Scum)
    .Dial(Shp::Quadjumper, Fac::Scum).Dial(Shp::Quadjumper, Fac::Scum).Dial(Shp::Quadjumper, Fac::Scum)
    .Dial(Shp::Scurrg, Fac::Scum).Dial(Shp::Scurrg, Fac::Scum)
    .Dial(Shp::StarViper, Fac::Scum).Dial(Shp::StarViper, Fac::Scum)
    .Dial(Shp::YV666, Fac::Scum).Dial(Shp::YV666, Fac::Scum)
    .Dial(Shp::Z95, Fac::Scum).Dial(Shp::Z95, Fac::Scum).Dial(Shp::Z95, Fac::Scum).Dial(Shp::Z95, Fac::Scum)
    .DialId(Shp::Aggressor).DialId(Shp::Aggressor)
    .DialId(Shp::BTLA4YWing).DialId(Shp::BTLA4YWing)
    .DialId(Shp::Firespray).DialId(Shp::Firespray)
    .DialId(Shp::HWK290).DialId(Shp::HWK290)
    .DialId(Shp::Kihraxz).DialId(Shp::Kihraxz).DialId(Shp::Kihraxz)
    .DialId(Shp::M3A).DialId(Shp::M3A).DialId(Shp::M3A).DialId(Shp::M3A)
    .DialId(Shp::StarViper).DialId(Shp::StarViper)
    .DialId(Shp::YV666).DialId(Shp::YV666)
    .DialId(Shp::Z95).DialId(Shp::Z95).DialId(Shp::Z95).DialId(Shp::Z95)
    .Ship(Plt::IG88A,Plt::IG88A).Ship(Plt::IG88B,Plt::IG88B).Ship(Plt::IG88C,Plt::IG88C).Ship(Plt::IG88D,Plt::IG88D)
    .Ship(Plt::FRau_FF,Plt::SkullSqPlt).Ship(Plt::OTeroch,Plt::ZealousRecruit).Ship(Plt::KSolus,Plt::ZealousRecruit).Ship(Plt::SkullSqPlt,Plt::ZealousRecruit).Ship(Plt::JRekkoff,Plt::JRekkoff)
    .Ship(Plt::BFett,Plt::KTrelix).Ship(Plt::EAzzameen,Plt::BountyHunter).Ship(Plt::KScarlet,Plt::BountyHunter).Ship(Plt::KFrost,Plt::KFrost)
    .Ship(Plt::FourLOM,Plt::GandFindsman).Ship(Plt::Zuckuss,Plt::GandFindsman)
    .Ship(Plt::DBonearm,Plt::SpiceRunner).Ship(Plt::PGodalhi,Plt::SpiceRunner).Ship(Plt::TMux,Plt::SpiceRunner)
    .Ship(Plt::Dengar,Plt::TTrevura).Ship(Plt::Manaroo,Plt::ContractedSc)
    .Ship(Plt::TCobra,Plt::BlackSunAce).Ship(Plt::Graz,Plt::BlackSunAce).Ship(Plt::VHel,Plt::CartMarauder).Ship(Plt::CaptJostero,Plt::CartMarauder).Ship(Plt::BlackSunAce,Plt::CartMarauder)
    .Ship(Plt::TKulda,Plt::CartExecution).Ship(Plt::DOberos_KG,Plt::CartExecution)
    .Ship(Plt::KOnyo,Plt::AVentress).Ship(Plt::SWren_LC,Plt::ShadowportHun)
    .Ship(Plt::Serissu,Plt::TansariiPtVet).Ship(Plt::GRed,Plt::TansariiPtVet).Ship(Plt::Inaldra,Plt::CartelSpacer).Ship(Plt::LAshera,Plt::CartelSpacer).Ship(Plt::QJast,Plt::CartelSpacer).Ship(Plt::SBounder,Plt::CartelSpacer)
    .Ship(Plt::ConstZuvio,Plt::JGunrunner).Ship(Plt::SPlank,Plt::JGunrunner).Ship(Plt::UPlutt,Plt::JGunrunner)
    .Ship(Plt::CaptNym,Plt::LRevenant).Ship(Plt::SSixxa,Plt::LRevenant)
    .Ship(Plt::Guri,Plt::BSAssassin).Ship(Plt::DOberos_SV,Plt::BSEnforcer).Ship(Plt::PXizor,Plt::BSEnforcer).Ship(Plt::BSAssassin,Plt::BSEnforcer)
    .Ship(Plt::Bossk_YV,Plt::LRazzi).Ship(Plt::MEval,Plt::TSlaver)
    .Ship(Plt::Kavil,Plt::HiredGun).Ship(Plt::DRenthal,Plt::CrymorahGoon).Ship(Plt::HiredGun,Plt::CrymorahGoon)
    .Ship(Plt::NSuhlak,Plt::BinayrePirate).Ship(Plt::KLeeachos,Plt::BSSoldier).Ship(Plt::BSSoldier,Plt::BinayrePirate).Ship(Plt::BSSoldier,Plt::BinayrePirate).Ship(Plt::NashtahPup,Plt::NashtahPup)
    .Calc(3).Cloak(1).Disarm(3).Focus(3).ForceCharge(3).Ion(2).Jam(3).Reinforce(2).StandardCharge(5).Stress(2).Tractor(3)
    .SmSiTurret(4,Fac::Scum).LaSiTurret(3,Fac::Scum)
    .ListeningDev(1)
  },

  { Rel::SWZ09, "SWZ09", "841333105662", "Rebel Alliance Maneuver Dial Upgrade Kit", RelGroup::Accessories,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/x-wing-second-edition/" },
      {},
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ10, "SWZ10", "841333105679", "Galactic Empire Maneuver Dial Upgrade Kit", RelGroup::Accessories,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/x-wing-second-edition/" },
      {},
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ11, "SWZ11", "841333105686", "Scum and Villainy Maneuver Dial Upgrade Kit", RelGroup::Accessories,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/x-wing-second-edition/" },
      {},
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ12, "SWZ12", "841333106041", "T-65 X-Wing Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/all-wings-report-in-1/" },
    { { {2018, 6,11}, "https://www.fantasyflightgames.com/en/news/2018/6/11/symbol-of-the-rebellion/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {{Shp::T65XWing, ""}},
    { Plt::WAntilles_T65, Plt::TKyrell, Plt::GDreis_XW, Plt::BDarklighter, Plt::RedSqVet, Plt::BlueSqEsc },
    { Upg::R2Astro, Upg::R4Astro, Upg::SmSFoils, Upg::Selfless, Upg::IonTrp },
    {},
    {},
    TokenCollection()
    .Dial(Shp::T65XWing, Fac::Rebel)
    .DialId(Shp::T65XWing)
    .Ship(Plt::WAntilles_T65,Plt::RedSqVet).Ship(Plt::TKyrell,Plt::GDreis_XW).Ship(Plt::BDarklighter,Plt::BlueSqEsc)
    .Disarm(1).ID(3,7).Ion(3).Lock(2,7).Shield(2).StandardCharge(4)
  },

  { Rel::SWZ13, "SWZ13", "841333106058", "BTL-A4 Y-Wing Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/all-wings-report-in-1/" },
    { { {2018, 6,18}, "https://www.fantasyflightgames.com/en/news/2018/6/18/begin-attack-run/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {{Shp::BTLA4YWing, ""}},
    { Plt::NWexley_YW, Plt::DVander, Plt::HSalm, Plt::EVerlaine, Plt::GoldSqVet, Plt::GraySqBomber },
    {
      Upg::R5Astro,
      Upg::ProtonBmb, Upg::SeismicCh,
      Upg::VetTurret,
      Upg::ExpHan,
      Upg::IonCanTrt,
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::BTLA4YWing, Fac::Rebel)
    .DialId(Shp::BTLA4YWing)
    .Ship(Plt::DVander,Plt::GraySqBomber).Ship(Plt::HSalm,Plt::GoldSqVet).Ship(Plt::NWexley_YW,Plt::EVerlaine)
    .Disarm(1).Ion(3).Shield(2)
    .SmSiTurret(1,Fac::Rebel)
    .ProtonBomb(2).SeismicCharge(2)
  },

  { Rel::SWZ14, "SWZ14", "841333106065", "TIE/ln Fighter Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/crush-the-rebellion-1/" },
    { { {2018, 7,16}, "https://www.fantasyflightgames.com/en/news/2018/7/16/space-superiority-1/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {{Shp::TIElnFighter, ""}},
    {
      Plt::Howlrunner, Plt::MMithel, Plt::SSkutu, Plt::DMeeko, Plt::GHask_TIEln,
      Plt::SMarana, Plt::BlackSqAce_Imp, Plt::ObsidianSqPlt, Plt::AcademyPlt, Plt::Wampa
    },
    {
       Upg::StealthDev,
       Upg::CrackShot, Upg::Juke, Upg::Marksman
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIElnFighter, Fac::Imperial)
    .DialId(Shp::TIElnFighter)
    .Ship(Plt::Howlrunner,Plt::ObsidianSqPlt).Ship(Plt::MMithel,Plt::BlackSqAce_Imp).Ship(Plt::SSkutu,Plt::GHask_TIEln).Ship(Plt::SMarana,Plt::DMeeko).Ship(Plt::Wampa,Plt::AcademyPlt)
    .Evade(2).Focus(2).StandardCharge(2)
  },

  { Rel::SWZ15, "SWZ15", "841333106072", "TIE Advanced x1 Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/crush-the-rebellion-1/" },
    { { {2018, 7,23}, "https://www.fantasyflightgames.com/en/news/2018/7/23/on-the-cutting-edge/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {{Shp::TIEAdvX1, ""}},
    { Plt::DVader_x1, Plt::MStele, Plt::VFoslo, Plt::ZStrom, Plt::StormSqAce, Plt::TempestSqPlt },
    {
      Upg::HeightPerc, Upg::SNReflex,
      Upg::ClustMsl,
      Upg::FCS,
      Upg::Ruthless, Upg::SquadLdr
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEAdvX1, Fac::Imperial)
    .DialId(Shp::TIEAdvX1)
    .Ship(Plt::DVader_x1,Plt::StormSqAce).Ship(Plt::MStele,Plt::VFoslo).Ship(Plt::ZStrom,Plt::TempestSqPlt)
    .ID(3,8).Lock(2,8).ForceCharge(3).Shield(2).StandardCharge(4).Stress(1)
  },

  { Rel::SWZ16, "SWZ16", "841333106089", "Slave I Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/become-infamous/" },
    { { {2018, 7, 2}, "https://www.fantasyflightgames.com/en/news/2018/7/2/any-methods-necessary-1/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {{Shp::Firespray, ""}},
    { Plt::BFett, Plt::EAzzameen, Plt::KScarlet, Plt::KFrost, Plt::KTrelix, Plt::BountyHunter },
    {
      Upg::HLC,
      Upg::BFett,
      Upg::ProxMine, Upg::SeismicCh,
      Upg::VetTailGun,
      Upg::InertDamp,
      Upg::ConcusMsl,
      Upg::LoneWolf, Upg::PerCPilot,
      Upg::Andrasta, Upg::Marauder, Upg::SlaveI
     },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Firespray, Fac::Scum)
    .DialId(Shp::Firespray)
    .Ship(Plt::BFett,Plt::KTrelix).Ship(Plt::KScarlet,Plt::EAzzameen).Ship(Plt::KFrost,Plt::BountyHunter)
    .Reinforce(1).Shield(4).StandardCharge(3)
    .ProxMine(2).SeismicCharge(2)
  },

  { Rel::SWZ17, "SWZ17", "841333106096", "Fang Fighter Expansion Pack", RelGroup::Wave1,
    {2018, 5, 1}, {2018, 9,13},
    {
      { {2018, 5, 1}, "https://www.fantasyflightgames.com/en/news/2018/5/1/become-infamous/" },
    { { {2018, 7, 9}, "https://www.fantasyflightgames.com/en/news/2018/7/9/direct-confrontation/" } },
      { {2018, 9,13}, "https://www.fantasyflightgames.com/en/news/2018/9/13/take-command-3/" }
    },
    "",
    {{Shp::FangFighter, ""}},
    { Plt::FRau_FF, Plt::JRekkoff, Plt::KSolus, Plt::OTeroch, Plt::SkullSqPlt, Plt::ZealousRecruit },
    {
      Upg::Afterburn,
      Upg::Daredevil,
      Upg::Fearless,
      Upg::IonTrp
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::FangFighter, Fac::Scum)
    .DialId(Shp::FangFighter)
    .Ship(Plt::FRau_FF,Plt::SkullSqPlt).Ship(Plt::OTeroch,Plt::JRekkoff).Ship(Plt::KSolus,Plt::ZealousRecruit)
    .Crit(1).Focus(1).ID(3,10).Ion(3).Lock(2,10).StandardCharge(2).Stress(1)
  },

  { Rel::SWZ18, "SWZ18", "", "First Order Conversion Kit", RelGroup::Wave2,
    {2018, 8, 2}, {2018,12,13},
    {
      { {2018, 8, 3}, "https://www.fantasyflightgames.com/en/news/2018/8/3/evil-resurgent/" },
    { { {2018,10, 8}, "https://www.fantasyflightgames.com/en/news/2018/10/8/a-new-order/" } },
      { {2018,12,13}, "https://www.fantasyflightgames.com/en/news/2018/12/13/happy-beeps/" }
    },
    "",
    {},
    {
      Plt::Midnight, Plt::CmdrMalarus, Plt::Scorch_FO, Plt::Static, Plt::Longshot, Plt::OmegaSqAce, Plt::OmegaSqAce, Plt::OmegaSqAce, Plt::OmegaSqAce, Plt::OmegaSqAce, Plt::OmegaSqAce, Plt::Muse, Plt::TN3465, Plt::ZetaSqPlt, Plt::ZetaSqPlt, Plt::ZetaSqPlt, Plt::ZetaSqPlt, Plt::ZetaSqPlt, Plt::ZetaSqPlt, Plt::ZetaSqPlt, Plt::EpsilonSqCadet, Plt::EpsilonSqCadet, Plt::EpsilonSqCadet, Plt::EpsilonSqCadet, Plt::EpsilonSqCadet, Plt::EpsilonSqCadet, Plt::EpsilonSqCadet, Plt::LtRivas, Plt::Null,
      Plt::Quickdraw, Plt::Backdraft, Plt::OmegaSqExpert, Plt::OmegaSqExpert, Plt::OmegaSqExpert, Plt::OmegaSqExpert, Plt::ZetaSqSurvivor, Plt::ZetaSqSurvivor, Plt::ZetaSqSurvivor, Plt::ZetaSqSurvivor, Plt::ZetaSqSurvivor,
      Plt::Blackout, Plt::KRen_VN, Plt::FOTestPlt, Plt::FOTestPlt, Plt::FOTestPlt, Plt::Recoil, Plt::Avenger, Plt::SJEngineer, Plt::SJEngineer, Plt::SJEngineer,
      Plt::CaptCardinal, Plt::MajStridan, Plt::LtTavson, Plt::LtDormitz, Plt::StarkillerBsPlt, Plt::StarkillerBsPlt, Plt::StarkillerBsPlt, Plt::PetOffThanisson
    },
    {
      Upg::HLC, Upg::HLC, Upg::IonCan, Upg::IonCan, Upg::JamBeam, Upg::JamBeam, Upg::TracBeam, Upg::TracBeam,
      Upg::CaptPhasma, Upg::FLSlicer, Upg::FLSlicer, Upg::GenHux, Upg::Gonk, Upg::Gonk, Upg::Informant, Upg::KRen, Upg::NovTech, Upg::NovTech, Upg::PerCPilot, Upg::PerCPilot, Upg::PetOffThanisson, Upg::SeasonNav, Upg::SeasonNav, Upg::SupLdrSnoke,
      Upg::Hate, Upg::PredictiveShot,
      Upg::HsGunner, Upg::HsGunner, Upg::SpecForGun, Upg::SpecForGun, Upg::SpecForGun, Upg::SpecForGun,
      Upg::AblatPlat, Upg::AblatPlat, Upg::ElectBaff, Upg::ElectBaff, Upg::HullUpg, Upg::HullUpg,Upg::MuniFailSa, Upg::MuniFailSa, Upg::ShieldUpg, Upg::ShieldUpg, Upg::StaDcVanes, Upg::StaDcVanes, Upg::StealthDev, Upg::StealthDev, Upg::TactScramb, Upg::TactScramb,
      Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::IonMsl, Upg::IonMsl, Upg::PRockets, Upg::PRockets,
      Upg::AdvSensors, Upg::AdvSensors,Upg::CollisDet, Upg::CollisDet, Upg::FCS, Upg::FCS,
      Upg::CrackShot, Upg::CrackShot, Upg::Daredevil, Upg::Daredevil, Upg::DebGambit, Upg::DebGambit, Upg::Elusive, Upg::Elusive, Upg::Fanatical, Upg::Fanatical, Upg::Fanatical, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Marksman, Upg::Marksman, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Predator, Upg::Predator, Upg::SquadLdr, Upg::SwarmTac, Upg::SwarmTac, Upg::TrickShot, Upg::TrickShot,
      Upg::AdvOptics, Upg::AdvOptics, Upg::BiohexCodes, Upg::HsTrackingData, Upg::HsTrackingData, Upg::PatAnalyzer, Upg::PatAnalyzer, Upg::PThrusters, Upg::PThrusters, Upg::TargetSync, Upg::TargetSync,
      Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::IonTrp, Upg::IonTrp, Upg::ProtTrp, Upg::ProtTrp
    },
    { Cnd::ISYTDS, Cnd::LstnDev },
    {},
    TokenCollection()
    .Dial(Shp::TIEfoFighter,Fac::FirstOrder).Dial(Shp::TIEfoFighter,Fac::FirstOrder).Dial(Shp::TIEfoFighter,Fac::FirstOrder).Dial(Shp::TIEfoFighter,Fac::FirstOrder).Dial(Shp::TIEfoFighter,Fac::FirstOrder).Dial(Shp::TIEfoFighter,Fac::FirstOrder).Dial(Shp::TIEfoFighter,Fac::FirstOrder)
    .Dial(Shp::TIEsfFighter,Fac::FirstOrder).Dial(Shp::TIEsfFighter,Fac::FirstOrder).Dial(Shp::TIEsfFighter,Fac::FirstOrder).Dial(Shp::TIEsfFighter,Fac::FirstOrder).Dial(Shp::TIEsfFighter,Fac::FirstOrder)
    .Dial(Shp::TIEvnSilencer,Fac::FirstOrder).Dial(Shp::TIEvnSilencer,Fac::FirstOrder).Dial(Shp::TIEvnSilencer,Fac::FirstOrder)
    .Dial(Shp::Upsilon,Fac::FirstOrder).Dial(Shp::Upsilon,Fac::FirstOrder).Dial(Shp::Upsilon,Fac::FirstOrder)
    .DialId(Shp::TIEfoFighter).DialId(Shp::TIEfoFighter).DialId(Shp::TIEfoFighter).DialId(Shp::TIEfoFighter).DialId(Shp::TIEfoFighter).DialId(Shp::TIEfoFighter).DialId(Shp::TIEfoFighter)
    .Ship(Plt::Midnight,Plt::OmegaSqAce).Ship(Plt::CmdrMalarus,Plt::CmdrMalarus).Ship(Plt::Scorch_FO,Plt::OmegaSqAce).Ship(Plt::Static,Plt::OmegaSqAce).Ship(Plt::Longshot,Plt::OmegaSqAce).Ship(Plt::OmegaSqAce,Plt::Muse).Ship(Plt::OmegaSqAce,Plt::Null).Ship(Plt::TN3465,Plt::TN3465).Ship(Plt::ZetaSqPlt,Plt::EpsilonSqCadet).Ship(Plt::ZetaSqPlt,Plt::EpsilonSqCadet).Ship(Plt::ZetaSqPlt,Plt::EpsilonSqCadet).Ship(Plt::ZetaSqPlt,Plt::EpsilonSqCadet).Ship(Plt::ZetaSqPlt,Plt::EpsilonSqCadet).Ship(Plt::ZetaSqPlt,Plt::EpsilonSqCadet).Ship(Plt::ZetaSqPlt,Plt::EpsilonSqCadet).Ship(Plt::LtRivas,Plt::LtRivas)
    .Ship(Plt::Quickdraw,Plt::OmegaSqExpert).Ship(Plt::Backdraft,Plt::ZetaSqSurvivor).Ship(Plt::OmegaSqExpert,Plt::ZetaSqSurvivor).Ship(Plt::OmegaSqExpert,Plt::ZetaSqSurvivor).Ship(Plt::OmegaSqExpert,Plt::ZetaSqSurvivor).Ship(Plt::OmegaSqExpert,Plt::ZetaSqSurvivor)
    .Ship(Plt::Blackout,Plt::SJEngineer).Ship(Plt::KRen_VN,Plt::FOTestPlt).Ship(Plt::FOTestPlt,Plt::SJEngineer).Ship(Plt::FOTestPlt,Plt::SJEngineer).Ship(Plt::Recoil,Plt::Recoil).Ship(Plt::Avenger,Plt::Avenger)
    .Ship(Plt::CaptCardinal,Plt::CaptCardinal).Ship(Plt::MajStridan,Plt::StarkillerBsPlt).Ship(Plt::LtTavson,Plt::StarkillerBsPlt).Ship(Plt::LtDormitz,Plt::StarkillerBsPlt).Ship(Plt::PetOffThanisson,Plt::PetOffThanisson)
    .ForceCharge(4).Ion(4).Jam(6).Reinforce(3).Shield(7).StandardCharge(11).Tractor(5)
    .SmSiTurret(5, Fac::FirstOrder)
    .ISYTDS(1).ListeningDev(1)
  },

  { Rel::SWZ19, "SWZ19", "", "Resistance Conversion Kit", RelGroup::Wave2,
    {2018, 8, 2}, {2018,12,13},
    {
      { {2018, 8, 3}, "https://www.fantasyflightgames.com/en/news/2018/8/3/spark-of-rebellion-1/" },
    { { {2018,10, 3}, "https://www.fantasyflightgames.com/en/news/2018/10/3/continuing-the-fight/" } },
      { {2018,12,13}, "https://www.fantasyflightgames.com/en/news/2018/12/13/happy-beeps/" }
    },
    "",
    {},
    {
      Plt::FDallow, Plt::BTeene, Plt::EKappehl, Plt::Vennie, Plt::Cat, Plt::CobaltSqBomber, Plt::CobaltSqBomber, Plt::CobaltSqBomber,
      Plt::HSoloRes, Plt::Rey, Plt::ChewieRes, Plt::ResSymp, Plt::ResSymp, Plt::ResSymp,
      Plt::PDameron, Plt::EAsty, Plt::NNunb, Plt::BlackSqAce_Res, Plt::KKun, Plt::TWexley, Plt::JPava, Plt::JSeastriker, Plt::RedSqExpert, Plt::RedSqExpert, Plt::RedSqExpert, Plt::RedSqExpert, Plt::LtBastian, Plt::BlueSqRookie, Plt::BlueSqRookie, Plt::BlueSqRookie, Plt::BlueSqRookie, Plt::JTubbs,
    },
    {
      Upg::BB8, Upg::BBAstro, Upg::BBAstro, Upg::BBAstro, Upg::BBAstro, Upg::M9G8, Upg::R2HA, Upg::R2Astro, Upg::R2Astro, Upg::R3Astro, Upg::R3Astro, Upg::R4Astro, Upg::R4Astro, Upg::R5X3, Upg::R5Astro, Upg::R5Astro,
      Upg::HLC, Upg::HLC, Upg::IonCan, Upg::IonCan, Upg::JamBeam, Upg::JamBeam, Upg::TracBeam, Upg::TracBeam,
      Upg::IntSFoils, Upg::IntSFoils, Upg::IntSFoils, Upg::IntSFoils,
      Upg::C3PORes, Upg::ChewieRes, Upg::FLSlicer, Upg::FLSlicer, Upg::Gonk, Upg::Gonk, Upg::HSolo, Upg::Informant, Upg::NovTech, Upg::NovTech, Upg::PerCPilot, Upg::PerCPilot, Upg::RTico, Upg::SeasonNav, Upg::SeasonNav, Upg::TactOff, Upg::TactOff,
      Upg::Bomblet, Upg::ConnerNet, Upg::ConnerNet, Upg::ProtonBmb, Upg::ProtonBmb, Upg::ProxMine, Upg::ProxMine, Upg::SeismicCh, Upg::SeismicCh,
      Upg::Finn, Upg::HsGunner, Upg::HsGunner, Upg::PTico, Upg::Rey, Upg::SkBombard, Upg::SkBombard, Upg::VetTurret, Upg::VetTurret,
      Upg::ContraCyb, Upg::ContraCyb, Upg::DeadmansSw, Upg::DeadmansSw, Upg::FeedbackAr, Upg::FeedbackAr, Upg::InertDamp, Upg::InertDamp, Upg::RigCargoCh, Upg::RigCargoCh,
      Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::HomingMsl, Upg::HomingMsl, Upg::IonMsl, Upg::IonMsl, Upg::PRockets, Upg::PRockets,
      Upg::AblatPlat, Upg::AblatPlat, Upg::AdvSLAM, Upg::ElectBaff, Upg::ElectBaff, Upg::EngUpg, Upg::EngUpg, Upg::HullUpg, Upg::HullUpg, Upg::MuniFailSa, Upg::MuniFailSa, Upg::ShieldUpg, Upg::ShieldUpg, Upg::StaDcVanes, Upg::StaDcVanes, Upg::StealthDev, Upg::StealthDev, Upg::TactScramb, Upg::TactScramb,
      Upg::AdvSensors, Upg::AdvSensors, Upg::CollisDet, Upg::CollisDet, Upg::FCS, Upg::FCS, Upg::TrajSim, Upg::TrajSim,
      Upg::CrackShot, Upg::CrackShot, Upg::Daredevil, Upg::Daredevil, Upg::DebGambit, Upg::DebGambit, Upg::Elusive, Upg::Elusive, Upg::Heroic, Upg::Heroic, Upg::Heroic, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Marksman, Upg::Marksman, Upg::Outmaneuv, Upg::Outmaneuv, Upg::Predator, Upg::Predator, Upg::SquadLdr, Upg::SwarmTac, Upg::SwarmTac, Upg::TrickShot, Upg::TrickShot,
      Upg::AdvOptics, Upg::AdvOptics, Upg::PatAnalyzer, Upg::PatAnalyzer, Upg::PThrusters, Upg::PThrusters, Upg::TargetSync, Upg::TargetSync,
      Upg::BlackOne, Upg::ReysMF,
      Upg::AdvProtTrp, Upg::AdvProtTrp, Upg::IonTrp, Upg::IonTrp, Upg::ProtTrp, Upg::ProtTrp
    },
    { Cnd::LstnDev, Cnd::Rattled },
    {},
    TokenCollection()
    .Dial(Shp::MG100, Fac::Resistance).Dial(Shp::MG100, Fac::Resistance).Dial(Shp::MG100, Fac::Resistance)
    .Dial(Shp::T70XWing, Fac::Resistance).Dial(Shp::T70XWing, Fac::Resistance).Dial(Shp::T70XWing, Fac::Resistance).Dial(Shp::T70XWing, Fac::Resistance)
    .Dial(Shp::ScaYT1300, Fac::Resistance).Dial(Shp::ScaYT1300, Fac::Resistance).Dial(Shp::ScaYT1300, Fac::Resistance)
    .DialId(Shp::T70XWing).DialId(Shp::T70XWing).DialId(Shp::T70XWing).DialId(Shp::T70XWing)
    .Ship(Plt::FDallow,Plt::CobaltSqBomber).Ship(Plt::BTeene,Plt::CobaltSqBomber).Ship(Plt::EKappehl,Plt::EKappehl).Ship(Plt::Vennie,Plt::Vennie).Ship(Plt::Cat,Plt::CobaltSqBomber)
    .Ship(Plt::HSoloRes,Plt::ResSymp).Ship(Plt::Rey,Plt::ResSymp).Ship(Plt::ChewieRes,Plt::ResSymp)
    .Ship(Plt::PDameron,Plt::RedSqExpert).Ship(Plt::EAsty,Plt::RedSqExpert).Ship(Plt::NNunb,Plt::RedSqExpert).Ship(Plt::BlackSqAce_Res,Plt::BlackSqAce_Res).Ship(Plt::KKun,Plt::BlueSqRookie).Ship(Plt::TWexley,Plt::BlueSqRookie).Ship(Plt::JPava,Plt::BlueSqRookie).Ship(Plt::JSeastriker,Plt::BlueSqRookie).Ship(Plt::RedSqExpert,Plt::BlueSqRookie).Ship(Plt::LtBastian,Plt::LtBastian).Ship(Plt::JTubbs,Plt::JTubbs)
    .Calc(2).Disarm(4).Evade(6).Focus(2).ForceCharge(4).Ion(4).Jam(3).Shield(4).StandardCharge(13).Tractor(3)
    .LaDoTurret(6,Fac::Resistance)
    .ListeningDev(1).Rattled(1)
  },

  { Rel::SWZ20, "SWZ20", "", "First Order Maneuver Dial Upgrade Kit", RelGroup::Accessories,
    {2018, 8, 3}, {2019, 2,14},
    {
      { {2018, 8, 3}, "https://www.fantasyflightgames.com/en/news/2018/8/3/evil-resurgent/" },
      {},
      { {2019, 2,14}, "https://www.fantasyflightgames.com/en/news/2019/2/14/available-now-february-14/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ21, "SWZ21", "", "Resistance Maneuver Dial Upgrade Kit", RelGroup::Accessories,
    {2018, 8, 3}, {2019, 2,14},
    {
      { {2018, 8, 3}, "https://www.fantasyflightgames.com/en/news/2018/8/3/spark-of-rebellion-1/" },
      {},
      { {2019, 2,14}, "https://www.fantasyflightgames.com/en/news/2019/2/14/available-now-february-14/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ22, "SWZ22", "", "RZ-2 A-Wing Expansion Pack", RelGroup::Wave2,
    {2018, 8, 2}, {2018,12,13},
    {
      { {2018, 8, 3}, "https://www.fantasyflightgames.com/en/news/2018/8/3/spark-of-rebellion-1/" },
    { { {2018,11,12}, "https://www.fantasyflightgames.com/en/news/2018/11/12/deadly-speed/" } },
      { {2018,12,13}, "https://www.fantasyflightgames.com/en/news/2018/12/13/happy-beeps/" }
    },
    "",
    {{Shp::RZ2AWing,""}},
    { Plt::LLampar, Plt::TLintra, Plt::GSonnel, Plt::GreenSqExp, Plt::ZBangel, Plt::BlueSqRecruit },
    {
      Upg::HomingMsl, Upg::PRockets,
      Upg::Heroic,
      Upg::FPaint, Upg::PThrusters,
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::RZ2AWing, Fac::Resistance)
    .DialId(Shp::RZ2AWing)
    .Ship(Plt::LLampar,Plt::GreenSqExp).Ship(Plt::TLintra,Plt::ZBangel).Ship(Plt::GSonnel,Plt::BlueSqRecruit)
    .Evade(1).Focus(1).ID(3,9).Lock(2,9).Shield(2).StandardCharge(4).Stress(3)
    .SmSiTurret(1,Fac::Resistance)
  },

  { Rel::SWZ23, "SWZ23", "", "Mining Guild TIE Expansion Pack", RelGroup::Wave2,
    {2018, 8, 2}, {2018,12,13},
    {
      { {2018, 8, 3}, "https://www.fantasyflightgames.com/en/news/2018/8/3/mining-modifications/" },
    { { {2018,11,27}, "https://www.fantasyflightgames.com/en/news/2018/11/27/secure-your-operations/" } },
      { {2018,12,13}, "https://www.fantasyflightgames.com/en/news/2018/12/13/happy-beeps/" }
    },
    "",
    {{Shp::ModifiedTIEln,""}},
    { Plt::ForemanProach, Plt::Ahhav, Plt::CaptSeevor, Plt::OverseerYushyn, Plt::MGSurveyor, Plt::MGSentry },
    {
      Upg::HullUpg, Upg::StaDcVanes,
      Upg::Elusive, Upg::SwarmTac, Upg::TrickShot
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::ModifiedTIEln, Fac::Scum)
    .DialId(Shp::ModifiedTIEln)
    .Ship(Plt::ForemanProach,Plt::MGSurveyor).Ship(Plt::Ahhav,Plt::OverseerYushyn).Ship(Plt::CaptSeevor,Plt::MGSentry)
    .Crit(1).Evade(1).Focus(1).ID(3,10).Jam(1).Lock(2,10).StandardCharge(1).Stress(2).Tractor(1)
  },

  { Rel::SWZ25, "SWZ25", "", "T-70 X-Wing Expansion Pack", RelGroup::Wave2,
    {2018, 8, 2}, {2018,12,13},
    {
      { {2018, 8, 3}, "https://www.fantasyflightgames.com/en/news/2018/8/3/spark-of-rebellion-1/" },
    { { {2018,10,24}, "https://www.fantasyflightgames.com/en/news/2018/10/24/a-new-classic/" } },
      { {2018,12,13}, "https://www.fantasyflightgames.com/en/news/2018/12/13/happy-beeps/" }
    },
    "",
    {{Shp::T70XWing,""}},
    { Plt::PDameron, Plt::EAsty, Plt::NNunb, Plt::BlackSqAce_Res, Plt::KKun, Plt::TWexley, Plt::JPava, Plt::JSeastriker, Plt::RedSqExpert, Plt::LtBastian, Plt::BlueSqRookie, Plt::JTubbs },
    {
      Upg::BB8, Upg::BBAstro, Upg::M9G8,
      Upg::IntSFoils,
      Upg::TargetSync,
      Upg::BlackOne
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::T70XWing, Fac::Resistance)
    .DialId(Shp::T70XWing)
    .Ship(Plt::PDameron,Plt::BlackSqAce_Res).Ship(Plt::EAsty,Plt::LtBastian).Ship(Plt::NNunb,Plt::RedSqExpert).Ship(Plt::KKun,Plt::TWexley).Ship(Plt::JPava,Plt::BlueSqRookie).Ship(Plt::JSeastriker,Plt::JTubbs)
    .Crit(1).Disarm(1).Evade(2).Focus(1).ID(3,12).Ion(1).Lock(2,12).Shield(3).StandardCharge(4).Stress(1)
  },

  { Rel::SWZ26, "SWZ26", "", "TIE/fo Fighter Expansion Pack", RelGroup::Wave2,
    {2018, 8, 2}, {2018,12,13},
    {
      { {2018, 8, 3}, "https://www.fantasyflightgames.com/en/news/2018/8/2/evil-resurgent/" },
    { { {2018,11, 7}, "https://www.fantasyflightgames.com/en/news/2018/11/7/advanced-terror/" } },
      { {2018,12,13}, "https://www.fantasyflightgames.com/en/news/2018/12/13/happy-beeps/" }
    },
    "",
    {{Shp::TIEfoFighter,""}},
    { Plt::Midnight, Plt::CmdrMalarus, Plt::Scorch_FO, Plt::Static, Plt::Longshot, Plt::OmegaSqAce, Plt::Muse, Plt::TN3465, Plt::ZetaSqPlt, Plt::EpsilonSqCadet, Plt::LtRivas, Plt::Null },
    {
      Upg::Fanatical, Upg::SquadLdr, Upg::SwarmTac,
      Upg::AdvOptics, Upg::TargetSync,
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEfoFighter, Fac::FirstOrder)
    .DialId(Shp::TIEfoFighter)
    .Ship(Plt::Midnight,Plt::OmegaSqAce).Ship(Plt::CmdrMalarus,Plt::Static).Ship(Plt::Scorch_FO,Plt::ZetaSqPlt).Ship(Plt::Longshot,Plt::TN3465).Ship(Plt::Muse,Plt::EpsilonSqCadet).Ship(Plt::LtRivas,Plt::Null)
    .Crit(1).Evade(1).Focus(1).ID(3,13).Lock(2,13).Shield(1).StandardCharge(3).Stress(2)
  },

  { Rel::SWZ27, "SWZ27", "841333106805", "TIE/vn Silencer Expansion Pack", RelGroup::Wave4,
    {2019,02, 8}, {2019, 7,12},
    {
      { {2019, 2, 8}, "https://www.fantasyflightgames.com/en/news/2019/2/8/expand-your-operations/" },
      {},
      { {2019, 7,12}, "https://www.fantasyflightgames.com/en/news/2019/7/12/available-now-july-12/" }
    },
    "",
    {{Shp::TIEvnSilencer, ""}},
    { Plt::Blackout, Plt::KRen_VN, Plt::FOTestPlt, Plt::Recoil, Plt::Avenger, Plt::SJEngineer },
    { Upg::Hate, Upg::PredictiveShot, Upg::Marksman, Upg::PThrusters, Upg::AdvProtTrp },
    { Cnd::ISYTDS },
    {},
    TokenCollection()
    .Dial(Shp::TIEvnSilencer, Fac::FirstOrder)
    .DialId(Shp::TIEvnSilencer)
    .Ship(Plt::Blackout,Plt::Recoil).Ship(Plt::KRen_VN,Plt::FOTestPlt).Ship(Plt::Avenger,Plt::SJEngineer)
    .Crit(1).Focus(1).ForceCharge(2).ID(3,14).Lock(2,14).Shield(2).StandardCharge(1).Stress(3)
    .ISYTDS(1)
  },

  { Rel::SWZ29, "SWZ29", "", "Servants of Strife Squadron Pack", RelGroup::Wave3,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
    { { {2019, 1, 8}, "https://www.fantasyflightgames.com/en/news/2019/1/8/seize-the-galaxy/" },
      { {2019, 1,18}, "https://www.fantasyflightgames.com/en/news/2019/1/18/power-and-profit/" } },
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "",
    {{Shp::Belbullab22, ""}, {Shp::Vulture, "Grievous's colors"}, {Shp::Vulture, "Geirvous's colors"}},
    {
      Plt::GenGrievous, Plt::SkakoanAce, Plt::WTambor, Plt::CaptSear, Plt::FOAutopilot,
      Plt::DFS081, Plt::PrecHunter, Plt::PrecHunter, Plt::SepDrone, Plt::SepDrone, Plt::HaorChallProto, Plt::HaorChallProto, Plt::TradeFedDrone, Plt::TradeFedDrone
    },
    {
      Upg::GrapStruts, Upg::GrapStruts,
      Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::EnShellCh, Upg::EnShellCh, Upg::HomingMsl, Upg::HomingMsl,Upg::PRockets, Upg::PRockets,
      Upg::Afterburn, Upg::Afterburn, Upg::Afterburn, Upg::ElectBaff, Upg::ElectBaff, Upg::ImpervPlat, Upg::MuniFailSa, Upg::MuniFailSa, Upg::StaDcVanes, Upg::StaDcVanes, Upg::StaDcVanes, Upg::StealthDev, Upg::StealthDev, Upg::StealthDev,
      Upg::Kraken, Upg::TV94,
      Upg::Composure, Upg::Composure, Upg::CrackShot, Upg::CrackShot, Upg::Daredevil, Upg::Daredevil, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Marksman, Upg::Marksman, Upg::SwarmTac, Upg::SwarmTac, Upg::Treacherous, Upg::Treacherous, Upg::TrickShot, Upg::TrickShot,
      Upg::SoullessOne
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Belbullab22, Fac::Separatist).Dial(Shp::Vulture, Fac::Separatist).Dial(Shp::Vulture, Fac::Separatist)
    .DialId(Shp::Belbullab22).DialId(Shp::Vulture).DialId(Shp::Vulture)
    .Ship(Plt::GenGrievous,Plt::SkakoanAce).Ship(Plt::WTambor,Plt::FOAutopilot).Ship(Plt::CaptSear,Plt::FOAutopilot)
    .Ship(Plt::DFS081,Plt::SepDrone).Ship(Plt::PrecHunter,Plt::SepDrone).Ship(Plt::PrecHunter,Plt::SepDrone).Ship(Plt::HaorChallProto,Plt::TradeFedDrone).Ship(Plt::HaorChallProto,Plt::TradeFedDrone)
    .Calc(12).Crit(3).Disarm(3).Focus(2).ID(3,7).ID(3,8).ID(3,9).Ion(2).Lock(2,7).Lock(2,8).Lock(2,9).Shield(2).StandardCharge(12).Strain(4).Stress(3)
    .Gas(3)
  },

  { Rel::SWZ30, "SWZ30", "", "Sith Infiltrator Expansion Pack", RelGroup::Wave3,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
    { { {2019, 2,25}, "https://www.fantasyflightgames.com/en/news/2019/2/25/a-phantom-menace/" } },
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "",
    {{Shp::SithInf, ""}},
    { Plt::DMaul, Plt::CDooku, Plt::O66, Plt::DarkCourier },
    { Upg::HLC, Upg::IonCan, Upg::TracBeam, Upg::ChanPalp, Upg::CountDooku, Upg::GenGrievous, Upg::NovTech, Upg::PerCPilot, Upg::SeasonNav, Upg::DRK1ProbeDroids, Upg::BrilEva, Upg::BrilEva, Upg::Hate, Upg::Hate, Upg::PredictiveShot, Upg::PredictiveShot, Upg::K2B4, Upg::Scimitar, Upg::AdvProtTrp, Upg::IonTrp },
    {},
    {},
    TokenCollection()
    .Dial(Shp::SithInf, Fac::Separatist)
    .DialId(Shp::SithInf)
    .Ship(Plt::DMaul,Plt::O66).Ship(Plt::CDooku,Plt::DarkCourier)
    .Calc(2).Cloak(1).Crit(1).Focus(2).ForceCharge(3).ID(3,13).Ion(3).Jam(3).Lock(2,13).Shield(4).StandardCharge(4).Strain(3).Tractor(3)
    .DRK1ProbeDroid(2)
  },

  { Rel::SWZ31, "SWZ31", "", "Vulture-class Droid Fighter Expansion", RelGroup::Wave3,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
    { { {2019, 3, 7}, "https://www.fantasyflightgames.com/en/news/2019/3/7/terrifying-technology/" } },
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "",
    {{Shp::Vulture, ""}},
    { Plt::PrecHunter, Plt::SepDrone, Plt::DFS311, Plt::HaorChallProto, Plt::TradeFedDrone },
    { Upg::GrapStruts, Upg::DiscordMsl, Upg::EnShellCh, Upg::ConcusMsl, Upg::MuniFailSa  },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Vulture, Fac::Separatist)
    .DialId(Shp::Vulture)
    .Ship(Plt::PrecHunter,Plt::SepDrone).Ship(Plt::DFS311,Plt::TradeFedDrone).Ship(Plt::HaorChallProto,Plt::TradeFedDrone)
    .Calc(2).Crit(1).Disarm(1).ID(3,14).Lock(2,14).StandardCharge(3).Stress(1)
    .BuzzDroid(1)
  },

  { Rel::SWZ32, "SWZ32", "", "Guardians of the Republic Squadron Pack", RelGroup::Wave3,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
    { { {2019, 2, 4}, "https://www.fantasyflightgames.com/en/news/2019/2/4/protecting-peace/" },
      { {2019, 2,11}, "https://www.fantasyflightgames.com/en/news/2019/2/11/for-the-republic/" } },
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "This expansion comes with extra maneuver dials as the original ones have an error.",
    {{Shp::Delta7, ""}, {Shp::V19, ""}, {Shp::V19, ""}},
    {
      Plt::OKenobi, Plt::PKoon, Plt::MWindu, Plt::STiin, Plt::JediKnight,
      Plt::OddBall_V19, Plt::Kickback, Plt::Axe, Plt::Swoop, Plt::BlueSqProtector, Plt::BlueSqProtector, Plt::Tucker, Plt::GoldSqTrooper, Plt::GoldSqTrooper
    },
    {
      Upg::R4Astro, Upg::R4PAstro, Upg::R4P17, Upg::R5Astro,
      Upg::CalLasTgt, Upg::Delta7B,
      Upg::BattleMed, Upg::BrilEva, Upg::PredictiveShot,
      Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl, Upg::HomingMsl, Upg::HomingMsl,Upg::PRockets, Upg::PRockets,
      Upg::Afterburn, Upg::Afterburn, Upg::ElectBaff, Upg::MuniFailSa, Upg::MuniFailSa, Upg::SparePartsCan, Upg::StaDcVanes, Upg::StealthDev, Upg::StealthDev, Upg::SyncCon, Upg::SyncCon, Upg::SyncCon,
      Upg::Composure, Upg::Composure, Upg::CrackShot, Upg::CrackShot, Upg::Dedicated, Upg::Dedicated, Upg::ExpHan, Upg::ExpHan, Upg::Intimidat, Upg::Intimidat, Upg::Juke, Upg::Juke, Upg::LoneWolf, Upg::Marksman, Upg::Marksman, Upg::SatSalvo, Upg::SatSalvo, Upg::SwarmTac, Upg::SwarmTac, Upg::TrickShot, Upg::TrickShot,
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Delta7, Fac::Republic).Dial(Shp::V19, Fac::Republic).Dial(Shp::V19, Fac::Republic)
    .DialId(Shp::Delta7).DialId(Shp::V19).DialId(Shp::V19)
    .Ship(Plt::OKenobi,Plt::JediKnight).Ship(Plt::PKoon,Plt::JediKnight).Ship(Plt::MWindu,Plt::JediKnight).Ship(Plt::STiin,Plt::JediKnight)
    .Ship(Plt::OddBall_V19,Plt::BlueSqProtector).Ship(Plt::Kickback,Plt::BlueSqProtector).Ship(Plt::Axe,Plt::GoldSqTrooper).Ship(Plt::Swoop,Plt::GoldSqTrooper).Ship(Plt::Tucker,Plt::GoldSqTrooper)
    .Crit(3).Evade(3).Focus(3).ForceCharge(3).ID(3,10).ID(3,11).ID(3,12).Ion(3).Lock(2,10).Lock(2,11).Lock(2,12).Shield(3).StandardCharge(12).Strain(3).Stress(3)
    .Gas(3)
    .SpareParts(1)
  },

  { Rel::SWZ33, "SWZ33", "", "ARC-170 Starfighter Expansion Pack", RelGroup::Wave3,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
    { { {2019, 3,13}, "https://www.fantasyflightgames.com/en/news/2019/3/13/hammer-the-opposition/"} },
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "",
    {{Shp::ARC170, ""}},
    { Plt::OddBall_ARC, Plt::Wolffe, Plt::Jag, Plt::Sinker, Plt::SquadSevenVet, Plt::OneOhFourthBP },
    { Upg::R3Astro, Upg::R4P44, Upg::ChanPalp, Upg::NovTech, Upg::PerCPilot, Upg::SeasonNav, Upg::CloneCmdrCody, Upg::SeventhFltGun, Upg::VetTailGun, Upg::SyncCon, Upg::Dedicated, Upg::ExpHan, Upg::IonTrp, Upg::ProtTrp },
    {},
    {},
    TokenCollection()
    .Dial(Shp::ARC170, Fac::Republic)
    .DialId(Shp::ARC170)
    .Ship(Plt::OddBall_ARC,Plt::SquadSevenVet).Ship(Plt::Wolffe,Plt::SquadSevenVet).Ship(Plt::Jag,Plt::OneOhFourthBP).Ship(Plt::Sinker,Plt::OneOhFourthBP)
    .Calc(2).Crit(1).Disarm(1).Focus(2).ForceCharge(1).ID(3,15).Ion(3).Lock(2,15).Shield(3).StandardCharge(5).Strain(2).Stress(1)
  },

  { Rel::SWZ34, "SWZ34", "", "Delta-7 Aethersprite Expansion Pack", RelGroup::Wave3,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
    { { {2019, 3,20}, "https://www.fantasyflightgames.com/en/news/2019/3/20/a-powerful-ally/" } },
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "",
    {{Shp::Delta7, "Yellow & gray"}},
    { Plt::ASkywalker_D7, Plt::BOffee, Plt::LUnduli, Plt::ATano_D7, Plt::JediKnight },
    { Upg::R3Astro, Upg::R4PAstro, Upg::CalLasTgt, Upg::Delta7B, Upg::BattleMed, Upg::BrilEva },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Delta7, Fac::Republic)
    .DialId(Shp::Delta7)
    .Ship(Plt::ASkywalker_D7,Plt::JediKnight).Ship(Plt::BOffee,Plt::JediKnight).Ship(Plt::LUnduli,Plt::JediKnight).Ship(Plt::ATano_D7,Plt::JediKnight)
    .Focus(1).ForceCharge(3).ID(3,16).Lock(2,16).Shield(3).StandardCharge(2).Stress(1)
  },

  { Rel::SWZ35, "SWZ35", "", "Separatist Alliance Maneuver Dial Upgrade Kit", RelGroup::Accessories,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
      {},
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ36, "SWZ36", "", "Galactic Republic Maneuver Dial Upgrade Kit", RelGroup::Accessories,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
      {},
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ37, "SWZ37", "", "Z-95-AF4 Headhunter Expansion Pack", RelGroup::Wave3,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
      {},
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "",
    {{Shp::Z95, ""}},
    { Plt::NSuhlak, Plt::KLeeachos, Plt::BSSoldier, Plt::BinayrePirate },
    { Upg::DeadmansSw, Upg::ClustMsl, Upg::ConcusMsl, Upg::MuniFailSa, Upg::CrackShot },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Z95, Fac::Scum)
    .DialId(Shp::Z95)
    .Ship(Plt::NSuhlak,Plt::BSSoldier).Ship(Plt::KLeeachos,Plt::BinayrePirate)
    .Calc(1).Crit(1).Focus(1).ID(3,9).Ion(3).Lock(2,9).Shield(2).StandardCharge(4).Stress(2)
  },

  { Rel::SWZ38, "SWZ38", "", "TIE/sk Striker Expansion Pack", RelGroup::Wave3,
    {2018,11, 9}, {2019, 3,21},
    {
      { {2018,11, 9}, "https://www.fantasyflightgames.com/en/news/2018/11/9/enter-the-clone-wars/" },
      {},
      { {2019, 3,21}, "https://www.fantasyflightgames.com/en/news/2019/3/21/available-now-march-21-1/" }
    },
    "",
    {{Shp::TIEskStriker, ""}},
    { Plt::Duchess, Plt::Countdown, Plt::PureSabacc, Plt::BlackSqScout, Plt::PlanetarySent },
    { Upg::ConnerNet, Upg::ProtonBmb, Upg::SkBombard, Upg::Intimidat, Upg::TrickShot },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEskStriker, Fac::Imperial)
    .DialId(Shp::TIEskStriker)
    .Ship(Plt::Duchess,Plt::BlackSqScout).Ship(Plt::Countdown,Plt::PlanetarySent).Ship(Plt::PureSabacc,Plt::BlackSqScout)
    .Crit(1).Evade(1).Focus(1).Ion(3).StandardCharge(2).Stress(1)
    .ConnerNet(1).ProtonBomb(1)
  },

  { Rel::SWZ39, "SWZ39", "841333107352", "Millennium Falcon Expansion Pack", RelGroup::Wave4,
    {2019, 2, 8}, {2019, 7,12},
    {
      { {2019, 2, 8}, "https://www.fantasyflightgames.com/en/news/2019/2/8/expand-your-operations/" },
      {},
      { {2019, 7,12}, "https://www.fantasyflightgames.com/en/news/2019/7/12/available-now-july-12/" }
    },
    "",
    {{Shp::ModYT1300, ""}},
    { Plt::HSoloReb, Plt::LCalrissianReb, Plt::Chewbacca, Plt::OuterRimSmug },
    { Upg::C3POReb, Upg::ChewieReb, Upg::HSoloReb, Upg::Informant, Upg::LCalrissianReb, Upg::LOrganaReb, Upg::NNunb, Upg::R2D2C, Upg::LSkywalkerG, Upg::RigCargoCh, Upg::HomingMsl, Upg::EngUpg, Upg::SwarmTac, Upg::MilFalcon },
    { Cnd::LstnDev },
    {},
    TokenCollection()
    .Dial(Shp::ModYT1300, Fac::Rebel)
    .DialId(Shp::ModYT1300)
    .Ship(Plt::HSoloReb,Plt::OuterRimSmug).Ship(Plt::LCalrissianReb,Plt::Chewbacca)
    .Calc(2).Evade(2).Focus(2).ForceCharge(1).Shield(5).StandardCharge(6).Stress(1)
    .LaDoTurret(1,Fac::Rebel)
    .LooseCargo(1)
    .ListeningDev(1)
  },

  { Rel::SWZ40, "SWZ40", "841333108076", "Naboo Royal N-1 Starfighter Expansion Pack", RelGroup::Wave4,
    {2019, 2, 8}, {2019, 7,12},
    {
      { {2019, 2, 8}, "https://www.fantasyflightgames.com/en/news/2019/2/8/expand-your-operations/" },
    { { {2019, 5,20}, "https://www.fantasyflightgames.com/en/news/2019/5/20/new-tricks/" } },
      { {2019, 7,12}, "https://www.fantasyflightgames.com/en/news/2019/7/12/available-now-july-12/" }
    },
    "",
    {{Shp::N1, ""}},
    { Plt::ROlie, Plt::ASkywalker_N1, Plt::PAmidala, Plt::DEllberger, Plt::BravoFlightOff, Plt::NabooHandmaiden },
    { Upg::R2Astro, Upg::R2A6, Upg::R2C4, Upg::CollisDet, Upg::PassiveSensors, Upg::Daredevil, Upg::PlasmaTrp },
    { Cnd::Decoyed },
    {},
    TokenCollection()
    .Dial(Shp::N1, Fac::Republic)
    .DialId(Shp::N1)
    .Ship(Plt::ROlie,Plt::DEllberger).Ship(Plt::ASkywalker_N1,Plt::BravoFlightOff).Ship(Plt::PAmidala,Plt::NabooHandmaiden)
    .Disarm(1).Evade(1).Focus(1).ForceCharge(1).ID(3,17).Lock(2,17).Shield(2).StandardCharge(4).Stress(1)
    .Decoyed(1)
  },

  { Rel::SWZ41, "SWZ41", "841333108083", "Hyena-class Droid Bomber Expansion Pack", RelGroup::Wave4,
    {2019, 2, 8}, {2019, 7,12},
    {
      { {2019, 2, 8}, "https://www.fantasyflightgames.com/en/news/2019/2/8/expand-your-operations/" },
    { { {2019, 5,13}, "https://www.fantasyflightgames.com/en/news/2019/5/13/living-starfighters/" } },
      { {2019, 7,12}, "https://www.fantasyflightgames.com/en/news/2019/7/12/available-now-july-12/" }
    },
    "",
    {{Shp::Hyena, ""}},
    { Plt::DBS404, Plt::DBS32C, Plt::BombardmentDrn, Plt::SepBomber, Plt::BaktoidProto, Plt::TUBomber },
    { Upg::LandStruts, Upg::BarRockets, Upg::DiaBorMsl, Upg::DelayedFuses, Upg::Bomblet, Upg::ElectroProtBmb, Upg::PassiveSensors, Upg::TrajSim, Upg::TA175, Upg::PlasmaTrp },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Hyena, Fac::Separatist)
    .DialId(Shp::Hyena)
    .Ship(Plt::DBS404,Plt::DBS32C).Ship(Plt::BombardmentDrn,Plt::SepBomber).Ship(Plt::BaktoidProto,Plt::TUBomber)
    .Calc(2).Disarm(1).Fuse(2).Ion(3).Jam(1).StandardCharge(3).Stress(1)
    .Bomblet(2).ElectroProtBomb(1)
  },

  { Rel::SWZ42, "SWZ42", "841333108090", "A/SF-01 B-Wing Expansion Pack", RelGroup::Wave4,
    {2019, 2, 8}, {2019, 7,12},
    {
      { {2019, 2, 8}, "https://www.fantasyflightgames.com/en/news/2019/2/8/expand-your-operations/" },
      {},
      { {2019, 7,12}, "https://www.fantasyflightgames.com/en/news/2019/7/12/available-now-july-12/" }
    },
    "",
    {{Shp::ASF01BWing, ""}},
    { Plt::BStramm, Plt::TNumb, Plt::BladeSqVet, Plt::BlueSqPlt },
    { Upg::HLC, Upg::IonCan, Upg::JamBeam, Upg::Afterburn, Upg::ElectBaff, Upg::FCS, Upg::SquadLdr, Upg::AdvProtTrp },
    {},
    {},
    TokenCollection()
    .Dial(Shp::ASF01BWing, Fac::Rebel)
    .DialId(Shp::ASF01BWing)
    .Ship(Plt::BStramm,Plt::BladeSqVet).Ship(Plt::TNumb,Plt::BlueSqPlt)
    .Focus(1).ID(3,8).Ion(3).Jam(3).Lock(2,8).Shield(4).StandardCharge(3).Stress(1)
  },

  { Rel::SWZ43, "SWZ43", "841333108106", "VT-49 Decimator Expansion Pack", RelGroup::Wave4,
    {2019, 2, 8}, {2019, 7,12},
    {
      { {2019, 2, 8}, "https://www.fantasyflightgames.com/en/news/2019/2/8/expand-your-operations/" },
      {},
      { {2019, 7,12}, "https://www.fantasyflightgames.com/en/news/2019/7/12/available-now-july-12/" }
    },
    "",
    {{Shp::VT49, ""}},
    { Plt::RAdmChiraneau, Plt::CaptOicunn, Plt::PatrolLdr },
    { Upg::TripleZero, Upg::AgentKallus, Upg::DVader, Upg::FifthBrother, Upg::Gonk, Upg::GrandInq, Upg::SeventhSister, Upg::ProxMine, Upg::BT1, Upg::VetTurret, Upg::TactScramb, Upg::LoneWolf, Upg::Dauntless },
    { Cnd::Hunted },
    {},
    TokenCollection()
    .Dial(Shp::VT49, Fac::Imperial)
    .DialId(Shp::VT49)
    .Ship(Plt::RAdmChiraneau,Plt::PatrolLdr).Ship(Plt::RAdmChiraneau,Plt::PatrolLdr)
    .Calc(2).Focus(2).ForceCharge(3).Jam(2).Reinforce(1).Shield(4).StandardCharge(4).Stress(1).Tractor(2)
    .LaDoTurret(1,Fac::Imperial)
    .ProxMine(1)
    .Hunted(1)
  },

  { Rel::SWZ44, "SWZ44", "841333108113", "TIE/sf Fighter Expansion Pack", RelGroup::Wave4,
    {2019, 2, 8}, {2019, 7,12},
    {
      { {2019, 2, 8}, "https://www.fantasyflightgames.com/en/news/2019/2/8/expand-your-operations/" },
      {},
      { {2019, 7,12}, "https://www.fantasyflightgames.com/en/news/2019/7/12/available-now-july-12/" }
    },
    "",
    {{Shp::TIEsfFighter, ""}},
    { Plt::Quickdraw, Plt::Backdraft, Plt::OmegaSqExpert, Plt::ZetaSqSurvivor },
    { Upg::IonMsl, Upg::HsGunner, Upg::SpecForGun, Upg::Afterburn, Upg::CollisDet, Upg::Juke, Upg::PatAnalyzer },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEsfFighter, Fac::FirstOrder)
    .DialId(Shp::TIEsfFighter)
    .Ship(Plt::Quickdraw,Plt::OmegaSqExpert).Ship(Plt::Backdraft,Plt::ZetaSqSurvivor)
    .Focus(1).ID(3,16).Ion(2).Lock(2,16).Shield(3).StandardCharge(4).Stress(2)
    .SmSiTurret(1,Fac::FirstOrder)
  },

  { Rel::SWZ45, "SWZ45", "841333108120", "Resistance Transport Expansion Pack", RelGroup::Wave4,
    {2019, 2, 8}, {2019, 7,12},
    {
      { {2019, 2, 8}, "https://www.fantasyflightgames.com/en/news/2019/2/8/expand-your-operations/" },
    { { {2019, 6,17}, "https://www.fantasyflightgames.com/en/news/2019/6/17/rebellion-reborn/" } },
      { {2019, 7,12}, "https://www.fantasyflightgames.com/en/news/2019/7/12/available-now-july-12/" }
    },
    "",
    {{Shp::ResTransport, ""}},
    {
      Plt::CNell, Plt::PNGoode, Plt::NChavdri, Plt::LogDivPlt,
      Plt::BB8, Plt::RTico, Plt::Finn, Plt::VMoradi,
    },
    { Upg::R2HA, Upg::R5X3, Upg::Autoblasters, Upg::AHoldo, Upg::GA97, Upg::KConnix, Upg::KSella, Upg::LDAcy, Upg::LOrganaRes, Upg::PZ4CO, Upg::Afterburn, Upg::AngledDef, Upg::SparePartsCan, Upg::Composure, Upg::ExpHan, Upg::PlasmaTrp },
    { Cnd::CompIntel, Cnd::ITR },
    {},
    TokenCollection()
    .Dial(Shp::ResTransport, Fac::Resistance).Dial(Shp::ResTransportPod, Fac::Resistance)
    .DialId(Shp::ResTransport).DialId(Shp::ResTransportPod)
    .Ship(Plt::CNell,Plt::PNGoode).Ship(Plt::NChavdri,Plt::LogDivPlt)
    .Ship(Plt::BB8,Plt::VMoradi).Ship(Plt::RTico,Plt::Finn)
    .Calc(1).Evade(1).Focus(1).ForceCharge(1).ID(3,13).Jam(1).Lock(2,13).Reinforce(1).Shield(3).StandardCharge(5).Strain(1).Stress(2)
    .SpareParts(1)
    .CompIntel(1).ITR(1)
  },

  { Rel::SWZ46, "SWZ46", "", "Deluxe Movement Tools and Range Ruler", RelGroup::Accessories,
    {2019, 2, 8}, {2019, 7,12},
    {
      { {2019, 2, 8}, "https://www.fantasyflightgames.com/en/news/2019/2/8/expand-your-operations/" },
      {},
      { {2019, 7,12}, "https://www.fantasyflightgames.com/en/news/2019/7/12/available-now-july-12/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ47, "SWZ47", "841333106935", "Nantex-class Starfighter Expansion Pack", RelGroup::Wave5,
    {2019, 4,13}, {2019, 9,13},
    {
      { {2019, 4,13}, "https://www.fantasyflightgames.com/en/news/2019/4/13/the-fate-of-the-galaxy/" },
    { { {2019, 8,13}, "https://www.fantasyflightgames.com/en/news/2019/8/13/pulling-the-strings/" } },
      { {2019, 9,13}, "https://www.fantasyflightgames.com/en/news/2019/9/13/available-now-september-13/" }
    },
    "",
    {{Shp::Nantex, ""}},
    { Plt::SFac, Plt::BKret, Plt::Chertek, Plt::PetArenaAce, Plt::StalgasinHG, Plt::Gorgol },
    { Upg::StealthDev, Upg::TargetComp, Upg::Ensnare, Upg::GravDef, Upg::Juke, Upg::SnapShot },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Nantex, Fac::Separatist)
    .DialId(Shp::Nantex)
    .Ship(Plt::SFac,Plt::PetArenaAce).Ship(Plt::BKret,Plt::StalgasinHG).Ship(Plt::Chertek,Plt::Gorgol)
    .Crit(1).Evade(1).Focus(1).ID(3,13).Lock(2,13).StandardCharge(2).Stress(1).Tractor(2)
    .SmSiTurret(1,Fac::Separatist)
  },

  { Rel::SWZ48, "SWZ48", "841333106942", "BTL-B Y-Wing Expansion Pack", RelGroup::Wave5,
    {2019, 4,13}, {2019, 9,13},
    {
      { {2019, 4,13}, "https://www.fantasyflightgames.com/en/news/2019/4/13/the-fate-of-the-galaxy/" },
    { { {2019, 8,28}, "https://www.fantasyflightgames.com/en/news/2019/8/28/lead-the-attack/" } },
      { {2019, 9,13}, "https://www.fantasyflightgames.com/en/news/2019/9/13/available-now-september-13/" }
    },
    "",
    {{Shp::BTLBYWing, ""}},
    { Plt::ASkywalker_BTLB, Plt::OddBall_BTLB, Plt::Matchstick, Plt::Broadside, Plt::ShadowSqVet, Plt::Goji, Plt::R2D2, Plt::RedSqBomber },
    { Upg::C110P, Upg::C3PORep, Upg::ElectroProtBmb, Upg::ProtonBmb, Upg::Foresight, Upg::PrecogReflexes, Upg::ATano, Upg::DelayedFuses, Upg::SnapShot, Upg::IonCanTrt },
    {},
    {},
    TokenCollection()
    .Dial(Shp::BTLBYWing, Fac::Republic)
    .DialId(Shp::BTLBYWing)
    .Ship(Plt::ASkywalker_BTLB,Plt::ShadowSqVet).Ship(Plt::OddBall_BTLB,Plt::Goji).Ship(Plt::Matchstick,Plt::R2D2).Ship(Plt::Broadside,Plt::RedSqBomber)
    .Calc(2).Disarm(1).Focus(1).ForceCharge(4).Fuse(2).ID(3,14).Ion(2).Jam(1).Lock(2,14).Shield(3).StandardCharge(5).Strain(1)
    .SmSiTurret(1,Fac::Republic)
    .ElectroProtBomb(1).ProtonBomb(1)
  },

  { Rel::SWZ49, "SWZ49", "841333109127", "Ghost Expansion Pack", RelGroup::Wave5,
    {2019, 4,13}, {2019, 9,13},
    {
      { {2019, 4,13}, "https://www.fantasyflightgames.com/en/news/2019/4/13/the-fate-of-the-galaxy/" },
      {},
      { {2019, 9,13}, "https://www.fantasyflightgames.com/en/news/2019/9/13/available-now-september-13/" }
    },
    "",
    {{Shp::VCX100, ""}, {Shp::Sheathipede, ""}},
    {
      Plt::HSyndulla_VCX, Plt::KJarrus_VCX, Plt::Chopper, Plt::LothalRebel,
      Plt::FRau_Sh, Plt::EBridger_Sh, Plt::ZOrrelios_Sh, Plt::AP5
    },
    { Upg::ChopperA, Upg::ChopperC, Upg::HSyndulla, Upg::KJarrus, Upg::Maul, Upg::ZOrrelios, Upg::Hate, Upg::PredictiveShot, Upg::AgileGunner, Upg::TactScramb, Upg::CollisDet, Upg::SquadLdr, Upg::Ghost, Upg::Phantom, Upg::IonTrp, Upg::DorsalTrt },
    {},
    {},
    TokenCollection()
    .Dial(Shp::VCX100, Fac::Rebel).Dial(Shp::Sheathipede, Fac::Rebel)
    .DialId(Shp::VCX100).DialId(Shp::Sheathipede)
    .Ship(Plt::HSyndulla_VCX,Plt::Chopper).Ship(Plt::KJarrus_VCX,Plt::LothalRebel)
    .Ship(Plt::FRau_Sh,Plt::ZOrrelios_Sh).Ship(Plt::EBridger_Sh,Plt::AP5)
    .Calc(2).Focus(1).ForceCharge(4).Ion(3).Jam(1).Reinforce(1).Shield(5).StandardCharge(4).Stress(1)
    .LaSiTurret(1,Fac::Rebel)
  },

  { Rel::SWZ50, "SWZ50", "841333109134", "Inquisitors' TIE Expansion Pack", RelGroup::Wave5,
    {2019, 4,13}, {2019, 9,13},
    {
      { {2019, 4,13}, "https://www.fantasyflightgames.com/en/news/2019/4/13/the-fate-of-the-galaxy/" },
      {},
      { {2019, 9,13}, "https://www.fantasyflightgames.com/en/news/2019/9/13/available-now-september-13/" }
    },
    "",
    {{Shp::TIEAdvV1, ""}},
    { Plt::GrandInq, Plt::SeventhSister, Plt::BaronOfEmp, Plt::Inquisitor },
    { Upg::Hate, Upg::HeightPerc, Upg::PredictiveShot, Upg::PRockets, Upg::Afterburn },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEAdvV1, Fac::Imperial)
    .DialId(Shp::TIEAdvV1)
    .Ship(Plt::GrandInq,Plt::Inquisitor).Ship(Plt::SeventhSister,Plt::BaronOfEmp)
    .Crit(1).Evade(1).Focus(1).ForceCharge(3).ID(3,10).Lock(2,10).Shield(2).StandardCharge(3).Stress(1)
  },

  { Rel::SWZ51, "SWZ51", "841333109141", "Punishing One Expansion Pack", RelGroup::Wave5,
    {2019, 4,13}, {2019, 9,13},
    {
      { {2019, 4,13}, "https://www.fantasyflightgames.com/en/news/2019/4/13/the-fate-of-the-galaxy/" },
      {},
      { {2019, 9,13}, "https://www.fantasyflightgames.com/en/news/2019/9/13/available-now-september-13/" }
    },
    "",
    {{Shp::JM5K, ""}},
    { Plt::Dengar, Plt::TTrevura, Plt::Manaroo, Plt::ContractedSc },
    { Upg::R2Astro, Upg::R5P8, Upg::TripleZero, Upg::Informant, Upg::LRazzi, Upg::PerCPilot, Upg::Dengar, Upg::ContraCyb, Upg::LoneWolf, Upg::PunishingOne, Upg::AdvProtTrp, Upg::IonTrp },
    { Cnd::LstnDev },
    {},
    TokenCollection()
    .Dial(Shp::JM5K, Fac::Scum)
    .DialId(Shp::JM5K)
    .Ship(Plt::Dengar,Plt::Manaroo).Ship(Plt::TTrevura,Plt::ContractedSc)
    .Calc(1).Crit(1).Disarm(1).Focus(1).ID(3,11).Ion(3).Lock(2,11).Shield(3).StandardCharge(9).Stress(2)
    .LaSiTurret(1,Fac::Scum)
    .ListeningDev(1)
  },

  { Rel::SWZ52, "SWZ52", "841333109158", "M3-A Interceptor Expansion Pack", RelGroup::Wave5,
    {2019, 4,13}, {2019, 9,13},
    {
      { {2019, 4,13}, "https://www.fantasyflightgames.com/en/news/2019/4/13/the-fate-of-the-galaxy/" },
      {},
      { {2019, 9,13}, "https://www.fantasyflightgames.com/en/news/2019/9/13/available-now-september-13/" }
    },
    "",
    {{Shp::M3A, ""}},
    { Plt::Serissu, Plt::GRed, Plt::LAshera, Plt::QJast, Plt::TansariiPtVet, Plt::Inaldra, Plt::CartelSpacer, Plt::SBounder },
    { Upg::IonCan, Upg::JamBeam, Upg::IonMsl, Upg::Intimidat, Upg::ProtTrp },
    {},
    {},
    TokenCollection()
    .Dial(Shp::M3A, Fac::Scum)
    .DialId(Shp::M3A)
    .Ship(Plt::Serissu,Plt::TansariiPtVet).Ship(Plt::GRed,Plt::Inaldra).Ship(Plt::LAshera,Plt::SBounder).Ship(Plt::QJast,Plt::CartelSpacer)
    .Crit(1).Evade(1).Disarm(1).Focus(1).ID(3,12).Ion(2).Jam(2).Lock(2,12).Shield(1).StandardCharge(3)
  },

  { Rel::SWZ53, "SWZ53", "841333106959", "Huge Ship Conversion Kit", RelGroup::Huge,
    {2019, 7,11}, {2019,11, 8},
    {
      { {2019, 7,11}, "https://www.fantasyflightgames.com/en/news/2019/7/11/bigger-battles/" },
      {},
      { {2019,11, 8}, "https://www.fantasyflightgames.com/en/news/2019/11/8/available-now-october-25-1/" }
    },
    "",
    {},
    {
      Plt::SynSmugglers,  // C-ROC   (Scum)
      Plt::SepPrivateers, // C-ROC   (Separatist)
      Plt::AlderaanGuard, // CR-90   (Rebel)
      Plt::RepJudiciary,  // CR-90   (Republic)
      Plt::OuterRimGar,   // Gozanti (Imperial)
      Plt::FOSymp,        // Gozanti (FO)
      Plt::EchoBaseEvac,  // GR-75   (Rebel)
      Plt::NewRepVol,     // GR-75   (Resistance)
      Plt::OuterRimPat,   // Raider  (Imperial)
      Plt::FOCollab       // Raider  (FO)
    },
    {
      Upg::HLC,
      Upg::AdaptiveShlds, Upg::AdaptiveShlds, Upg::BoostedScanners, Upg::BoostedScanners, Upg::OptPowerCore, Upg::OptPowerCore, Upg::TibannaRes, Upg::TibannaRes,
      Upg::AdmOzzel, Upg::Azmorigan, Upg::CaptNeeda, Upg::CRieekan, Upg::JDodonna, Upg::RAntilles, Upg::StalwartCapt, Upg::StratCmdr,
      Upg::CorsairRefit,
      Upg::NovTech, Upg::SeasonNav, Upg::TFarr,
      Upg::AgileGunner, Upg::AgileGunner, Upg::HsGunner, Upg::HsGunner,
      Upg::IonCanBat, Upg::IonCanBat, Upg::OrdTubes, Upg::OrdTubes, Upg::PointDefBat, Upg::PointDefBat, Upg::TargetingBat, Upg::TargetingBat, Upg::TurbolaserBat, Upg::TurbolaserBat,
      Upg::QRLocks, Upg::SabMap, Upg::ScanBaff,
      Upg::ClustMsl, Upg::ClustMsl, Upg::ConcusMsl, Upg::ConcusMsl,
      Upg::ProxMine,
      Upg::FCS,
      Upg::BombSpec, Upg::BombSpec, Upg::CommsTeam, Upg::CommsTeam, Upg::DamageCtrlTeam, Upg::DamageCtrlTeam, Upg::GunnerySpec, Upg::GunnerySpec, Upg::IGRMDroids, Upg::IGRMDroids, Upg::OrdnanceTeam, Upg::OrdnanceTeam, Upg::SensExp, Upg::SensExp,
      Upg::Assailer, Upg::BloodCrow, Upg::BrightHope, Upg::BrokenHorn, Upg::Corvus, Upg::DodonnasPride, Upg::Impetuous, Upg::InsatiableWorrt, Upg::Instigator, Upg::JainasLight, Upg::Liberator, Upg::Luminous, Upg::MerchantOne, Upg::QuantumStorm, Upg::Requiem, Upg::Suppressor, Upg::TantiveIV, Upg::Thunderstrike, Upg::Vector,
      Upg::AdvProtTrp, Upg::AdvProtTrp,
      Upg::DorsalTrt, Upg::DorsalTrt,
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::CR90,Fac::Rebel).Dial(Shp::CR90,Fac::Republic)
    .Dial(Shp::CROC,Fac::Scum).Dial(Shp::CROC,Fac::Separatist)
    .Dial(Shp::Gozanti,Fac::Imperial).Dial(Shp::Gozanti,Fac::FirstOrder)
    .Dial(Shp::GR75,Fac::Rebel).Dial(Shp::GR75,Fac::Resistance)
    .Dial(Shp::Raider,Fac::Imperial).Dial(Shp::Raider,Fac::FirstOrder)
    .DialId(Shp::CR90).DialId(Shp::CROC).DialId(Shp::Gozanti).DialId(Shp::GR75).DialId(Shp::Raider)
    .Ship(Plt::AlderaanGuard,Plt::RepJudiciary)
    .Ship(Plt::OuterRimPat,Plt::FOCollab)
    .Ship(Plt::EchoBaseEvac,Plt::NewRepVol)
    .Ship(Plt::OuterRimGar,Plt::FOSymp)
    .Ship(Plt::SynSmugglers,Plt::SepPrivateers)
    .Calc(2).Disarm(2).Evade(2).ID(5,7).ID(5,8).Ion(4).Jam(3).Lock(3,7).Lock(3,8).Reinforce(2).TurretMount(6)
    .HuSiTurret(4,std::nullopt).HuDoTurret(2,std::nullopt)
    .Cargo(2)
    .ProxMine(2)
  },

/*
***
2019-11-22
Further Wave 6 Clarification/News
Also, two quick updates on upcoming products. The reprinted ships from Wave 6
(Interceptor / Defender / Hound's Tooth / RZ-1 A-Wing) won't be able to make a
December release date. We've pushed them back to an expected release date in
January accordingly.

In addition, due to overwhelming stock of the first edition Imperial Raider (grey
box), we will not be printing or releasing the second edition Imperial Raider (black
box). We're very sorry for the miscommunication on this, and it should not happen
again with any of the other products announced for X-Wing.

And finally! As promised, System Open information and prizes are going up today!
Scheduled for 12:00 PM Central Time.
***
  { Rel::SWZ54, "SWZ54", "841333109172", "Imperial Raider Expansion Pack", RelGroup::Huge,
    {2019, 8, 7}, {0000,00,00},
    {
      { {2019, 8, 7}, "https://www.fantasyflightgames.com/en/news/2019/8/7/the-weight-of-the-empire/" },
      {},
      { {0000,00,00}, "" }
    },
    {{Shp::Raider, ""}},
    { Plt::FOCollab, Plt::OuterRimPat },
    {
      Upg::AdaptiveShlds, Upg::BoostedScanners, Upg::OptPowerCore, Upg::TibannaRes,
      Upg::AdmOzzel, Upg::CaptNeeda, Upg::StalwartCapt, Upg::StratCmdr,
      Upg::NovTech, Upg::SeasonNav,
      Upg::AgileGunner, Upg::HsGunner,
      Upg::IonCanBat, Upg::OrdTubes, Upg::PointDefBat, Upg::TargetingBat, Upg::TurbolaserBat,
      Upg::ClustMsl, Upg::ConcusMsl,
      Upg::BombSpec, Upg::CommsTeam, Upg::DamageCtrlTeam, Upg::GunnerySpec, Upg::OrdnanceTeam, Upg::SensExp,
      Upg::Assailer, Upg::Corvus, Upg::Impetuous, Upg::Instigator,
      Upg::AdvProtTrp
    },
    {},
    {},
    {}
  },
*/

  { Rel::SWZ55, "SWZ55", "841333109189", "Tantive IV Expansion Pack", RelGroup::Huge,
    {2019, 7,11}, {2019,11, 8},
    {
      { {2019, 7,11}, "https://www.fantasyflightgames.com/en/news/2019/7/11/bigger-battles/" },
    { { {2019, 9,24}, "https://www.fantasyflightgames.com/en/news/2019/9/24/delivering-hope/" } },
      { {2019,11, 8}, "https://www.fantasyflightgames.com/en/news/2019/11/8/available-now-october-25-1/" }
    },
    "This expansion has some errors: the maneuver dial has the maneuvers for the Raider and the dial ID has a Raider on the back side.",
    {{Shp::CR90, ""}},
    { Plt::AlderaanGuard, Plt::RepJudiciary },
    {
      Upg::AdaptiveShlds, Upg::BoostedScanners, Upg::OptPowerCore, Upg::TibannaRes,
      Upg::CRieekan, Upg::JDodonna, Upg::RAntilles, Upg::StalwartCapt, Upg::StratCmdr,
      Upg::NovTech, Upg::SeasonNav, Upg::TFarr,
      Upg::AgileGunner, Upg::HsGunner,
      Upg::IonCanBat, Upg::PointDefBat, Upg::TargetingBat, Upg::TurbolaserBat,
      Upg::BombSpec, Upg::CommsTeam, Upg::DamageCtrlTeam, Upg::GunnerySpec, Upg::SensExp,
      Upg::DodonnasPride, Upg::JainasLight, Upg::Liberator, Upg::TantiveIV, Upg::Thunderstrike
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::CR90,Fac::Rebel).Dial(Shp::CR90,Fac::Republic)  // ignoring that fact that the initial prints came with the Raider maneuvers...
    .DialId(Shp::CR90) // this is probably a misprint too
    .Ship(Plt::AlderaanGuard,Plt::RepJudiciary)
    .Calc(2).Disarm(1).Evade(2).ID(5,10).Ion(6).Jam(3).Lock(3,10).Reinforce(1).StandardCharge(12).TurretMount(3)
    .HuSiTurret(2,std::nullopt).HuDoTurret(1,std::nullopt)
  },

  { Rel::SWZ56, "SWZ56", "841333109196", "C-ROC Cruiser Expansion Pack", RelGroup::Huge,
    {2019, 7,11}, {2019,11,15},
    {
      { {2019, 7,11}, "https://www.fantasyflightgames.com/en/news/2019/7/11/bigger-battles/" },
    { { {2019,10, 2}, "https://www.fantasyflightgames.com/en/news/2019/10/2/outside-the-law-1/" } },
      { {2019,11,15}, "https://www.fantasyflightgames.com/en/news/2019/11/15/available-now-november-15/" }
    },
    "",
    {{Shp::CROC, ""}},
    { Plt::SynSmugglers, Plt::SepPrivateers },
    {
      Upg::HLC,
      Upg::AdaptiveShlds, Upg::BoostedScanners, Upg::OptPowerCore, Upg::TibannaRes,
      Upg::Azmorigan, Upg::StalwartCapt, Upg::StratCmdr,
      Upg::CorsairRefit,
      Upg::NovTech, Upg::SeasonNav,
      Upg::AgileGunner, Upg::HsGunner,
      Upg::IonCanBat, Upg::OrdTubes, Upg::PointDefBat, Upg::TargetingBat, Upg::TurbolaserBat,
      Upg::QRLocks, Upg::SabMap, Upg::ScanBaff,
      Upg::ClustMsl, Upg::ConcusMsl,
      Upg::ProxMine,
      Upg::BombSpec, Upg::CommsTeam, Upg::DamageCtrlTeam, Upg::GunnerySpec, Upg::IGRMDroids, Upg::OrdnanceTeam, Upg::SensExp,
      Upg::BrokenHorn, Upg::InsatiableWorrt, Upg::MerchantOne,
      Upg::DorsalTrt
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::CROC,Fac::Scum).Dial(Shp::CROC,Fac::Separatist)
    .DialId(Shp::CROC)
    .Ship(Plt::SynSmugglers,Plt::SepPrivateers)
    .Calc(5).Crit(1).Disarm(1).Evade(2).ID(5,11).Ion(4).Jam(3).Lock(3,11).Reinforce(1).StandardCharge(14).Tractor(3).TurretMount(3)
    .HuSiTurret(2,std::nullopt).HuDoTurret(1,std::nullopt)
    .Cargo(2)
    .ProxMine(2)
  },

  { Rel::SWZ57, "SWZ57", "841333109165", "Epic Battles Multiplayer Expansion", RelGroup::Huge,
    {2019, 7,11}, {2019,11, 8},
    {
      { {2019, 7,11}, "https://www.fantasyflightgames.com/en/news/2019/7/11/a-larger-world/" },
    { { {2019,10,11}, "https://www.fantasyflightgames.com/en/news/2019/10/11/escalated-tensions/" } },
      { {2019,11, 8}, "https://www.fantasyflightgames.com/en/news/2019/11/8/available-now-october-25-1/" }
    },
    "",
    {},
    {},
    { Upg::AgentotEmp, Upg::DreadHunter, Upg::FOElite, Upg::VetWingLdr },
    {},
    {},
    TokenCollection()
  },

  { Rel::SWZ58, "SWZ58", "", "Hound's Tooth Expansion Pack", RelGroup::Wave6,
    {2019, 8, 1}, {2020, 1,31},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/echoes-of-war/" },
      {},
      { {2020, 1,31}, "https://www.fantasyflightgames.com/en/news/2020/1/31/available-now-january-24-1/" }
    },
    "",
    {{Shp::YV666, ""}},
    { Plt::Bossk_YV, Plt::MEval, Plt::LRazzi, Plt::TSlaver, Plt::NashtahPup, Plt::Bossk_Z95 },
    { Upg::TracBeam, Upg::CVizago, Upg::FLSlicer, Upg::Gonk, Upg::Jabba, Upg::TactOff, Upg::Bossk, Upg::BT1, Upg::Greedo, Upg::FeedbackAr, Upg::HomingMsl, Upg::AblatPlat, Upg::SquadLdr, Upg::HoundsTooth },
    {},
    {},
    TokenCollection()
    .Dial(Shp::YV666,Fac::Scum).Dial(Shp::Z95,Fac::Scum)
    .DialId(Shp::YV666).DialId(Shp::Z95)
    .Ship(Plt::Bossk_YV,Plt::TSlaver).Ship(Plt::MEval,Plt::LRazzi)
    .Ship(Plt::Bossk_Z95,Plt::NashtahPup)
    .Crit(1).Disarm(1).Focus(1).ID(3,10).Ion(3).Jam(1).Lock(2,10).Reinforce(1).Shield(5).StandardCharge(10).Stress(1).Tractor(3)
  },

  { Rel::SWZ59, "SWZ59", "", "TIE/in Interceptor Expansion Pack", RelGroup::Wave6,
    {2019, 8, 1}, {2020, 1,31},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/echoes-of-war/" },
      {},
      { {2020, 1,31}, "https://www.fantasyflightgames.com/en/news/2020/1/31/available-now-january-24-1/" }
    },
    "",
    {{Shp::TIEinInterceptor, ""}},
    { Plt::SFel, Plt::TPhennir, Plt::SaberSqAce, Plt::AlphaSqPlt },
    { Upg::HullUpg, Upg::ShieldUpg, Upg::Daredevil, Upg::Outmaneuv, Upg::Predator },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEinInterceptor,Fac::Imperial)
    .DialId(Shp::TIEinInterceptor)
    .Ship(Plt::SFel,Plt::SaberSqAce).Ship(Plt::TPhennir,Plt::AlphaSqPlt)
    .Crit(1).Evade(1).Focus(2).Ion(1).Shield(1).Stress(1)
  },

  { Rel::SWZ60, "SWZ60", "", "TIE/D Defender Expansion Pack", RelGroup::Wave6,
    {2019, 8, 1}, {2020, 1,31},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/echoes-of-war/" },
      {},
      { {2020, 1,31}, "https://www.fantasyflightgames.com/en/news/2020/1/31/available-now-january-24-1/" }
    },
    "",
    {{Shp::TIEdDefender, ""}},
    { Plt::RBrath, Plt::ColVessery, Plt::CountessRyad, Plt::OnyxSqAce, Plt::DeltaSqPlt },
    { Upg::TracBeam, Upg::IonMsl, Upg::AdvSensors, Upg::Elusive,  },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEdDefender,Fac::Imperial)
    .DialId(Shp::TIEdDefender)
    .Ship(Plt::RBrath,Plt::OnyxSqAce).Ship(Plt::ColVessery,Plt::OnyxSqAce).Ship(Plt::CountessRyad,Plt::DeltaSqPlt)
    .Crit(1).Evade(1).Focus(1).ID(3,10).Ion(2).Lock(2,10).Shield(4).StandardCharge(1).Stress(1).Tractor(3)
  },

  { Rel::SWZ61, "SWZ61", "", "RZ-1 A-Wing Expansion Pack", RelGroup::Wave6,
    {2019, 8, 1}, {2020, 1,31},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/echoes-of-war/" },
      {},
      { {2020, 1,31}, "https://www.fantasyflightgames.com/en/news/2020/1/31/available-now-january-24-1/" }
    },
    "",
    {{Shp::RZ1AWing, ""}},
    { Plt::JFarrell, Plt::ACrynyd, Plt::GreenSqPlt, Plt::PhoenixSqPlt },
    { Upg::ConcusMsl, Upg::PRockets, Upg::Daredevil, Upg::Intimidat, Upg::Juke },
    {},
    {},
    TokenCollection()
    .Dial(Shp::RZ1AWing,Fac::Rebel)
    .DialId(Shp::RZ1AWing)
    .Ship(Plt::JFarrell,Plt::PhoenixSqPlt).Ship(Plt::ACrynyd,Plt::GreenSqPlt)
    .Crit(1).Evade(1).Focus(1).ID(3,9).Ion(1).Lock(2,9).Shield(2).StandardCharge(3).Stress(1)
  },

  { Rel::SWZ62, "SWZ62", "", "Major Vonreg's TIE Expansion Pack", RelGroup::Wave6,
    {2019, 8, 1}, {2020, 1,31},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/echoes-of-war/" },
    { { {2020, 1, 8}, "https://www.fantasyflightgames.com/en/news/2020/1/8/legend-reborn/" } },
      { {2020, 1,31}, "https://www.fantasyflightgames.com/en/news/2020/1/31/available-now-january-24-1/" }
    },
    "",
    {{Shp::TIEbaInterceptor, ""}},
    { Plt::MajVonreg, Plt::Holo, Plt::Ember, Plt::FOProvocateur },
    { Upg::ConcusMsl, Upg::MagPulseWar, Upg::MuniFailSa, Upg::ProudTrad, Upg::SnapShot, Upg::DeuteriumPC },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEbaInterceptor,Fac::FirstOrder)
    .DialId(Shp::TIEbaInterceptor)
    .Ship(Plt::MajVonreg,Plt::FOProvocateur).Ship(Plt::Holo,Plt::Ember)
    .Crit(1).Deplete(3).Disarm(1).Evade(1).Focus(1).ID(3,7).Ion(1).Jam(1).Lock(2,7).Shield(2).StandardCharge(5).Strain(2).Stress(1)
  },

  { Rel::SWZ63, "SWZ63", "", "Fireball Expansion Pack", RelGroup::Wave6,
    {2019, 8, 1}, {2020, 1,31},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/echoes-of-war/" },
    { { {2020, 1,15}, "https://www.fantasyflightgames.com/en/news/2020/1/15/explosive-speed/"}},
      { {2020, 1,31}, "https://www.fantasyflightgames.com/en/news/2020/1/31/available-now-january-24-1/" }
    },
    "",
    {{Shp::Fireball, ""}},
    { Plt::JYeager, Plt::KXiono, Plt::R1J5, Plt::ColStationMech },
    { Upg::R1J5, Upg::CoaxiumHyp, Upg::MagPulseWar, Upg::AdvSLAM, Upg::TargetComp, Upg::SnapShot, Upg::KazsFireball },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Fireball,Fac::Resistance)
    .DialId(Shp::Fireball)
    .Ship(Plt::JYeager,Plt::R1J5).Ship(Plt::KXiono,Plt::ColStationMech)
    .Calc(1).Crit(1).Deplete(2).Disarm(1).Evade(1).Focus(1).Ion(1).Jam(1).StandardCharge(5).Stress(1)
  },

  { Rel::SWZ64, "SWZ64", "841333110307", "Never Tell Me the Odds Obstacles Pack", RelGroup::CardPack,
    {2019, 8, 1}, {2020, 1,31},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/aces-in-the-hole/" },
    { { {2019,12,17}, "https://www.fantasyflightgames.com/en/news/2019/12/17/uncharted-territory/" } },
      { {2020, 1,31}, "https://www.fantasyflightgames.com/en/news/2020/1/31/available-now-january-24-1/" }
    },
    "",
    {},
    {},
    { Upg::RigCargoCh, Upg::SparePartsCan },
    {},
    { Env::AsteroidShower, Env::Clouzon36Deposits, Env::CometTail, Env::IonClouds, Env::MynockInfestation, Env::RecentWreckage },
    TokenCollection()
    .Strain(3).StandardCharge(2)
    .Asteroid(9).Debris(3).Gas(3)
    .LooseCargo(1).SpareParts(1)
  },

  { Rel::SWZ65, "SWZ65", "841333110314", "Fully Loaded Devices Pack", RelGroup::CardPack,
    {2019, 8, 1}, {2020, 1,31},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/aces-in-the-hole/" },
    { { {2019,12,17}, "https://www.fantasyflightgames.com/en/news/2019/12/17/uncharted-territory/" } },
      { {2020, 1,31}, "https://www.fantasyflightgames.com/en/news/2020/1/31/available-now-january-24-1/" }
    },
    "",
    {},
    {},
    { Upg::Bomblet, Upg::ClusterMines, Upg::ConnerNet, Upg::ElectroProtBmb, Upg::IonBmb, Upg::ProtonBmb, Upg::ProxMine, Upg::SeismicCh, Upg::DelayedFuses, Upg::DelayedFuses, Upg::TrajSim, Upg::TrajSim },
    {},
    { Env::ContinuousBombardment, Env::Countdown, Env::Minefield, Env::MunitionsCache, Env::PinpointBombardment, Env::UnexplodedOrdnance },
    TokenCollection()
    .Disarm(3).Ion(6).StandardCharge(6).Fuse(8).Victory(6)
    .Bomblet(2).ClusterMine(1).ConnerNet(1).ElectroProtBomb(1).IonBomb(2).ProtonBomb(2).ProxMine(2).SeismicCharge(2)
  },

  { Rel::SWZ66, "SWZ66", "841333110321", "Hotshots and Aces Reinforcements Pack", RelGroup::CardPack,
    {2019, 8, 1}, {2020, 1,31},
    {
      { {2019, 8, 1}, "https://www.fantasyflightgames.com/en/news/2019/8/1/aces-in-the-hole/" },
    { { {2019,12,23}, "https://www.fantasyflightgames.com/en/news/2019/12/23/joining-the-cause/" },
      { {2020,01,02}, "https://www.fantasyflightgames.com/en/news/2020/1/2/seizing-power/" } },
      { {2020, 1,31}, "https://www.fantasyflightgames.com/en/news/2020/1/31/available-now-january-24-1/" }
    },
    "",
    {},
    {
      Plt::GMoonsong, Plt::K2SO, Plt::LOrgana, Plt::AKallus,
      Plt::FifthBrother, Plt::Vagabond, Plt::MKee,
      Plt::NLumb, Plt::G4RGORVM, Plt::Bossk_Z95,
      Plt::PTico, Plt::ZTlo, Plt::RBlario,
      Plt::LtLeHuse, Plt::CaptPhasma, Plt::Rush
    },
    {
      Upg::Autoblasters, Upg::Autoblasters,
      Upg::StabSFoils, Upg::StabSFoils,
      Upg::TripleZero, Upg::K2SO, Upg::Maul,
      Upg::BrilEva, Upg::BrilEva, Upg::Foresight, Upg::Foresight, Upg::Hate, Upg::Hate, Upg::PrecogReflexes, Upg::PrecogReflexes, Upg::PredictiveShot, Upg::PredictiveShot,
      Upg::AgileGunner, Upg::AgileGunner, Upg::BT1,
      Upg::CoaxiumHyp, Upg::CoaxiumHyp,
      Upg::MagPulseWar, Upg::MagPulseWar, Upg::BarRockets, Upg::BarRockets, Upg::DiaBorMsl,
      Upg::AngledDef, Upg::AngledDef, Upg::TargetComp, Upg::TargetComp, Upg::TargetComp,
      Upg::PassiveSensors, Upg::PassiveSensors,
      Upg::Composure, Upg::Composure, Upg::SnapShot, Upg::SnapShot,
      Upg::AdvOptics, Upg::AdvOptics, Upg::PatAnalyzer, Upg::PatAnalyzer, Upg::PThrusters, Upg::PThrusters,
      Upg::MoldyCrow,
      Upg::PlasmaTrp, Upg::PlasmaTrp
    },
    {},
    {},
    TokenCollection()
    .Ship(Plt::GMoonsong,Plt::GMoonsong).Ship(Plt::K2SO,Plt::K2SO).Ship(Plt::LOrgana,Plt::LOrgana).Ship(Plt::AKallus,Plt::AKallus)
    .Ship(Plt::FifthBrother,Plt::FifthBrother).Ship(Plt::Vagabond,Plt::Vagabond).Ship(Plt::MKee,Plt::MKee)
    .Ship(Plt::NLumb,Plt::NLumb).Ship(Plt::G4RGORVM,Plt::G4RGORVM).Ship(Plt::Bossk_Z95,Plt::Bossk_Z95)
    .Ship(Plt::PTico,Plt::PTico).Ship(Plt::RBlario,Plt::RBlario).Ship(Plt::ZTlo,Plt::ZTlo)
    .Ship(Plt::CaptPhasma,Plt::CaptPhasma).Ship(Plt::LtLeHuse,Plt::LtLeHuse).Ship(Plt::Rush,Plt::Rush)
    .Calc(2).Deplete(2).Disarm(2).Evade(2).Focus(2).ForceCharge(2).ID(3,11).ID(3,12).Jam(2).Lock(2,11).Lock(2,12).Reinforce(2).StandardCharge(4).Strain(2)
  },

  { Rel::SWZ67, "SWZ67", "", "TIE/rb Heavy Expansion Pack", RelGroup::Wave7,
    {2020, 7,29}, {2020,10,30},
    {
      { {2020, 7,30}, "https://www.fantasyflightgames.com/en/news/2020/7/30/experience-the-saga/" },
    { { {2020, 9,16}, "https://www.fantasyflightgames.com/en/news/2020/9/16/fury-of-the-empire/" } },
      { {2020,10,30}, "https://www.fantasyflightgames.com/en/news/2020/10/30/available-now-october-30/" }
    },
    "",
    {{Shp::TIErbHeavy, ""}},
    { Plt::Rampage, Plt::LDree, Plt::OnyxSqSentry, Plt::CaridaAcaCadet },
    { Upg::HLC, Upg::IonCan, Upg::SyncLasCan, Upg::ManAssMGK300, Upg::TarAssMGK300, Upg::AblatPlat, Upg::DeadeyeShot, Upg::DeadeyeShot, Upg::IonLimOverride, Upg::IonLimOverride,  Upg::SnapShot, Upg::SnapShot },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIErbHeavy,Fac::Imperial)
    .DialId(Shp::TIErbHeavy)
    .Ship(Plt::Rampage,Plt::OnyxSqSentry).Ship(Plt::LDree,Plt::CaridaAcaCadet)
    .Calc(3).Crit(1).Focus(2).ID(3,13).Ion(3).Lock(2,13).Reinforce(1).StandardCharge(2).Strain(3).Stress(2)
    .LaSiTurret(1,Fac::Imperial)
  },

  { Rel::SWZ68, "SWZ68", "", "Heralds of Hope Squadron Pack", RelGroup::Wave7,
    {2020, 7,29}, {2020,10,30},
    {
      { {2020, 7,30}, "https://www.fantasyflightgames.com/en/news/2020/7/30/experience-the-saga/" },
    { { {2020, 9,23}, "https://www.fantasyflightgames.com/en/news/2020/9/23/fight-to-the-finish/" } },
      { {2020,10,30}, "https://www.fantasyflightgames.com/en/news/2020/10/30/available-now-october-30/" }
    },
    "",
    {{Shp::T70XWing,"Orange"}, {Shp::T70XWing,"Green"}, {Shp::RZ2AWing,"Green"}},
    {
      Plt::PDameron_RC, Plt::BlackSqAce_Res, Plt::BlackSqAce_Res, Plt::CThrenalli_T70, Plt::TWexley_BT, Plt::RedSqExpert, Plt::RedSqExpert, Plt::NChireen, Plt::BlueSqRookie, Plt::BlueSqRookie,
      Plt::SVanik, Plt::WTyce, Plt::SJavos, Plt::MCobben, Plt::GreenSqExp, Plt::BlueSqRecruit
    },
    {
      Upg::R2D2_A_Res, Upg::R4Astro, Upg::R6D8,
      Upg::IonCan, Upg::Underslung, Upg::Underslung,
      Upg::IntSFoils, Upg::IntSFoils,
      Upg::ConcusMsl, Upg::IonMsl,
      Upg::OverdriveThr,
      Upg::BackwardsTail, Upg::BackwardsTail, Upg::BackwardsTail, Upg::Daredevil, Upg::DeadeyeShot, Upg::DeadeyeShot, Upg::SnapShot, Upg::SnapShot, Upg::StarbirdSlash, Upg::StarbirdSlash,
      Upg::AutoTgtPri, Upg::AutoTgtPri, Upg::PThrusters,
      Upg::BlackOne
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::T70XWing,Fac::Resistance).Dial(Shp::T70XWing,Fac::Resistance).Dial(Shp::RZ2AWing,Fac::Resistance)
    .DialId(Shp::T70XWing).DialId(Shp::T70XWing).DialId(Shp::RZ2AWing)
    .Ship(Plt::PDameron_RC,Plt::BlackSqAce_Res).Ship(Plt::CThrenalli_T70,Plt::BlueSqRookie).Ship(Plt::TWexley,Plt::RedSqExpert).Ship(Plt::BlackSqAce_Res,Plt::RedSqExpert).Ship(Plt::NChireen,Plt::BlueSqRookie)
    .Ship(Plt::SVanik,Plt::GreenSqExp).Ship(Plt::WTyce,Plt::BlueSqRecruit).Ship(Plt::SJavos,Plt::MCobben)
    .Calc(2).Crit(1).Deplete(1).Disarm(1).Evade(1).Focus(1).ID(3,11).ID(3,12).Ion(3).Lock(2,11).Lock(2,12).Shield(7).StandardCharge(10).Strain(2)
    .SmSiTurret(3,Fac::Resistance)
  },

  { Rel::SWZ69, "SWZ69", "", "Xi-class Light Shuttle Expansion Pack", RelGroup::Wave7,
    {2020, 1, 8}, {2020, 9,25},
    {
      { {2020, 1, 8}, "https://www.fantasyflightgames.com/en/news/2020/1/8/attack-on-all-fronts/" },
    { { {2020, 8,26}, "https://www.fantasyflightgames.com/en/news/2020/8/26/out-of-the-ashes/" } },
      { {2020, 9,25}, "https://www.fantasyflightgames.com/en/news/2020/9/25/available-now-september-25/" }
    },
    "",
    {{Shp::Xi, ""}},
    { Plt::CmdrMalarus_Xi, Plt::GHask_Xi, Plt::AgentTerex, Plt::FOCourier },
    { Upg::AgentTerex, Upg::CmdrMalarus, Upg::CmdrPyre, Upg::TactOff, Upg::DeadmansSw, Upg::ContraCyb, Upg::InertDamp, Upg::RigCargoCh, Upg::DeadeyeShot, Upg::DeadeyeShot, Upg::SnapShot, Upg::AutoTgtPri, Upg::AutoTgtPri, Upg::SensorBuoySuite },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Xi,Fac::FirstOrder)
    .DialId(Shp::Xi)
    .Ship(Plt::CmdrMalarus_Xi,Plt::AgentTerex).Ship(Plt::GHask_Xi,Plt::FOCourier)
    .Calc(3).Crit(1).Focus(1).Jam(2).ID(3,8).Lock(2,8).Shield(2).StandardCharge(2).Strain(2).Stress(1)
    .LooseCargo(1)
    .SensorBuoyBlue(1).SensorBuoyRed(1)
  },

  { Rel::SWZ70, "SWZ70", "841333111175", "LAAT/i Gunship Expansion Pack", RelGroup::Wave7,
    {2020, 1, 8}, {2020, 9,25},
    {
      { {2020, 1, 8}, "https://www.fantasyflightgames.com/en/news/2020/1/8/attack-on-all-fronts/" },
    { { {2020, 9, 2}, "https://www.fantasyflightgames.com/en/news/2020/9/2/concentrate-your-fire/" } },
      { {2020, 9,25}, "https://www.fantasyflightgames.com/en/news/2020/9/25/available-now-september-25/" }
    },
    "",
    {{Shp::LAAT, ""}},
    { Plt::Hawk, Plt::Warthog, Plt::Hound, Plt::TwoOneTwoBatPlt },
    { Upg::ASecura, Upg::Fives, Upg::GhostCompany, Upg::KFisto, Upg::PKoon, Upg::Wolfpack, Upg::Yoda, Upg::AgileGunner, Upg::CloneCaptRex, Upg::SuppGunner, Upg::ConcusMsl, Upg::IonMsl, Upg::MultiMslPods, Upg::DeadeyeShot },
    {},
    {},
    TokenCollection()
    .Dial(Shp::LAAT,Fac::Republic)
    .DialId(Shp::LAAT)
    .Ship(Plt::Hawk,Plt::Hound).Ship(Plt::Warthog,Plt::TwoOneTwoBatPlt)
    .Crit(1).Deplete(3).Evade(2).Focus(2).ForceCharge(3).ID(3,17).Ion(3).Lock(2,17).Reinforce(2).Shield(2).StandardCharge(5).Strain(3).Stress(2)
    .LaDoTurret(1,Fac::Republic)
  },

  { Rel::SWZ71, "SWZ71", "841333111182", "HMP Droid Gunship Expansion Pack", RelGroup::Wave7,
    {2020, 1, 8}, {2020, 9,25},
    {
      { {2020, 1, 8}, "https://www.fantasyflightgames.com/en/news/2020/1/8/attack-on-all-fronts/" },
    { { {2020, 9, 9}, "https://www.fantasyflightgames.com/en/news/2020/9/9/rule-skies/" } },
      { {2020, 9,25}, "https://www.fantasyflightgames.com/en/news/2020/9/25/available-now-september-25/" }
    },
    "",
    {{Shp::HMP, ""}},
    { Plt::OnderonOpp, Plt::DGS286, Plt::SepPredator, Plt::GeonosianProto, Plt::DGS047, Plt::BaktoidDrone },
    { Upg::SyncLasCan, Upg::Repulsorlift, Upg::IonMsl, Upg::MultiMslPods, Upg::DelayedFuses, Upg::ConcussionBmb, Upg::ConcussionBmb, Upg::Kalani },
    {},
    {},
    TokenCollection()
    .Dial(Shp::HMP,Fac::Separatist)
    .DialId(Shp::HMP)
    .Ship(Plt::DGS286,Plt::SepPredator).Ship(Plt::OnderonOpp,Plt::GeonosianProto).Ship(Plt::DGS047,Plt::BaktoidDrone)
    .Crit(1).Calc(2).Fuse(2).ID(3,15).Ion(3).Lock(2,15).StandardCharge(8).Shield(3).Strain(2).Stress(1)
    .ConcussionBomb(2)
  },

  { Rel::SWZ72, "SWZ72", "", "Rebel Alliance Damage Deck", RelGroup::Accessories,
    {2019, 9, 3}, {2020, 6,24},
    {
      { {2019, 9, 3}, "https://www.fantasyflightgames.com/en/news/2019/9/3/beautiful-destruction/" },
      {},
      { {2020, 6,24}, "https://www.fantasyflightgames.com/en/news/2020/6/26/available-now-june-26/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ73, "SWZ73", "", "Galactic Empire Damage Deck", RelGroup::Accessories,
    {2019, 9, 3}, {2020, 6,24},
    {
      { {2019, 9, 3}, "https://www.fantasyflightgames.com/en/news/2019/9/3/beautiful-destruction/" },
      {},
      { {2020, 6,24}, "https://www.fantasyflightgames.com/en/news/2020/6/26/available-now-june-26/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ74, "SWZ74", "", "Scum and Villainy Damage Deck", RelGroup::Accessories,
    {2019, 9, 3}, {2020, 6,24},
    {
      { {2019, 9, 3}, "https://www.fantasyflightgames.com/en/news/2019/9/3/beautiful-destruction/" },
      {},
      { {2020, 6,24}, "https://www.fantasyflightgames.com/en/news/2020/6/26/available-now-june-26/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ75, "SWZ75", "", "Resistance Damage Deck", RelGroup::Accessories,
    {2019, 9, 3}, {2020, 6,24},
    {
      { {2019, 9, 3}, "https://www.fantasyflightgames.com/en/news/2019/9/3/beautiful-destruction/" },
      {},
      { {2020, 6,24}, "https://www.fantasyflightgames.com/en/news/2020/6/26/available-now-june-26/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ76, "SWZ76", "", "First Order Damage Deck", RelGroup::Accessories,
    {2019, 9, 3}, {2020, 6,24},
    {
      { {2019, 9, 3}, "https://www.fantasyflightgames.com/en/news/2019/9/3/beautiful-destruction/" },
      {},
      { {2020, 6,24}, "https://www.fantasyflightgames.com/en/news/2020/6/26/available-now-june-26/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ77, "SWZ77", "", "Galactic Republic Damage Deck", RelGroup::Accessories,
    {2019, 9, 3}, {2020, 6,24},
    {
      { {2019, 9, 3}, "https://www.fantasyflightgames.com/en/news/2019/9/3/beautiful-destruction/" },
      {},
      { {2020, 6,24}, "https://www.fantasyflightgames.com/en/news/2020/6/26/available-now-june-26/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ78, "SWZ78", "", "Separatist Alliance Damage Deck", RelGroup::Accessories,
    {2019, 9, 3}, {2020, 6,24},
    {
      { {2019, 9, 3}, "https://www.fantasyflightgames.com/en/news/2019/9/3/beautiful-destruction/" },
      {},
      { {2020, 6,24}, "https://www.fantasyflightgames.com/en/news/2020/6/26/available-now-june-26/" }
    },
    "",
    {},{},{},{},{},
    TokenCollection()
  },

  { Rel::SWZ79, "SWZ79", "", "Eta-2 Actis Expansion Pack", RelGroup::Wave8,
    {2020, 7,29}, {2020,11,27},
    {
      { {2020, 7,30}, "https://www.fantasyflightgames.com/en/news/2020/7/30/experience-the-saga/" },
    { { {2020,10, 7}, "https://www.fantasyflightgames.com/en/news/2020/10/7/push-the-limit/" } },
      { {2020,11,27}, "https://www.fantasyflightgames.com/en/news/2020/11/27/available-now-november-27/" }
    },
    "",
    {{Shp::Eta2Actis, ""},{Shp::Syliure, ""}},
    { Plt::ASkywalker_Eta2, Plt::OKenobi_Eta2, Plt::ASecura, Plt::STi, Plt::JediGen, Plt::Yoda, Plt::TransGalMegCL },
    { Upg::R2D2_A_Rep, Upg::JediCmdr, Upg::ExtManeuvers, Upg::Patience, Upg::Syliure31, Upg::MargSabl },
    {},
    {},
    TokenCollection()
    .Dial(Shp::Eta2Actis,Fac::Republic)
    .DialId(Shp::Eta2Actis)
    .Ship(Plt::ASkywalker_Eta2,Plt::ASecura).Ship(Plt::OKenobi_Eta2,Plt::STi).Ship(Plt::Yoda,Plt::JediGen).Ship(Plt::TransGalMegCL,Plt::TransGalMegCL)
    .Crit(1).Deplete(2).Evade(2).Focus(2).ForceCharge(3).ID(3,13).Lock(2,13).Shield(2).StandardCharge(2).Strain(1).Stress(1)
  },

  { Rel::SWZ80, "SWZ80", "", "Nimbus-class V-Wing Expansion Pack", RelGroup::Wave8,
    {2020, 7,29}, {2020,11,27},
    {
      { {2020, 7,30}, "https://www.fantasyflightgames.com/en/news/2020/7/30/experience-the-saga/" },
    { { {2020,10,15}, "https://www.fantasyflightgames.com/en/news/2020/10/15/stay-sharp/" } },
      { {2020,11,27}, "https://www.fantasyflightgames.com/en/news/2020/11/27/available-now-november-27/" }
    },
    "",
    {{Shp::NimbusClass, ""}},
    { Plt::Contrail, Plt::OddBall_Vw, Plt::Klick, Plt::WTarkin, Plt::ShadowSqEsc, Plt::LoyalistVol },
    { Upg::Q7Astro, Upg::R7A7, Upg::Alpha3B, Upg::Alpha3E, Upg::PrecIonEng, Upg::ThermalDet, Upg::IonLimOverride },
    {},
    {},
    TokenCollection()
    .Dial(Shp::NimbusClass,Fac::Republic)
    .DialId(Shp::NimbusClass)
    .Ship(Plt::Contrail,Plt::ShadowSqEsc).Ship(Plt::OddBall_Vw,Plt::Klick).Ship(Plt::WTarkin,Plt::LoyalistVol)
    .Crit(1).Focus(1).ID(3,14).Ion(2).Lock(2,14).Shield(2).StandardCharge(10).Strain(2).Stress(1)
    .ThermalDet(4)
  },

  { Rel::SWZ81, "SWZ81", "", "Droid Tri-Fighter Expansion Pack", RelGroup::Wave8,
    {2020, 7,29}, {2020,11,27},
    {
      { {2020, 7,30}, "https://www.fantasyflightgames.com/en/news/2020/7/30/experience-the-saga/" },
    { { {2020,10,21}, "https://www.fantasyflightgames.com/en/news/2020/10/21/total-destruction/" } },
      { {2020,11,27}, "https://www.fantasyflightgames.com/en/news/2020/11/27/available-now-november-27/" }
    },
    "",
    {{Shp::DroidTriFighter, ""}},
    { Plt::PhlacArpProto, Plt::DIST81, Plt::FearsomePred, Plt::DIS347, Plt::SepInt, Plt::ColInt },
    { Upg::InterceptBoost, Upg::DiscordMsl, Upg::XX23, Upg::IndepCalc, Upg::IndepCalc, Upg::FCS, Upg::MargSabl },
    { Cnd::FearfulPrey },
    {},
    TokenCollection()
    .Dial(Shp::DroidTriFighter,Fac::Separatist)
    .DialId(Shp::DroidTriFighter)
    .Ship(Plt::PhlacArpProto,Plt::ColInt).Ship(Plt::DIST81,Plt::SepInt).Ship(Plt::DIS347,Plt::FearsomePred)
    .Calc(3).Crit(1).Disarm(1).Evade(1).ID(3,10).Lock(2,10).StandardCharge(5).Strain(1).Stress(1)
    .BuzzDroid(1)
    .FearfulPrey(1)
  },

  { Rel::SWZ82, "SWZ82", "", "Jango Fett's Slave I Expansion Pack", RelGroup::Wave8,
    {2020, 7,29}, {2020,11,27},
    {
      { {2020, 7,30}, "https://www.fantasyflightgames.com/en/news/2020/7/30/experience-the-saga/" },
    { { {2020,10,29}, "https://www.fantasyflightgames.com/en/news/2020/10/29/build-your-legend/" } },
      { {2020,11,27}, "https://www.fantasyflightgames.com/en/news/2020/11/27/available-now-november-27/" }
    },
    "",
    {{Shp::Firespray, ""}},
    { Plt::JFett, Plt::ZWesell, Plt::BFett_Sep, Plt::SepRacketeer },
    { Upg::JamBeam, Upg::HOhnaka, Upg::JFett, Upg::ZWesell, Upg::BFett_G, Upg::SuppGunner, Upg::WeaponsSysOff, Upg::FalseTransCodes, Upg::AblatPlat, Upg::ThermalDet, Upg::DeadeyeShot, Upg::DebGambit, Upg::SlaveI_Sep },
    { Cnd::YBMB, Cnd::YBMB, Cnd::YSTM, Cnd::YSTM },
    {},
    TokenCollection()
    .Dial(Shp::Firespray, Fac::Separatist)
    .DialId(Shp::Firespray)
    .Ship(Plt::JFett,Plt::BFett_Sep).Ship(Plt::ZWesell,Plt::SepRacketeer)
    .Crit(1).Deplete(1).Evade(1).Focus(1).ID(3,11).Ion(3).Jam(1).Lock(2,11).Reinforce(1).Shield(4).StandardCharge(9).Strain(2).Stress(1)
    .ThermalDet(4)
    .YBMBYSTM(1)
  },

  { Rel::SWZ83, "SWZ83", "", "Phoenix Cell Squadron Pack", RelGroup::Wave9,
    {2021, 2, 3}, {2021, 3,26},
    {
      { {2021, 2,15}, "https://www.fantasyflightgames.com/en/news/2021/2/15/reinforcements-of-renown/" },
    { { {2021, 2,24}, "https://www.fantasyflightgames.com/en/news/2021/2/24/from-out-of-the-flames/" } },
      { {0000,00,00}, "" }
    },
    "",
    {{Shp::ASF01BWing, "Phoenix Cell"},{Shp::RZ1AWing, "Phoenix Cell"},{Shp::RZ1AWing, "Phoenix Cell"}},
    {
      Plt::HSyndulla_ASF01, Plt::NPollard, Plt::BladeSqVet, Plt::BlueSqPlt,
      Plt::HSyndulla_RZ1, Plt::ATano_RZ1, Plt::SBey_RZ1, Plt::WAntilles_RZ1, Plt::DKlivian, Plt::SWren_RZ1, Plt::GreenSqPlt, Plt::GreenSqPlt, Plt::PhoenixSqPlt, Plt::PhoenixSqPlt
    },
    {
      Upg::Autoblasters, Upg::SyncLasCan,
      Upg::B6Prototype_CMD, Upg::PhoenixSq,
      Upg::StabSFoils, Upg::StabSFoils, Upg::VectoredCanRZ1, Upg::VectoredCanRZ1, Upg::VectoredCanRZ1, Upg::VectoredCanRZ1,
      Upg::ExtManeuvers, Upg::InstAim, Upg::Patience, Upg::Sense,
      Upg::SWren_Gun, Upg::SuppGunner, Upg::WeaponsSysOff,
      Upg::MagPulseWar, Upg::MagPulseWar, Upg::XX23, Upg::XX23,
      Upg::PassiveSensors,
      Upg::DeadeyeShot, Upg::DebGambit, Upg::Hopeful, Upg::Hopeful, Upg::Hopeful, Upg::MargSabl, Upg::MargSabl, Upg::SatSalvo, Upg::StarbirdSlash, Upg::StarbirdSlash, Upg::TierfonBR, Upg::TierfonBR,
      Upg::B6Prototype,
      Upg::PlasmaTrp,
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::ASF01BWing,Fac::Rebel).Dial(Shp::RZ1AWing,Fac::Rebel).Dial(Shp::RZ1AWing,Fac::Rebel)
    .DialId(Shp::ASF01BWing).DialId(Shp::RZ1AWing).DialId(Shp::RZ1AWing)
    .Ship(Plt::HSyndulla_ASF01,Plt::BlueSqPlt).Ship(Plt::NPollard,Plt::BladeSqVet)
    .Ship(Plt::HSyndulla_RZ1,Plt::PhoenixSqPlt).Ship(Plt::ATano_RZ1,Plt::PhoenixSqPlt).Ship(Plt::SBey_RZ1,Plt::GreenSqPlt).Ship(Plt::WAntilles_RZ1,Plt::GreenSqPlt).Ship(Plt::DKlivian,Plt::GreenSqPlt).Ship(Plt::SWren_RZ1,Plt::PhoenixSqPlt)
    .Crit(3).Deplete(1).Evade(3).Focus(3).ForceCharge(3).ID(3,11).ID(3,12).Ion(1).Jam(1).Lock(2,11).Lock(2,12).Shield(8).StandardCharge(6).Strain(2).Stress(3)
    .SmSiTurret(4,Fac::Rebel)
  },

  { Rel::SWZ84, "SWZ84", "", "Skystrike Academy Squadron Pack", RelGroup::Wave9,
    {2021, 2, 3}, {2021, 3,26},
    {
      { {2021, 2,15}, "https://www.fantasyflightgames.com/en/news/2021/2/15/reinforcements-of-renown/" },
    { { {2021, 2,25}, "https://www.fantasyflightgames.com/en/news/2021/2/25/strike-without-mercy/" },
      { {2021, 3,19}, "https://www.fantasyflightgames.com/en/news/2021/3/19/skystrike-academy-squadron-pack/" } },
      { {0000,00,00}, "" }
    },
    "",
    {{Shp::TIEdDefender, "Skystrike Academy"},{Shp::TIEinInterceptor, "Skystrike Academy"},{Shp::TIEinInterceptor, "Skystrike Academy"}},
    {
      Plt::DVader_d, Plt::VSkerris_d, Plt::CaptDobbs, Plt::OnyxSqAce, Plt::DeltaSqPlt,
      Plt::CRee, Plt::VSkerris_TIEin, Plt::CmdtGoran, Plt::GHask_TIEin, Plt::LtLorrir, Plt::NWindrider, Plt::SaberSqAce, Plt::SaberSqAce, Plt::AlphaSqPlt, Plt::AlphaSqPlt
    },
    {
      Upg::Autoblasters, Upg::TracBeam,
      Upg::ShadowWing, Upg::SkystrikeAcaCla, Upg::SkystrikeAcaCla,
      Upg::SensitiveCtrls, Upg::SensitiveCtrls, Upg::SensitiveCtrls, Upg::SensitiveCtrls, Upg::TIEDefElite, Upg::TIEDefElite,
      Upg::ExtManeuvers, Upg::InstAim, Upg::Sense,
      Upg::MagPulseWar, Upg::XX23,
      Upg::HullUpg, Upg::PrecIonEng, Upg::PrecIonEng, Upg::PrecIonEng, Upg::ShieldUpg, Upg::StaDcVanes, Upg::TargetComp, Upg::TargetComp,
      Upg::PassiveSensors,
      Upg::Composure, Upg::Daredevil, Upg::Disciplined, Upg::Disciplined, Upg::Disciplined, Upg::Disciplined, Upg::InterloperTurn, Upg::InterloperTurn, Upg::MargSabl, Upg::MargSabl,
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEdDefender,Fac::Imperial).Dial(Shp::TIEinInterceptor,Fac::Imperial).Dial(Shp::TIEinInterceptor,Fac::Imperial)
    .DialId(Shp::TIEdDefender).DialId(Shp::TIEinInterceptor).DialId(Shp::TIEinInterceptor)
    .Ship(Plt::DVader_d,Plt::DVader_d).Ship(Plt::VSkerris_d,Plt::OnyxSqAce).Ship(Plt::CaptDobbs,Plt::DeltaSqPlt)
    .Ship(Plt::CRee,Plt::AlphaSqPlt).Ship(Plt::VSkerris_TIEin,Plt::SaberSqAce).Ship(Plt::CmdtGoran,Plt::SaberSqAce).Ship(Plt::GHask_TIEin,Plt::AlphaSqPlt).Ship(Plt::LtLorrir,Plt::SaberSqAce).Ship(Plt::NWindrider,Plt::AlphaSqPlt)
    .Crit(3).Deplete(1).Evade(3).Focus(3).ForceCharge(3).ID(3,13).ID(3,14).Ion(1).Jam(1).Lock(2,13).Lock(2,14).Shield(5).StandardCharge(6).Strain(2).Stress(3).Tractor(2)
  },

  { Rel::SWZ85, "SWZ85", "", "Fugitives and Collaborators Squadron Pack", RelGroup::Wave9,
    {2021, 2, 3}, {2021, 3,26},
    {
      { {2021, 2,15}, "https://www.fantasyflightgames.com/en/news/2021/2/15/reinforcements-of-renown/" },
    { { {2021, 2,26}, "https://www.fantasyflightgames.com/en/news/2021/2/26/the-desperate-and-the-devious/" } },
      { {0000,00,00}, "" }
    },
    "",
    {{Shp::HWK290, "Fugitives and Collarborators"}, {Shp::BTLA4YWing, "Fugitives and Collarborators"}, {Shp::BTLA4YWing, "Fugitives and Collarborators"}},
    {
      Plt::Tapusk, Plt::GKey, Plt::KJarrus_HWK, Plt::SpiceRunner,
      Plt::LKai, Plt::AHadrassian, Plt::Padric, Plt::AmaxWarrior, Plt::AmaxWarrior, Plt::AmaxWarrior, Plt::JinataSecOff, Plt::JinataSecOff, Plt::JinataSecOff
    },
    {
      Upg::Genius, Upg::R4B11, Upg::R5TK,
      Upg::Bounty, Upg::InIt,
      Upg::GKey, Upg::HOhnaka, Upg::JFett, Upg::ProtGleb, Upg::ZWesell,
      Upg::ExtManeuvers, Upg::Patience,
      Upg::BFett_G, Upg::SuppGunner, Upg::WeaponsSysOff, Upg::WeaponsSysOff,
      Upg::FalseTransCodes, Upg::FalseTransCodes,
      Upg::EngUpg,
      Upg::ConcussionBmb, Upg::ThermalDet, Upg::ThermalDet,
      Upg::Cutthroat, Upg::Cutthroat, Upg::Cutthroat, Upg::DeadeyeShot, Upg::MargSabl, Upg::MargSabl, Upg::SatSalvo, Upg::TierfonBR, Upg::TierfonBR,
      Upg::AutoTgtPri, Upg::AutoTgtPri, Upg::TargetSync,
      Upg::PlasmaTrp,
      Upg::DorsalTrt, Upg::IonCanTrt,
    },
    {Cnd::YBMB, Cnd::YBMB, Cnd::YSTM, Cnd::YSTM},
    {},
    TokenCollection()
    .Dial(Shp::HWK290,Fac::Scum).Dial(Shp::BTLA4YWing,Fac::Scum).Dial(Shp::BTLA4YWing,Fac::Scum)
    .DialId(Shp::HWK290).DialId(Shp::BTLA4YWing).DialId(Shp::BTLA4YWing)
    .Ship(Plt::Tapusk,Plt::SpiceRunner).Ship(Plt::GKey,Plt::SpiceRunner).Ship(Plt::KJarrus_HWK,Plt::SpiceRunner)
    .Ship(Plt::LKai, Plt::JinataSecOff).Ship(Plt::AHadrassian, Plt::AmaxWarrior).Ship(Plt::Padric,Plt::AmaxWarrior).Ship(Plt::AmaxWarrior,Plt::JinataSecOff).Ship(Plt::AmaxWarrior,Plt::JinataSecOff)
    .Calc(2).Crit(3).Deplete(1).Focus(3).ForceCharge(1).ID(3,8).ID(3,9).Jam(2).Lock(2,8).Lock(2,9).Shield(6).StandardCharge(8).Strain(4).Stress(3)
    .SmSiTurret(3,Fac::Scum)
    .ConcussionBomb(4).ThermalDet(8)
    .YBMBYSTM(1)
  },

  { Rel::SWZ86, "SWZ86", "", "BTA-NR2 Y-wing Expansion Pack", RelGroup::Wave10,
    {2021, 7,23}, {2021, 9,24},
    {
      { {0000,00,00}, "" },
      { },
      { {0000,00,00}, "" }
    },
    "",
    {{Shp::BTANR2YWing, "Red"},{Shp::BTANR2YWing, "Blue"}},
    { Plt::ZBliss, Plt::TNasz, Plt::WTeshlo, Plt::LFossang, Plt::NewRepPatrol, Plt::NewRepPatrol, Plt::SZaro, Plt::AAckbar, Plt::CThrenalli_BTA, Plt::KijimiSR, Plt::KijimiSR, Plt::CKapellim },
    {
      Upg::L4ER5, Upg::WatchfulAstro, Upg::WatchfulAstro,
      Upg::WartimeLoad, Upg::WartimeLoad,
      Upg::BFrik, Upg::OvertunedMod, Upg::OvertunedMod,
      Upg::ElecChaffMsl, Upg::ElecChaffMsl,
      Upg::DelayedFuses, Upg::EngUpg, Upg::TargetComp,
      Upg::ConcussionBmb,
      Upg::ExpHan, Upg::TierfonBR, Upg::TierfonBR,
      Upg::IonCanTrt,
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::BTANR2YWing, Fac::Resistance).Dial(Shp::BTANR2YWing, Fac::Resistance)
    .DialId(Shp::BTANR2YWing).DialId(Shp::BTANR2YWing)
    .Ship(Plt::ZBliss,Plt::NewRepPatrol).Ship(Plt::TNasz,Plt::NewRepPatrol).Ship(Plt::WTeshlo,Plt::NewRepPatrol).Ship(Plt::SZaro,Plt::NewRepPatrol)
    .Ship(Plt::LFossang,Plt::KijimiSR).Ship(Plt::CThrenalli_BTA,Plt::KijimiSR).Ship(Plt::AAckbar,Plt::KijimiSR).Ship(Plt::CKapellim,Plt::KijimiSR)
    .Calc(3).Disarm(2).Evade(1).Focus(2).Fuse(2).ID(3,10).ID(3,11).Ion(3).Jam(2).Lock(2,10).Lock(2,11).Shield(10).StandardCharge(6).Strain(4)
    .ConcussionBomb(2).ElectroChaff(2)
    .SmSiTurret(2,Fac::Resistance)
  },

  { Rel::SWZ87, "SWZ87", "", "Fury of the First Order Expansion Pack", RelGroup::Wave10,
    {2021, 7,23}, {2021, 9,24},
    {
      { {0000,00,00}, "" },
      { },
      { {0000,00,00}, "" }
    },
    "",
    {{Shp::TIEwiWhisperMod, ""},{Shp::TIEseBomber, ""},{Shp::TIEseBomber, ""}},
    {
      Plt::KRen_WI, Plt::Wrath, Plt::Nightfall, Plt::SevOhNineLegAce, Plt::Whirlwind, Plt::RedFuryZealot,
      Plt::Breach, Plt::Scorch_SE, Plt::Dread, Plt::FOCadet, Plt::FOCadet, Plt::Grudge, Plt::SJTestPlt, Plt::SJTestPlt
    },
    {
      Upg::EnJamSuite,
      Upg::Compassion, Upg::Compassion, Upg::Malice, Upg::Malice, Upg::ShatteringShot, Upg::ShatteringShot,
      Upg::DT798, Upg::FOOrdTech, Upg::FOOrdTech, Upg::SuppGunner, Upg::SuppGunner,
      Upg::ClustMsl, Upg::HomingMsl, Upg::PRockets,
      Upg::DelayedFuses, Upg::DelayedFuses, Upg::EngUpg, Upg::EngUpg,
      Upg::ElecChaffMsl, Upg::ElecChaffMsl,
      Upg::ConcussionBmb, Upg::ConcussionBmb,
      Upg::FeedbackPing, Upg::FeedbackPing, Upg::IonLimOverride, Upg::IonLimOverride,
      Upg::AdvOptics, Upg::PThrusters, Upg::SensorScramble, Upg::SensorScramble,
      Upg::IonTrp
    },
    {},
    {},
    TokenCollection()
    .Dial(Shp::TIEwiWhisperMod,Fac::FirstOrder).Dial(Shp::TIEseBomber,Fac::FirstOrder).Dial(Shp::TIEseBomber,Fac::FirstOrder)
    .DialId(Shp::TIEwiWhisperMod).DialId(Shp::TIEseBomber).DialId(Shp::TIEseBomber)
    .Ship(Plt::KRen_WI,Plt::Whirlwind).Ship(Plt::Wrath,Plt::SevOhNineLegAce).Ship(Plt::Nightfall,Plt::RedFuryZealot)
    .Ship(Plt::Breach,Plt::FOCadet).Ship(Plt::Scorch_SE,Plt::FOCadet).Ship(Plt::Dread,Plt::SJTestPlt).Ship(Plt::Grudge,Plt::SJTestPlt)
    .Cloak(2).Crit(2).Deplete(3).Disarm(2).Evade(3).Focus(3).ForceCharge(3).Fuse(2).ID(3,12).ID(3,13).ID(3,14).Ion(3).Jam(4).Lock(3,12).Lock(3,13).Lock(3,14).Shield(6).StandardCharge(12).Strain(3).Stress(3)
    .ConcussionBomb(2).ElectroChaff(2)
    .SmSiTurret(1,Fac::FirstOrder)
  },

  { Rel::SWZ88, "SWZ88", "", "Trident-class Assault Ship", RelGroup::Huge,
    {2021, 3,18}, {2021, 6,25},
    {
      { {0000,00,00}, "" },
      {},
      { {0000,00,00}, "" }
    },
    "",
    {{Shp::Trident, ""}},
    { Plt::ColicoidDest, Plt::LawlessPirates },
    {
      Upg::AdaptiveShlds, Upg::BoostedScanners, Upg::OptPowerCore, Upg::TibannaRes, // cargo
      Upg::AVentress, Upg::GenGrievous_CMD, Upg::HOhnaka_CMD, Upg::MTuuk, Upg::RTamson, Upg::StalwartCapt, Upg::StratCmdr, Upg::ZealousCapt, // command/crew
      Upg::NovTech, Upg::SeasonNav, // crew
      Upg::AgileGunner, Upg::HsGunner, // gunner
      Upg::CorsairCrew, Upg::CorsairCrew, // gunner/team
      Upg::IonCanBat, Upg::OrdTubes, Upg::PointDefBat, Upg::TargetingBat, Upg::TractorTent, // hardpoint
      Upg::DrillBeak, Upg::EnhancedProp, Upg::ProtCanBat, Upg::ProtCanBat, Upg::ProtCanBat, // hardpoint/cargo
      Upg::BombSpec, Upg::CommsTeam, Upg::DamageCtrlTeam, Upg::DroidCrew, Upg::DroidCrew, Upg::GunnerySpec, Upg::OrdnanceTeam, Upg::SensExp, Upg::TractorTech, Upg::TractorTech, Upg::TractorTech, // team
      Upg::Grappler, Upg::NautolansRev, Upg::NeimoidianGrasp, Upg::Trident, // title
      Upg::TrackingTrp // tro
    },
    { },
    { },
    TokenCollection()
    .Dial(Shp::Trident,Fac::Separatist).Dial(Shp::Trident,Fac::Scum)
    .DialId(Shp::Trident).DialId(Shp::Trident)
    .Ship(Plt::ColicoidDest, Plt::LawlessPirates)
    .Calc(3).Deplete(6).Evade(1).Focus(1).ForceCharge(1).ID(3,15).Ion(5).Jam(1).Lock(2,15).Reinforce(1).StandardCharge(10).Strain(6).Tractor(6)
    .HuSiTurret(2,std::nullopt).HuDoTurret(1,std::nullopt)
    .StratMark(1).StratMark(2).StratMark(3).StratMark(4).TrackTorp("A").TrackTorp("B").TrackTorp("C").TrackTorpLock("A").TrackTorpLock("B").TrackTorpLock("C")
    .TridentAgg(1).Victory(6)
  },

/*
  { Rel::, "", "", "", RelGroup::,
    {0000,00,00}, {0000,00,00},
    {
      { {0000,00,00}, "" },
      {},
      { {0000,00,00}, "" }
    },
    "notes...",
    {{Shp::, ""}},
    { Plt:: },
    { Upg:: },
    { Cnd:: },
    { Env:: },
    TokenCollection()
  },
*/

};

}
