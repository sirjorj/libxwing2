#include "modifier.h"

namespace libxwing2 {

Modifier::Modifier() {}

Modifier& Modifier::ActAdd(ActionBar a)                       { this->_actAdd = a;    return *this; }
Modifier& Modifier::Agility(int16_t a)                        { this->_agility = a;   return *this; }
Modifier& Modifier::AttackAdd(PriAttacks a)                   { this->_attackAdd = a; return *this; }
Modifier& Modifier::Attack(AttackMod a)                       { this->_attackMod = a; return *this; }
Modifier& Modifier::Energy(int16_t e)                         { this->_energy = e;    return *this; }
Modifier& Modifier::Force(FAf f)                              { this->_forceAf = f;   return *this; }
Modifier& Modifier::Hull(int16_t h)                           { this->_hull = h;      return *this; }
Modifier& Modifier::Shield(int16_t s)                         { this->_shield = s;    return *this; }
Modifier& Modifier::UpgAdd(UpgradeBar u)                      { this->_upgAdd = u;    return *this; }
Modifier& Modifier::UpgRem(UpgradeBar u)                      { this->_upgRem = u;    return *this; }
Modifier& Modifier::ManDec(std::vector<ManeuverDifficulty> m) { this->_manDec = m;    return *this; }
Modifier& Modifier::ManInc(std::vector<ManeuverDifficulty> m) { this->_manInc = m;    return *this; }

ActionBar Modifier::GetActAdd() const {
  if(this->_actAdd) { return *this->_actAdd; }
  else              { return {}; }
}

int16_t Modifier::GetAgility() const {
  if(this->_agility) { return *this->_agility; }
  else               { return 0; }
}
  
PriAttacks Modifier::GetAttackAdd() const {
  PriAttacks ret;
  if(this->_attackAdd) {
    for(const PriAttack& a : *this->_attackAdd) {
      ret.push_back(a);
    }
  }
  return ret;
}

AttackMod Modifier::GetAttackMod() const {
  if(this->_attackMod) { return *this->_attackMod; }
  else                 { return {Arc::None,0};     }
}

int16_t Modifier::GetEnergy() const {
  if(this->_energy) { return *this->_energy; }
  else              { return 0; }
}

FAf Modifier::GetForceAf() const {
  if(this->_forceAf) { return *this->_forceAf; }
  else               { return FAf::None; }
}

int16_t Modifier::GetHull() const {
  if(this->_hull) { return *this->_hull; }
  else            { return 0; }
}

int16_t Modifier::GetShield() const {
  if(this->_shield) { return *this->_shield; }
  else              { return 0; }
}

UpgradeBar Modifier::GetUpgAdd() const {
  UpgradeBar ret;
  if(this->_upgAdd) {
    for(const UpgradeSlot& s : *this->_upgAdd) {
      ret.push_back(s);
    }
  }
  return ret;
}

UpgradeBar Modifier::GetUpgRem() const {
  UpgradeBar ret;
  if(this->_upgRem) {
    for(const UpgradeSlot& s : *this->_upgRem) {
      ret.push_back(s);
    }
  }
  return ret;
}

std::vector<Modifier::ManeuverDifficulty> Modifier::GetManDec() const {
  if(this->_manDec) { return *this->_manDec; }
  else              { return {}; }
}

std::vector<Modifier::ManeuverDifficulty> Modifier::GetManInc() const {
  if(this->_manInc) { return *this->_manInc; }
  else              { return {}; }
}

}
