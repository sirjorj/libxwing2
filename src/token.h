#pragma once

#include <array>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>
#include <stdint.h>

namespace libxwing2 {

enum class Tok : uint16_t {
  // ship
  Dial = 0,
  DialId,
  Ship,

  // game effects
  Calculate,
  Cloak,
  Crit,
  Deplete,
  Disarm,
  Evade,
  Focus,
  ForceCharge,
  Fuse,
  ID,
  Ion,
  Jam,
  Lock,
  Reinforce,
  Shield,
  StandardCharge,
  Strain,
  Stress,
  Tractor,
  TurretMountMarker,

  // turrets
  SmallSingleTurret = 64,
  SmallDoubleTurret,
  LargeSingleTurret,
  LargeDoubleTurret,
  HugeSingleTurret,
  HugeDoubleTurret,

  // obstacles
  Asteroid          = 128, // shape
  DebrisCloud,             // shape
  GasCloud,                // shape
  CargoDrift,
  ElectroChaffCloud,

  // devices
  Bomblet           = 256,
  ClusterMineSet,
  ConcussionBomb,
  ConnerNet,
  ElectroProtBomb,
  IonBomb,
  LooseCargo,
  ProtonBomb,
  ProximityMine,
  SeismicCharge,
  SpareParts,
  ThermalDetonator,

  // conditions
  CompIntel         = 384,
  Decoyed,
  FearfulPrey,
  Hunted,
  ISYTDS,
  ITR,
  ListeningDevice,
  OptimizedPrototype,
  Rattled,
  SuppressiveFire,
  YBMBYSTM,

  // remotes
  BuzzDroidSwarm    = 512,
  DRK1ProbeDroid,
  SensorBuoyBlue,
  SensorBuoyRed,
  TrackTorp,
  TrackTorpLock,

  // misc
  FirstPlayerMarker = 640,
  HyperspaceMarker,
  StrategicMarker,
  TridentAggMarker,
  VictoryCounter,
};

class TokenNotFound : public std::runtime_error {
 public:
  TokenNotFound(Tok t);
};



//
// token
//

class Token {
 public:
  static Token GetToken(Tok t);
  static std::vector<Tok> GetAllToks();
  Tok         GetTok()       const;
  std::string GetName()      const;
  std::string GetShortName() const;

 private:
  Tok tok;
  std::string name;
  std::string shortName;

  static std::vector<Token> tokens;

  Token(Tok         t,
        std::string name,
        std::string shortName);
};



//
// tokens
//

enum class Fac;
enum class Plt;
enum class Shp;

class Tokens {
 public:
  Tokens(Tok t);
  virtual ~Tokens();
  Tok GetTok() const;
  void Add(int c);
  virtual uint16_t GetCount() const;
 private:
  Tok tok;
  uint16_t count;
};

class Dials : public Tokens {
public:
  Dials();
  void Add(Shp s, Fac f);
  std::vector<std::pair<Shp,Fac>> GetDials() const;
private:
  std::vector<std::pair<Shp,Fac>> dials;
};

class DialIds : public Tokens {
public:
  DialIds();
  void Add(Shp s);
  std::map<const Shp,int> GetDialIds() const;
private:
  std::map<const Shp,int> dialIds;
};

class ShTokens : public Tokens {
 public:
  ShTokens();
  void Add(Plt p1, Plt p2);
  std::vector<std::pair<Plt,Plt>> GetPilots() const;
 private:
  std::vector<std::pair<Plt,Plt>> pilots;
};

class IDTokens : public Tokens {
 public:
  IDTokens(Tok t);
  void Add(int c, int id);
  std::map<int,int> GetIDCounts() const;
 private:
  std::map<int,int> ids; // id,count
};

class StrategicMarkers : public Tokens {
 public:
  StrategicMarkers();
  void Add(int id);
  std::map<int,int> GetIDCounts() const;
 private:
  std::map<int,int> ids; // id,count
};

class TrackingTorpedoTokens : public Tokens {
 public:
  TrackingTorpedoTokens();
  void Add(std::string id);
  std::map<std::string,int> GetIDCounts() const;
 private:
  std::map<std::string,int> ids; // id,count
};

class TrackingTorpedoLocks : public Tokens {
 public:
  TrackingTorpedoLocks();
  void Add(std::string id);
  std::map<std::string,int> GetIDCounts() const;
 private:
  std::map<std::string,int> ids; // id,count
};

class TurretTokens : public Tokens {
 public:
  TurretTokens(Tok);
  void Add(Tok t, int c, std::optional<Fac> f);
  std::map<std::optional<Fac>,int> GetFacCounts() const;
 private:
  std::map<std::optional<Fac>,int> fac;
};



//
// tokencollection
//

class TokenCollection {
 public:
  TokenCollection();
  TokenCollection& operator+=(const TokenCollection& rhs);

  // adders
  TokenCollection& Dial(Shp,Fac);
  TokenCollection& DialId(Shp);
  TokenCollection& Ship(Plt,Plt);
  TokenCollection& Calc(int c);
  TokenCollection& Cloak(int c);
  TokenCollection& Crit(int c);
  TokenCollection& Deplete(int c);
  TokenCollection& Disarm(int c);
  TokenCollection& Evade(int c);
  TokenCollection& Focus(int c);
  TokenCollection& ForceCharge(int c);
  TokenCollection& Fuse(int c);
  TokenCollection& ID(int c, int id);
  TokenCollection& Ion(int c);
  TokenCollection& Jam(int c);
  TokenCollection& Lock(int c, int id);
  TokenCollection& Reinforce(int c);
  TokenCollection& Shield(int c);
  TokenCollection& StandardCharge(int c);
  TokenCollection& Strain(int c);
  TokenCollection& Stress(int c);
  TokenCollection& Tractor(int c);
  TokenCollection& TurretMount(int c);
  TokenCollection& SmSiTurret(int c,std::optional<Fac> f);
  TokenCollection& SmDoTurret(int c,std::optional<Fac> f);
  TokenCollection& LaSiTurret(int c,std::optional<Fac> f);
  TokenCollection& LaDoTurret(int c,std::optional<Fac> f);
  TokenCollection& HuSiTurret(int c,std::optional<Fac> f);
  TokenCollection& HuDoTurret(int c,std::optional<Fac> f);
  TokenCollection& Asteroid(int c);
  TokenCollection& Debris(int c);
  TokenCollection& Gas(int c);
  TokenCollection& Cargo(int c);
  TokenCollection& Bomblet(int c);
  TokenCollection& ClusterMine(int c);
  TokenCollection& ConcussionBomb(int c);
  TokenCollection& ConnerNet(int c);
  TokenCollection& ElectroChaff(int c);
  TokenCollection& ElectroProtBomb(int c);
  TokenCollection& IonBomb(int c);
  TokenCollection& LooseCargo(int c);
  TokenCollection& ProtonBomb(int c);
  TokenCollection& ProxMine(int c);
  TokenCollection& SeismicCharge(int c);
  TokenCollection& SpareParts(int c);
  TokenCollection& ThermalDet(int c);
  TokenCollection& CompIntel(int c);
  TokenCollection& Decoyed(int c);
  TokenCollection& FearfulPrey(int c);
  TokenCollection& Hunted(int c);
  TokenCollection& ISYTDS(int c);
  TokenCollection& ITR(int c);
  TokenCollection& ListeningDev(int c);
  TokenCollection& OptPrototype(int c);
  TokenCollection& Rattled(int c);
  TokenCollection& SuppFire(int c);
  TokenCollection& BuzzDroid(int c);
  TokenCollection& DRK1ProbeDroid(int c);
  TokenCollection& SensorBuoyBlue(int c);
  TokenCollection& SensorBuoyRed(int c);
  TokenCollection& TrackTorp(std::string id);
  TokenCollection& TrackTorpLock(std::string id);
  TokenCollection& TridentAgg(int c);
  TokenCollection& FirstPlayer(int c);
  TokenCollection& Hyperspace(int c);
  TokenCollection& StratMark(int i);
  TokenCollection& Victory(int c);
  TokenCollection& YBMBYSTM(int c);

  std::map<Tok,std::shared_ptr<Tokens>> GetAllTokens() const;
  Dials GetDials() const;
  DialIds GetDialIds() const;
  ShTokens GetShipTokens() const;
  StrategicMarkers GetStrategicMarkers() const;
  TrackingTorpedoTokens GetTrackingTorpedoes() const;
  TrackingTorpedoLocks GetTrackingTorpedoLocks() const;
  IDTokens GetIDTokens(Tok tok) const;
  TurretTokens GetTurretTokens(Tok tok) const;
  Tokens GetTokens(Tok tok) const;

 private:
  std::map<Tok,std::shared_ptr<Tokens>> tokens;

  // adder helpers
  void AddDial(Shp s, Fac f);
  void AddDialId(Shp s);
  void AddShip(Plt p1, Plt p2);
  void AddIds(Tok t, int c, int id);
  void AddStrategicMarkers(int i);
  void AddTrackingTorpedo(std::string i);
  void AddTrackingTorpedoLock(std::string i);
  void AddTurret(Tok t, int c, std::optional<Fac> f);
  void AddTokens(Tok t, int c);
};

}
