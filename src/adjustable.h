#pragma once
#include "pilot.h"
#include "upgrade.h"
#include <map>
#include <stdint.h>

namespace libxwing2 {

struct AdjPilot {
  std::optional<int16_t>    cost;
  std::optional<UpgradeBar> upgrades;
};

struct AdjUpgrade {
  std::optional<int16_t> cost;
};

struct AdjStatus {
  uint16_t line;
  bool isError;
  std::string message;
};


class Adjustable {
 public:
  Adjustable();

  std::optional<int16_t>    GetCost(Plt pilot);
  std::optional<UpgradeBar> GetUpgrades(Plt pilot);
  std::optional<int16_t>    GetCost(Upg upgrade);

  std::vector<AdjStatus> GetStatus();

 private:
  static std::string GetFilename();
  std::map<Plt, AdjPilot> pilots;
  std::map<Upg, AdjUpgrade> upgrades;
  std::vector<AdjStatus> status;
};

}
