#include "environment.h"
#include "helper.h"

namespace libxwing2 {

std::vector<Environment> Environment::environments = {
  {
    Env::AsteroidShower, "Asteroid Shower",
    "9 Asteroids", "", "",
    "In player order, players take turns choosing an obstacle and placing it into the play area until 3 obstacles have been placed.  Set all remaining ostacles aside.",
    {
      "Obstacles must be placed beyond range 2 of each other obstacle and beyond range 2 of each edge of the play area."
    },
    "During the End Phase of each odd-numbered round, the first player places 1 set-aside obstacle into the play area.  During the End Phase of each even-numbered round, the second player places 1 set-aside obstacle into the play area.",
    {
      "Obstacles must be placed beyond range 1 of each enemy ship and beyond range 0 of each other object."
    }
  },
  {
    Env::Clouzon36Deposits, "Clouzon-36 Deposits",
    "3 Asteroids, 3 Gas Clouds", "", "",
    "In player order, players take turns choosing an obstacle and placing it into the play area until all obstacles have been placed.",
    {
      "Asteroids must be placed beyond range 2 of each other asteroid and beyond range 2 of each edge of the play area.",
      "Gas clouds must placed touching (but not overlapping) an asteroid and beyond range 2 of each other gas cloud."
    },
    "After a ship at range 0 of or obstructed by a gas cloud defends, if the defender suffered at least 1 {CRIT} damage, each ship at range 0-1 of the gas cloud suffers 1 {CRIT} damage.  Then, remove the gas cloud.",
    {}
  },
  {
    Env::CometTail, "Comet Tail",
    "3 Asteroids, 2 Debris Coulds, 2 Gas Clouds", "", "",
    "In player order, players take turns choosing an obstacle and placing it into the play area until all abstacles have been placed.",
    {
      "The first obstacle must be an asteroid and must be placed beyond range 3 of each edge of the play area.",
      "Each additional obstacle must be placed at range 1 of 1 obstacle and beyond range 1 of each other obstacle."
    },
    "", {}
  },
  {
    Env::ContinuousBombardment, "Continuous Bombardment",
    "6 Obstacles", "2 Bomblets, 2 Ion Bombs, 2 Proton Bombs, 2 Seismic Charges", "",
    "In player order, players take turns choosing an obstacle and placing it into the play area until all obstacles have been placed.  Set all devices aside.",
    {
      "Obstacles must be placed beyond range 1 of each other obstacle and beyond range 2 of each edge of the play area."
    },
    "During the End Phase of each odd-numbered round, the first player places one set-aside device into the play area.  During the End Phase of each even-numbered round, the second player places one set-aside device into the play area and places 1 fuse marker on that device.",
    {
      "Devices must be placed at range 1 of an obstacle and beyond range 0 of each other object."
    }
  },
  {
    Env::Countdown, "Countdown",
    "5 Obstacles", "1 Electro-Proton Bomb", "",
    "Each player gains 3 timers ({COUNTER}).  In player order, players take turns choosing an obstacle or device and placing it into the play area until all obstacles and devices have been placed.",
    {
      "Obstacles and devices must be placed beyond range 1 of each other and beyond range 2 of the play area.  After the Electro-Proton Bomb is placed, place 2 fuse markers on it."
    },
    "During the End Phase, in player order, each player may spend 1 timer ({COUNTER}) to add or remove 1 fuse marker from the Electro-Proton Bomb.",
    {}
  },
  {
    Env::IonClouds, "Ion Clouds",
    "1 Asteroid, 2 Debris Clouds, 3 Gas Clouds", "", "",
    "In player order, players take turns choosing an obstacle and placing it into the play area until all obstacles have been placed.",
    {
      "Obstacles must be placed beyond range 1 of each other obstacle and beyond range 2 of each edge of the play area."
    },
    "After a ship moves through or overlaps a gas cloud, it gains 4 ion tokens.",
    {}
  },
  {
    Env::Minefield, "Minefield",
    "4 Obstacles", "1 Cluster Mine set, 1 Conner Net, 2 Proximity Mines", "",
    "In player order, players take turns choosing an obstacle or device and placing it into the play area until all obstacles and devices have been placed.",
    {
      "Obstacles must be placed beyond range 1 of each other obstacle, beyond range 0 of each other object, and beyond range 2 of each edge of they play area.",
      "Devices must be placed beyond range 1 of each other deivce, beyond range 0 of each other object, and beyond range 1 of each edge of the play area."
    },
    "", {}
  },
  {
    Env::MunitionsCache, "Munitions Cache",
    "6 Obstacles", "", "Cluster Mines, Conner Nets, Ion Bombs, Proton Bombs, Proton Torpedoes, Proximity Mines, Seismic Charges",
    "Gather the listed upgrades (including Proton Torpedoes from the Core Set) and set them aside.  In player order, players take turns choosing an obstacle and placing it into the play area until all obstacles have been placed.",
    {
      "Obstacles must be paced beyond range 1 of each other obstacle and beyond range 2 of each edge of the play area.  After each obstacle is placed, place 1 cache ({COUNTER} on it.)"
    },
    "After a ship overlaps an obstacle with a cache({COUNTER}), draw 1 upgrade at random from the set-aside upgrades and equip it to that ship (ignoring its restrictions).  Then remove that cache ({COUNTER}).",
    {}
  },
  {
    Env::MynockInfestation, "Mynock Infestation",
    "3 Asteroids, 3 Debris Clouds", "", "",
    "In player order, players take turns choosing an obstacle and placing it into the play area until all obstacles have been placed.",
    {
      "Obstacles must be placed beyond range 1 of each other obstacle and beyond range 2 of each edge of the play area."
    },
    "When a ship rolls an attack die due to moving through or overlapping an asteroid or debris cloud, resolve the following (in addition to the normal effects):",
    {
      "On a blank result, the ship gains 1 strain token.",
      "On a {FOCUS} result, the ship gains 1 disarm token."
    }
  },
  {
    Env::PinpointBombardment, "Pinpoint Bombardment",
    "6 Obstacles", "", "Bomblet Generator, Electro-Proton Bomb, Ion Bombs, Proton Bombs, Seismic Charges",
    "In player order, each player chooses one of the listed upgrades and equips it to one of their ships (ignoring its Restrictions).  In player order, players take turns choosing an obstacle and placing it into the play area until all obstacles have been placed.",
    {
      "Obstacles must be placed beyond range 1 of each other obstacle and beyond range 2 of each edge of the play area.",
      "Then, each player places 1 target point ({COUNTER}) in the play area." // this is technically not a point
    },
    "When a bomb at range 1 of a target point ({COUNTER}) detonates, it detonates twice instead.",
    {}
  },
  {
    Env::RecentWreckage, "Recent Wreckage",
    "6 Debris Clouds, 3 Gas Clouds", "", "",
    "In player order, players take turns choosing an obstacle and placing it into the play area until all obstacles have been placed.",
    {
      "Debris clouds must be placed beyond range 1 of each other debris cloud and beyond range 2 of each edge of the play area.",
      "Gas clouds must be placed touching (but not overlapping)..."
    },
    "",
    {}
  },
  {
    Env::UnexplodedOrdnance, "Unexploded Ordnance",
    "5 Obstacles", "1 Bomblet, 1 Ion Bomb, 1 Proton Bomb, 1 Seismic Charge", "",
    "In player order, players take turns choosing an obstacle or device and placing it into the play area until all obstacles and devices have been placed.",
    {
      "Obstacles must be placed beyond range 1 of each other obstacle, beyond range 0 of each other object, and beyond range 2 of each edge of the play area.",
      "Devices must be placed at range 1 of an obstacle, beyond range 1 of each other device, and beyond range 1 of each edge of the play area.  After a device is placed, place 2 fuse markers on it."
    },
    "During the End Phase, for each Bomb device in the play area, roll 1 attack die.  On a {FOCUS} result, add 2 fuse markers to that device.  On a {HIT} result, remove 1 fuse marker from that device.  On a {CRIT} result, remove all fuse markers from that device.",
    {}
  },

  /*
  {
    Env::, "",
    "", "",
    "",
    {},
    "",
    {}
  },
  */

};



EnvironmentNotFound::EnvironmentNotFound(Env e) : runtime_error("Environment not found (enum " + std::to_string((int)e) + ")") { }
EnvironmentNotFound::EnvironmentNotFound(std::string e) : runtime_error("Environment not found (" + e + ")") { }



Environment Environment::GetEnvironment(Env e) {
  for(const Environment& environment : Environment::environments) {
    if(environment.GetEnv() == e) {
      return environment;
    }
  }
  throw EnvironmentNotFound(e);
}

Environment Environment::GetEnvironment(std::string estr) {
  for(const Environment& environment : Environment::environments) {
    if(environment.GetEnvStr() == estr) {
      return environment;
    }
  }
  throw EnvironmentNotFound(estr);
}

std::vector<Env> Environment::GetAllEnvs() {
  std::vector<Env> ret;
  for(const Environment& environment : Environment::environments) {
    ret.push_back(environment.GetEnv());
  }
  return ret;
}

Env                      Environment::GetEnv()               const { return this->env; }
std::string              Environment::GetName()              const { return this->name; }
std::string              Environment::GetEnvStr()            const { return ToStringHelper(this->name); }
std::string              Environment::GetObstacles()         const { return this->obstacles; }
std::string              Environment::GetDevices()           const { return this->devices; }
std::string              Environment::GetUpgrades()          const { return this->upgrades; }
std::string              Environment::GetSetup()             const { return this->setup; }
std::vector<std::string> Environment::GetSetupPoints()       const { return this->setupPoints; }
std::string              Environment::GetSpecialRule()       const { return this->specialRule; }
std::vector<std::string> Environment::GetSpecialRulePoints() const { return this->specialRulePoints; }

Environment::Environment(Env e,
			 std::string n,
			 std::string o,
			 std::string d,
       std::string u,
			 std::string s,
			 std::vector<std::string> sp,
			 std::string r,
			 std::vector<std::string> rp)
  : env(e), name(n), obstacles(o), devices(d), upgrades(u), setup(s), setupPoints(sp), specialRule(r), specialRulePoints(rp) { }

}
