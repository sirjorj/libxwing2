#include "keyword.h"

namespace libxwing2 {

std::vector<Keyword> Keyword::keywords = {
  { KWd::Assault,      "Assault Ship"  },
  { KWd::Awing,        "A-wing"        },
  { KWd::Bwing,        "B-wing"        },
  { KWd::BountyHunter, "Bounty Hunter" },
  { KWd::Clone,        "Clone"         },
  { KWd::DarkSide,     "Dark Side"     },
  { KWd::Droid,        "Droid"         },
  { KWd::Freighter,    "Freighter"     },
  { KWd::Jedi,         "Jedi"          },
  { KWd::LightSide,    "Light Side"    },
  { KWd::Mandalorian,  "Mandalorian"   },
  { KWd::Partisan,     "Partisan"      },
  { KWd::Sith,         "Sith"          },
  { KWd::Spectre,      "Spectre"       },
  { KWd::TIE,          "TIE"           },
  { KWd::Xwing,        "X-wing"        },
  { KWd::YT1300,       "YT-1300"       },
  { KWd::Ywing,        "Y-wing"        },
};

KeywordNotFound::KeywordNotFound(KWd k)           : runtime_error("Keyword not found (enum " + std::to_string((int)k) + ")") { }
KeywordNotFound::KeywordNotFound(std::string xws) : runtime_error("Keyword not found (" + xws + ")") { }

Keyword Keyword::GetKeyword(KWd k) {
  for(const Keyword& keyword : Keyword::keywords) {
    if(keyword.GetKWd() == k) {
      return keyword;
    }
  }
  throw KeywordNotFound(k);
}

std::vector<KWd> Keyword::GetAllKWds() {
  std::vector<KWd> ret;
  for(const Keyword& keyword : Keyword::keywords) {
    ret.push_back(keyword.GetKWd());
  }
  return ret;
}

KWd         Keyword::GetKWd()     const { return this->kwd; }
std::string Keyword::GetName()    const { return this->name; }

Keyword::Keyword(KWd         k,
                 std::string n)
  : kwd(k), name(n) { }

}
