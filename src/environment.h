#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

enum Env {
  AsteroidShower,
  Clouzon36Deposits,
  CometTail,
  ContinuousBombardment,
  Countdown,
  IonClouds,
  Minefield,
  MunitionsCache,
  MynockInfestation,
  PinpointBombardment,
  RecentWreckage,
  UnexplodedOrdnance,
};

class EnvironmentNotFound : public std::runtime_error {
 public:
  EnvironmentNotFound(Env e);
  EnvironmentNotFound(std::string e);
};

class Environment {
 public:
  static Environment GetEnvironment(Env e);
  static Environment GetEnvironment(std::string envstr);
  static std::vector<Env> GetAllEnvs();

  Env                      GetEnv()               const;
  std::string              GetName()              const;
  std::string              GetEnvStr()            const;
  std::string              GetObstacles()         const;
  std::string              GetDevices()           const;
  std::string              GetUpgrades()          const;
  std::string              GetSetup()             const;
  std::vector<std::string> GetSetupPoints()       const;
  std::string              GetSpecialRule()       const;
  std::vector<std::string> GetSpecialRulePoints() const;

 private:
  Env env;
  std::string name;
  std::string obstacles;
  std::string devices;
  std::string upgrades;
  std::string setup;
  std::vector<std::string> setupPoints;
  std::string specialRule;
  std::vector<std::string> specialRulePoints;

  static std::vector<Environment> environments;

  Environment(Env e,
	      std::string n,
	      std::string o,
	      std::string d,
        std::string u,
	      std::string s,
	      std::vector<std::string> sp,
	      std::string r,
	      std::vector<std::string> rp);
};

}
