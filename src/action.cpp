#include "action.h"
#include <sstream>

namespace libxwing2 {

// Actions
std::vector<Action> Action::actions = {
  //{ Act::None,       "None",        "   " },
  { Act::BarrelRoll, "Barrel Roll", "Brl" },
  { Act::Boost,      "Boost",       "Bst" },
  { Act::Calculate,  "Calculate",   "Clc" },
  { Act::Cloak,      "Cloak",       "Clk" },
  { Act::Coordinate, "Coordinate",  "Crd" },
  { Act::Evade,      "Evade",       "Evd" },
  { Act::Focus,      "Focus",       "Foc" },
  { Act::Jam,        "Jam",         "Jam" },
  { Act::Lock,       "Lock",        "Lck" },
  { Act::Reinforce,  "Reinforce",   "Rnf" },
  { Act::Reload,     "Reload",      "Rld" },
  { Act::RotateArc,  "Rotate Arc",  "Rot" },
  { Act::SLAM,       "SLAM",        "SLM" },
};

ActionNotFound::ActionNotFound(Act a) : runtime_error("Action not found (enum " + std::to_string((int)a) + ")") { }

Action Action::GetAction(Act act) {
  for(const Action& action : Action::actions) {
    if(action.GetAct() == act) {
      return action;
    }
  }
  throw ActionNotFound(act);
}

std::vector<Act> Action::GetAllActs() {
  std::vector<Act> ret;
  for(const Action& action : Action::actions) {
    ret.push_back(action.GetAct());
  }
  return ret;
}

Act         Action::GetAct()       const { return this->act; }
std::string Action::GetName()      const { return this->name; }
std::string Action::GetShortName() const { return this->shortName; }

Action::Action(Act         a,
               std::string n,
               std::string s)
  : act(a), name(n), shortName(s) { }



void ForEachAction(Act actions, std::function<void(Act)>f) {
  for(int i=0; i<sizeof(Act)*8; i++) {
    uint32_t v = (1<<i) & (uint32_t)actions;
    if(v) {
      Act a = (Act)v;
      f(a);
    }
  }
}

bool operator ==(const SAct a, const SAct b) {
  return (a.difficulty == b.difficulty) && (a.action == b.action);
}
  
}
