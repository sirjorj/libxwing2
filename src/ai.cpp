#include "ai.h"

namespace libxwing2 {

std::vector<AiCard> AiCard::aiCards = {
// there is a facedown card in the spread
  { AiC::Tri_Lunge,      "Lunge Forth",       "If the ship with the aggression marker is in the Trident's {FULLFRONTARC}, the Trident executes a white [5 {STRAIGHT}].[P]If the ship with the aggression marker is in the Trident's {FULLREARARC} or flanking it, the Trident executes a red [2 {REVERSESTRAIGHT}].",                                                                             {{Act::Calculate,""},{Act::Lock,"Lock the ship with the aggression marker if able."}}, Shp::Trident, 2, 6 },
  { AiC::Tri_Retreat,    "Retreat",           "If the ship with the aggression... in the Trident's {FULLFRONTARC}, the Trident...",                                                                                                                                                                                                                                                               {}, Shp::Trident, 0, 0 },
  { AiC::Tri_Reposition, "Reposition",        "If the ship with the aggression marker is in the Trident's {LEFTARC}, the Trident executes a red [0 {RIGHTBANK}].[P]...marker is in the...",                                                                                                                                                                                                       {}, Shp::Trident, 0, 0 },
  { AiC::Tri_Approach,   "Cautious Approach", "If the ship with the aggression marker is in the Trident's {LEFTARC}, the Trident executes a blue [2 {LBANK}].[P]If the ship with the aggression marker is in the Trident's {RIGHTARC}, the Trident executes a blue [2 {RBANK}].[P]Otherwise, the Trident executes a blue [3 {STRIGHT}], then assigns the aggression marker to the nearest ship.", {{Act::Calculate,""},{Act::Reinforce,"Reinforce front [{FULLFRONTARC}]"}}, Shp::Trident, 5, 6 },
  { AiC::Tri_Sweep,      "Aggressive Sweep",  "If the ship with the aggression marker is in the Trident's {LEFTARC}, the Trident executes a white [3...[P]If the ship with the aggression marker is in the Trident's {RIGHTARC}, the Trident executes a white [...[P]Otherwise, the Trident executes a white [4 {STRAIGHT}, then assigns the aggression mar... the nearest ship.",                {{Act::Calculate,""},{Act::Lock,"Lock the ship with the aggression marker if able."}}, Shp::Trident, 6, 6 },
};



AiCard::AiCard(AiC                   a,
	       std::string           n,
	       std::string           m,
	       std::vector<AiAction> ac,
	       Shp                   s,
	       uint8_t               c,
	       uint8_t               cs)
  : aic(a), name(n), maneuver(m), actions(ac), shp(s), cardNum(c), numOfCards(cs) {
}

}
