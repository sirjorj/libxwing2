#include "condition.h"
#include "helper.h"
#include "release.h"

namespace libxwing2 {



std::vector<Condition> Condition::conditions = {
  { Cnd::CompIntel,   1, "Compromising Intel",          "Comp Intel", "During the System Phase, if the enemy Vi Moradi is at range 0-3, flip your dial faceup.  While you defend or perform an attack against the enemy Vi Moradi, you cannot spend focus tokens." },
  { Cnd::Decoyed,     1, "Decoyed",                     "Decoyed",    "While you defend, each friendly Naboo Handmaiden in the attack arc may spend 1 evade token to change one of your results to an {EVADE} result.  If you are a Naboo Royal N-1 Starfighter, each friendly Naboo Handmaiden in the attack may spend 1 evade token to add 1 {EVADE} result instead." },
  { Cnd::FearfulPrey, 1, "Fearful Prey",                "Fearful",    "After you defend against an enemy Fearsome Predator, if you did not spend at least 1 green token during the attack, gain 1 strain token." },
  { Cnd::Hunted,      1, "Hunted",                      "Hunted",     "After you are destroyed, you must choose another friendly ship and assign this condition to it, if able." },
  { Cnd::ISYTDS,      1, "I'll Show You the Dark Side", "ISYTDS",     "When this card is assigned, if there is no faceup damage card on it, the player who assigned it searches the damage deck for 1 Pilot damage card and places it faceup on this card.  Then shuffle the damage deck.  When you would suffer 1 {CRIT} damage, you are instead dealt the faceup damage card on this card.  Then, remove this card." },
  { Cnd::ITR,         1, "It's the Resistance",         "ITR",        "Setup: Start in reserve.  When you deploy, you are placed within range 1 of any table edge and beyond range 3 of any enemy ship.  At the start of the round, if all of the friendly GA-97's {CHARGE} are active, you must deploy.  Then remove this card.  After the friendly GA-97 is destroyed, you must deploy.  Than gain 1 disarm token and remove this card." },
  { Cnd::LstnDev,     1, "Listening Device",            "List Dev",   "During the System Phase, if an enemy ship with the Informant upgrade is at range 0-2, flip your dial faceup." },
  { Cnd::MercilessPur,2, "Merculess Pursuit",           "Merc Purs",  "After you perform an attack, if the defender is equipped with The Child, you may acquire a lock on the defender."},
  { Cnd::OptProt,     1, "Optimized Prototype",         "Opt Proto",  "While you perform a {FRONTARC} primary attack against a ship locked by a friendly ship with the Director Krennic upgrade, you may spend 1 {HIT}, {CRIT}, or {FOCUS} result.  If you do, choose one: the defender loses 1 shield, or the defender flips 1 of its facedown damage cards."},
  { Cnd::Rattled,     1, "Rattled",                     "Rattled",    "After a bomb or mine at range 0-1 detonates, suffer 1 {CRIT} damage.  Then, remove this card.  ACTION: If there are no bombs or mines at range 0-1, remove this card." },
  { Cnd::SupFire,     1, "Suppressive Fire",            "Sup Fire",   "While you perform an attack against a ship other than Captain Rex, roll 1 fewer attack die.  After Captain Rex defends, remove this card.  At the end of the Combat phase, if Captain Rex did not perform an attack this phase, remove this card.  After Captain Rex is destroyed, remove this card." },
  { Cnd::YBMB,        1, "You'd Better Mean Business",  "YBMB",       "This condition is assigned facedown.  Reveal it after you defend.  After you defend, you may spend 2 {CHARGE} from Zam Wesell.  if you do, perform a bonus attack against the attacker.  At the end of the Engagement Phase, if this card is facedown and you are in an enemy ship's firing arc, you may reveal this card.  If you do, Zam Wesell receovers 2 {CHARGE}.  At the start of the System Phase, remove this condition." },
  { Cnd::YSTM,        1, "You Should Thank Me",         "YSTM",       "This condition is assigned facedown.  Reveal it after you defend.  After you defend, Zam Wesell recovers 1 {CHARGE}.  Then, you may acquire a lock on the attacker.  At the end of the Engagement Phase, if this card is facedown and you are in the enemy ship's firing arc, you may reveal this card and spend 2 {CHARGE} from Zam Wesell.  If you do, you may perform a bonus attack.  At the start of the System Phase, remove the condition." },
};



ConditionNotFound::ConditionNotFound(Cnd cnd)            : runtime_error("Condition not found (enum " + std::to_string((int)cnd) + ")") { }
ConditionNotFound::ConditionNotFound(std::string cndstr) : runtime_error("Condition not found '" + cndstr + "'") { }



Condition::Condition(Cnd         c,
		     uint8_t     l,
		     std::string n,
		     std::string s,
		     std::string txt)
  : cnd(c), limited(l), name(n), nameShort(s), text(txt) { }



Condition Condition::GetCondition(Cnd cnd) {
  for(const Condition& condition : Condition::conditions) {
    if(condition.GetCnd() == cnd) {
      return condition;
    }
  }
  throw ConditionNotFound(cnd);
}

Condition Condition::GetCondition(std::string cndstr) {
  for(const Condition& condition : Condition::conditions) {
    if(condition.GetCndStr() == cndstr) {
      return condition;
    }
  }
  throw ConditionNotFound(cndstr);
}

std::vector<Cnd> Condition::GetAllCnds() {
  std::vector<Cnd> ret;
  for(const Condition& condition : Condition::conditions) {
    ret.push_back(condition.GetCnd());
  }
  return ret;
}

Cnd         Condition::GetCnd()       const { return this->cnd; }
std::string Condition::GetCndStr()    const { return ToStringHelper(this->nameShort); }
uint8_t     Condition::GetLimited()   const { return this->limited; }
std::string Condition::GetName()      const { return this->name; }
std::string Condition::GetShortName() const { return this->nameShort; }
std::string Condition::GetText()      const { return this->text; }
bool        Condition::IsUnreleased() const {
  for(Rel rel : Release::GetAllRels()) {
    Release release = Release::GetRelease(rel);
    if(!release.IsUnreleased()) {
      for(Cnd c : release.GetConditions()) {
	if(c == this->GetCnd()) {
	  return false;
	}
      }
    }
  }
  return true;
}

}
