#include "chargeable.h"

namespace libxwing2 {

Chargeable::Chargeable(int8_t cap, int8_t recur)
  : capacity(cap), recurring(recur) { }

int8_t Chargeable::GetCapacity() const { return this->capacity; }
int8_t Chargeable::GetRecurring() const { return this->recurring;   }

Chargeable& Chargeable::operator+=(uint8_t a) {
  this->capacity += a;
  return *this;
}

Chargeable& Chargeable::operator-=(uint8_t a) {
  this->capacity -= a;
  return *this;
}

}
