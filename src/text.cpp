#include "text.h"

namespace libxwing2 {

static inline std::string ReplaceString(std::string str, std::string rem, std::string rep) {
  size_t p;
  while((p=str.find(rem)) != std::string::npos) {
    str.replace(p, rem.size(), rep);
  }
  return str;
}

static inline std::string RemoveMarkup(std::string input) {
  std::string ret = input;
  ret = ReplaceString(ret, "[P]",  "  ");
  ret = ReplaceString(ret, "[B]",  "");
  ret = ReplaceString(ret, "[/B]", "");
  ret = ReplaceString(ret, "[I]",  "");
  ret = ReplaceString(ret, "[/I]", "");
  return ret;
}

static inline std::vector<std::string> GetSegments(std::string s, bool clean=false) {
  std::vector<std::string> ret;
  size_t secSta=0;
  size_t secEnd=0;
  std::string section;
  const std::string del = "[P]";
  while((secEnd=s.find(del,secSta)) != std::string::npos) {
    section = s.substr(secSta, secEnd-secSta);
    ret.push_back(section);
    secSta = secEnd + del.size();
  }
  section = s.substr(secSta, secEnd-secSta);
  if(clean) {
    section = RemoveMarkup(section);
  }
  ret.push_back(section);
  return ret;
}

Text::Text(std::string t) : text(t) { }

const std::string Text::GetCleanText()  const { return RemoveMarkup(this->text); }
const std::string Text::GetMarkedText() const { return this->text; }

const std::vector<std::string> Text::GetCleanSegments()  const { return GetSegments(this->text, true); }
const std::vector<std::string> Text::GetMarkedSegments() const { return GetSegments(this->text); }
}
