#include "converter.h"
#include "./json/json.h"
#include <fstream>
#include <sstream>

namespace libxwing2 {
namespace converter {

#define CARDBASE  "https://raw.githubusercontent.com/CrazyVulcan/x-wing2.0-project-goldenrod/main/"
//#define CARDBASE  "https://raw.githubusercontent.com/sirjorj/x-wing2.0-project-goldenrod/renaming/"

//
// data
//

// factions
struct FFG_FactionData  {};
struct XWS_FactionData  { std::string id; };
struct YASB_FactionData { std::string id; };
struct FactionEntry {
  Fac fac;
  std::optional<FFG_FactionData>  ffg;
  std::optional<XWS_FactionData>  xws;
  std::optional<YASB_FactionData> yasb;
};
static std::vector<FactionEntry> factionData = {
  { Fac::Rebel,      {}, {{"rebelalliance"}},      {{"Rebel%20Alliance"}}      },
  { Fac::Imperial,   {}, {{"galacticempire"}},     {{"Galactic%20Empire"}}     },
  { Fac::Scum,       {}, {{"scumandvillainy"}},    {{"Scum%20and%20Villainy"}} },
  { Fac::Resistance, {}, {{"resistance"}},         {{"Resistance"}}            },
  { Fac::FirstOrder, {}, {{"firstorder"}},         {{"First%20Order"}}         },
  { Fac::Separatist, {}, {{"separatistalliance"}}, {{"Separatist%20Alliance"}} },
  { Fac::Republic,   {}, {{"galacticrepublic"}},   {{"Galactic%20Republic"}}   }
};

// ships
struct FFG_ShipData  {};
struct XWS_ShipData  { std::string id; };
struct YASB_ShipData {};
struct ShipEntry {
  Shp shp;
  std::optional<FFG_ShipData>  ffg;
  std::optional<XWS_ShipData>  xws;
  std::optional<YASB_ShipData> yasb;
};
static std::vector<ShipEntry> shipData = {
  { Shp::Aggressor,        {}, {{"aggressorassaultfighter"}},         {} },
  { Shp::AlphaClass,       {}, {{"alphaclassstarwing"}},              {} },
  { Shp::ARC170,           {}, {{"arc170starfighter"}},               {} },
  { Shp::ASF01BWing,       {}, {{"asf01bwing"}},                      {} },
  { Shp::AttackShuttle,    {}, {{"attackshuttle"}},                   {} },
  { Shp::Auzituck,         {}, {{"auzituckgunship"}},                 {} },
  { Shp::Belbullab22,      {}, {{"belbullab22starfighter"}},          {} },
  { Shp::BTANR2YWing,      {}, {{"btanr2ywing"}},                     {} },
  { Shp::BTLA4YWing,       {}, {{"btla4ywing"}},                      {} },
  { Shp::BTLBYWing,        {}, {{"btlbywing"}},                       {} },
  { Shp::BTLS8KWing,       {}, {{"btls8kwing"}},                      {} },
  { Shp::CR90,             {}, {{"cr90corelliancorvette"}},           {} },
  { Shp::CROC,             {}, {{"croccruiser"}},                     {} },
  { Shp::CustYT1300,       {}, {{"customizedyt1300lightfreighter"}},  {} },
  { Shp::Delta7,           {}, {{"delta7aethersprite"}},              {} },
  { Shp::DroidTriFighter,  {}, {{"droidtrifighter"}},                 {} },
  { Shp::EscCraft,         {}, {{"escapecraft"}},                     {} },
  { Shp::Eta2Actis,        {}, {{"eta2actis"}},                       {} },
  { Shp::EWing,            {}, {{"ewing"}},                           {} },
  { Shp::FangFighter,      {}, {{"fangfighter"}},                     {} },
  { Shp::Fireball,         {}, {{"fireball"}},                        {} },
  { Shp::Firespray,        {}, {{"firesprayclasspatrolcraft"}},       {} },
  { Shp::G1A,              {}, {{"g1astarfighter"}},                  {} },
  { Shp::Gozanti,          {}, {{"gozanticlasscruiser"}},             {} },
  { Shp::GR75,             {}, {{"gr75mediumtransport"}},             {} },
  { Shp::HMP,              {}, {{"hmpdroidgunship"}},                 {} },
  { Shp::HWK290,           {}, {{"hwk290lightfreighter"}},            {} },
  { Shp::Hyena,            {}, {{"hyenaclassdroidbomber"}},           {} },
  { Shp::JM5K,             {}, {{"jumpmaster5000"}},                  {} },
  { Shp::Kihraxz,          {}, {{"kihraxzfighter"}},                  {} },
  { Shp::LAAT,             {}, {{"laatigunship"}},                    {} },
  { Shp::Lambda,           {}, {{"lambdaclasst4ashuttle"}},           {} },
  { Shp::LancerClass,      {}, {{"lancerclasspursuitcraft"}},         {} },
  { Shp::M12LKimogila,     {}, {{"m12lkimogilafighter"}},             {} },
  { Shp::M3A,              {}, {{"m3ainterceptor"}},                  {} },
  { Shp::MG100,            {}, {{"mg100starfortress"}},               {} },
  { Shp::ModifiedTIEln,    {}, {{"modifiedtielnfighter"}},            {} },
  { Shp::ModYT1300,        {}, {{"modifiedyt1300lightfreighter"}},    {} },
  { Shp::N1,               {}, {{"nabooroyaln1starfighter"}},         {} },
  { Shp::Nantex,           {}, {{"nantexclassstarfighter"}},          {} },
  { Shp::NimbusClass,      {}, {{"nimbusclassvwing"}},                {} },
  { Shp::Quadjumper,       {}, {{"quadrijettransferspacetug"}},       {} },
  { Shp::Raider,           {}, {{"raiderclasscorvette"}},             {} },
  { Shp::ResTransport,     {}, {{"resistancetransport"}},             {} },
  { Shp::ResTransportPod,  {}, {{"resistancetransportpod"}},          {} },
  { Shp::RZ1AWing,         {}, {{"rz1awing"}},                        {} },
  { Shp::RZ2AWing,         {}, {{"rz2awing"}},                        {} },
  { Shp::ScaYT1300,        {}, {{"scavengedyt1300"}},                 {} },
  { Shp::Scurrg,           {}, {{"scurrgh6bomber"}},                  {} },
  { Shp::Sheathipede,      {}, {{"sheathipedeclassshuttle"}},         {} },
  { Shp::SithInf,          {}, {{"sithinfiltrator"}},                 {} },
  { Shp::StarViper,        {}, {{"starviperclassattackplatform"}},    {} },
  { Shp::Syliure,          {}, {{"syliureclasshyperspacering"}},      {} },
  { Shp::T65XWing,         {}, {{"t65xwing"}},                        {} },
  { Shp::T70XWing,         {}, {{"t70xwing"}},                        {} },
  { Shp::TIEAdvV1,         {}, {{"tieadvancedv1"}},                   {} },
  { Shp::TIEAdvX1,         {}, {{"tieadvancedx1"}},                   {} },
  { Shp::TIEagAggressor,   {}, {{"tieagaggressor"}},                  {} },
  { Shp::TIEbaInterceptor, {}, {{"tiebainterceptor"}},                {} },
  { Shp::TIEcaPunisher,    {}, {{"tiecapunisher"}},                   {} },
  { Shp::TIEdDefender,     {}, {{"tieddefender"}},                    {} },
  { Shp::TIEfoFighter,     {}, {{"tiefofighter"}},                    {} },
  { Shp::TIEinInterceptor, {}, {{"tieininterceptor"}},                {} },
  { Shp::TIElnFighter,     {}, {{"tielnfighter"}},                    {} },
  { Shp::TIEphPhantom,     {}, {{"tiephphantom"}},                    {} },
  { Shp::TIErbHeavy,       {}, {{"tierbheavy"}},                      {} },
  { Shp::TIEReaper,        {}, {{"tiereaper"}},                       {} },
  { Shp::TIEsaBomber,      {}, {{"tiesabomber"}},                     {} },
  { Shp::TIEseBomber,      {}, {{"tiesebomber"}},                     {} },
  { Shp::TIEsfFighter,     {}, {{"tiesffighter"}},                    {} },
  { Shp::TIEskStriker,     {}, {{"tieskstriker"}},                    {} },
  { Shp::TIEvnSilencer,    {}, {{"tievnsilencer"}},                   {} },
  { Shp::TIEwiWhisperMod,  {}, {{"tiewiwhispermodifiedinterceptor"}}, {} },
  { Shp::Trident,          {}, {{"tridentclassassaultship"}},         {} },
  { Shp::Upsilon,          {}, {{"upsilonclassshuttle"}},             {} },
  { Shp::UWing,            {}, {{"ut60duwing"}},                      {} },
  { Shp::V19,              {}, {{"v19torrentstarfighter"}},           {} },
  { Shp::VCX100,           {}, {{"vcx100lightfreighter"}},            {} },
  { Shp::VT49,             {}, {{"vt49decimator"}},                   {} },
  { Shp::Vulture,          {}, {{"vultureclassdroidfighter"}},        {} },
  { Shp::Xi,               {}, {{"xiclasslightshuttle"}},             {} },
  { Shp::YT2400,           {}, {{"yt2400lightfreighter"}},            {} },
  { Shp::YV666,            {}, {{"yv666lightfreighter"}},             {} },
  { Shp::Z95,              {}, {{"z95af4headhunter"}},                {} },
};

// pilots
struct FFG_PilotData  { int16_t id; std::string art; };
struct XWS_PilotData  { std::string id; };
struct YASB_PilotData { int16_t id; };
struct PilotEntry {
  Plt plt;
  std::optional<FFG_PilotData>  ffg;
  std::optional<XWS_PilotData>  xws;
  std::optional<YASB_PilotData> yasb;
};
static std::vector<PilotEntry> pilotData = {
//  --libxwing2---------  --ffg---------------------------------------------------------------------------------------  --xwing-data2--------------------------------------  --yasb-
//  id                       id   atwork                                       xws                                                  id
  { Plt::IG88A,           {{ 197, "Card_art_XW_P_197.jpg"                }}, {{"ig88a"                                        }}, {{ 81}} },
  { Plt::IG88B,           {{ 198, "Card_art_XW_P_198.jpg"                }}, {{"ig88b"                                        }}, {{ 82}} },
  { Plt::IG88C,           {{ 199, "Card_art_XW_P_199.jpg"                }}, {{"ig88c"                                        }}, {{ 83}} },
  { Plt::IG88D,           {{ 200, "Card_art_XW_P_200.jpg"                }}, {{"ig88d"                                        }}, {{ 84}} },

  { Plt::MajVynder,       {{ 135, "Card_art_XW_P_135.jpg"                }}, {{"majorvynder"                                  }}, {{161}} },
  { Plt::LtKarsabi,       {{ 136, "Card_art_XW_P_136.jpg"                }}, {{"lieutenantkarsabi"                            }}, {{162}} },
  { Plt::RhoSqPlt,        {{ 137, "Card_art_XW_P_137.jpg"                }}, {{"rhosquadronpilot"                             }}, {{163}} },
  { Plt::NuSqPlt,         {{ 138, "Card_art_XW_P_138.jpg"                }}, {{"nusquadronpilot"                              }}, {{164}} },

  { Plt::NWexley_ARC,     {{  65, "Card_art_XW_P_65.jpg"                 }}, {{"norrawexley"                                  }}, {{ 77}} },
  { Plt::GDreis_ARC,      {{  66, "Card_art_XW_P_66.jpg"                 }}, {{"garvendreis"                                  }}, {{ 79}} },
  { Plt::SBey_ARC,        {{  67, "Card_art_XW_P_67.jpg"                 }}, {{"sharabey"                                     }}, {{ 78}} },
  { Plt::Ibtisam,         {{  68, "Card_art_XW_P_68.jpg"                 }}, {{"ibtisam"                                      }}, {{ 80}} },

  { Plt::OddBall_ARC,     {{ 523, "597ace7e901187c88d9ff75bb34a1301.jpg" }}, {{"oddball-arc170starfighter"                    }}, {{338}} },
  { Plt::Wolffe,          {{ 524, "933ef28f6a8fda0c5741bd6a9a5f308d.jpg" }}, {{"wolffe"                                       }}, {{342}} },
  { Plt::Jag,             {{ 525, "124b11dde89cf986fcd8bc8e89094cf5.jpg" }}, {{"jag"                                          }}, {{339}} },
  { Plt::Sinker,          {{ 526, "04b3e18c19a0843f89353949c9ffcb77.jpg" }}, {{"sinker"                                       }}, {{280}} },
  { Plt::SquadSevenVet,   {{ 527, "3c9cad4a6c8cdb6a7749a76e1b42ec23.jpg" }}, {{"squadsevenveteran",                           }}, {{340}} },
  { Plt::OneOhFourthBP,   {{ 528, "5081daee997a2a55de474dad2e8e4a07.jpg" }}, {{"104thbattalionpilot"                          }}, {{341}} },

  { Plt::HSyndulla_ASF01, {{ 878, "8a1e9cd504cde297c35d03aa728f5f40.jpg" }}, {{"herasyndulla-asf01bwing"                      }}, {{450}} },
  { Plt::GMoonsong,       {{ 628, "df07f280040d4284c04c194e18c62c9e.jpg" }}, {{"ginamoonsong"                                 }}, {{391}} },
  { Plt::BStramm,         {{  23, "Card_art_XW_P_23.jpg"                 }}, {{"braylenstramm"                                }}, {{ 73}} },
  { Plt::TNumb,           {{  24, "Card_art_XW_P_24.jpg"                 }}, {{"tennumb"                                      }}, {{ 74}} },
  { Plt::BladeSqVet,      {{  25, "Card_art_XW_P_25.jpg"                 }}, {{"bladesquadronveteran"                         }}, {{ 75}} },
  { Plt::NPollard,        {{ 879, "8e8ea8e29324398b455ec7b01bc1622e.jpg" }}, {{"netrempollard"                                }}, {{464}} },
  { Plt::BlueSqPlt,       {{  26, "Card_art_XW_P_26.jpg"                 }}, {{"bluesquadronpilot"                            }}, {{ 76}} },

  { Plt::HSyndulla_AS,    {{  34, "Card_art_XW_P_34.jpg"                 }}, {{"herasyndulla"                                 }}, {{ 65}} },
  { Plt::EBridger_AS,     {{  36, "Card_art_XW_P_36.jpg"                 }}, {{"ezrabridger"                                  }}, {{ 67}} },
  { Plt::SWren_AS,        {{  35, "Card_art_XW_P_35.jpg"                 }}, {{"sabinewren"                                   }}, {{ 66}} },
  { Plt::ZOrrelios_AS,    {{  37, "Card_art_XW_P_37.jpg"                 }}, {{"zeborrelios"                                  }}, {{ 68}} },

  { Plt::Wullffwarro,     {{  31, "Card_art_XW_P_31.jpg"                 }}, {{"wullffwarro"                                  }}, {{ 58}} },
  { Plt::Lowhhrick,       {{  32, "Card_art_XW_P_32.jpg"                 }}, {{"lowhhrick"                                    }}, {{ 59}} },
  { Plt::KashyyykDef,     {{  33, "Card_art_XW_P_33.jpg"                 }}, {{"kashyyykdefender"                             }}, {{ 60}} },

  { Plt::GenGrievous,     {{ 492, "3e4dee70764ead7ebe581246a0d4b85d.jpg" }}, {{"generalgrievous"                              }}, {{305}} },
  { Plt::SkakoanAce,      {{ 495, "af0e63b7754ef598f1f36ed9a6c4b4ee.jpg" }}, {{"skakoanace"                                   }}, {{344}} },
  { Plt::WTambor,         {{ 493, "95127fa6d286fe64f3070e742dc64a7c.jpg" }}, {{"wattambor"                                    }}, {{306}} },
  { Plt::CaptSear,        {{ 494, "91a7a38482d28f21b2620d058800a8fe.jpg" }}, {{"captainsear"                                  }}, {{308}} },
  { Plt::FOAutopilot,     {{ 496, "91052cb8f4ba464f5ac4572a725f757b.jpg" }}, {{"feethanottrawautopilot"                       }}, {{307}} },

  { Plt::ZBliss,          {                                               }, {{"zoriibliss"                                   }}, {{477}} },
  { Plt::TNasz,           {                                               }, {{"tezanasz"                                     }}, {{478}} },
  { Plt::WTeshlo,         {                                               }, {{"wilsateshlo"                                  }}, {{479}} },
  { Plt::LFossang,        {                                               }, {{"legafossang"                                  }}, {{481}} },
  { Plt::NewRepPatrol,    {                                               }, {{"newrepublicpatrol"                            }}, {{485}} },
  { Plt::SZaro,           {                                               }, {{"shasazaro"                                    }}, {{480}} },
  { Plt::AAckbar,         {                                               }, {{"aftabackbar"                                  }}, {{482}} },
  { Plt::CThrenalli_BTA,  {                                               }, {{"caithrenalli-btanr2ywing"                     }}, {{484}} },
  { Plt::KijimiSR,        {                                               }, {{"kijimispicerunner"                            }}, {{486}} },
  { Plt::CKapellim,       {                                               }, {{"coruskapellim"                                }}, {{483}} },

  { Plt::NWexley_YW,      {{  13, "Card_art_XW_P_13.jpg"                 }}, {{"norrawexley-btla4ywing"                       }}, {{ 25}} },
  { Plt::DVander,         {{  14, "Card_art_XW_P_14.jpg"                 }}, {{"dutchvander"                                  }}, {{ 27}} },
  { Plt::HSalm,           {{  15, "Card_art_XW_P_15.jpg"                 }}, {{"hortonsalm"                                   }}, {{ 26}} },
  { Plt::EVerlaine,       {{  16, "Card_art_XW_P_16.jpg"                 }}, {{"evaanverlaine"                                }}, {{ 28}} },
  { Plt::GoldSqVet,       {{  17, "Card_art_XW_P_17.jpg"                 }}, {{"goldsquadronveteran"                          }}, {{ 29}} },
  { Plt::GraySqBomber,    {{  18, "Card_art_XW_P_18.jpg"                 }}, {{"graysquadronbomber"                           }}, {{ 30}} },

  { Plt::Kavil,           {{ 165, "Card_art_XW_P_165.jpg"                }}, {{"kavil"                                        }}, {{ 85}} },
  { Plt::LKai,            {{ 898, "4e6423bdaa2b09478f46f795c5328c17.jpg" }}, {{"leemakai"                                     }}, {{458}} },
  { Plt::AHadrassian,     {{ 899, "f00b0ed67b0de579c3bc4fdaceb3f3ab.jpg" }}, {{"arlizhadrassian"                              }}, {{459}} },
  { Plt::DRenthal,        {{ 166, "Card_art_XW_P_166.jpg"                }}, {{"drearenthal"                                  }}, {{ 86}} },
  { Plt::AmaxWarrior,     {{ 901, "1a6296a88e63a0896952e7dd4178105b.jpg" }}, {{"amaxinewarrior"                               }}, {{472}} },
  { Plt::Padric,          {{ 900, "7d5a18e41b8e3caf1d623cdf06acafc8.jpg" }}, {{"padric"                                       }}, {{460}} },
  { Plt::HiredGun,        {{ 167, "Card_art_XW_P_167.jpg"                }}, {{"hiredgun"                                     }}, {{ 87}} },
  { Plt::JinataSecOff,    {{ 902, "d1b998eff3b93d5ed05870096aa27817.jpg" }}, {{"jinatasecurityofficer"                        }}, {{471}} },
  { Plt::CrymorahGoon,    {{ 168, "Card_art_XW_P_168.jpg"                }}, {{"crymorahgoon"                                 }}, {{ 88}} },

  { Plt::ASkywalker_BTLB, {{ 596, "7fc9010418aceb84cd9fb48d34e39ec8.jpg" }}, {{"anakinskywalker-btlbywing"                    }}, {{359}} },
  { Plt::OddBall_BTLB,    {{ 597, "5f37609150489b6d49ca659d1b1caa8a.jpg" }}, {{"oddball-btlbywing"                            }}, {{366}} },
  { Plt::Matchstick,      {{ 598, "8d8b63edff7a5e9969ca61ebb4456837.jpg" }}, {{"matchstick"                                   }}, {{365}} },
  { Plt::Broadside,       {{ 599, "d34e3bcb51d765f9dd12a9fc266e7ad9.jpg" }}, {{"broadside"                                    }}, {{364}} },
  { Plt::ShadowSqVet,     {{ 602, "84e5bef1597b232a20e52e1aa28a4c7e.jpg" }}, {{"shadowsquadronveteran"                        }}, {{360}} },
  { Plt::Goji,            {{ 601, "026f989543c030d7c8e6757a9aab7a68.jpg" }}, {{"goji"                                         }}, {{363}} },
  { Plt::R2D2,            {{ 600, "90c3f9aac7f44e4a6a860c81f93aae99.jpg" }}, {{"r2d2"                                         }}, {{362}} },
  { Plt::RedSqBomber,     {{ 603, "e91e0d3a30e85c878f7493360d1b668f.jpg" }}, {{"redsquadronbomber"                            }}, {{361}} },

  { Plt::MDoni,           {{  62, "Card_art_XW_P_62.jpg"                 }}, {{"mirandadoni"                                  }}, {{ 17}} },
  { Plt::ETuketu,         {{  63, "Card_art_XW_P_63.jpg"                 }}, {{"esegetuketu"                                  }}, {{ 18}} },
  { Plt::WardenSqPlt,     {{  64, "Card_art_XW_P_64.jpg"                 }}, {{"wardensquadronpilot"                          }}, {{ 20}} },

  { Plt::AlderaanGuard,   {{ 706, "c029d78ab010ebb2d5a9405924efbf5a.jpg" }}, {{"alderaanianguard"                             }}, {{368}} },
  { Plt::RepJudiciary,    {{ 705, "a04b07c3b0b3dbe1bfac935ca3a9f0d4.jpg" }}, {{"republicjudiciary"                            }}, {{367}} },

  { Plt::SynSmugglers,    {{ 713, "17ae9e44f24e0a38ba81f4523857c4f0.jpg" }}, {{"syndicatesmugglers"                           }}, {{376}} },
  { Plt::SepPrivateers,   {{ 714, "25a3457e2b32c661210524ff8b1fc785.jpg" }}, {{"separatistprivateers"                         }}, {{375}} },

  { Plt::HSoloScum,       {{ 222, "Card_art_XW_P_222.jpg"                }}, {{"hansolo"                                      }}, {{ 89}} },
  { Plt::LCalrissianScum, {{ 223, "Card_art_XW_P_223.jpg"                }}, {{"landocalrissian"                              }}, {{ 90}} },
  { Plt::L337C,           {{ 224, "Card_art_XW_P_224.jpg"                }}, {{"l337"                                         }}, {{ 91}} },
  { Plt::FreighterCapt,   {{ 225, "Card_art_XW_P_225.jpg"                }}, {{"freightercaptain"                             }}, {{ 92}} },

  { Plt::ASkywalker_D7,   {{ 507, "dc121e1b285f30e4708bda6c0594b55f.jpg" }}, {{"anakinskywalker"                              }}, {{273}} },
  { Plt::OKenobi,         {{ 511, "f449693ccfd6529244ea7765eed2f83f.jpg" }}, {{"obiwankenobi"                                 }}, {{278}} },
  { Plt::PKoon,           {{ 513, "a4f58c67ecedcb9fcfa6ee45613c002e.jpg" }}, {{"plokoon"                                      }}, {{312}} },
  { Plt::LUnduli,         {{ 508, "22519908330c72696863815565a2beda.jpg" }}, {{"luminaraunduli"                               }}, {{274}} },
  { Plt::MWindu,          {{ 512, "ba6c851af3b8f650d8dcb865a05636f2.jpg" }}, {{"macewindu"                                    }}, {{314}} },
  { Plt::BOffee,          {{ 509, "6eb248dfda52dc29a8aa4fda7e414eab.jpg" }}, {{"barrissoffee"                                 }}, {{275}} },
  { Plt::STiin,           {{ 514, "e36d5e616e0b0135b0faa1c9f46992a1.jpg" }}, {{"saeseetiin"                                   }}, {{313}} },
  { Plt::ATano_D7,        {{ 510, "0fc229b9b79c9f6c750a587614729293.jpg" }}, {{"ahsokatano"                                   }}, {{276}} },
  { Plt::JediKnight,      {{ 515, "d3bf6f2f5482c2b68a4d3d02cd758a68.jpg" }}, {{"jediknight"                                   }}, {{277}} },

  { Plt::PhlacArpProto,   {{ 796, "eb98630629620582fd4c0d091205da1f.jpg" }}, {{"phlacarphoccprototype"                        }}, {{423}} },
  { Plt::DIST81,          {{ 794, "dcb7fa7d662c724a28e03ddb3370da25.jpg" }}, {{"dist81"                                       }}, {{422}} },
  { Plt::FearsomePred,    {{ 797, "a98ea9c3fc6a99ebda7b5c841d5c179e.jpg" }}, {{"fearsomepredator"                             }}, {{420}} },
  { Plt::DIS347,          {{ 795, "805930d3844bde8727c6bb17eb973c8e.jpg" }}, {{"dis347"                                       }}, {{421}} },
  { Plt::SepInt,          {{ 798, "3981f72bca3374e9654a11d70531354e.jpg" }}, {{"separatistinterceptor"                        }}, {{425}} },
  { Plt::ColInt,          {{ 799, "a0b36a5320377ef6cb2611e4d0d39a6a.jpg" }}, {{"colicoidinterceptor"                          }}, {{424}} },

  { Plt::LCalrissian_EC,  {{ 226, "Card_art_XW_P_226.jpg"                }}, {{"landocalrissian-escapecraft"                  }}, {{ 93}} },
  { Plt::ORPioneer,       {{ 227, "Card_art_XW_P_227.jpg"                }}, {{"outerrimpioneer"                              }}, {{ 94}} },
  { Plt::L337E,           {{ 228, "Card_art_XW_P_228.jpg"                }}, {{"l337-escapecraft"                             }}, {{ 95}} },
  { Plt::AutoPltDrone,    {{ 229, "Card_art_XW_P_229.jpg"                }}, {{"autopilotdrone"                               }}, {{ 96}} },

  { Plt::ASkywalker_Eta2, {{ 781, "59d5bfe1c3cae085918a62ab75573032.jpg" }}, {{"anakinskywalker-eta2actis"                    }}, {{441}} },
  { Plt::OKenobi_Eta2,    {{ 782, "7718872b876f1cd2a508276a6b79e187.jpg" }}, {{"obiwankenobi-eta2actis"                       }}, {{440}} },
  { Plt::ASecura,         {{ 783, "a2171b5544ebfd56e22775efa9b73fc4.jpg" }}, {{"aaylasecura"                                  }}, {{439}} },
  { Plt::STi,             {{ 784, "c6e6e480dbdf934d2a7d04f1800f5e45.jpg" }}, {{"shaakti"                                      }}, {{438}} },
  { Plt::JediGen,         {{ 786, "b6b83d1e5f47b832018274f35339492a.jpg" }}, {{"jedigeneral"                                  }}, {{436}} },
  { Plt::Yoda,            {{ 785, "cbe2495ee143163b82cd7808fbb4f7cd.jpg" }}, {{"yoda"                                         }}, {{437}} },

  { Plt::CHorn,           {{  50, "Card_art_XW_P_50.jpg"                 }}, {{"corranhorn"                                   }}, {{ 21}} },
  { Plt::GDarklighter,    {{  51, "Card_art_XW_P_51.jpg"                 }}, {{"gavindarklighter"                             }}, {{ 22}} },
  { Plt::RogueSqEsc,      {{  52, "Card_art_XW_P_52.jpg"                 }}, {{"roguesquadronescort"                          }}, {{ 23}} },
  { Plt::KnaveSqEsc,      {{  53, "Card_art_XW_P_53.jpg"                 }}, {{"knavesquadronescort"                          }}, {{ 24}} },

  { Plt::FRau_FF,         {{ 155, "Card_art_XW_P_155.jpg"                }}, {{"fennrau"                                      }}, {{ 97}} },
  { Plt::OTeroch,         {{ 156, "Card_art_XW_P_156.jpg"                }}, {{"oldteroch"                                    }}, {{ 98}} },
  { Plt::JRekkoff,        {{ 157, "Card_art_XW_P_157.jpg"                }}, {{"joyrekkoff"                                   }}, {{100}} },
  { Plt::KSolus,          {{ 158, "Card_art_XW_P_158.jpg"                }}, {{"kadsolus"                                     }}, {{ 99}} },
  { Plt::SkullSqPlt,      {{ 159, "Card_art_XW_P_159.jpg"                }}, {{"skullsquadronpilot"                           }}, {{101}} },
  { Plt::ZealousRecruit,  {{ 160, "Card_art_XW_P_160.jpg"                }}, {{"zealousrecruit"                               }}, {{102}} },

  { Plt::JYeager,         {{ 620, "21bd6c9d7e36c873f88b0fad11ff8fe6.jpg" }}, {{"jarekyeager"                                  }}, {{377}} },
  { Plt::KXiono,          {{ 621, "4ffdf7cc8ed023322ccf4217ff64b4d2.jpg" }}, {{"kazudaxiono"                                  }}, {{378}} },
  { Plt::R1J5,            {{ 622, "686597246669987fe1b938f1419e598d.jpg" }}, {{"r1j5"                                         }}, {{379}} },
  { Plt::ColStationMech,  {{ 623, "a8a4a52f9ced88c0bb7b9e90e0e75a18.jpg" }}, {{"colossusstationmechanic"                      }}, {{380}} },

  { Plt::BFett,           {{ 149, "Card_art_XW_P_149.jpg"                }}, {{"bobafett"                                     }}, {{103}} },
  { Plt::EAzzameen,       {{ 150, "Card_art_XW_P_150.jpg"                }}, {{"emonazzameen"                                 }}, {{104}} },
  { Plt::KScarlet,        {{ 151, "Card_art_XW_P_151.jpg"                }}, {{"kathscarlet"                                  }}, {{105}} },
  { Plt::KFrost,          {{ 152, "Card_art_XW_P_152.jpg"                }}, {{"koshkafrost"                                  }}, {{106}} },
  { Plt::KTrelix,         {{ 153, "Card_art_XW_P_153.jpg"                }}, {{"krassistrelix"                                }}, {{107}} },
  { Plt::BountyHunter,    {{ 154, "Card_art_XW_P_154.jpg"                }}, {{"bountyhunter"                                 }}, {{108}} },

  { Plt::JFett,           {{ 800, "b6d21423ce39d0a57330782ac9f37e24.jpg" }}, {{"jangofett"                                    }}, {{429}} },
  { Plt::ZWesell,         {{ 801, "99cf31dd1b9dc66bb0bd9522aacea4ce.jpg" }}, {{"zamwesell"                                    }}, {{428}} },
  { Plt::BFett_Sep,       {{ 802, "3b76c2f168aa5b6644c9d270200a3ef1.jpg" }}, {{"bobafett-separatistalliance"                  }}, {{427}} },
  { Plt::SepRacketeer,    {{ 803, "fd061204d3235827f2457d5be76cc18b.jpg" }}, {{"separatistracketeer"                          }}, {{426}} },

  { Plt::FourLOM,         {{ 201, "Card_art_XW_P_201.jpg"                }}, {{"4lom"                                         }}, {{109}} },
  { Plt::Zuckuss,         {{ 202, "Card_art_XW_P_202.jpg"                }}, {{"zuckuss"                                      }}, {{110}} },
  { Plt::GandFindsman,    {{ 203, "Card_art_XW_P_203.jpg"                }}, {{"gandfindsman"                                 }}, {{111}} },

  { Plt::OuterRimGar,     {{ 711, "205015c2eb127931dbdca3e841ca8668.jpg" }}, {{"outerrimgarrison"                             }}, {{373}} },
  { Plt::FOSymp,          {{ 712, "99965cf5c4a2611773c6e7f4ecb6608f.jpg" }}, {{"firstordersympathizers"                       }}, {{374}} },

  { Plt::EchoBaseEvac,    {{ 709, "4a49f20575c252f47a70d3c4da909301.jpg" }}, {{"echobaseevacuees"                             }}, {{371}} },
  { Plt::NewRepVol,       {{ 710, "5c7cf19b2300729a88d9d58ca60ef3df.jpg" }}, {{"newrepublicvolunteers"                        }}, {{372}} },

  { Plt::OnderonOpp,      {{ 665, "ba5a6c72eb21bd7bd58c9f7072dd0fdd.jpg" }}, {{"onderonoppressor"                             }}, {{411}} },
  { Plt::DGS286,          {{ 664, "20ce8cad5b251ad3ea16d849297e82db.jpg" }}, {{"dgs286"                                       }}, {{410}} },
  { Plt::SepPredator,     {{ 667, "af4f7665df100530f6dc8e3cf80a52d4.jpg" }}, {{"separatistpredator"                           }}, {{407}} },
  { Plt::GeonosianProto,  {{ 668, "3a95eb82b3cf0420376c75f649d436ee.jpg" }}, {{"geonosianprototype"                           }}, {{408}} },
  { Plt::DGS047,          {{ 666, "1cd30a5d258ff70472e785c816139d1f.jpg" }}, {{"dgs047"                                       }}, {{409}} },
  { Plt::BaktoidDrone,    {{ 669, "8117e7b2349219b237157bfeda3028d0.jpg" }}, {{"baktoiddrone"                                 }}, {{405}} },

  { Plt::JOrs,            {{  42, "Card_art_XW_P_42.jpg"                 }}, {{"janors"                                       }}, {{ 46}} },
  { Plt::RGarnet,         {{  44, "Card_art_XW_P_44.jpg"                 }}, {{"roarkgarnet"                                  }}, {{ 47}} },
  { Plt::KKatarn,         {{  43, "Card_art_XW_P_43.jpg"                 }}, {{"kylekatarn"                                   }}, {{ 48}} },
  { Plt::RebelScout,      {{  45, "Card_art_XW_P_45.jpg"                 }}, {{"rebelscout"                                   }}, {{ 49}} },

  { Plt::Tapusk,          {{ 895, "b88cf93a5391d3b1ebd7d619a7aabdf9.jpg" }}, {{"tapusk"                                       }}, {{474}} },
  { Plt::DBonearm,        {{ 174, "Card_art_XW_P_174.jpg"                }}, {{"dacebonearm"                                  }}, {{113}} },
  { Plt::GKey,            {{ 896, "c505641a8f7b883897b41fd07809d37d.jpg" }}, {{"gamutkey"                                     }}, {{473}} },
  { Plt::KJarrus_HWK,     {{ 897, "a1ad87f3418ee0b755b42f7afb259858.jpg" }}, {{"kananjarrus-hwk290lightfreighter"             }}, {{457}} },
  { Plt::PGodalhi,        {{ 175, "Card_art_XW_P_175.jpg"                }}, {{"palobgodalhi"                                 }}, {{112}} },
  { Plt::TMux,            {{ 176, "Card_art_XW_P_176.jpg"                }}, {{"torkilmux"                                    }}, {{114}} },
  { Plt::SpiceRunner,     {{ 177, "Card_art_XW_P_177.jpg"                }}, {{"spicerunner"                                  }}, {{230}} },

  { Plt::DBS404,          {{ 563, "fb1f119c8dcd69db43a442fecf25fceb.jpg" }}, {{"dbs404"                                       }}, {{326}} },
  { Plt::DBS32C,          {{ 564, "f58f50898f4fa3900eb1b7d01aec4ae5.jpg" }}, {{"dbs32c"                                       }}, {{328}} },
  { Plt::BombardmentDrn,  {{ 565, "bc5e862af323dbe8db28d0d6bc6be4ad.jpg" }}, {{"bombardmentdrone"                             }}, {{325}} },
  { Plt::SepBomber,       {{ 567, "162821196c9ca9ca7ef8d1cb1acab15b.jpg" }}, {{"separatistbomber"                             }}, {{327}} },
  { Plt::BaktoidProto,    {{ 566, "f1e719d3490aceee6e5d93ac5b2a6cb2.jpg" }}, {{"baktoidprototype"                             }}, {{329}} },
  { Plt::TUBomber,        {{ 568, "38a1ea6b53a619fcc6121cc32a91024d.jpg" }}, {{"technounionbomber"                            }}, {{324}} },

  { Plt::Dengar,          {{ 214, "Card_art_XW_P_214.jpg"                }}, {{"dengar"                                       }}, {{115}} },
  { Plt::TTrevura,        {{ 216, "Card_art_XW_P_216.jpg"                }}, {{"teltrevura"                                   }}, {{116}} },
  { Plt::Manaroo,         {{ 215, "Card_art_XW_P_215.jpg"                }}, {{"manaroo"                                      }}, {{117}} },
  { Plt::ContractedSc,    {{ 217, "Card_art_XW_P_217.jpg"                }}, {{"contractedscout"                              }}, {{118}} },
  { Plt::NLumb,           {{ 637, "3de089d708ab1c7d8aee35ef99a1f1ed.jpg" }}, {{"nomlumb"                                      }}, {{400}} },

  { Plt::TCobra,          {{ 191, "Card_art_XW_P_191.jpg"                }}, {{"talonbanecobra"                               }}, {{119}} },
  { Plt::Graz,            {{ 192, "Card_art_XW_P_192.jpg"                }}, {{"graz"                                         }}, {{120}} },
  { Plt::VHel,            {{ 193, "Card_art_XW_P_193.jpg"                }}, {{"viktorhel"                                    }}, {{121}} },
  { Plt::BlackSunAce,     {{ 195, "Card_art_XW_P_195.jpg"                }}, {{"blacksunace"                                  }}, {{123}} },
  { Plt::CaptJostero,     {{ 194, "Card_art_XW_P_194.jpg"                }}, {{"captainjostero"                               }}, {{122}} },
  { Plt::CartMarauder,    {{ 196, "Card_art_XW_P_196.jpg"                }}, {{"cartelmarauder"                               }}, {{124}} },

  { Plt::Hawk,            {{ 660, "14be5b23859dff4b6213511eb97e4e5b.jpg" }}, {{"hawk"                                         }}, {{415}} },
  { Plt::Warthog,         {{ 661, "f1866068850a0c806bfbe03c8bba2d07.jpg" }}, {{"warthog"                                      }}, {{414}} },
  { Plt::Hound,           {{ 662, "a88730785b8495c24b1711278ed18934.jpg" }}, {{"hound"                                        }}, {{413}} },
  { Plt::TwoOneTwoBatPlt, {{ 663, "b8538000c5745f53f79e9b6650e6deec.jpg" }}, {{"212thbattalionpilot"                          }}, {{412}} },

  { Plt::CaptKagi,        {{ 142, "Card_art_XW_P_142.jpg"                }}, {{"captainkagi"                                  }}, {{165}} },
  { Plt::ColJendon,       {{ 143, "Card_art_XW_P_143.jpg"                }}, {{"coloneljendon"                                }}, {{167}} },
  { Plt::LtSai,           {{ 144, "Card_art_XW_P_144.jpg"                }}, {{"lieutenantsai"                                }}, {{166}} },
  { Plt::OmicronGrpPlt,   {{ 145, "Card_art_XW_P_145.jpg"                }}, {{"omicrongrouppilot"                            }}, {{168}} },

  { Plt::KOnyo,           {{ 218, "Card_art_XW_P_218.jpg"                }}, {{"ketsuonyo"                                    }}, {{126}} },
  { Plt::AVentress,       {{ 219, "Card_art_XW_P_219.jpg"                }}, {{"asajjventress"                                }}, {{125}} },
  { Plt::SWren_LC,        {{ 220, "Card_art_XW_P_220.jpg"                }}, {{"sabinewren-lancerclasspursuitcraft"           }}, {{127}} },
  { Plt::ShadowportHun,   {{ 221, "Card_art_XW_P_221.jpg"                }}, {{"shadowporthunter"                             }}, {{128}} },

  { Plt::TKulda,          {{ 207, "Card_art_XW_P_207.jpg"                }}, {{"toranikulda"                                  }}, {{129}} },
  { Plt::DOberos_KG,      {{ 208, "Card_art_XW_P_208.jpg"                }}, {{"dalanoberos"                                  }}, {{130}} },
  { Plt::CartExecution,   {{ 209, "Card_art_XW_P_209.jpg"                }}, {{"cartelexecutioner"                            }}, {{131}} },

  { Plt::Serissu,         {{ 183, "Card_art_XW_P_183.jpg"                }}, {{"serissu"                                      }}, {{132}} },
  { Plt::GRed,            {{ 184, "Card_art_XW_P_184.jpg"                }}, {{"genesisred"                                   }}, {{133}} },
  { Plt::LAshera,         {{ 185, "Card_art_XW_P_185.jpg"                }}, {{"laetinashera"                                 }}, {{134}} },
  { Plt::QJast,           {{ 186, "Card_art_XW_P_186.jpg"                }}, {{"quinnjast"                                    }}, {{135}} },
  { Plt::TansariiPtVet,   {{ 189, "Card_art_XW_P_189.jpg"                }}, {{"tansariipointveteran"                         }}, {{136}} },
  { Plt::Inaldra,         {{ 187, "Card_art_XW_P_187.jpg"                }}, {{"inaldra"                                      }}, {{137}} },
  { Plt::CartelSpacer,    {{ 190, "Card_art_XW_P_190.jpg"                }}, {{"cartelspacer"                                 }}, {{139}} },
  { Plt::SBounder,        {{ 188, "Card_art_XW_P_188.jpg"                }}, {{"sunnybounder"                                 }}, {{138}} },
  { Plt::G4RGORVM,        {{ 636, "f1d43e799b5f829b40c091a2274e570f.jpg" }}, {{"g4rgorvm"                                     }}, {{399}} },

  { Plt::PTico,           {{ 640, "2bd70a86ef09e15aa93b165a56f32834.jpg" }}, {{"paigetico"                                    }}, {{389}} },
  { Plt::FDallow,         {{ 431, "dafd2937accc362f766ca0da3308ccbc.jpg" }}, {{"finchdallow"                                  }}, {{250}} },
  { Plt::BTeene,          {{ 432, "74ca7444aec38cb0034b67b04f6aaa76.jpg" }}, {{"benteene"                                     }}, {{292}} },
  { Plt::EKappehl,        {{ 447, "bbac51ca4ba44f26d90a53b6c5db5e2c.jpg" }}, {{"edonkappehl"                                  }}, {{293}} },
  { Plt::Vennie,          {{ 448, "511b878f719e9fa59a50cdf980d10ead.jpg" }}, {{"vennie"                                       }}, {{294}} },
  { Plt::Cat,             {{ 433, "d927ccf08a9444340378449fe71e9f70.jpg" }}, {{"cat"                                          }}, {{291}} },
  { Plt::CobaltSqBomber,  {{ 434, "514351d5c41a8341ea5da5673269d917.jpg" }}, {{"cobaltsquadronbomber"                         }}, {{260}} },

  { Plt::ForemanProach,   {{ 441, "f789aa53866112fc44fd27ed9e177993.jpg" }}, {{"foremanproach"                                }}, {{302}} },
  { Plt::Ahhav,           {{ 442, "2d162fae88ae58b9eca31f7dc0b7a9da.jpg" }}, {{"ahhav"                                        }}, {{249}} },
  { Plt::CaptSeevor,      {{ 443, "6ead4bbfa14075a19972dc57b90a34e1.jpg" }}, {{"captainseevor"                                }}, {{247}} },
  { Plt::OverseerYushyn,  {{ 444, "1f454eb7c12b572103e59a9a782c3f50.jpg" }}, {{"overseeryushyn"                               }}, {{303}} },
  { Plt::MGSurveyor,      {{ 445, "ad058d0b6d46f668f06bf0007207a30a.jpg" }}, {{"miningguildsurveyor"                          }}, {{248}} },
  { Plt::MGSentry,        {{ 446, "09c955b8008750a30fe398c200431160.jpg" }}, {{"miningguildsentry"                            }}, {{304}} },

  { Plt::HSoloReb,        {{  69, "Card_art_XW_P_69.jpg"                 }}, {{"hansolo-modifiedyt1300lightfreighter"         }}, {{ 42}} },
  { Plt::LCalrissianReb,  {{  70, "Card_art_XW_P_70.jpg"                 }}, {{"landocalrissian-modifiedyt1300lightfreighter" }}, {{ 43}} },
  { Plt::LOrgana,         {{ 630, "dc207b5c5f00a98e0fad8dc3905373f9.jpg" }}, {{"leiaorgana"                                   }}, {{393}} },
  { Plt::Chewbacca,       {{  71, "Card_art_XW_P_71.jpg"                 }}, {{"chewbacca"                                    }}, {{ 44}} },
  { Plt::OuterRimSmug,    {{  72, "Card_art_XW_P_72.jpg"                 }}, {{"outerrimsmuggler"                             }}, {{ 45}} },

  { Plt::ROlie,           {{ 557, "26080de6b705112c0d7d53de48484bb0.jpg" }}, {{"ricolie"                                      }}, {{333}} },
  { Plt::ASkywalker_N1,   {{ 558, "d9180cc98a1a4384bb2a0e11efbf1311.jpg" }}, {{"anakinskywalker-nabooroyaln1starfighter"      }}, {{322}} },
  { Plt::PAmidala,        {{ 559, "254a567fa647e783bcd3fb9c09377e3d.jpg" }}, {{"padmeamidala"                                 }}, {{332}} },
  { Plt::DEllberger,      {{ 560, "3f71de47ef3b5986a433757a00f4230b.jpg" }}, {{"dineeellberger"                               }}, {{331}} },
  { Plt::BravoFlightOff,  {{ 562, "4e1a2195431b026de73c90f8ce5f7c20.jpg" }}, {{"bravoflightofficer"                           }}, {{323}} },
  { Plt::NabooHandmaiden, {{ 561, "afe1521a12124eeff41ed4234e7591ac.jpg" }}, {{"naboohandmaiden"                              }}, {{330}} },

  { Plt::SFac,            {{ 604, "ace32f2bdd974de54020be34bfb85a76.jpg" }}, {{"sunfac"                                       }}, {{357}} },
  { Plt::BKret,           {{ 605, "8466283eeb7d476744913f5d9d69e745.jpg" }}, {{"berwerkret"                                   }}, {{358}} },
  { Plt::Chertek,         {{ 606, "fc7fff6c1e6eb73ff6ae10768f5491a7.jpg" }}, {{"chertek"                                      }}, {{356}} },
  { Plt::PetArenaAce,     {{ 608, "5c32b5d481ad922be7a0ec4a3743d1d6.jpg" }}, {{"petranakiarenaace"                            }}, {{354}} },
  { Plt::StalgasinHG,     {{ 609, "62c29b8b7f82e37980d58b39023a800a.jpg" }}, {{"stalgasinhiveguard"                           }}, {{353}} },
  { Plt::Gorgol,          {{ 607, "8749f3b0f17a7c517e8633cc2558792f.jpg" }}, {{"gorgol"                                       }}, {{355}} },

  { Plt::OddBall_Vw,      {{ 788, "03ff8a220e58ba5c122bd82292447182.jpg" }}, {{"oddball-nimbusclassvwing"                     }}, {{435}} },
  { Plt::Contrail,        {{ 789, "a513ddef007bed78558369fd70379d15.jpg" }}, {{"contrail"                                     }}, {{434}} },
  { Plt::Klick,           {{ 790, "2331f92ad91a30e81c5c6baafc0dfe08.jpg" }}, {{"klick"                                        }}, {{433}} },
  { Plt::WTarkin,         {{ 791, "02fb0df2a559b8bc66fd475442dbe8df.jpg" }}, {{"wilhufftarkin"                                }}, {{432}} },
  { Plt::ShadowSqEsc,     {{ 792, "a9dd9a8a65facfae13018bb0d1aad602.jpg" }}, {{"shadowsquadronescort"                         }}, {{431}} },
  { Plt::LoyalistVol,     {{ 793, "963d472b274bb777fdc9be15527e28f1.jpg" }}, {{"loyalistvolunteer"                            }}, {{430}} },

  { Plt::ConstZuvio,      {{ 161, "Card_art_XW_P_161.jpg"                }}, {{"constablezuvio"                               }}, {{140}} },
  { Plt::SPlank,          {{ 162, "Card_art_XW_P_162.jpg"                }}, {{"sarcoplank"                                   }}, {{141}} },
  { Plt::UPlutt,          {{ 163, "Card_art_XW_P_163.jpg"                }}, {{"unkarplutt"                                   }}, {{142}} },
  { Plt::JGunrunner,      {{ 164, "Card_art_XW_P_164.jpg"                }}, {{"jakkugunrunner"                               }}, {{143}} },

  { Plt::FOCollab,        {{ 708, "b9a9671d88e370fd473ec5d87491ad65.jpg" }}, {{"firstordercollaborators"                      }}, {{370}} },
  { Plt::OuterRimPat,     {{ 707, "bbcb9af1eb386d2855f34e7ffa730528.jpg" }}, {{"outerrimpatrol"                               }}, {{369}} },

  { Plt::CNell,           {{ 569, "27956dc3600e6e06bf26f9a981205410.jpg" }}, {{"covanell"                                     }}, {{351}} },
  { Plt::PNGoode,         {{ 570, "e5f2e692fcaf63d513cd5c7c1294283e.jpg" }}, {{"pammichnerrogoode"                            }}, {{347}} },
  { Plt::NChavdri,        {{ 571, "b0179494a00c2538a4467793e722db3e.jpg" }}, {{"nodinchavdri"                                 }}, {{352}} },
  { Plt::LogDivPlt,       {{ 572, "005545edf9fc1c82464f52edad236b27.jpg" }}, {{"logisticsdivisionpilot"                       }}, {{346}} },

  { Plt::BB8,             {{ 573, "bbe37aa13822e8c94d9b0950cdd9cdb0.jpg" }}, {{"bb8"                                          }}, {{349}} },
  { Plt::RTico,           {{ 574, "40cba6527802ef9afa41e4009265873c.jpg" }}, {{"rosetico"                                     }}, {{345}} },
  { Plt::Finn,            {{ 575, "6e5fea21ac8a7ee345fd07f1c42cc81d.jpg" }}, {{"finn"                                         }}, {{350}} },
  { Plt::VMoradi,         {{ 576, "f13328acfd703dc5489fa2249b9f675e.jpg" }}, {{"vimoradi"                                     }}, {{348}} },

  { Plt::HSyndulla_RZ1,   {{ 880, "a50b1f5e19e9b389bf521becfa71d447.jpg" }}, {{"herasyndulla-rz1awing"                        }}, {{451}} },
  { Plt::ATano_RZ1,       {{ 881, "40fe14090b12fb634888c845a8efbb1c.jpg" }}, {{"ahsokatano-rz1awing"                          }}, {{463}} },
  { Plt::JFarrell,        {{  19, "Card_art_XW_P_19.jpg"                 }}, {{"jakefarrell"                                  }}, {{ 50}} },
  { Plt::SBey_RZ1,        {{ 882, "c7de7d8ea5e6d4624e8541423f23f447.jpg" }}, {{"sharabey-rz1awing"                            }}, {{462}} },
  { Plt::WAntilles_RZ1,   {{ 883, "11bb21ea8f81f374ff862f86e722444f.jpg" }}, {{"wedgeantilles-rz1awing"                       }}, {{452}} },
  { Plt::ACrynyd,         {{  20, "Card_art_XW_P_20.jpg"                 }}, {{"arvelcrynyd"                                  }}, {{ 51}} },
  { Plt::DKlivian,        {{ 884, "ab937640d11798db7513aecc3de53a16.jpg" }}, {{"derekklivian"                                 }}, {{461}} },
  { Plt::GreenSqPlt,      {{  21, "Card_art_XW_P_21.jpg"                 }}, {{"greensquadronpilot"                           }}, {{ 52}} },
  { Plt::SWren_RZ1,       {{ 885, "9db76c98a8bd8bff2c4c46408d631ac0.jpg" }}, {{"sabinewren-rz1awing"                          }}, {{453}} },
  { Plt::PhoenixSqPlt,    {{  22, "Card_art_XW_P_22.jpg"                 }}, {{"phoenixsquadronpilot"                         }}, {{ 53}} },

  { Plt::LLampar,         {{ 435, "b97a025a7859f54bbc68374ff5d8116e.jpg" }}, {{"lulolampar"                                   }}, {{239}} },
  { Plt::SVanik,          {{ 701, "ee11a475190af6c517ca872b19cdaa62.jpg" }}, {{"seftinvanik"                                  }}, {{447}} },
  { Plt::TLintra,         {{ 436, "ee8c3c26ce6432d7581c5f61392597bc.jpg" }}, {{"tallissanlintra"                              }}, {{240}} },
  { Plt::ZTlo,            {{ 638, "7d5b4f0691b55e9c755b1e71bd16a422.jpg" }}, {{"zizitlo"                                      }}, {{387}} },
  { Plt::GSonnel,         {{ 437, "84bf1ce21926d4500b54e122da01b162.jpg" }}, {{"greersonnel"                                  }}, {{270}} },
  { Plt::WTyce,           {{ 702, "cdc08804712c43ad1f8c6d0597f9d8e3.jpg" }}, {{"wrobietyce"                                   }}, {{446}} },
  { Plt::GreenSqExp,      {{ 439, "8427bdfb1cf9497a9ab797e2c955ba41.jpg" }}, {{"greensquadronexpert"                          }}, {{300}} },
  { Plt::SJavos,          {{ 703, "255fef80a9e49eb154f254405b6d4ff5.jpg" }}, {{"suralindajavos"                               }}, {{445}} },
  { Plt::ZBangel,         {{ 438, "2fe44e1e5496645c16f4d2189a1746e3.jpg" }}, {{"zaribangel"                                   }}, {{271}} },
  { Plt::RBlario,         {{ 639, "0930bea45803774f4b0d863b92a64328.jpg" }}, {{"ronithblario"                                 }}, {{388}} },
  { Plt::BlueSqRecruit,   {{ 440, "3df643a75106a59899e5f32ef56e8a5e.jpg" }}, {{"bluesquadronrecruit"                          }}, {{301}} },
  { Plt::MCobben,         {{ 704, "c51115091ff7f31d6c2da890ed303dd8.jpg" }}, {{"merlcobben"                                   }}, {{444}} },

  { Plt::HSoloRes,        {{ 427, "15263defc98e4a54d76ae9224534dacf.jpg" }}, {{"hansolo-scavengedyt1300"                      }}, {{245}} },
  { Plt::Rey,             {{ 428, "1ff06aeff0d74773e9c9f3846b38b75d.jpg" }}, {{"rey"                                          }}, {{244}} },
  { Plt::ChewieRes,       {{ 429, "c40ec30b7df138c4510b08f4f02d54b5.jpg" }}, {{"chewbacca-scavengedyt1300"                    }}, {{246}} },
  { Plt::ResSymp,         {{ 430, "3e567923957881e9a4dbef46789c7fbf.jpg" }}, {{"resistancesympathizer"                        }}, {{295}} },

  { Plt::CaptNym,         {{ 204, "Card_art_XW_P_204.jpg"                }}, {{"captainnym"                                   }}, {{144}} },
  { Plt::SSixxa,          {{ 205, "Card_art_XW_P_205.jpg"                }}, {{"solsixxa"                                     }}, {{145}} },
  { Plt::LRevenant,       {{ 206, "Card_art_XW_P_206.jpg"                }}, {{"lokrevenant"                                  }}, {{146}} },

  { Plt::FRau_Sh,         {{  38, "Card_art_XW_P_38.jpg",                }}, {{"fennrau-sheathipedeclassshuttle"              }}, {{ 69}} },
  { Plt::EBridger_Sh,     {{  39, "Card_art_XW_P_39.jpg",                }}, {{"ezrabridger-sheathipedeclassshuttle"          }}, {{ 70}} },
  { Plt::ZOrrelios_Sh,    {{  40, "Card_art_XW_P_40.jpg",                }}, {{"zeborrelios-sheathipedeclassshuttle"          }}, {{ 71}} },
  { Plt::AP5,             {{  41, "Card_art_XW_P_41.jpg",                }}, {{"ap5"                                          }}, {{ 72}} },

  { Plt::DMaul,           {{ 503, "04b1c1fea3735eb844455dd3278346d0.jpg" }}, {{"darthmaul"                                    }}, {{272}} },
  { Plt::CDooku,          {{ 504, "65eabe68c7d1ae072442d8c8808414db.jpg" }}, {{"countdooku"                                   }}, {{334}} },
  { Plt::O66,             {{ 505, "c0a65aa29f085d517d907bc7799e4146.jpg" }}, {{"066"                                          }}, {{335}} },
  { Plt::DarkCourier,     {{ 506, "8b9c158df033daab2ea6acecd29c5c0d.jpg" }}, {{"darkcourier"                                  }}, {{336}} },

  { Plt::Guri,            {{ 178, "Card_art_XW_P_178.jpg"                }}, {{"guri"                                         }}, {{147}} },
  { Plt::DOberos_SV,      {{ 179, "Card_art_XW_P_179.jpg"                }}, {{"dalanoberos-starviperclassattackplatform"     }}, {{149}} },
  { Plt::PXizor,          {{ 180, "Card_art_XW_P_180.jpg"                }}, {{"princexizor"                                  }}, {{148}} },
  { Plt::BSAssassin,      {{ 181, "Card_art_XW_P_181.jpg"                }}, {{"blacksunassassin"                             }}, {{150}} },
  { Plt::BSEnforcer,      {{ 182, "Card_art_XW_P_182.jpg"                }}, {{"blacksunenforcer"                             }}, {{151}} },

  { Plt::TransGalMegCL,   {{ 787, "76681fa18172af1ad516b1a8c84eabd6.jpg" }}, {{"transgalmegcontrollink"                       }}, {{406}} },

  { Plt::WAntilles_T65,   {{   1, "Card_art_XW_P_1.jpg"                  }}, {{"wedgeantilles"                                }}, {{  5}} },
  { Plt::LSkywalker,      {{   2, "Card_art_XW_P_2.jpg"                  }}, {{"lukeskywalker"                                }}, {{  4}} },
  { Plt::TKyrell,         {{   3, "Card_art_XW_P_3.jpg"                  }}, {{"thanekyrell"                                  }}, {{  9}} },
  { Plt::GDreis_XW,       {{   4, "Card_art_XW_P_4.jpg"                  }}, {{"garvendreis-t65xwing"                         }}, {{  6}} },
  { Plt::JPorkins,        {{   5, "Card_art_XW_P_5.jpg"                  }}, {{"jekporkins"                                   }}, {{  3}} },
  { Plt::KSperado,        {{   6, "Card_art_XW_P_6.jpg"                  }}, {{"kullbeesperado"                               }}, {{ 12}} },
  { Plt::BDarklighter,    {{   7, "Card_art_XW_P_7.jpg"                  }}, {{"biggsdarklighter"                             }}, {{  7}} },
  { Plt::LTenza,          {{   8, "Card_art_XW_P_8.jpg"                  }}, {{"leevantenza"                                  }}, {{ 10}} },
  { Plt::RedSqVet,        {{  10, "Card_art_XW_P_10.jpg"                 }}, {{"redsquadronveteran"                           }}, {{  2}} },
  { Plt::BlueSqEsc,       {{  11, "Card_art_XW_P_11.jpg"                 }}, {{"bluesquadronescort"                           }}, {{  1}} },
  { Plt::ETwoTubes,       {{   9, "Card_art_XW_P_9.jpg"                  }}, {{"edriotwotubes"                                }}, {{  8}} },
  { Plt::CAZealot,        {{  12, "Card_art_XW_P_12.jpg"                 }}, {{"cavernangelszealot"                           }}, {{  0}} },

  { Plt::PDameron,        {{ 418, "cb8efd0b723b8ceb0c35b7a64b309bf2.jpg" }}, {{"poedameron"                                   }}, {{231}} },
  { Plt::PDameron_RC,     {{ 697, "ecefa0fae78bbecdcb598ff36d323d4d.jpg" }}, {{"poedameron-swz68"                             }}, {{442}} },
  { Plt::EAsty,           {{ 419, "ff73537d7ab2f063e7a510c05013269e.jpg" }}, {{"elloasty"                                     }}, {{299}} },
  { Plt::NNunb,           {{ 420, "5daa441317975eb576396af36e852f74.jpg" }}, {{"niennunb"                                     }}, {{298}} },
  { Plt::BlackSqAce_Res,  {{ 451, "e75228d8174f7879c51157fdd9b26e61.jpg" }}, {{"blacksquadronace-t70xwing"                    }}, {{256}} },
  { Plt::CThrenalli_T70,  {{ 699, "b9ec74da0f0350bdc545aa068ff3f154.jpg" }}, {{"caithrenalli"                                 }}, {{449}} },
  { Plt::KKun,            {{ 421, "f121bc27f2b283258a65d348bcafe40d.jpg" }}, {{"karekun"                                      }}, {{252}} },
  { Plt::TWexley,         {{ 422, "6acd118b12a34136d4d1df6c765a0deb.jpg" }}, {{"temminwexley"                                 }}, {{297}} },
  { Plt::TWexley_BT,      {{ 698, "9ff6521bca10294ce8413081a1068ee0.jpg" }}, {{"temminwexley-swz68"                           }}, {{443}} },
  { Plt::JPava,           {{ 423, "05c48dc381e54c6951d3ee42d3587577.jpg" }}, {{"jessikapava"                                  }}, {{296}} },
  { Plt::JSeastriker,     {{ 424, "b5b43d179b039649e764e6bd4f212a29.jpg" }}, {{"jophseastriker"                               }}, {{253}} },
  { Plt::RedSqExpert,     {{ 425, "0e847453270afe089ea331316f7b7731.jpg" }}, {{"redsquadronexpert"                            }}, {{257}} },
  { Plt::LtBastian,       {{ 449, "52f96e7e98dc51c1280052514e303704.jpg" }}, {{"lieutenantbastian"                            }}, {{254}} },
  { Plt::NChireen,        {{ 700, "8c6284a4820a6c6f83eb60111d4ba978.jpg" }}, {{"nimichireen"                                  }}, {{448}} },
  { Plt::BlueSqRookie,    {{ 426, "8ceeaf3d985f16da8b8d5a1ebc49ea2b.jpg" }}, {{"bluesquadronrookie"                           }}, {{258}} },
  { Plt::JTubbs,          {{ 450, "8874efb312a64da49889c66f96338f9c.jpg" }}, {{"jaycristubbs"                                 }}, {{255}} },

  { Plt::GrandInq,        {{  99, "Card_art_XW_P_99.jpg"                 }}, {{"grandinquisitor"                              }}, {{169}} },
  { Plt::FifthBrother,    {{ 632, "e79e45f2bdb5a2ab6ff6728b42db0b74.jpg" }}, {{"fifthbrother"                                 }}, {{394}} },
  { Plt::SeventhSister,   {{ 100, "Card_art_XW_P_100.jpg"                }}, {{"seventhsister"                                }}, {{170}} },
  { Plt::BaronOfEmp,      {{ 101, "Card_art_XW_P_101.jpg"                }}, {{"baronoftheempire"                             }}, {{172}} },
  { Plt::Inquisitor,      {{ 102, "Card_art_XW_P_102.jpg"                }}, {{"inquisitor"                                   }}, {{171}} },

  { Plt::DVader_x1,       {{  93, "Card_art_XW_P_93.jpg"                 }}, {{"darthvader"                                   }}, {{173}} },
  { Plt::MStele,          {{  94, "Card_art_XW_P_94.jpg"                 }}, {{"maarekstele"                                  }}, {{174}} },
  { Plt::VFoslo,          {{  95, "Card_art_XW_P_95.jpg"                 }}, {{"vedfoslo"                                     }}, {{175}} },
  { Plt::ZStrom,          {{  96, "Card_art_XW_P_96.jpg"                 }}, {{"zertikstrom"                                  }}, {{176}} },
  { Plt::StormSqAce,      {{  97, "Card_art_XW_P_97.jpg"                 }}, {{"stormsquadronace"                             }}, {{177}} },
  { Plt::TempestSqPlt,    {{  98, "Card_art_XW_P_98.jpg"                 }}, {{"tempestsquadronpilot"                         }}, {{178}} },

  { Plt::LtKestal,        {{ 127, "Card_art_XW_P_127.jpg"                }}, {{"lieutenantkestal"                             }}, {{187}} },
  { Plt::OnyxSqScout,     {{ 129, "Card_art_XW_P_129.jpg"                }}, {{"onyxsquadronscout"                            }}, {{189}} },
  { Plt::DblEdge,         {{ 128, "Card_art_XW_P_128.jpg"                }}, {{"doubleedge"                                   }}, {{188}} },
  { Plt::SienSpecialist,  {{ 130, "Card_art_XW_P_130.jpg"                }}, {{"sienarspecialist"                             }}, {{190}} },

  { Plt::MajVonreg,       {{ 624, "4d8fdab1a5317159fcd56f722b6d73e5.jpg" }}, {{"majorvonreg"                                  }}, {{381}} },
  { Plt::Holo,            {{ 625, "61d81abede1ce312b0de7d16623c2e60.jpg" }}, {{"holo"                                         }}, {{382}} },
  { Plt::Ember,           {{ 626, "47fd0abc0fa4cea1508a800e75d18ebe.jpg" }}, {{"ember"                                        }}, {{383}} },
  { Plt::FOProvocateur,   {{ 627, "947aa948233f6a5fe5833dcf45cf8f01.jpg" }}, {{"firstorderprovocateur"                        }}, {{384}} },

  { Plt::DVader_d,        {{ 886, "9c2cfc58582671a7073c6b5339ed3004.jpg" }}, {{"darthvader-tieddefender"                      }}, {{469}} },
  { Plt::RBrath,          {{ 122, "Card_art_XW_P_122.jpg"                }}, {{"rexlerbrath"                                  }}, {{196}} },
  { Plt::VSkerris_d,      {{ 887, "b515e5eda1c2df774f598afc4091af09.jpg" }}, {{"vultskerris"                                  }}, {{454}} },
  { Plt::ColVessery,      {{ 123, "Card_art_XW_P_123.jpg"                }}, {{"colonelvessery"                               }}, {{194}} },
  { Plt::CountessRyad,    {{ 124, "Card_art_XW_P_124.jpg"                }}, {{"countessryad"                                 }}, {{195}} },
  { Plt::OnyxSqAce,       {{ 125, "Card_art_XW_P_125.jpg"                }}, {{"onyxsquadronace"                              }}, {{197}} },
  { Plt::CaptDobbs,       {{ 888, "1dde041cd0f46fb1ca2afe15264a3ad4.jpg" }}, {{"captaindobbs"                                 }}, {{470}} },
  { Plt::DeltaSqPlt,      {{ 126, "Card_art_XW_P_126.jpg"                }}, {{"deltasquadronpilot"                           }}, {{198}} },

  { Plt::Howlrunner,      {{  81, "Card_art_XW_P_81.jpg"                 }}, {{"howlrunner"                                   }}, {{217}} },
  { Plt::MMithel,         {{  80, "Card_art_XW_P_80.jpg"                 }}, {{"maulermithel"                                 }}, {{219}} },
  { Plt::SSkutu,          {{  82, "Card_art_XW_P_82.jpg"                 }}, {{"scourgeskutu"                                 }}, {{220}} },
  { Plt::DMeeko,          {{  85, "Card_art_XW_P_85.jpg"                 }}, {{"delmeeko"                                     }}, {{222}} },
  { Plt::GHask_TIEln,     {{  84, "Card_art_XW_P_84.jpg"                 }}, {{"gideonhask"                                   }}, {{223}} },
  { Plt::IVersio,         {{  83, "Card_art_XW_P_83.jpg"                 }}, {{"idenversio"                                   }}, {{218}} },
  { Plt::SMarana,         {{  86, "Card_art_XW_P_86.jpg"                 }}, {{"seynmarana"                                   }}, {{224}} },
  { Plt::BlackSqAce_Imp,  {{  90, "Card_art_XW_P_90.jpg"                 }}, {{"blacksquadronace"                             }}, {{227}} },
  { Plt::VRudor,          {{  87, "Card_art_XW_P_87.jpg"                 }}, {{"valenrudor"                                   }}, {{225}} },
  { Plt::NightBeast,      {{  88, "Card_art_XW_P_88.jpg"                 }}, {{"nightbeast"                                   }}, {{226}} },
  { Plt::ObsidianSqPlt,   {{  91, "Card_art_XW_P_91.jpg"                 }}, {{"obsidiansquadronpilot"                        }}, {{228}} },
  { Plt::AcademyPlt,      {{  92, "Card_art_XW_P_92.jpg"                 }}, {{"academypilot"                                 }}, {{229}} },
  { Plt::Wampa,           {{  89, "Card_art_XW_P_89.jpg"                 }}, {{"wampa"                                        }}, {{221}} },

  { Plt::EBridger_TF,     {{  46, "Card_art_XW_P_46.jpg"                 }}, {{"ezrabridger-tielnfighter"                     }}, {{ 14}} },
  { Plt::SWren_TF,        {{  47, "Card_art_XW_P_47.jpg"                 }}, {{"sabinewren-tielnfighter"                      }}, {{ 13}} },
  { Plt::ZOrrelios_TF,    {{  49, "Card_art_XW_P_49.jpg"                 }}, {{"zeborrelios-tielnfighter"                     }}, {{ 15}} },
  { Plt::CaptRex,         {{  48, "Card_art_XW_P_48.jpg"                 }}, {{"captainrex"                                   }}, {{ 16}} },

  { Plt::Midnight,        {{ 397, "c6a43d25d22d4112dd7e968cab4eb3d5.jpg" }}, {{"midnight"                                     }}, {{233}} },
  { Plt::CmdrMalarus,     {{ 452, "d9cfa6aacc29d55a47aaa0d9f75d362e.jpg" }}, {{"commandermalarus"                             }}, {{266}} },
  { Plt::Scorch_FO,       {{ 398, "dd225b2dab46b921e622dca6d799591f.jpg" }}, {{"scorch"                                       }}, {{262}} },
  { Plt::Static,          {{ 399, "30e0837d4877df289c220ea0ae174078.jpg" }}, {{"static"                                       }}, {{264}} },
  { Plt::Longshot,        {{ 400, "31ad38b2fc0d39f37ad82e1c70f62135.jpg" }}, {{"longshot"                                     }}, {{263}} },
  { Plt::OmegaSqAce,      {{ 403, "4e5010f7b60902288dac36bf646dcde9.jpg" }}, {{"omegasquadronace"                             }}, {{267}} },
  { Plt::Muse,            {{ 401, "d3f8b9baf0bbd8a7d2b785a616dacbcf.jpg" }}, {{"muse"                                         }}, {{235}} },
  { Plt::TN3465,          {{ 453, "9fce0e75539a225e9ff1536e466c3c13.jpg" }}, {{"tn3465"                                       }}, {{261}} },
  { Plt::ZetaSqPlt,       {{ 404, "d20d6cc2a052afc783d535c802874d23.jpg" }}, {{"zetasquadronpilot"                            }}, {{268}} },
  { Plt::EpsilonSqCadet,  {{ 405, "610cb198e4cda75aab0207841c6e4a87.jpg" }}, {{"epsilonsquadroncadet"                         }}, {{269}} },
  { Plt::LtRivas,         {{ 454, "66f969d008fc995bd940bf1ab647109f.jpg" }}, {{"lieutenantrivas"                              }}, {{265}} },
  { Plt::Null,            {{ 402, "eb788d9622d096c6d96c96cf20dc1939.jpg" }}, {{"null"                                         }}, {{290}} },

  { Plt::CRee,            {{ 889, "1e8066a3e41db4df36bbb155feac4c20.jpg" }}, {{"cienaree"                                     }}, {{455}} },
  { Plt::SFel,            {{ 103, "Card_art_XW_P_103.jpg"                }}, {{"soontirfel"                                   }}, {{179}} },
  { Plt::VSkerris_TIEin,  {{ 890, "528647fa86b7a42c3e48992185e422b5.jpg" }}, {{"vultskerris-tieininterceptor"                 }}, {{468}} },
  { Plt::CmdtGoran,       {{ 891, "cf15fb0b36550e1066381dbd38eba259.jpg" }}, {{"commandantgoran"                              }}, {{465}} },
  { Plt::GHask_TIEin,     {{ 892, "f97eca9b805c2e0f8bb833ffce44b033.jpg" }}, {{"gideonhask-tieininterceptor"                  }}, {{456}} },
  { Plt::SaberSqAce,      {{ 105, "Card_art_XW_P_105.jpg"                }}, {{"sabersquadronace"                             }}, {{181}} },
  { Plt::TPhennir,        {{ 104, "Card_art_XW_P_104.jpg"                }}, {{"turrphennir"                                  }}, {{180}} },
  { Plt::LtLorrir,        {{ 893, "0dc2601a5cdde23815e1d0fc54e7817b.jpg" }}, {{"lieutenantlorrir"                             }}, {{467}} },
  { Plt::NWindrider,      {{ 894, "1470ad9f77f0c8ac14d9fa291895ec9a.jpg" }}, {{"nashwindrider"                                }}, {{466}} },
  { Plt::AlphaSqPlt,      {{ 106, "Card_art_XW_P_106.jpg"                }}, {{"alphasquadronpilot"                           }}, {{182}} },

  { Plt::Whisper,         {{ 131, "Card_art_XW_P_131.jpg"                }}, {{"whisper"                                      }}, {{199}} },
  { Plt::Echo,            {{ 132, "Card_art_XW_P_132.jpg"                }}, {{"echo"                                         }}, {{200}} },
  { Plt::SigmaSqAce,      {{ 133, "Card_art_XW_P_133.jpg"                }}, {{"sigmasquadronace"                             }}, {{201}} },
  { Plt::ImdaarTestPlt,   {{ 134, "Card_art_XW_P_134.jpg"                }}, {{"imdaartestpilot"                              }}, {{202}} },

  { Plt::Redline,         {{ 139, "Card_art_XW_P_139.jpg"                }}, {{"redline"                                      }}, {{191}} },
  { Plt::Deathrain,       {{ 140, "Card_art_XW_P_140.jpg"                }}, {{"deathrain"                                    }}, {{192}} },
  { Plt::CutlassSqPlt,    {{ 141, "Card_art_XW_P_141.jpg"                }}, {{"cutlasssquadronpilot"                         }}, {{193}} },

  { Plt::Rampage,         {{ 693, "af083fe92dc1b51c2602dad42301d033.jpg" }}, {{"rampage"                                      }}, {{419}} },
  { Plt::LDree,           {{ 694, "d8bce1a532475fe05a10f9d219a67969.jpg" }}, {{"lyttandree"                                   }}, {{418}} },
  { Plt::OnyxSqSentry,    {{ 695, "9431b36aeec89d1dbf988375a3ce8a5b.jpg" }}, {{"onyxsquadronsentry"                           }}, {{417}} },
  { Plt::CaridaAcaCadet,  {{ 696, "89c60dc2a00750df2606c5b6e24f60c3.jpg" }}, {{"caridaacademycadet"                           }}, {{416}} },

  { Plt::MajVermeil,      {{ 113, "Card_art_XW_P_113.jpg"                }}, {{"majorvermeil"                                 }}, {{183}} },
  { Plt::CaptFeroph,      {{ 114, "Card_art_XW_P_114.jpg"                }}, {{"captainferoph"                                }}, {{184}} },
  { Plt::Vizier,          {{ 115, "Card_art_XW_P_115.jpg"                }}, {{"vizier"                                       }}, {{185}} },
  { Plt::ScarifBsPlt,     {{ 116, "Card_art_XW_P_116.jpg"                }}, {{"scarifbasepilot"                              }}, {{186}} },

  { Plt::TBren,           {{ 107, "Card_art_XW_P_107.jpg"                }}, {{"tomaxbren"                                    }}, {{205}} },
  { Plt::CaptJonus,       {{ 108, "Card_art_XW_P_108.jpg"                }}, {{"captainjonus"                                 }}, {{203}} },
  { Plt::MajRhymer,       {{ 109, "Card_art_XW_P_109.jpg"                }}, {{"majorrhymer"                                  }}, {{204}} },
  { Plt::GammaSqAce,      {{ 111, "Card_art_XW_P_111.jpg"                }}, {{"gammasquadronace"                             }}, {{207}} },
  { Plt::Deathfire,       {{ 110, "Card_art_XW_P_110.jpg"                }}, {{"deathfire"                                    }}, {{206}} },
  { Plt::ScimitarSqPlt,   {{ 112, "Card_art_XW_P_112.jpg"                }}, {{"scimitarsquadronpilot"                        }}, {{208}} },

  { Plt::Breach,          {                                               }, {{"breach"                                       }}, {{493}} },
  { Plt::Scorch_SE,       {                                               }, {{"scorch-tiesebomber"                           }}, {{494}} },
  { Plt::Dread,           {                                               }, {{"dread"                                        }}, {{495}} },
  { Plt::FOCadet,         {                                               }, {{"firstordercadet"                              }}, {{497}} },
  { Plt::Grudge,          {                                               }, {{"grudge"                                       }}, {{496}} },
  { Plt::SJTestPlt,       {                                               }, {{"sienarjaemustestpilot"                        }}, {{498}} },

  { Plt::Quickdraw,       {{ 406, "86f8ccc959081a43dc4d0dbeb921d0ba.jpg" }}, {{"quickdraw"                                    }}, {{243}} },
  { Plt::LtLeHuse,        {{ 641, "e359738e224a6b5509fe19ee0cbf253c.jpg" }}, {{"lieutenantlehuse"                             }}, {{397}} },
  { Plt::Backdraft,       {{ 407, "6c3a06877712596601ba1cc4ec533626.jpg" }}, {{"backdraft"                                    }}, {{242}} },
  { Plt::CaptPhasma,      {{ 642, "c771d32332787ea1c723a78abb463cb4.jpg" }}, {{"captainphasma"                                }}, {{385}} },
  { Plt::OmegaSqExpert,   {{ 408, "2caf1312bd6aba3630ef6edc1ff81f53.jpg" }}, {{"omegasquadronexpert"                          }}, {{285}} },
  { Plt::ZetaSqSurvivor,  {{ 409, "55f72ce4974962db5b2ab3fac316b896.jpg" }}, {{"zetasquadronsurvivor"                         }}, {{259}} },

  { Plt::Blackout,        {{ 415, "20308b5887fb20d6b8ecdb3ede0bede3.jpg" }}, {{"blackout"                                     }}, {{237}} },
  { Plt::KRen_VN,         {{ 414, "80b87be22656fc01742fca490193d440.jpg" }}, {{"kyloren"                                      }}, {{236}} },
  { Plt::FOTestPlt,       {{ 416, "86349c032fc169cb2000d3db7c9fbef4.jpg" }}, {{"firstordertestpilot"                          }}, {{287}} },
  { Plt::Recoil,          {{ 455, "c4590088696ecc687f5c0f004d1d97ab.jpg" }}, {{"recoil"                                       }}, {{284}} },
  { Plt::Avenger,         {{ 456, "3778411ec66e33951231314e909b981d.jpg" }}, {{"avenger"                                      }}, {{283}} },
  { Plt::Rush,            {{ 643, "3b748daed26df2870694ab9dc679c5ee.jpg" }}, {{"rush"                                         }}, {{386}} },
  { Plt::SJEngineer,      {{ 417, "18b2a1b00b2f8c6669b6b1d1b278dcf2.jpg" }}, {{"sienarjaemusengineer"                         }}, {{286}} },

  { Plt::KRen_WI,         {                                               }, {{"kyloren-tiewiwhispermodifiedinterceptor"      }}, {{487}} },
  { Plt::Wrath,           {                                               }, {{"wrath"                                        }}, {{488}} },
  { Plt::Nightfall,       {                                               }, {{"nightfall"                                    }}, {{489}} },
  { Plt::SevOhNineLegAce, {                                               }, {{"709thlegionace"                               }}, {{491}} },
  { Plt::Whirlwind,       {                                               }, {{"whirlwind"                                    }}, {{490}} },
  { Plt::RedFuryZealot,   {                                               }, {{"redfuryzealot"                                }}, {{492}} },

  { Plt::Duchess,         {{ 117, "Card_art_XW_P_117.jpg"                }}, {{"duchess"                                      }}, {{211}} },
  { Plt::Countdown,       {{ 118, "Card_art_XW_P_118.jpg"                }}, {{"countdown"                                    }}, {{209}} },
  { Plt::PureSabacc,      {{ 119, "Card_art_XW_P_119.jpg"                }}, {{"puresabacc"                                   }}, {{210}} },
  { Plt::BlackSqScout,    {{ 120, "Card_art_XW_P_120.jpg"                }}, {{"blacksquadronscout"                           }}, {{212}} },
  { Plt::Vagabond,        {{ 633, "308e253711036e143e22857365bcb7ca.jpg" }}, {{"vagabond"                                     }}, {{395}} },
  { Plt::PlanetarySent,   {{ 121, "Card_art_XW_P_121.jpg"                }}, {{"planetarysentinel"                            }}, {{213}} },

  { Plt::LawlessPirates,  {{ 949, "738e8fdd6aaf28620b67efda2e8f63f7.jpg" }}, {{"lawlesspirates"                               }}, {{476}} },
  { Plt::ColicoidDest,    {{ 950, "84a0e86cd474679365d38ead7d6f19f0.jpg" }}, {{"colicoiddestroyer"                            }}, {{475}} },

  { Plt::CaptCardinal,    {{ 457, "a88822cc408323e561efd9e2acb83f2a.jpg" }}, {{"captaincardinal"                              }}, {{282}} },
  { Plt::MajStridan,      {{ 410, "862f9dcc1ce9809f9a394d8f0b05f268.jpg" }}, {{"majorstridan"                                 }}, {{251}} },
  { Plt::LtTavson,        {{ 411, "151154b50732a6dd42b411fc312137b9.jpg" }}, {{"lieutenanttavson"                             }}, {{289}} },
  { Plt::LtDormitz,       {{ 412, "4505cb1930309673fe5592dbc112d733.jpg" }}, {{"lieutenantdormitz"                            }}, {{238}} },
  { Plt::StarkillerBsPlt, {{ 413, "1ea0266ea42691778e8ecff6a5b50e45.jpg" }}, {{"starkillerbasepilot"                          }}, {{288}} },
  { Plt::PetOffThanisson, {{ 458, "24a40f6ec1875bfae9e7531b02252993.jpg" }}, {{"pettyofficerthanisson"                        }}, {{281}} },

  { Plt::BRook,           {{  54, "Card_art_XW_P_54.jpg"                 }}, {{"bodhirook"                                    }}, {{ 31}} },
  { Plt::SGerrera,        {{  55, "Card_art_XW_P_55.jpg"                 }}, {{"sawgerrera"                                   }}, {{ 35}} },
  { Plt::CAndor,          {{  56, "Card_art_XW_P_56.jpg"                 }}, {{"cassianandor"                                 }}, {{ 32}} },
  { Plt::K2SO,            {{ 629, "eeb6e9aeefe6f988949aff17e7e7986d.jpg" }}, {{"k2so"                                         }}, {{390}} },
  { Plt::MYarro,          {{  57, "Card_art_XW_P_57.jpg"                 }}, {{"magvayarro"                                   }}, {{ 34}} },
  { Plt::BTwoTubes,       {{  58, "Card_art_XW_P_58.jpg"                 }}, {{"benthictwotubes"                              }}, {{ 36}} },
  { Plt::BlueSqScout,     {{  60, "Card_art_XW_P_60.jpg"                 }}, {{"bluesquadronscout"                            }}, {{ 37}} },
  { Plt::HTobber,         {{  59, "Card_art_XW_P_59.jpg"                 }}, {{"hefftobber"                                   }}, {{ 33}} },
  { Plt::PartRenegade,    {{  61, "Card_art_XW_P_61.jpg"                 }}, {{"partisanrenegade"                             }}, {{ 38}} },

  { Plt::OddBall_V19,     {{ 516, "2a20c8f88cf55b75df4909f74b0221a2.jpg" }}, {{"oddball"                                      }}, {{316}} },
  { Plt::Kickback,        {{ 517, "50b08d380769e7127b0d3f5dbbd89d1f.jpg" }}, {{"kickback"                                     }}, {{315}} },
  { Plt::Axe,             {{ 518, "3129e7fc822115a91a04004c35ceda18.jpg" }}, {{"axe"                                          }}, {{318}} },
  { Plt::BlueSqProtector, {{ 521, "74667d2878cb823d14035e91c8773816.jpg" }}, {{"bluesquadronprotector"                        }}, {{320}} },
  { Plt::Swoop,           {{ 519, "c8c96604a64fce6b953a8b4dc4ff86e6.jpg" }}, {{"swoop"                                        }}, {{317}} },
  { Plt::GoldSqTrooper,   {{ 522, "e4ba0b540259e9026142e6fa5e837685.jpg" }}, {{"goldsquadrontrooper"                          }}, {{321}} },
  { Plt::Tucker,          {{ 520, "0b24c3261656d83b685ac59e6dfb1817.jpg" }}, {{"tucker"                                       }}, {{319}} },

  { Plt::HSyndulla_VCX,   {{  73, "Card_art_XW_P_73.jpg"                 }}, {{"herasyndulla-vcx100lightfreighter"            }}, {{ 61}} },
  { Plt::AKallus,         {{ 631, "af4c16c8955bddf75d5eb7723c7a5f2d.jpg" }}, {{"alexsandrkallus"                              }}, {{392}} },
  { Plt::KJarrus_VCX,     {{  74, "Card_art_XW_P_74.jpg"                 }}, {{"kananjarrus"                                  }}, {{ 62}} },
  { Plt::Chopper,         {{  75, "Card_art_XW_P_75.jpg"                 }}, {{"chopper"                                      }}, {{ 63}} },
  { Plt::LothalRebel,     {{  76, "Card_art_XW_P_76.jpg"                 }}, {{"lothalrebel"                                  }}, {{ 64}} },

  { Plt::RAdmChiraneau,   {{ 147, "Card_art_XW_P_147.jpg"                }}, {{"rearadmiralchiraneau"                         }}, {{214}} },
  { Plt::MKee,            {{ 634, "296bcef6b8b45cae418c80825f6eb512.jpg" }}, {{"mornakee"                                     }}, {{396}} },
  { Plt::CaptOicunn,      {{ 146, "Card_art_XW_P_146.jpg"                }}, {{"captainoicunn"                                }}, {{215}} },
  { Plt::PatrolLdr,       {{ 148, "Card_art_XW_P_148.jpg"                }}, {{"patrolleader"                                 }}, {{216}} },

  { Plt::PrecHunter,      {{ 500, "fab82de87d230b77a5e4b2360ea9af93.jpg" }}, {{"precisehunter"                                }}, {{309}} },
  { Plt::DFS081,          {{ 499, "2d797107c628ebd2ab8e526fcbb6076d.jpg" }}, {{"dfs081"                                       }}, {{311}} },
  { Plt::SepDrone,        {{ 497, "df638358903b64e7b246b3a56fa68af7.jpg" }}, {{"separatistdrone"                              }}, {{343}} },
  { Plt::DFS311,          {{ 501, "5fb72145f183eeddd8d37ce1a4d114f7.jpg" }}, {{"dfs311"                                       }}, {{337}} },
  { Plt::HaorChallProto,  {{ 502, "18cdc19a9627a38d0ec2783ccd5cb183.jpg" }}, {{"haorchallprototype"                           }}, {{310}} },
  { Plt::TradeFedDrone,   {{ 498, "df075613c4c3dbe7803df6bfa5082262.jpg" }}, {{"tradefederationdrone"                         }}, {{279}} },

  { Plt::CmdrMalarus_Xi,  {{ 656, "9fc217ecda8e05095b2e4d5957a474ba.jpg" }}, {{"commandermalarus-xiclasslightshuttle"         }}, {{404}} },
  { Plt::GHask_Xi,        {{ 657, "f0cc7ebb6dc5f2e0d15fd681655777b3.jpg" }}, {{"gideonhask-xiclasslightshuttle"               }}, {{403}} },
  { Plt::AgentTerex,      {{ 658, "ee4ceab2fd34a3ccd68c74652f21b782.jpg" }}, {{"agentterex"                                   }}, {{402}} },
  { Plt::FOCourier,       {{ 659, "85aadb1aa3100fe7e92296ca0465f58b.jpg" }}, {{"firstordercourier"                            }}, {{401}} },

  { Plt::DRendar,         {{  77, "Card_art_XW_P_77.jpg"                 }}, {{"dashrendar"                                   }}, {{ 39}} },
  { Plt::Leebo,           {{  78, "Card_art_XW_P_78.jpg"                 }}, {{"leebo"                                        }}, {{ 40}} },
  { Plt::WildSpaceFrin,   {{  79, "Card_art_XW_P_79.jpg"                 }}, {{"wildspacefringer"                             }}, {{ 41}} },

  { Plt::Bossk_YV,        {{ 210, "Card_art_XW_P_210.jpg"                }}, {{"bossk"                                        }}, {{153}} },
  { Plt::MEval,           {{ 211, "Card_art_XW_P_211.jpg"                }}, {{"moraloeval"                                   }}, {{152}} },
  { Plt::LRazzi,          {{ 212, "Card_art_XW_P_212.jpg"                }}, {{"lattsrazzi"                                   }}, {{154}} },
  { Plt::TSlaver,         {{ 213, "Card_art_XW_P_213.jpg"                }}, {{"trandoshanslaver"                             }}, {{155}} },

  { Plt::ACracken,        {{  27, "Card_art_XW_P_27.jpg"                 }}, {{"airencracken"                                 }}, {{ 54}} },
  { Plt::LtBlount,        {{  28, "Card_art_XW_P_28.jpg"                 }}, {{"lieutenantblount"                             }}, {{ 55}} },
  { Plt::TalaSqPlt,       {{  29, "Card_art_XW_P_29.jpg"                 }}, {{"talasquadronpilot"                            }}, {{ 56}} },
  { Plt::BanditSqPlt,     {{  30, "Card_art_XW_P_30.jpg"                 }}, {{"banditsquadronpilot"                          }}, {{ 57}} },

  { Plt::Bossk_Z95,       {{ 635, "48cf03ca0e16baf2f506829f20517b83.jpg" }}, {{"bossk-z95af4headhunter"                       }}, {{398}} },
  { Plt::NSuhlak,         {{ 169, "Card_art_XW_P_169.jpg"                }}, {{"ndrusuhlak"                                   }}, {{156}} },
  { Plt::BSSoldier,       {{ 172, "Card_art_XW_P_172.jpg"                }}, {{"blacksunsoldier"                              }}, {{158}} },
  { Plt::KLeeachos,       {{ 170, "Card_art_XW_P_170.jpg"                }}, {{"kaatoleeachos"                                }}, {{157}} },
  { Plt::BinayrePirate,   {{ 173, "Card_art_XW_P_173.jpg"                }}, {{"binayrepirate"                                }}, {{159}} },
  { Plt::NashtahPup,      {{ 171, "Card_art_XW_P_171.jpg"                }}, {{"nashtahpup"                                   }}, {{160}} },
};

// upgrade types
struct FFG_UpgradeTypeData  {};
struct XWS_UpgradeTypeData  { std::string id; };
struct YASB_UpgradeTypeData {};
struct UpgradeTypeEntry {
  UpT upt;
  std::optional<FFG_UpgradeTypeData>  ffg;
  std::optional<XWS_UpgradeTypeData>  xws;
  std::optional<YASB_UpgradeTypeData> yasb;
};
static std::vector<UpgradeTypeEntry> upgradeTypeData = {
  { UpT::Astro,     {}, {{"Astromech"}},      {} },
  { UpT::Cannon,    {}, {{"Cannon"}},         {} },
  { UpT::Cargo,     {}, {{"Cargo"}},          {} },
  { UpT::Command,   {}, {{"Command"}},        {} },
  { UpT::Config,    {}, {{"Configuration"}},  {} },
  { UpT::Crew,      {}, {{"Crew"}},           {} },
  { UpT::Force,     {}, {{"Force Power"}},    {} },
  { UpT::Gunner,    {}, {{"Gunner"}},         {} },
  { UpT::Hardpoint, {}, {{"Hardpoint"}},      {} },
  { UpT::Hyperdr,   {}, {{"Hyperdrive"}},     {} },
  { UpT::Illicit,   {}, {{"Illicit"}},        {} },
  { UpT::Missile,   {}, {{"Missile"}},        {} },
  { UpT::Mod,       {}, {{"Modification"}},   {} },
  { UpT::Payload,   {}, {{"Device"}},         {} },
  { UpT::Sensor,    {}, {{"Sensor"}},         {} },
  { UpT::TactRel,   {}, {{"Tactical Relay"}}, {} },
  { UpT::Talent,    {}, {{"Talent"}},         {} },
  { UpT::Team,      {}, {{"Team"}},           {} },
  { UpT::Tech,      {}, {{"Tech"}},           {} },
  { UpT::Title,     {}, {{"Title"}},          {} },
  { UpT::Torpedo,   {}, {{"Torpedo"}},        {} },
  { UpT::Turret,    {}, {{"Turret"}},         {} },
};

// upgrades
struct FFG_UpgradeData  { int16_t id; std::string art; };
struct XWS_UpgradeData  { std::string id; };
struct YASB_UpgradeData { int16_t id; };
struct UpgradeEntry {
  Upg upg;
  std::optional<std::vector<FFG_UpgradeData>>  ffg;
  std::optional<XWS_UpgradeData>  xws;
  std::optional<YASB_UpgradeData> yasb;
};
static std::vector<UpgradeEntry> upgradeData = {
//  --libxwing2---------  --ffg----------------------------------------------------------------------------------------  --xwing-data2----------------------  --yasb-
//                          --S1---------------------------------------- --S2-----------------------------------------
//  id                       id   artwork                                 id  artwork                                      xws                                  id
  { Upg::BB8,             {{{479,"e8a75e0e143a5857ac3931d56ccde86c.jpg"}                                             }}, {{"bb8"                          }}, {{195}} },
  { Upg::BBAstro,         {{{480,"a2e3aaf77e8690a37e76ef4ae2087180.jpg"}                                             }}, {{"bbastromech"                  }}, {{196}} },
  { Upg::C110P,           {{{618,"815bfa9be6941de313eb1e318a02b2ae.jpg"},{617,"c480434cf330e99e269eaf0fe83e2444.jpg"}}}, {{"c110p"                        }}, {{252}} },
  { Upg::ChopperA,        {{{323,"Card_art_XW_U_99.jpg"                }                                             }}, {{"chopper"                      }}, {{  0}} },
  { Upg::Genius,          {{{368,"Card_art_XW_U_143.jpg"               }                                             }}, {{"genius"                       }}, {{  1}} },
  { Upg::L4ER5,           {                                                                                           }, {{"l4er5"                        }}, {{408}} },
  { Upg::M9G8,            {{{481,"f810d46699343a134003deea0f423131.jpg"}                                             }}, {{"m9g8"                         }}, {{197}} },
  { Upg::Q7Astro,         {{{863,"36187a497f1d8fcd81e0209c279c2c2f.jpg"}                                             }}, {{"q7astromech"                  }}, {{365}} },
  { Upg::R2Astro,         {{{282,"Card_art_XW_U_53.jpg"                }                                             }}, {{"r2astromech"                  }}, {{  2}} },
  { Upg::R1J5,            {{{644,"2f64dab83e6e4c52702d5906a18dbae6.jpg"}                                             }}, {{"r1j5"                         }}, {{312}} },
  { Upg::R2A6,            {{{588,"fe5da38a69cf0f5212d5cb06ea9053de.jpg"}                                             }}, {{"r2a6"                         }}, {{241}} },
  { Upg::R2C4,            {{{589,"db2012557dc9b2953aeeb8c3f98d5052.jpg"}                                             }}, {{"r2c4"                         }}, {{233}} },
  { Upg::R2D2,            {{{324,"Card_art_XW_U_100.jpg"               }                                             }}, {{"r2d2"                         }}, {{  3}} },
  { Upg::R2D2_A_Rep,      {{{860,"cc1695a002e056cd11643b166658169e.jpg"}                                             }}, {{"r2d2-republic"                }}, {{340}} },
  { Upg::R2D2_A_Res,      {{{720,"f22fbeaa2ade3bd405388482bd747c27.jpg"}                                             }}, {{"r2d2-resistance"              }}, {{358}} },
  { Upg::R2HA,            {{{482,"d72ab7fa7cd398d614466a98076a2e6b.jpg"}                                             }}, {{"r2ha"                         }}, {{190}} },
  { Upg::R3Astro,         {{{283,"Card_art_XW_U_54.jpg"                }                                             }}, {{"r3astromech"                  }}, {{  4}} },
  { Upg::R4Astro,         {{{284,"Card_art_XW_U_55.jpg"                }                                             }}, {{"r4astromech"                  }}, {{  5}} },
  { Upg::R4B11,           {{{920,"5f2c2265fbbcf55a749acc6e224f8946.jpg"}                                             }}, {{"r4b11"                        }}, {{389}} },
  { Upg::R4PAstro,        {{{546,"081dbeb7591c8dfd1f752729b27bdf2a.jpg"}                                             }}, {{"r4pastromech"                 }}, {{213}} },
  { Upg::R4P17,           {{{547,"7b3b50078bef620079c43a1689bb005a.jpg"}                                             }}, {{"r4p17"                        }}, {{214}} },
  { Upg::R4P44,           {{{551,"313d08463a9c2e7c5d9377a39277ef03.jpg"}                                             }}, {{"r4p44"                        }}, {{226}} },
  { Upg::R5Astro,         {{{285,"Card_art_XW_U_56.jpg"                }                                             }}, {{"r5astromech"                  }}, {{  6}} },
  { Upg::R5D8,            {{{325,"Card_art_XW_U_101.jpg"               }                                             }}, {{"r5d8"                         }}, {{  7}} },
  { Upg::R5P8,            {{{369,"Card_art_XW_U_144.jpg"               }                                             }}, {{"r5p8"                         }}, {{  8}} },
  { Upg::R5TK,            {{{370,"Card_art_XW_U_145.jpg"               }                                             }}, {{"r5tk"                         }}, {{  9}} },
  { Upg::R5X3,            {{{483,"7361f88154703f61221bc2a775f4a9b6.jpg"}                                             }}, {{"r5x3"                         }}, {{205}} },
  { Upg::R6D8,            {{{719,"7155fb226f88c98baf36c5b3fa1e2ef6.jpg"}                                             }}, {{"r6d8"                         }}, {{359}} },
  { Upg::R7A7,            {{{862,"21a23aa5bad45f33650cc2f7a3b3ecce.jpg"}                                             }}, {{"r7a7"                         }}, {{364}} },
  { Upg::WatchfulAstro,   {                                                                                           }, {{"watchfulastromech"            }}, {{410}} },

  { Upg::Autoblasters,    {{{578,"dbdad938bd9f4ce64af1d7106dfd5b5e.jpg"}                                             }}, {{"autoblasters"                 }}, {{232}} },
  { Upg::HLC,             {{{256,"Card_art_XW_U_27.jpg"                }                                             }}, {{"heavylasercannon"             }}, {{ 10}} },
  { Upg::IonCan,          {{{257,"Card_art_XW_U_28.jpg"                }                                             }}, {{"ioncannon"                    }}, {{ 11}} },
  { Upg::JamBeam,         {{{258,"Card_art_XW_U_29.jpg"                }                                             }}, {{"jammingbeam"                  }}, {{ 12}} },
  { Upg::SyncLasCan,      {{{674,"70d44704aaa8bb8d1f1e2bc904b18c77.jpg"}                                             }}, {{"syncedlasercannons"           }}, {{354}} },
  { Upg::TracBeam,        {{{259,"Card_art_XW_U_30.jpg"                }                                             }}, {{"tractorbeam"                  }}, {{ 13}} },
  { Upg::Underslung,      {{{718,"734d68b47d976624629d101299719222.jpg"}                                             }}, {{"underslungblastercannon"      }}, {{360}} },

  { Upg::AdaptiveShlds,   {{{756,"c7816ed5fed0e34f4fc992b34e7779d4.jpg"}                                             }}, {{"adaptiveshields"              }}, {{282}} },
  { Upg::BoostedScanners, {{{757,"c712981e1437a13ac6f0de95d4a46cb9.jpg"}                                             }}, {{"boostedscanners"              }}, {{283}} },
  { Upg::OptPowerCore,    {{{758,"cd071c9b13ca8da6c061c5cbf32e0cf6.jpg"}                                             }}, {{"optimizedpowercore"           }}, {{286}} },
  { Upg::TibannaRes,      {{{759,"48877ef89bde008d353387a89ccf2478.jpg"}                                             }}, {{"tibannareserves"              }}, {{285}} },

  { Upg::AgentotEmp,      {{{732,"f7021137f14e5ee530cd1cc9e5b9264a.jpg"}                                             }}, {{"agentoftheempire"             }}, {{257}} },
  { Upg::B6Prototype_CMD, {{{903,"71fe49e6d10dc74321a0a96f0b2f64e6.jpg"}                                             }}, {{"b6bladewingprototype-command" }}, {{377}} },
  { Upg::Bounty,          {{{904,"6877dc90b8296a93c3fc9129d5087020.jpg"},{904,"4502e115fb501138e161b28b0d3f0474.jpg"}}}, {{"bounty"                       }}, {{385}} },
  { Upg::DreadHunter,     {{{735,"077db882ed94ceec1150c405bd689c33.jpg"}                                             }}, {{"dreadnoughthunter"            }}, {{260}} },
  { Upg::FOElite,         {{{733,"aa85719e74f86fec8b7232cf901f9f29.jpg"}                                             }}, {{"firstorderelite"              }}, {{258}} },
  { Upg::InIt,            {{{906,"26e935cc224ffd02de5e797faa5685c6.jpg"},{906,"977af5c5c1604a9e45cd040408f5e482.jpg"}}}, {{"initforthemoneyrebellion"     }}, {{384}} },
  { Upg::JediCmdr,        {{{835,"04b5550c3226ede7611fcf46a0c6253f.jpg"}                                             }}, {{"jedicommander"                }}, {{361}} },
  { Upg::PhoenixSq,       {{{908,"554a0d6690a6a3c4a175b1f363084670.jpg"},{908,"554a0d6690a6a3c4a175b1f363084670.jpg"}}}, {{"phoenixsquadron"              }}, {{380}} },
  { Upg::ShadowWing,      {{{910,"8842d1539a912e7ae3b011bfdb4bbe0a.jpg"},{910,"8842d1539a912e7ae3b011bfdb4bbe0a.jpg"}}}, {{"shadowwing"                   }}, {{383}} },
  { Upg::SkystrikeAcaCla, {{{912,"8b24e79586085204391faa4d4b3d1f4c.jpg"},{912,"8b24e79586085204391faa4d4b3d1f4c.jpg"}}}, {{"skystrikeacademyclass"        }}, {{382}} },
  { Upg::VetWingLdr,      {{{734,"d40521b56b50feb76368a2233b4da4d9.jpg"}                                             }}, {{"veteranwingleader"            }}, {{259}} },

  { Upg::AdmOzzel,        {{{724,"f5f82fcbc211ffa39f9a213fac643a76.jpg"}                                             }}, {{"admiralozzel"                 }}, {{261}} },
  { Upg::AVentress,       {{{927,"938c370244b13196796a7c3c18815154.jpg"}                                             }}, {{"asajjventress"                }}, {{390}} },
  { Upg::Azmorigan,       {{{725,"4dfc9c0a148c3aac400435771a229870.jpg"}                                             }}, {{"azmorigan"                    }}, {{262}} },
  { Upg::CaptNeeda,       {{{726,"0583ea3a0143ba8e16ced0d3338f0df1.jpg"}                                             }}, {{"captainneeda"                 }}, {{263}} },
  { Upg::CRieekan,        {{{728,"a86c9d037c976d140ae0d6095f39225a.jpg"}                                             }}, {{"carlistrieekan"               }}, {{264}} },
  { Upg::GenGrievous_CMD, {{{928,"9caa1bd8dc362966526cb361438c87e5.jpg"}                                             }}, {{"generalgrievous-command"      }}, {{391}} },
  { Upg::HOhnaka_CMD,     {{{929,"3a996d2237125c8ee70294e21dee31eb.jpg"}                                             }}, {{"hondoohnaka-command"          }}, {{392}} },
  { Upg::JDodonna,        {{{729,"fa6dba670282a19b80dc0e53dec78536.jpg"}                                             }}, {{"jandodonna"                   }}, {{265}} },
  { Upg::MTuuk,           {{{930,"ceef87aa3688afbc57092ebfc487a918.jpg"}                                             }}, {{"martuuk"                      }}, {{393}} },
  { Upg::RAntilles,       {{{730,"ac03675e91a973a4a31a9b4d380080a6.jpg"}                                             }}, {{"raymusantilles"               }}, {{266}} },
  { Upg::RTamson,         {{{931,"0ad46b4e395d4abf4504fc88891aa2ed.jpg"}                                             }}, {{"rifftamson"                   }}, {{394}} },
  { Upg::StalwartCapt,    {{{731,"e204e200410fdea80a1e7582db0dcd32.jpg"}                                             }}, {{"stalwartcaptain"              }}, {{267}} },
  { Upg::StratCmdr,       {{{727,"14ae4cbb0383462f172497c35890440e.jpg"}                                             }}, {{"strategiccommander"           }}, {{268}} },
  { Upg::ZealousCapt,     {{{932,"3402108c6b10085cb2e33a90dc15a783.jpg"}                                             }}, {{"zealouscaptain"               }}, {{395}} },

  { Upg::Alpha3B,         {{{870,"2e6d4fc53253cdf04086649b7eaf917a.jpg"}                                             }}, {{"alpha3bbesh"                  }}, {{337}} },
  { Upg::Alpha3E,         {{{871,"678c7a36aacdd1cc20b61287df2fc0d9.jpg"}                                             }}, {{"alpha3eesk"                   }}, {{363}} },
  { Upg::CalLasTgt,       {{{549,"ffe9b36272a15f7c5e2ba2fa075d27dd.jpg"}                                             }}, {{"calibratedlasertargeting"     }}, {{200}} },
  { Upg::CorsairRefit,    {{{780,"1ee2df3c1a0ce5aa60439927e2d1e9ca.jpg"}                                             }}, {{"corsairrefit"                 }}, {{308}} },
  { Upg::Delta7B,         {{{548,"6530f18639b7efff5a5a659589e5d65c.jpg"}                                             }}, {{"delta7b"                      }}, {{201}} },
  { Upg::EnJamSuite,      {                                                                                           }, {{"enhancedjammingsuite"         }}, {{413}} },
  { Upg::GrapStruts,      {{{555,"24c35aeb94ef846dc498e4a59fb9bf73.jpg"},{535,"55781f0b97bc691866379ff9e1d6d354.jpg"}}}, {{"grapplingstruts"              }}, {{208}} },
  { Upg::InterceptBoost,  {{{873,"1c125de5b479dbc58f1a0bc2ec770ed3.jpg"},{872,"e0d4a33513507acf6852de1329ad7b85.jpg"}}}, {{"interceptbooster"             }}, {{366}} },
  { Upg::IntSFoils,       {{{486,"a8233a67adce770e91e4b6b9025670ed.jpg"},{487,"bf5e7b98158570f9d4fc259b5553f4ca.jpg"}}}, {{"integratedsfoils"             }}, {{175}} },
  { Upg::LandStruts,      {{{594,"1340d2e5df785275c674d2be07379bf1.jpg"},{595,"172286420e4263a098c3a2ecb0e73ce2.jpg"}}}, {{"landingstruts"                }}, {{237}} },
  { Upg::ManAssMGK300,    {{{722,"8b7bc25a4c99fafd0c61ac6efb1129ae.jpg"}                                             }}, {{"maneuverassistmgk300"         }}, {{329}} },
  { Upg::Os1ArsenalLdt,   {{{350,"Card_art_XW_U_125.jpg"               }                                             }}, {{"os1arsenalloadout"            }}, {{139}} },
  { Upg::PvtWing,         {{{332,"Card_art_XW_U_107b.jpg"              },{331,"Card_art_XW_U_107.jpg",              }}}, {{"pivotwing"                    }}, {{140}} },
  { Upg::Repulsorlift,    {{{692,"b16d22d90b1b5048a15cf1734334d8fa.jpg"},{691,"37c67b48073cea976e4b6a6949976aee.jpg"}}}, {{"repulsorliftstabilizers"      }}, {{323}} },
  { Upg::SensitiveCtrls,  {{{924,"d6fbd4fb3e406c87af923912d0ff866c.jpg"}                                             }}, {{"sensitivecontrols"            }}, {{374}} },
  { Upg::SmSFoils,        {{{334,"Card_art_XW_U_108b.jpg"              },{333,"Card_art_XW_U_108.jpg",              }}}, {{"servomotorsfoils"             }}, {{142}} },
  { Upg::StabSFoils,      {{{646,"80efc4163ceb2000a6981b9851c3d1a9.jpg"},{645,"e6316661a866ec6d474363d135ae75d4.jpg"}}}, {{"stabilizedsfoils"             }}, {{313}} },
  { Upg::TarAssMGK300,    {{{723,"954401c2bc0cc705693553362d078536.jpg"}                                             }}, {{"targetassistmgk300"           }}, {{356}} },
  { Upg::TIEDefElite,     {{{925,"a70e6ac15fdc103d5ee2dd50aa315f9c.jpg"}                                             }}, {{"tiedefenderelite"             }}, {{373}} },
  { Upg::VectoredCanRZ1,  {{{926,"fb4cf1a8b51ab44fddee4c9dfbc280a8.jpg"}                                             }}, {{"vectoredcannonsrz1"           }}, {{371}} },
  { Upg::WartimeLoad,     {                                                                                           }, {{"wartimeloadout"               }}, {{409}} },
  { Upg::Xg1AssaultCfg,   {{{351,"Card_art_XW_U_126.jpg"               }                                             }}, {{"xg1assaultconfiguration"      }}, {{144}} },

  { Upg::TripleZero,      {{{352,"Card_art_XW_U_127.jpg"               }                                             }}, {{"000"                          }}, {{ 63}} },
  { Upg::FourLOM,         {{{353,"Card_art_XW_U_128.jpg"               }                                             }}, {{"4lom"                         }}, {{ 31}} },
  { Upg::ASecura,         {{{675,"c1d9f86978058e101f6cf112c3946655.jpg"}                                             }}, {{"aaylasecura"                  }}, {{328}} },
  { Upg::AdmSloane,       {{{335,"Card_art_XW_U_109.jpg"               }                                             }}, {{"admiralsloane"                }}, {{ 14}} },
  { Upg::AgentKallus,     {{{336,"Card_art_XW_U_110.jpg"               }                                             }}, {{"agentkallus"                  }}, {{ 15}} },
  { Upg::AgentTerex,      {{{686,"6e7153cd20c808370bbd80d871cf85c3.jpg"},{685,"87f139d19477b1ab35c2dc2ab578eedc.jpg"}}}, {{"agentterex"                   }}, {{324}} },
  { Upg::AHoldo,          {{{581,"daa4aa8732efd994938dae56b6210ede.jpg"}                                             }}, {{"amilynholdo"                  }}, {{242}} },
  { Upg::BFett,           {{{354,"Card_art_XW_U_129.jpg"               }                                             }}, {{"bobafett"                     }}, {{ 16}} },
  { Upg::BMalbus,         {{{303,"Card_art_XW_U_79.jpg"                }                                             }}, {{"bazemalbus"                   }}, {{ 17}} },
  { Upg::C3POReb,         {{{304,"Card_art_XW_U_80.jpg"                }                                             }}, {{"c3po"                         }}, {{ 18}} },
  { Upg::C3PORep,         {{{616,"f64417de71053f39ded8886b72184819.jpg"}                                             }}, {{"c3po-republic"                }}, {{254}} },
  { Upg::C3PORes,         {{{472,"1629b5a262f8b69e1dee4b841150c9b7.jpg"}                                             }}, {{"c3po-crew"                    }}, {{191}} },
  { Upg::CAndor,          {{{305,"Card_art_XW_U_81.jpg"                }                                             }}, {{"cassianandor"                 }}, {{ 19}} },
  { Upg::CBane,           {{{355,"Card_art_XW_U_130.jpg"               }                                             }}, {{"cadbane"                      }}, {{ 20}} },
  { Upg::CaptPhasma,      {{{465,"71ad5db561ea39d9d59c4bbdc1b42f35.jpg"}                                             }}, {{"captainphasma"                }}, {{183}} },
  { Upg::ChanPalp,        {{{556,"d07f69ceb063c382e60ce33377e9bef7.jpg"},{538,"e0772c182d95e3abd540950c7689ab34.jpg"}}}, {{"chancellorpalpatine"          }}, {{217}} },
  { Upg::ChewieReb,       {{{306,"Card_art_XW_U_82.jpg"                }                                             }}, {{"chewbacca"                    }}, {{ 21}} },
  { Upg::ChewieRes,       {{{473,"3d124811dd272ec2355a30903a827034.jpg"}                                             }}, {{"chewbacca-crew-swz19"         }}, {{188}} },
  { Upg::ChewieScum,      {{{382,"Card_art_XW_U_157.jpg"               }                                             }}, {{"chewbacca-crew"               }}, {{ 22}} },
  { Upg::ChopperC,        {{{307,"Card_art_XW_U_83.jpg"                }                                             }}, {{"chopper-crew"                 }}, {{ 23}} },
  { Upg::CRee,            {{{337,"Card_art_XW_U_111.jpg"               }                                             }}, {{"cienaree"                     }}, {{ 24}} },
  { Upg::CVizago,         {{{356,"Card_art_XW_U_131.jpg"               }                                             }}, {{"cikatrovizago"                }}, {{ 25}} },
  { Upg::CmdrMalarus,     {{{684,"70fb233cb4aee8f02716292268d0b9c1.jpg"},{683,"c304667782eb50cd85af9fb996a924c9.jpg"}}}, {{"commandermalarus"             }}, {{347}} },
  { Upg::CmdrPyre,        {{{687,"7d1c00624b8253b9157856764d2c1004.jpg"}                                             }}, {{"commanderpyre"                }}, {{320}} },
  { Upg::CountDooku,      {{{539,"a91424eea1db4aeaf24ab49d5a519e27.jpg"}                                             }}, {{"countdooku"                   }}, {{218}} },
  { Upg::DVader,          {{{338,"Card_art_XW_U_112.jpg"               }                                             }}, {{"darthvader"                   }}, {{ 26}} },
  { Upg::DeathTr,         {{{339,"Card_art_XW_U_113.jpg"               }                                             }}, {{"deathtroopers"                }}, {{ 27}} },
  { Upg::DirKrennic,      {{{340,"Card_art_XW_U_114.jpg"               }                                             }}, {{"directorkrennic"              }}, {{ 28}} },
  { Upg::EmpPalp,         {{{341,"Card_art_XW_U_115.jpg"               }                                             }}, {{"emperorpalpatine"             }}, {{ 29}} },
  { Upg::Fives,           {{{679,"706062dbbf35d8d46c1e1f4b7dad8661.jpg"}                                             }}, {{"fives"                        }}, {{346}} },
  { Upg::FLSlicer,        {{{271,"Card_art_XW_U_42.jpg"                }                                             }}, {{"freelanceslicer"              }}, {{ 30}} },
  { Upg::GA97,            {{{582,"22b0be31694840d7725790e352fa825d.jpg"}                                             }}, {{"ga97"                         }}, {{230}} },
  { Upg::GKey,            {{{919,"d6faf9522ae2569b7a125b6a84d5df9a.jpg"}                                             }}, {{"gamutkey"                     }}, {{386}} },
  { Upg::GenGrievous,     {{{540,"3f6a042b2f8acfdcfcd1f05c28419fe3.jpg"}                                             }}, {{"generalgrievous"              }}, {{219}} },
  { Upg::GenHux,          {{{466,"94bee4960ca0fb907ba77d5cb2ff7abb.jpg"}                                             }}, {{"generalhux"                   }}, {{180}} },
  { Upg::GhostCompany,    {{{681,"673fb3c6f002702406456c1b135eda01.jpg"}                                             }}, {{"ghostcompany"                 }}, {{351}} },
  { Upg::Gonk,            {{{272,"Card_art_XW_U_43.jpg"                }                                             }}, {{"gnkgonkdroid"                 }}, {{ 32}} },
  { Upg::GrandInq,        {{{342,"Card_art_XW_U_116.jpg"               }                                             }}, {{"grandinquisitor"              }}, {{ 33}} },
  { Upg::GrandMoffTarkin, {{{343,"Card_art_XW_U_117.jpg"               }                                             }}, {{"grandmofftarkin"              }}, {{ 34}} },
  { Upg::HSolo,           {{{474,"af2ce405b6c28568aa2d66b78296b351.jpg"}                                             }}, {{"hansolo-crew"                 }}, {{192}} },
  { Upg::HOhnaka,         {{{853,"9345f2eadd69a3f1ea2f44648a17d9b2.jpg"}                                             }}, {{"hondoohnaka"                  }}, {{333}} },
  { Upg::HSyndulla,       {{{308,"Card_art_XW_U_84.jpg"                }                                             }}, {{"herasyndulla"                 }}, {{ 35}} },
  { Upg::IG88D,           {{{357,"Card_art_XW_U_132.jpg"               }                                             }}, {{"ig88d"                        }}, {{ 36}} },
  { Upg::Informant,       {{{273,"Card_art_XW_U_44.jpg"                }                                             }}, {{"informant"                    }}, {{ 37}} },
  { Upg::ISBSlicer,       {{{344,"Card_art_XW_U_118.jpg"               }                                             }}, {{"isbslicer"                    }}, {{ 38}} },
  { Upg::Jabba,           {{{358,"Card_art_XW_U_133.jpg"               }                                             }}, {{"jabbathehutt"                 }}, {{ 39}} },
  { Upg::JFett,           {{{854,"3a7ec5348f98da20c7b17b91535a3dd2.jpg"}                                             }}, {{"jangofett"                    }}, {{335}} },
  { Upg::JErso,           {{{309,"Card_art_XW_U_85.jpg"                }                                             }}, {{"jynerso"                      }}, {{ 40}} },
  { Upg::K2SO,            {{{647,"ced6485e9da6861b36fe3ab0c747fe76.jpg"}                                             }}, {{"k2so"                         }}, {{314}} },
  { Upg::KJarrus,         {{{310,"Card_art_XW_U_86.jpg"                }                                             }}, {{"kananjarrus"                  }}, {{ 41}} },
  { Upg::KConnix,         {{{583,"f14db8bb29fcb06dfee758cb33490fbe.jpg"}                                             }}, {{"kaydelconnix"                 }}, {{231}} },
  { Upg::KOnyo,           {{{359,"Card_art_XW_U_134.jpg"               }                                             }}, {{"ketsuonyo"                    }}, {{ 42}} },
  { Upg::KFisto,          {{{676,"2b1f84f53e0a2fe6f49dbdef202b6adb.jpg"}                                             }}, {{"kitfisto"                     }}, {{327}} },
  { Upg::KSella,          {{{584,"97aa5ed09744e698d42c2a12a369c786.jpg"}                                             }}, {{"korrsella"                    }}, {{245}} },
  { Upg::KRen,            {{{467,"a61302c8e1cd180d4198e9ae75b82e91.jpg"}                                             }}, {{"kyloren"                      }}, {{179}} },
  { Upg::L337,            {{{384,"Card_art_XW_U_158.jpg"               },{383,"b3af6a2a042345a1d95dd7f877fce8c7.jpg"}}}, {{"l337"                         }}, {{ 43}} },
  { Upg::LCalrissianReb,  {{{311,"Card_art_XW_U_87.jpg"                }                                             }}, {{"landocalrissian"              }}, {{ 44}} },
  { Upg::LCalrissianScum, {{{385,"Card_art_XW_U_159.jpg"               }                                             }}, {{"landocalrissian-crew"         }}, {{ 45}} },
  { Upg::LDAcy,           {{{585,"d147142728d8cc0aa647f97966df3988.jpg"}                                             }}, {{"larmadacy"                    }}, {{243}} },
  { Upg::LOrganaReb,      {{{312,"Card_art_XW_U_88.jpg"                }                                             }}, {{"leiaorgana"                   }}, {{ 46}} },
  { Upg::LOrganaRes,      {{{586,"bd5f0c91b7fb256f91fcfbd006e56acc.jpg"}                                             }}, {{"leiaorgana-resistance"        }}, {{244}} },
  { Upg::LRazzi,          {{{360,"Card_art_XW_U_135.jpg"               }                                             }}, {{"lattsrazzi"                   }}, {{ 47}} },
  { Upg::Maul,            {{{361,"Card_art_XW_U_136.jpg"               }                                             }}, {{"maul"                         }}, {{ 48}} },
  { Upg::MinTua,          {{{345,"Card_art_XW_U_119.jpg"               }                                             }}, {{"ministertua"                  }}, {{ 49}} },
  { Upg::MoffJerjerrod,   {{{346,"Card_art_XW_U_120.jpg"               }                                             }}, {{"moffjerjerrod"                }}, {{ 50}} },
  { Upg::MYarro,          {{{313,"Card_art_XW_U_89.jpg"                }                                             }}, {{"magvayarro"                   }}, {{ 51}} },
  { Upg::NNunb,           {{{314,"Card_art_XW_U_90.jpg"                }                                             }}, {{"niennunb"                     }}, {{ 52}} },
  { Upg::NovTech,         {{{274,"Card_art_XW_U_45.jpg"                }                                             }}, {{"novicetechnician"             }}, {{ 53}} },
  { Upg::PerCPilot,       {{{275,"Card_art_XW_U_46.jpg"                }                                             }}, {{"perceptivecopilot"            }}, {{ 54}} },
  { Upg::PetOffThanisson, {{{468,"007c0145ba0ec1c57de17c0448cafef2.jpg"}                                             }}, {{"pettyofficerthanisson"        }}, {{194}} },
  { Upg::PKoon,           {{{677,"7551a4c70f701c81e3e79ea9bb5a4237.jpg"}                                             }}, {{"plokoon"                      }}, {{325}} },
  { Upg::ProtGleb,        {{{920,"70c7a4a5786d2067e293d182d1fc44b2.jpg"}                                             }}, {{"protectorategleb"             }}, {{388}} },
  { Upg::PZ4CO,           {{{587,"219ba43a9fc9c9756a87676551aa7a69.jpg"}                                             }}, {{"pz4co"                        }}, {{246}} },
  { Upg::Qira,            {{{387,"Card_art_XW_U_161.jpg"               }                                             }}, {{"qira"                         }}, {{ 55}} },
  { Upg::R2D2C,           {{{315,"Card_art_XW_U_91.jpg"                }                                             }}, {{"r2d2-crew"                    }}, {{ 56}} },
  { Upg::RTico,           {{{475,"92a93952c31c95dff0fc96f31e88cfd3.jpg"}                                             }}, {{"rosetico"                     }}, {{173}} },
  { Upg::SWren_Crew,      {{{316,"Card_art_XW_U_92.jpg"                }                                             }}, {{"sabinewren"                   }}, {{ 57}} },
  { Upg::SGerrera,        {{{317,"Card_art_XW_U_93.jpg"                }                                             }}, {{"sawgerrera"                   }}, {{ 58}} },
  { Upg::SeasonNav,       {{{276,"Card_art_XW_U_47.jpg"                }                                             }}, {{"seasonednavigator"            }}, {{ 59}} },
  { Upg::SeventhSister,   {{{347,"Card_art_XW_U_121.jpg"               }                                             }}, {{"seventhsister"                }}, {{ 60}} },
  { Upg::SupLdrSnoke,     {{{469,"3b29995fb9e419822a34c672f2543fa6.jpg"}                                             }}, {{"supremeleadersnoke"           }}, {{184}} },
  { Upg::TactOff,         {{{277,"Card_art_XW_U_48.jpg"                }                                             }}, {{"tacticalofficer"              }}, {{ 61}} },
  { Upg::TFarr,           {{{760,"fa45744ddf40d228a433cca33a7e2f6f.jpg"}                                             }}, {{"torynfarr"                    }}, {{274}} },
  { Upg::TBeckett,        {{{386,"Card_art_XW_U_160.jpg"               }                                             }}, {{"tobiasbeckett"                }}, {{ 62}} },
  { Upg::UPlutt,          {{{362,"Card_art_XW_U_137.jpg"               }                                             }}, {{"unkarplutt"                   }}, {{ 64}} },
  { Upg::Wolfpack,        {{{680,"d5a7c585a6f5f06e76c7b927392b61c1.jpg"}                                             }}, {{"wolfpack"                     }}, {{352}} },
  { Upg::Yoda,            {{{678,"0f0e3e5576ed1df0e5e2b8ccee638dec.jpg"}                                             }}, {{"yoda"                         }}, {{322}} },
  { Upg::ZWesell,         {{{856,"b8819288e0722a11e26d15ecd0b2717f.jpg"}                                             }}, {{"zamwesell"                    }}, {{336}} },
  { Upg::ZOrrelios,       {{{318,"Card_art_XW_U_94.jpg"                }                                             }}, {{"zeborrelios"                  }}, {{ 65}} },
  { Upg::Zuckuss,         {{{363,"Card_art_XW_U_138.jpg"               }                                             }}, {{"zuckuss"                      }}, {{ 66}} },

  { Upg::BattleMed,       {{{545,"e9aed0602a75ddad090820e59036a7fd.jpg"}                                             }}, {{"battlemeditation"             }}, {{212}} },
  { Upg::BrilEva,         {{{536,"73bd7872a2c74e0402255f868d9d12bb.jpg"}                                             }}, {{"brilliantevasion"             }}, {{199}} },
  { Upg::Compassion,      {                                                                                           }, {{"compassion"                   }}, {{417}} },
  { Upg::ExtManeuvers,    {{{848,"9fde6e000d6e1d9dd28e3ffb301b9c04.jpg"}                                             }}, {{"extrememaneuvers"             }}, {{341}} },
  { Upg::Foresight,       {{{613,"87b83856576c1d4b7f3036d6008863f6.jpg"}                                             }}, {{"foresight"                    }}, {{251}} },
  { Upg::Hate,            {{{489,"a373c947f0a56ee4bcf4223250326dc0.jpg"}                                             }}, {{"hate"                         }}, {{204}} },
  { Upg::HeightPerc,      {{{248,"Card_art_XW_U_19.jpg",               }                                             }}, {{"heightenedperception"         }}, {{ 72}} },
  { Upg::InstAim,         {{{249,"Card_art_XW_U_20.jpg",               }                                             }}, {{"instinctiveaim"               }}, {{ 73}} },
  { Upg::Malice,          {                                                                                           }, {{"malice"                       }}, {{418}} },
  { Upg::Patience,        {{{849,"905d768c07c386930b14d1d5f7e74ba4.jpg"}                                             }}, {{"patience"                     }}, {{342}} },
  { Upg::PrecogReflexes,  {{{614,"7c139cebc9475051506c5b79a98f465a.jpg"}                                             }}, {{"precognitivereflexes"         }}, {{250}} },
  { Upg::PredictiveShot,  {{{490,"53ceabb0e2e66e61c077145475b18dab.jpg"}                                             }}, {{"predictiveshot"               }}, {{203}} },
  { Upg::Sense,           {{{250,"Card_art_XW_U_21.jpg",               }                                             }}, {{"sense"                        }}, {{ 75}} },
  { Upg::ShatteringShot,  {                                                                                           }, {{"shatteringshot"               }}, {{419}} },
  { Upg::SNReflex,        {{{251,"Card_art_XW_U_22.jpg",               }                                             }}, {{"supernaturalreflexes"         }}, {{ 74}} },

  { Upg::AgileGunner,     {{{388,"Card_art_XW_U_162.jpg"               }                                             }}, {{"agilegunner"                  }}, {{ 76}} },
  { Upg::ATano,           {{{615,"f37eb1e56dc1928bfcd53e4eb95a01ae.jpg"}                                             }}, {{"ahsokatano"                   }}, {{253}} },
  { Upg::Bistan,          {{{319,"Card_art_XW_U_95.jpg"                }                                             }}, {{"bistan"                       }}, {{ 77}} },
  { Upg::BFett_G,         {{{857,"d6d9f440cf8d6965a879db163d42d402.jpg"}                                             }}, {{"bobafett-gunner"              }}, {{334}} },
  { Upg::Bossk,           {{{364,"Card_art_XW_U_139.jpg"               }                                             }}, {{"bossk"                        }}, {{ 78}} },
  { Upg::BT1,             {{{365,"Card_art_XW_U_140.jpg"               }                                             }}, {{"bt1"                          }}, {{ 79}} },
  { Upg::CloneCaptRex,    {{{688,"245421ccca34a8495ab2b1176215763d.jpg"}                                             }}, {{"clonecaptainrex"              }}, {{321}} },
  { Upg::CloneCmdrCody,   {{{552,"209d0df0333dc348c74c0e8a760ca741.jpg"}                                             }}, {{"clonecommandercody"           }}, {{225}} },
  { Upg::Dengar,          {{{366,"Card_art_XW_U_141.jpg"               }                                             }}, {{"dengar"                       }}, {{ 80}} },
  { Upg::DT798,           {                                                                                           }, {{"dt798"                        }}, {{412}} },
  { Upg::EBridger,        {{{320,"Card_art_XW_U_96.jpg"                }                                             }}, {{"ezrabridger"                  }}, {{ 81}} },
  { Upg::FifthBrother,    {{{348,"Card_art_XW_U_122.jpg"               }                                             }}, {{"fifthbrother"                 }}, {{ 82}} },
  { Upg::Finn,            {{{476,"7d5d0c76d3c8fdbb5ec893c270eec7b2.jpg"}                                             }}, {{"finn"                         }}, {{174}} },
  { Upg::FOOrdTech,       {                                                                                           }, {{"firstorderordnancetech"       }}, {{414}} },
  { Upg::Greedo,          {{{367,"Card_art_XW_U_142.jpg"               }                                             }}, {{"greedo"                       }}, {{ 83}} },
  { Upg::HSoloReb,        {{{321,"Card_art_XW_U_97.jpg"                }                                             }}, {{"hansolo"                      }}, {{ 84}} },
  { Upg::HSoloScu,        {{{389,"Card_art_XW_U_163.jpg"               }                                             }}, {{"hansolo-gunner"               }}, {{ 85}} },
  { Upg::HsGunner,        {{{278,"Card_art_XW_U_49.jpg"                }                                             }}, {{"hotshotgunner"                }}, {{ 86}} },
  { Upg::LSkywalkerG,     {{{322,"Card_art_XW_U_98.jpg"                }                                             }}, {{"lukeskywalker"                }}, {{ 87}} },
  { Upg::PTico,           {{{477,"a34ab7a76083f91577110d31d20b6e14.jpg"}                                             }}, {{"paigetico"                    }}, {{189}} },
  { Upg::Rey,             {{{478,"ab5eea679d5ca9369cd122bc65001119.jpg"}                                             }}, {{"rey-gunner"                   }}, {{187}} },
  { Upg::SWren_Gun,       {{{921,"239d3285c1e55ae021c63a3f0f19985f.jpg"}                                             }}, {{"sabinewren-gunner"            }}, {{379}} },
  { Upg::SeventhFltGun,   {{{553,"8461a9f5c79195b802e8b04da922809f.jpg"}                                             }}, {{"seventhfleetgunner"           }}, {{227}} },
  { Upg::SkBombard,       {{{279,"Card_art_XW_U_50.jpg"                }                                             }}, {{"skilledbombardier"            }}, {{ 88}} },
  { Upg::SpecForGun,      {{{470,"b87bf63d7db6195febd879edc4880f13.jpg"}                                             }}, {{"specialforcesgunner"          }}, {{182}} },
  { Upg::SuppGunner,      {{{689,"4902fa74cccbaadfa2108ef7670bcbfe.jpg"}                                             }}, {{"suppressivegunner"            }}, {{350}} },
  { Upg::VetTailGun,      {{{280,"Card_art_XW_U_51.jpg"                }                                             }}, {{"veterantailgunner"            }}, {{ 89}} },
  { Upg::VetTurret,       {{{281,"Card_art_XW_U_52.jpg"                }                                             }}, {{"veteranturretgunner"          }}, {{ 90}} },
  { Upg::WeaponsSysOff,   {{{859,"c10278c132ecd7cf511b29d4a91c597b.jpg"}                                             }}, {{"weaponssystemsofficer"        }}, {{368}} },

  { Upg::IonCanBat,       {{{737,"64d964f87f3c406a380a4e3d23f8adff.jpg"},{736,"b7837b3c19f0e0e552a3e2024ada0080.jpg"}}}, {{"ioncannonbattery"             }}, {{269}} },
  { Upg::OrdTubes,        {{{741,"704417993312731838f68390884c699d.jpg"},{740,"1c29cf92ae4254dcc6d6e01649440928.jpg"}}}, {{"ordnancetubes"                }}, {{271}} },
  { Upg::PointDefBat,     {{{743,"190fb055e605840f31474e7c0d908f3a.jpg"},{742,"19ff0f46629df40158546791f897b55b.jpg"}}}, {{"pointdefensebattery"          }}, {{272}} },
  { Upg::TargetingBat,    {{{739,"f8b2128da475099b9d362e26f04b8e55.jpg"},{738,"b60587c3fead89e24b6990a2adb1131c.jpg"}}}, {{"targetingbattery"             }}, {{270}} },
  { Upg::TractorTent,     {{{933,"60c195f4af8e303047b0633161c42a7d.jpg"},{933,"4c1ea70c43bf582038cc80d0836b74e7.jpg"}}}, {{"tractortentacles"             }}, {{396}} },
  { Upg::TurbolaserBat,   {{{745,"4962c14923b8a068cd8ba6ac34ebd443.jpg"},{744,"3a88323269e88afa9557ec1b2fe46925.jpg"}}}, {{"turbolaserbattery"            }}, {{273}} },

  { Upg::DrillBeak,       {{{935,"794b36ed716af78be97f336ff49430f5.jpg"},{935,"02e9f2359fa0139841da025bccea4b23.jpg"}}}, {{"drillbeak"                    }}, {{397}} },
  { Upg::EnhancedProp,    {{{937,"601fd8977e28a3e5acea47e5727c3425.jpg"},{937,"60839557b8279b4de1f8a7ef19256cc2.jpg"}}}, {{"enhancedpropulsion"           }}, {{398}} },
  { Upg::ProtCanBat,      {{{939,"bbdc9a802049e98132ee424f4c2bc0ec.jpg"},{939,"97c81b90378cf7ff53587f74af561ea1.jpg"}}}, {{"protoncannonbattery"          }}, {{399}} },

  { Upg::Syliure31,       {{{850,"07ccb9fc6a068ecad5f1b14acd87f145.jpg"}                                             }}, {{"syliure31hyperdrive"          }}, {{362}} },

  { Upg::BFrik,           {                                                                                           }, {{"babufrik"                     }}, {{416}} },
  { Upg::CloakDev,        {{{286,"Card_art_XW_U_57.jpg"                }                                             }}, {{"cloakingdevice"               }}, {{ 91}} },
  { Upg::CoaxiumHyp,      {{{650,"791981d898356ad13e463b8699975155.jpg"}                                             }}, {{"coaxiumhyperfuel"             }}, {{310}} },
  { Upg::ContraCyb,       {{{287,"Card_art_XW_U_58.jpg"                }                                             }}, {{"contrabandcybernetics"        }}, {{ 92}} },
  { Upg::DeadmansSw,      {{{288,"Card_art_XW_U_59.jpg"                }                                             }}, {{"deadmansswitch"               }}, {{ 93}} },
  { Upg::FalseTransCodes, {{{877,"06d6e39ec52930c4faca371e8dcd3fd6.jpg"}                                             }}, {{"falsetranspondercodes"        }}, {{369}} },
  { Upg::FeedbackAr,      {{{289,"Card_art_XW_U_60.jpg"                }                                             }}, {{"feedbackarray"                }}, {{ 94}} },
  { Upg::InertDamp,       {{{290,"Card_art_XW_U_61.jpg"                }                                             }}, {{"inertialdampeners"            }}, {{ 95}} },
  { Upg::OvertunedMod,    {                                                                                           }, {{"overtunedmodulators"          }}, {{411}} },
  { Upg::QRLocks,         {{{753,"836fa300919eafe8e7ac5fbc47b8c0c6.jpg"}                                             }}, {{"quickreleaselocks"            }}, {{287}} },
  { Upg::RigCargoCh,      {{{291,"Card_art_XW_U_62.jpg"                }                                             }}, {{"riggedcargochute"             }}, {{ 96}} },
  { Upg::SabMap,          {{{754,"46a2b1477a848a09c56dbcd49ab6f9f4.jpg"}                                             }}, {{"saboteursmap"                 }}, {{288}} },
  { Upg::ScanBaff,        {{{755,"c4749e45f352215cb96d6b1125c09a91.jpg"}                                             }}, {{"scannerbaffler"               }}, {{289}} },

  { Upg::BarRockets,      {{{265,"Card_art_XW_U_36.jpg"                }                                             }}, {{"barragerockets"               }}, {{ 97}} },
  { Upg::ClustMsl,        {{{266,"Card_art_XW_U_37.jpg"                }                                             }}, {{"clustermissiles"              }}, {{ 98}} },
  { Upg::ConcusMsl,       {{{267,"Card_art_XW_U_38.jpg"                }                                             }}, {{"concussionmissiles"           }}, {{ 99}} },
  { Upg::DiaBorMsl,       {{{580,"bc97441f8e8461e6949ab70b0a4bf0c5.jpg"}                                             }}, {{"diamondboronmissiles"         }}, {{238}} },
  { Upg::DiscordMsl,      {{{543,"a2c1b72df5f0b429bf38fbd9ad61bc99.jpg"}                                             }}, {{"discordmissiles"              }}, {{224}} },
  { Upg::EnShellCh,       {{{532,"a0eeefcf98562fdbc606c8638c35b6db.jpg"}                                             }}, {{"energyshellcharges"           }}, {{209}} },
  { Upg::HomingMsl,       {{{268,"Card_art_XW_U_39.jpg"                }                                             }}, {{"homingmissiles"               }}, {{100}} },
  { Upg::IonMsl,          {{{269,"Card_art_XW_U_40.jpg"                }                                             }}, {{"ionmissiles"                  }}, {{101}} },
  { Upg::MagPulseWar,     {{{651,"71140b3339226017653867076e19310f.jpg"}                                             }}, {{"magpulsewarheads"             }}, {{311}} },
  { Upg::MultiMslPods,    {{{673,"af81354b305251139da023c343c9f70f.jpg"}                                             }}, {{"multimissilepods"             }}, {{326}} },
  { Upg::PRockets,        {{{270,"Card_art_XW_U_41.jpg"                }                                             }}, {{"protonrockets"                }}, {{102}} },
  { Upg::XX23,            {{{851,"34fa7fa46dbf8626f1480fc9937e7c1b.jpg"}                                             }}, {{"xx23sthreadtracers"           }}, {{332}} },

  { Upg::ElecChaffMsl,    {                                                                                           }, {{"electrochaffmissiles"         }}, {{420}} },

  { Upg::AblatPlat,       {{{292,"Card_art_XW_U_68.jpg"                }                                             }}, {{"ablativeplating"              }}, {{103}} },
  { Upg::AdvSLAM,         {{{293,"Card_art_XW_U_69.jpg"                }                                             }}, {{"advancedslam"                 }}, {{104}} },
  { Upg::Afterburn,       {{{294,"Card_art_XW_U_70.jpg"                }                                             }}, {{"afterburners"                 }}, {{105}} },
  { Upg::AngledDef,       {{{593,"691b45548136b6e5fd005e7797ae53d9.jpg"}                                             }}, {{"angleddeflectors"             }}, {{247}} },
  { Upg::DelayedFuses,    {{{592,"453d2de1f5059d0e6eb7884a4bf7986b.jpg"}                                             }}, {{"delayedfuses"                 }}, {{236}} },
  { Upg::ElectBaff,       {{{295,"Card_art_XW_U_71.jpg"                }                                             }}, {{"electronicbaffle"             }}, {{106}} },
  { Upg::EngUpg,          {{{296,"Card_art_XW_U_72.jpg"                }                                             }}, {{"engineupgrade"                }}, {{107}} },
  { Upg::HullUpg,         {{{297,"Card_art_XW_U_73.jpg"                }                                             }}, {{"hullupgrade"                  }}, {{164}} },
  { Upg::ImpervPlat,      {{{534,"20769de45863e2bbb180f05e6ed1e0e3.jpg"}                                             }}, {{"imperviumplating"             }}, {{207}} },
  { Upg::IndepCalc,       {{{866,"327275e06ddaac1afbe7b8209883aa4b.jpg"}                                             }}, {{"independentcalculations"      }}, {{367}} },
  { Upg::MuniFailSa,      {{{298,"Card_art_XW_U_74.jpg"                }                                             }}, {{"munitionsfailsafe"            }}, {{108}} },
  { Upg::OverdriveThr,    {{{721,"fb6fef19ae0bf5b42d4da716f4826c26.jpg"}                                             }}, {{"overdrivethruster"            }}, {{345}} },
  { Upg::PrecIonEng,      {{{867,"fbafd721fa85216ee1d16d305e31e694.jpg"}                                             }}, {{"precisionionengines"          }}, {{338}} },
  { Upg::ShieldUpg,       {{{299,"Card_art_XW_U_75.jpg"                }                                             }}, {{"shieldupgrade"                }}, {{165}} },
  { Upg::SparePartsCan,   {{{550,"a61b812e2e74fab5435c9684462cd9d7.jpg"}                                             }}, {{"sparepartscanisters"          }}, {{215}} },
  { Upg::StaDcVanes,      {{{300,"Card_art_XW_U_76.jpg"                }                                             }}, {{"staticdischargevanes"         }}, {{109}} },
  { Upg::StealthDev,      {{{301,"Card_art_XW_U_77.jpg"                }                                             }}, {{"stealthdevice"                }}, {{166}} },
  { Upg::SyncCon,         {{{554,"f105bb42b6d3500c300e48ab695c1647.jpg"}                                             }}, {{"synchronizedconsole"          }}, {{211}} },
  { Upg::TactScramb,      {{{302,"Card_art_XW_U_78.jpg"                }                                             }}, {{"tacticalscrambler"            }}, {{110}} },
  { Upg::TargetComp,      {{{619,"2e8e6572a5802967220296ec22e5d8cb.jpg"}                                             }}, {{"targetingcomputer"            }}, {{249}} },

  { Upg::Bomblet,         {{{392,"Card_art_XW_U_63.jpg"                }                                             }}, {{"bombletgenerator"             }}, {{ 67}} },
  { Upg::ClusterMines,    {{{648,"aea6bdafa5066a040a8929d6eb46499a.jpg"}                                             }}, {{"clustermines"                 }}, {{316}} },
  { Upg::ConcussionBmb,   {{{690,"960df489d295d30765d019cf42a1f450.jpg"}                                             }}, {{"concussionbombs"              }}, {{355}} },
  { Upg::ConnerNet,       {{{393,"Card_art_XW_U_64.jpg"                }                                             }}, {{"connernets"                   }}, {{ 68}} },
  { Upg::DRK1ProbeDroids, {{{541,"a69a3f1075a711b57cf7459e51647a7a.jpg"}                                             }}, {{"drk1probedroids"              }}, {{221}} },
  { Upg::ElectroProtBmb,  {{{591,"215f5298b9917bd57db474a38139ba08.jpg"}                                             }}, {{"electroprotonbomb"            }}, {{235}} },
  { Upg::IonBmb,          {{{649,"f69c5ecaca9ab01380f6329e49970ddf.jpg"}                                             }}, {{"ionbombs"                     }}, {{317}} },
  { Upg::ProtonBmb,       {{{394,"Card_art_XW_U_65.jpg"                }                                             }}, {{"protonbombs"                  }}, {{ 69}} },
  { Upg::ProxMine,        {{{395,"Card_art_XW_U_66.jpg"                }                                             }}, {{"proximitymines"               }}, {{ 70}} },
  { Upg::SeismicCh,       {{{396,"Card_art_XW_U_67.jpg"                }                                             }}, {{"seismiccharges"               }}, {{ 71}} },
  { Upg::ThermalDet,      {{{864,"79c228aa933ae9970641262f6352ac4a.jpg"}                                             }}, {{"thermaldetonators"            }}, {{339}} },

  { Upg::AdvSensors,      {{{252,"Card_art_XW_U_23.jpg"                }                                             }}, {{"advancedsensors"              }}, {{111}} },
  { Upg::CollisDet,       {{{253,"Card_art_XW_U_24.jpg"                }                                             }}, {{"collisiondetector"            }}, {{112}} },
  { Upg::FCS,             {{{254,"Card_art_XW_U_25.jpg"                }                                             }}, {{"firecontrolsystem"            }}, {{113}} },
  { Upg::PassiveSensors,  {{{577,"a9c69fd08df1a2ce84f240943fe05245.jpg"}                                             }}, {{"passivesensors"               }}, {{240}} },
  { Upg::TrajSim,         {{{255,"Card_art_XW_U_26.jpg"                }                                             }}, {{"trajectorysimulator"          }}, {{114}} },

  { Upg::K2B4,            {{{537,"fa9a0b38a079ec78b6db330c2ffc9b0a.jpg"}                                             }}, {{"k2b4"                         }}, {{220}} },
  { Upg::Kalani,          {{{682,"903cb81e543eb48cd7ae6872f214cb24.jpg"}                                             }}, {{"kalani"                       }}, {{353}} },
  { Upg::Kraken,          {{{531,"0e1c2510e830ff6d9e7e6226ef2792b4.jpg"}                                             }}, {{"kraken"                       }}, {{222}} },
  { Upg::TA175,           {{{590,"daa093356e5022b74d375979446e32ff.jpg"}                                             }}, {{"ta175"                        }}, {{239}} },
  { Upg::TV94,            {{{530,"f6177b6339e425fff560af884596a83b.jpg"}                                             }}, {{"tv94"                         }}, {{223}} },

  { Upg::BackwardsTail,   {{{715,"d30db8383beca909eca0993a9299d226.jpg"}                                             }}, {{"backwardstailslide"           }}, {{357}} },
  { Upg::Composure,       {{{381,"Card_art_XW_U_156.jpg"               }                                             }}, {{"composure"                    }}, {{115}} },
  { Upg::CrackShot,       {{{230,"Card_art_XW_U_1.jpg"                 }                                             }}, {{"crackshot"                    }}, {{116}} },
  { Upg::Cutthroat,       {{{914,"ae15ec14df97ac215567f78fe6c92343.jpg"}                                             }}, {{"cutthroat"                    }}, {{375}} },
  { Upg::Daredevil,       {{{231,"Card_art_XW_U_2.jpg"                 }                                             }}, {{"daredevil"                    }}, {{117}} },
  { Upg::DeadeyeShot,     {{{670,"9f2bb1378eecd7358ebb4fd42b3f2f65.jpg"}                                             }}, {{"deadeyeshot"                  }}, {{343}} },
  { Upg::DebGambit,       {{{232,"Card_art_XW_U_3.jpg"                 }                                             }}, {{"debrisgambit"                 }}, {{118}} },
  { Upg::Dedicated,       {{{544,"21b00eaf0d2f447db8fc7b2dc69222ab.jpg"}                                             }}, {{"dedicated"                    }}, {{210}} },
  { Upg::Disciplined,     {{{915,"bddffdbe4ada9aa8b6f43b883cfb7acb.jpg"}                                             }}, {{"disciplined"                  }}, {{381}} },
  { Upg::Elusive,         {{{233,"Card_art_XW_U_4.jpg"                 }                                             }}, {{"elusive"                      }}, {{119}} },
  { Upg::Ensnare,         {{{610,"9513320be3bb150bb8ee6d3504e0ed01.jpg"}                                             }}, {{"ensnare"                      }}, {{248}} },
  { Upg::ExpHan,          {{{234,"Card_art_XW_U_5.jpg"                 }                                             }}, {{"experthandling"               }}, {{120}} },
  { Upg::Fanatical,       {{{459,"0a7cac152b045daf71fa5d28504e54b4.jpg"}                                             }}, {{"fanatical"                    }}, {{181}} },
  { Upg::Fearless,        {{{235,"Card_art_XW_U_6.jpg"                 }                                             }}, {{"fearless"                     }}, {{121}} },
  { Upg::FeedbackPing,    {                                                                                           }, {{"feedbackping"                 }}, {{421}} },
  { Upg::GravDef,         {{{611,"8bcd2006892a98b5b29d9aba67733cb6.jpg"}                                             }}, {{"graviticdeflection"           }}, {{255}} },
  { Upg::Heroic,          {{{471,"0f6d1677c35001248d7768047c10aa87.jpg"}                                             }}, {{"heroic"                       }}, {{172}} },
  { Upg::Hopeful,         {{{916,"3c50ea7c72423be60ac00017cbaed807.jpg"}                                             }}, {{"hopeful"                      }}, {{378}} },
  { Upg::InterloperTurn,  {{{917,"1ae514eedd2e4129b311b69c4174e5f6.jpg"}                                             }}, {{"interloperturn"               }}, {{387}} },
  { Upg::Intimidat,       {{{236,"Card_art_XW_U_7.jpg"                 }                                             }}, {{"intimidation"                 }}, {{122}} },
  { Upg::IonLimOverride,  {{{717,"babf3b90df6a73d026d4ab7670306ab7.jpg"}                                             }}, {{"ionlimiteroverride"           }}, {{330}} },
  { Upg::Juke,            {{{237,"Card_art_XW_U_8.jpg"                 }                                             }}, {{"juke"                         }}, {{123}} },
  { Upg::LoneWolf,        {{{238,"Card_art_XW_U_9.jpg"                 }                                             }}, {{"lonewolf"                     }}, {{124}} },
  { Upg::MargSabl,        {{{846,"6c495e5b96157ab865a895c70d614ae6.jpg"}                                             }}, {{"margsablclosure"              }}, {{331}} },
  { Upg::Marksman,        {{{239,"Card_art_XW_U_10.jpg"                }                                             }}, {{"marksmanship"                 }}, {{125}} },
  { Upg::Outmaneuv,       {{{240,"Card_art_XW_U_11.jpg"                }                                             }}, {{"outmaneuver"                  }}, {{126}} },
  { Upg::ProudTrad,       {{{653,"8f9f85aa00fb260336b9938e2a3f7c33.jpg"},{652,"30486dca1780008aea8c4c905f44ead3.jpg"}}}, {{"proudtradition"               }}, {{319}} },
  { Upg::Predator,        {{{241,"Card_art_XW_U_12.jpg"                }                                             }}, {{"predator"                     }}, {{127}} },
  { Upg::Ruthless,        {{{242,"Card_art_XW_U_13.jpg"                }                                             }}, {{"ruthless"                     }}, {{128}} },
  { Upg::SatSalvo,        {{{243,"Card_art_XW_U_14.jpg"                }                                             }}, {{"saturationsalvo"              }}, {{129}} },
  { Upg::Selfless,        {{{244,"Card_art_XW_U_15.jpg"                }                                             }}, {{"selfless"                     }}, {{130}} },
  { Upg::SnapShot,        {{{612,"ec959ee48f953dbde71aa7c89d0b54bf.jpg"}                                             }}, {{"snapshot"                     }}, {{256}} },
  { Upg::SquadLdr,        {{{245,"Card_art_XW_U_16.jpg"                }                                             }}, {{"squadleader"                  }}, {{131}} },
  { Upg::StarbirdSlash,   {{{716,"9651ea60a9c2b74779fbcadcc989c171.jpg"}                                             }}, {{"starbirdslash"                }}, {{344}} },
  { Upg::SwarmTac,        {{{246,"Card_art_XW_U_17.jpg"                }                                             }}, {{"swarmtactics"                 }}, {{132}} },
  { Upg::TierfonBR,       {{{918,"a1b35914e278f332b59e04749c90452c.jpg"}                                             }}, {{"tierfonbellyrun"              }}, {{376}} },
  { Upg::Treacherous,     {{{529,"406e6007268488c36fc9066fb0a5d70f.jpg"}                                             }}, {{"treacherous"                  }}, {{228}} },
  { Upg::TrickShot,       {{{247,"Card_art_XW_U_18.jpg"                }                                             }}, {{"trickshot"                    }}, {{133}} },

  { Upg::BombSpec,        {{{746,"00df0c7fae30d4be941f32ba2fce1bc0.jpg"}                                             }}, {{"bombardmentspecialists"       }}, {{275}} },
  { Upg::CommsTeam,       {{{747,"accec3b3b4603e26ba5bf37cddc2ff1d.jpg"}                                             }}, {{"commsteam"                    }}, {{276}} },
  { Upg::DamageCtrlTeam,  {{{750,"2e47d434c593aca8030f04103b86a132.jpg"}                                             }}, {{"damagecontrolteam"            }}, {{277}} },
  { Upg::DroidCrew,       {{{942,"098ab4fc307417ec0ddf522baa417969.jpg"}                                             }}, {{"droidcrew"                    }}, {{400}} },
  { Upg::GunnerySpec,     {{{749,"e39555ce2c899f2f12dcf6fe6e0f5f25.jpg"}                                             }}, {{"gunneryspecialists"           }}, {{278}} },
  { Upg::IGRMDroids,      {{{748,"a2e89d07fdf5680e06bce60518845ba9.jpg"}                                             }}, {{"igrmdroids"                   }}, {{279}} },
  { Upg::OrdnanceTeam,    {{{751,"7a267ccc8299728e6f9fa8650b1321ca.jpg"}                                             }}, {{"ordnanceteam"                 }}, {{280}} },
  { Upg::SensExp,         {{{752,"779973fbba5deb810846a7f0bf7bf3c3.jpg"}                                             }}, {{"sensorexperts"                }}, {{281}} },
  { Upg::TractorTech,     {{{944,"97a0047dc14ad746d974f2e5514a0d9c.jpg"}                                             }}, {{"tractortechnicians"           }}, {{401}} },

  { Upg::CorsairCrew,     {{{943,"50dcf0a270d6bdc8e30cf6b8e3702072.jpg"}                                             }}, {{"corsaircrew"                  }}, {{402}} },

  { Upg::AdvOptics,       {{{460,"e19aa7cd99df250715c17386135cd140.jpg"}                                             }}, {{"advancedoptics"               }}, {{186}} },
  { Upg::AutoTgtPri,      {{{671,"d0f9614d25d622b900f5ceafc8028fbd.jpg"}                                             }}, {{"automatedtargetpriority"      }}, {{348}} },
  { Upg::BiohexCodes,     {{{491,"14c54aef2e36ac34564194e4785c98ce.jpg"}                                             }}, {{"biohexacryptcodes"            }}, {{202}} },
  { Upg::DeuteriumPC,     {{{654,"038636d357004cc01635a93c224d921d.jpg"}                                             }}, {{"deuteriumpowercells"          }}, {{318}} },
  { Upg::FPaint,          {{{488,"55ff3d199702695f23631d85a25bbae6.jpg"}                                             }}, {{"ferrospherepaint"             }}, {{198}} },
  { Upg::HsTrackingData,  {{{461,"fa5c01453d7aa2e2f1f100593e79669d.jpg"}                                             }}, {{"hyperspacetrackingdata"       }}, {{185}} },
  { Upg::PatAnalyzer,     {{{462,"811e07d55f95e53fef3182020d8cbf6e.jpg"}                                             }}, {{"patternanalyzer"              }}, {{206}} },
  { Upg::PThrusters,      {{{463,"3789dd873db0d0c74293531bef49e16e.jpg"}                                             }}, {{"primedthrusters"              }}, {{178}} },
  { Upg::SensorBuoySuite, {{{672,"8962c76fb021f876c97b6044b7da827b.jpg"}                                             }}, {{"sensorbuoysuite"              }}, {{349}} },
  { Upg::SensorScramble,  {                                                                                           }, {{"sensorscramblers"             }}, {{415}} },
  { Upg::TargetSync,      {{{464,"1597042d794000fb34d1eee30cc10f35.jpg"}                                             }}, {{"targetingsynchronizer"        }}, {{177}} },

  { Upg::Andrasta,        {{{371,"Card_art_XW_U_146.jpg"               }                                             }}, {{"andrasta"                     }}, {{146}} },
  { Upg::Assailer,        {{{769,"adbdc704e0f66a27d4ae2ecfb2323d2d.jpg"}                                             }}, {{"assailer"                     }}, {{297}} },
  { Upg::B6Prototype,     {{{923,"69e848f2da9a8f6f18d2f89096a94513.jpg"}                                             }}, {{"b6bladewingprototype"         }}, {{372}} },
  { Upg::BlackOne,        {{{484,"b617cc192e2ffb8368de79d69e1e7956.jpg"}                                             }}, {{"blackone"                     }}, {{171}} },
  { Upg::BloodCrow,       {{{773,"7043e25c4bf030ade5b026e5bc8bd43e.jpg"}                                             }}, {{"bloodcrow"                    }}, {{301}} },
  { Upg::BrightHope,      {{{766,"4f3cb0d471b9ea207665b120a5b27eb7.jpg"}                                             }}, {{"brighthope"                   }}, {{294}} },
  { Upg::BrokenHorn,      {{{777,"b6b2484bd0fc2c17ff2159b1280a2e5d.jpg"}                                             }}, {{"brokenhorn"                   }}, {{305}} },
  { Upg::Corvus,          {{{770,"89af7f90118cad6862fa2acb79f05e24.jpg"}                                             }}, {{"corvus"                       }}, {{298}} },
  { Upg::Dauntless,       {{{349,"Card_art_XW_U_123.jpg"               }                                             }}, {{"dauntless"                    }}, {{147}} },
  { Upg::DodonnasPride,   {{{761,"1dca6b93927cdf20f638ddc7362e0f0f.jpg"}                                             }}, {{"dodonnaspride"                }}, {{290}} },
  { Upg::Ghost,           {{{326,"Card_art_XW_U_102.jpg"               }                                             }}, {{"ghost"                        }}, {{148}} },
  { Upg::Grappler,        {{{946,"afefea235231e6f72be38b25d9b3a014.jpg"}                                             }}, {{"grappler"                     }}, {{403}} },
  { Upg::Havoc,           {{{372,"Card_art_XW_U_147.jpg"               }                                             }}, {{"havoc"                        }}, {{149}} },
  { Upg::HoundsTooth,     {{{373,"Card_art_XW_U_148.jpg"               }                                             }}, {{"houndstooth"                  }}, {{150}} },
  { Upg::IG2000,          {{{374,"Card_art_XW_U_149.jpg"               }                                             }}, {{"ig2000"                       }}, {{151}} },
  { Upg::Impetuous,       {{{771,"158a70d7bae002ca5bcd4c6fe94cb2d1.jpg"}                                             }}, {{"impetuous"                    }}, {{299}} },
  { Upg::InsatiableWorrt, {{{779,"0dc42b21873c86f819f3dd528cdae1ff.jpg"}                                             }}, {{"insatiableworrt"              }}, {{307}} },
  { Upg::Instigator,      {{{772,"943dfb58136280a8cf1ea781bf7b0c2f.jpg"}                                             }}, {{"instigator"                   }}, {{300}} },
  { Upg::JainasLight,     {{{762,"8276e8197e00c4f86b5f987643106334.jpg"}                                             }}, {{"jainaslight"                  }}, {{291}} },
  { Upg::KazsFireball,    {{{655,"0affe4d794e05c485d6103e5414e816a.jpg"}                                             }}, {{"kazsfireball"                 }}, {{315}} },
  { Upg::LandosMF,        {{{390,"Card_art_XW_U_164.jpg"               }                                             }}, {{"landosmillenniumfalcon"       }}, {{152}} },
  { Upg::Liberator,       {{{763,"18bb0bda87c23b452e0c4788136beb30.jpg"}                                             }}, {{"liberator"                    }}, {{292}} },
  { Upg::Luminous,        {{{767,"2989267c764d1eeff27159d85948b3df.jpg"}                                             }}, {{"luminous"                     }}, {{295}} },
  { Upg::Marauder,        {{{375,"Card_art_XW_U_150.jpg"               }                                             }}, {{"marauder"                     }}, {{153}} },
  { Upg::MerchantOne,     {{{778,"9c35d5bc7124123ec8818e53912449bd.jpg"}                                             }}, {{"merchantone"                  }}, {{306}} },
  { Upg::MilFalcon,       {{{327,"Card_art_XW_U_103.jpg"               }                                             }}, {{"millenniumfalcon"             }}, {{154}} },
  { Upg::MistHunter,      {{{376,"Card_art_XW_U_151.jpg"               }                                             }}, {{"misthunter"                   }}, {{155}} },
  { Upg::MoldyCrow,       {{{328,"Card_art_XW_U_104.jpg"               }                                             }}, {{"moldycrow"                    }}, {{156}} },
  { Upg::NautolansRev,    {{{945,"9cf4790c8ca1db96c274c4d0462c96e3.jpg"}                                             }}, {{"nautolansrevenge"             }}, {{404}} },
  { Upg::NeimoidianGrasp, {{{947,"2e5f7c3c05a3612dd0060312043a3b54.jpg"}                                             }}, {{"neimoidiangrasp"              }}, {{405}} },
  { Upg::Outrider,        {{{329,"Card_art_XW_U_105.jpg"               }                                             }}, {{"outrider"                     }}, {{157}} },
  { Upg::Phantom,         {{{330,"Card_art_XW_U_106.jpg"               }                                             }}, {{"phantom"                      }}, {{167}} },
  { Upg::PunishingOne,    {{{377,"Card_art_XW_U_152.jpg"               }                                             }}, {{"punishingone"                 }}, {{159}} },
  { Upg::QuantumStorm,    {{{768,"8b4166822602b0cb41a42d15ca563a0d.jpg"}                                             }}, {{"quantumstorm"                 }}, {{296}} },
  { Upg::Requiem,         {{{774,"81651841ae936b5090980c8fbf31fda3.jpg"}                                             }}, {{"requiem"                      }}, {{302}} },
  { Upg::ReysMF,          {{{485,"905ee2a41b7d3b6d1f76294b4cd3e99e.jpg"}                                             }}, {{"reysmillenniumfalcon"         }}, {{193}} },
  { Upg::Scimitar,        {{{542,"1380bed351b072e5c4df4bb776625fad.jpg"}                                             }}, {{"scimitar"                     }}, {{216}} },
  { Upg::ShadowCaster,    {{{378,"Card_art_XW_U_153.jpg"               }                                             }}, {{"shadowcaster"                 }}, {{160}} },
  { Upg::SlaveI,          {{{379,"Card_art_XW_U_154.jpg"               }                                             }}, {{"slavei"                       }}, {{161}} },
  { Upg::SlaveI_Sep,      {{{869,"d16195cbd5b4a9e1cfaf94b35dd188e7.jpg"}                                             }}, {{"slavei-swz82"                 }}, {{370}} },
  { Upg::SoullessOne,     {{{533,"36d9391908fe020dec45153ce9bd7d3d.jpg"}                                             }}, {{"soullessone"                  }}, {{229}} },
  { Upg::ST321,           {{{391,"Card_art_XW_U_124.jpg"               }                                             }}, {{"st321"                        }}, {{162}} },
  { Upg::Suppressor,      {{{775,"45c68a9c1b84a9ca1a6dd4ed5b618210.jpg"}                                             }}, {{"suppressor"                   }}, {{303}} },
  { Upg::TantiveIV,       {{{764,"de2147d3f99347d5c07789917402b470.jpg"}                                             }}, {{"tantiveiv"                    }}, {{293}} },
  { Upg::Thunderstrike,   {{{765,"4359306125f05df5481d93982c070c13.jpg"}                                             }}, {{"thunderstrike"                }}, {{309}} },
  { Upg::Trident,         {{{948,"b1d69fa239b5043759889c8fc890732a.jpg"}                                             }}, {{"trident"                      }}, {{406}} },
  { Upg::Vector,          {{{776,"63b122a7f37720ef1a7d58bacbc70d15.jpg"}                                             }}, {{"vector"                       }}, {{304}} },
  { Upg::Virago,          {{{380,"Card_art_XW_U_155.jpg"               }                                             }}, {{"virago"                       }}, {{163}} },

  { Upg::AdvProtTrp,      {{{262,"Card_art_XW_U_33.jpg"                }                                             }}, {{"advprotontorpedoes"           }}, {{134}} },
  { Upg::IonTrp,          {{{263,"Card_art_XW_U_34.jpg"                }                                             }}, {{"iontorpedoes"                 }}, {{135}} },
  { Upg::PlasmaTrp,       {{{579,"4123de29aa73a40eca130e1e75d9353b.jpg"}                                             }}, {{"plasmatorpedoes"              }}, {{234}} },
  { Upg::ProtTrp,         {{{264,"Card_art_XW_U_35.jpg"                }                                             }}, {{"protontorpedoes"              }}, {{136}} },
  { Upg::TrackingTrp,     {{{941,"cd20a2138b180c8ee2873194f6f9ab37.jpg"}                                             }}, {{"trackingtorpedoes"            }}, {{407}} },

  { Upg::DorsalTrt,       {{{260,"Card_art_XW_U_31.jpg"                }                                             }}, {{"dorsalturret"                 }}, {{137}} },
  { Upg::IonCanTrt,       {{{261,"Card_art_XW_U_32.jpg"                }                                             }}, {{"ioncannonturret"              }}, {{138}} },
};

// conditions
struct FFG_ConditionData  {};
struct XWS_ConditionData  { std::string id; };
struct YASB_ConditionData {};
struct ConditionEntry {
  Cnd cnd;
  std::optional<FFG_ConditionData>  ffg;
  std::optional<XWS_ConditionData>  xws;
  std::optional<YASB_ConditionData> yasb;
};
static std::vector<ConditionEntry> conditionData = {
  { Cnd::CompIntel, {}, {{"compromisingintel"}},     {} },
  { Cnd::Decoyed,   {}, {{"decoyed"}},               {} },
  { Cnd::Hunted,    {}, {{"hunted"}},                {} },
  { Cnd::ISYTDS,    {}, {{"illshowyouthedarkside"}}, {} },
  { Cnd::ITR,       {}, {{"itstheresistance"}},      {} },
  { Cnd::LstnDev,   {}, {{"listeningdevice"}},       {} },
  { Cnd::OptProt,   {}, {{"optimizedprototype"}},    {} },
  { Cnd::Rattled,   {}, {{"rattled"}},               {} },
  { Cnd::SupFire,   {}, {{"suppressivefire"}},       {} },
};



//
// functions
//
namespace ffg {
  // pilots
  PilotNotFound::PilotNotFound(Plt     p) : runtime_error("[ffg] Pilot not found (enum " + std::to_string((int)p) + ")") { }
  PilotNotFound::PilotNotFound(int16_t i) : runtime_error("[ffg] Pilot not found: '" + std::to_string(i) + "'") { }

  int16_t GetPilot(Plt plt) {
    for(const PilotEntry& pe : pilotData) {
      if((pe.plt == plt) && pe.ffg) {
    	return pe.ffg->id;
      }
    }
    throw PilotNotFound(plt);
  }

  Plt GetPilot(int16_t id) {
    for(const PilotEntry& pe : pilotData) {
      if(pe.ffg && (pe.ffg->id == id)) {
	return pe.plt;
      }
    }
    throw PilotNotFound(id);
  }

  std::string GetArt(Plt plt) {
    for(const PilotEntry& pe : pilotData) {
      if((pe.plt == plt) && pe.ffg && pe.ffg->art != "") {
	return "https://squadbuilder.fantasyflightgames.com/card_art/" + pe.ffg->art;
      }
    }
    throw PilotNotFound(plt);
  }

  std::string GetCard(Plt plt) {
    // since ffg quit hosting these, switch to a fan-made repo (which is a lot cleaner and easier!)
    std::string filename;
    switch(plt) {
    default: filename = xws::GetPilot(plt) + ".png";

    }
    return std::string(CARDBASE) + "src/images/En/pilots/" + filename;
  }

  // upgrades
  UpgradeNotFound::UpgradeNotFound(Upg     u) : runtime_error("[ffg] Upgrade not found (enum " + std::to_string((int)u) + ")") { }
  UpgradeNotFound::UpgradeNotFound(int16_t i) : runtime_error("[ffg] Upgrade not found: '" + std::to_string(i) + "'") { }

  int16_t GetUpgrade(Upg upg, uint16_t side) {
    for(const UpgradeEntry& ue : upgradeData) {
      if((ue.upg == upg) && ue.ffg && (ue.ffg->size() >= side+1)) {
	return (*ue.ffg)[side].id;
      }
    }
    throw UpgradeNotFound(upg);
  }

  Upg GetUpgrade(int16_t id, uint16_t side) {
    for(const UpgradeEntry& ue : upgradeData) {
      if(ue.ffg && (ue.ffg->size() >= side+1) && ((*ue.ffg)[side].id == id)) {
	return ue.upg;
      }
    }
    throw UpgradeNotFound(id);
  }

  std::string GetArt(Upg upg, uint16_t side) {
    for(const UpgradeEntry& ue : upgradeData) {
      if((ue.upg == upg) && ue.ffg && (ue.ffg->size() >= side+1) && (*ue.ffg)[side].art != "") {
    	return "https://squadbuilder.fantasyflightgames.com/card_art/" + (*ue.ffg)[side].art;
      }
    }
    throw UpgradeNotFound(upg);
  }

  std::string GetCard(Upg upg, uint16_t side) {
    std::string filename;
    // work around some naming differences
    switch(upg) {
    // xws errors
    case Upg::B6Prototype:     filename = "b6bladewingprototype1.png";                     break;
    case Upg::B6Prototype_CMD: filename = "b6bladewingprototype.png";                      break;
    case Upg::Bounty:          filename = (!side) ? "bounty.png" : "bountypaid-sideb.png"; break;
    case Upg::InIt:            filename = (!side) ? "initforthemoney.png" : "initforthemoney-sideb.png"; break;
    case Upg::TFarr:           filename = "TorynFarr.png"; break;
    // wrong sides
    case Upg::JediCmdr:        filename = (!side) ? "jedicommander-sideb.png" : "jedicommander.png"; break;
    case Upg::PhoenixSq:       filename = (!side) ? "phoenixsquadron-sideb.png" : "phoenixsquadron.png"; break;
    case Upg::ShadowWing:      filename = (!side) ? "shadowwing-sideb.png" : "shadowwing.png"; break;
    case Upg::SkystrikeAcaCla: filename = (!side) ? "skystrikeacademyclass-sideb.png" : "skystrikeacademyclass.png"; break;
    // I use Closed as first side and Open as second
    case Upg::IntSFoils:       filename = (!side) ? "integratedsfoils-sideb.png" : "integratedsfoils.png"; break;
    case Upg::PvtWing:         filename = (!side) ? "pivotwing-sideb.png" : "pivotwing.png"; break;
    case Upg::SmSFoils:        filename = (!side) ? "servomotorsfoils-sideb.png" : "servomotorsfoils.png"; break;
    case Upg::StabSFoils:      filename = (!side) ? "stabilizedsfoils-sideb.png" : "stabilizedsfoils.png"; break;
    // everything else is just xws.png or xws-sideb.png
    default: filename = (!side) ? xws::GetUpgrade(upg) + ".png" : xws::GetUpgrade(upg) + "-sideb.png";
    }
    return std::string(CARDBASE) + "src/images/En/upgrades/" + filename;
  }
}



namespace xws {
  // factions
  FactionNotFound::FactionNotFound(Fac f)           : runtime_error("[xws] Faction not found (enum " + std::to_string((int)f) + ")") { }
  FactionNotFound::FactionNotFound(std::string xws) : runtime_error("[xws] Faction not found '" + xws + "'") { }

  std::string GetFaction(Fac fac) {
    for(const FactionEntry& fe : factionData) {
      if((fe.fac == fac) && fe.xws) {
	return fe.xws->id;
      }
    }
    throw FactionNotFound(fac);
  }

  Fac GetFaction(std::string id) {
    for(const FactionEntry& fe : factionData) {
      if(fe.xws && (fe.xws->id == id)) {
	return fe.fac;
      }
    }
    throw FactionNotFound(id);
  }

  // ships
  ShipNotFound::ShipNotFound(Shp s)           : runtime_error("[xws] Ship not found (enum " + std::to_string((int)s) + ")") { }
  ShipNotFound::ShipNotFound(std::string xws) : runtime_error("[xws] Ship not found '" + xws + "'") { }

  std::string GetShip(Shp shp) {
    for(const ShipEntry& se : shipData) {
      if((se.shp == shp) && se.xws) {
	return se.xws->id;
      }
    }
    throw ShipNotFound(shp);
  }

  Shp GetShip(std::string id) {
    for(const ShipEntry& se : shipData) {
      if(se.xws && (se.xws->id == id)) {
	return se.shp;
      }
    }
    throw ShipNotFound(id);
  }

  // pilots
  PilotNotFound::PilotNotFound(Plt         p) : runtime_error("[xws] Pilot not found (enum " + std::to_string((int)p) + ")") { }
  PilotNotFound::PilotNotFound(std::string x) : runtime_error("[xws] Pilot not found: '" + x + "'") { }

  std::string GetPilot(Plt plt) {
    for(const PilotEntry& pe : pilotData) {
      if((pe.plt == plt) && pe.xws) {
    	return pe.xws->id;
      }
    }
    throw PilotNotFound(plt);
  }

  Plt GetPilot(std::string id) {
    for(const PilotEntry& pe : pilotData) {
      if(pe.xws && (pe.xws->id == id)) {
	return pe.plt;
      }
    }
    throw PilotNotFound(id);
  }

  // upgrade types
  UpgradeTypeNotFound::UpgradeTypeNotFound(UpT         u) : runtime_error("[xws] UpgradeType not found (enum " + std::to_string((int)u) + ")") { }
  UpgradeTypeNotFound::UpgradeTypeNotFound(std::string x) : runtime_error("[xws] UpgradeType not found: '" + x + "'") { }

  std::string GetUpgradeType(UpT upt) {
    for(const UpgradeTypeEntry& ute : upgradeTypeData) {
      if((ute.upt == upt) && ute.xws) {
	return ute.xws->id;
      }
    }
    throw UpgradeTypeNotFound(upt);
  }

  UpT GetUpgradeType(std::string id) {
    for(const UpgradeTypeEntry& ute : upgradeTypeData) {
      if(ute.xws && (ute.xws->id == id)) {
	return ute.upt;
      }
    }
    throw UpgradeTypeNotFound(id);
  }

  // upgrades
  UpgradeNotFound::UpgradeNotFound(Upg         u) : runtime_error("[xws] Upgrade not found (enum " + std::to_string((int)u) + ")") { }
  UpgradeNotFound::UpgradeNotFound(std::string x) : runtime_error("[xws] Upgrade not found: '" + x + "'") { }

  std::string GetUpgrade(Upg upg) {
    for(const UpgradeEntry& ue : upgradeData) {
      if((ue.upg == upg) && ue.xws) {
    	return ue.xws->id;
      }
    }
    throw UpgradeNotFound(upg);
  }

  Upg GetUpgrade(std::string id) {
    for(const UpgradeEntry& ue : upgradeData) {
      if(ue.xws && (ue.xws->id == id)) {
	return ue.upg;
      }
    }
    throw UpgradeNotFound(id);
  }

  // conditions
  ConditionNotFound::ConditionNotFound(Cnd         c) : runtime_error("[xws] Condition not found (enum " + std::to_string((int)c) + ")") { }
  ConditionNotFound::ConditionNotFound(std::string x) : runtime_error("[xws] Condition not found: '" + x + "'") { }

  std::string GetCondition(Cnd cnd) {
    for(const ConditionEntry& ce : conditionData) {
      if((ce.cnd == cnd) && ce.xws) {
	return ce.xws->id;
      }
    }
    throw ConditionNotFound(cnd);
  }

  Cnd GetCondition(std::string id) {
    for(const ConditionEntry& ce : conditionData) {
      if(ce.xws && (ce.xws->id == id)) {
	return ce.cnd;
      }
    }
    throw ConditionNotFound(id);
  }

  // squads
  SquadNotFound::SquadNotFound(std::string f) : runtime_error("[xws] Squad not found: '" + f + "'") { }

  Squad GetSquad(std::istream &x) {
    Squad sq;
    Json::Value root;
    x >> root;
    sq.SetName(root.get("name","").asString());
    sq.SetDescription(root.get("description","").asString());
    std::string f = root.get("faction","").asString();
    if(f != "") {
      sq.SetFac(xws::GetFaction(f));
    }
    // faction?
    for(int i=0; i<root["pilots"].size(); i++) {
      Json::Value jPilot = root["pilots"][i];
      Pilot p = Pilot::GetPilot(xws::GetPilot(jPilot.get("id","").asString()));
      Json::Value jUpgrades = jPilot["upgrades"];
      for(Json::ValueIterator iUpT = jUpgrades.begin(); iUpT != jUpgrades.end() ; iUpT++ ) {
	std::string upgradeType = iUpT.key().asString().c_str();
	Json::Value jUpT = jUpgrades[upgradeType];
	for(int i=0; i<jUpT.size(); i++) {
	  std::string upgrade = jUpT[i].asString();
	  Upgrade u = Upgrade::GetUpgrade(xws::GetUpgrade(upgrade));
	  p.ApplyUpgrade(u);
	}
      }
      sq.AddPilot(p);
    }
    return sq;
  }

  Squad GetSquad(std::string xwsFile) {
    std::ifstream xws(xwsFile, std::ifstream::binary);
    if(!xws.good()) { throw SquadNotFound(xwsFile); }
    return GetSquad(xws);
  }

  std::string GetSquad(Squad s) {
    int pc=0;
    Json::Value pilots;
    for(const Pilot& p : s.GetPilots()) {
      pilots[pc]["id"] = xws::GetPilot(p.GetPlt());
      pilots[pc]["name"] = xws::GetPilot(p.GetPlt());
      pilots[pc]["ship"] = xws::GetShip(p.GetShp());
      pilots[pc]["points"] = p.GetModCost() ? std::to_string(*p.GetModCost()) : "N/A";
      std::map<UpT, std::vector<Upgrade>> upgs;
      for(const Upgrade& u : p.GetAppliedUpgrades()) {
	upgs[u.GetUpT()].push_back(u);
      }

      Json::Value upgrades;
      for(const std::pair<const UpT, std::vector<Upgrade>>& up : upgs) {
	int uc=0;
	for(const Upgrade& u : up.second) {
	  upgrades[xws::GetUpgradeType(up.first)][uc] = xws::GetUpgrade(u.GetUpg());
	  uc++;
	}
      }
      pilots[pc]["upgrades"] = upgrades;
      pc++;
    }
    Json::Value root;
    root["name"] = s.GetName();
    root["description"] = s.GetDescription();
    root["faction"] = s.GetFac() ? xws::GetFaction(*s.GetFac()) : "";
    root["pilots"] = pilots;
    root["points"] = s.GetCost() ? std::to_string(*s.GetCost()) : "N/A";
    // just hardcode the vendor specific stuff for now
    root["vendor"]["xwing.cgi"]["builder"]       = "xwing.cgi";
    root["vendor"]["xwing.cgi"]["builderurlurl"] = "http://xhud.sirjorj.com/xwing.cgi";
    root["vendor"]["xwing.cgi"]["url"]           = "http://xhud.sirjorj.com/xwing.cgi";
    root["version"] = "2.0.0";
    Json::FastWriter w;
    return w.write(root);
  }
}



namespace yasb {
  // factions
  FactionNotFound::FactionNotFound(Fac         f) : runtime_error("Faction not found (enum " + std::to_string((int)f) + ")") { }
  FactionNotFound::FactionNotFound(std::string i) : runtime_error("Faction not found '" + i + "'") { }

  std::string GetFaction(Fac fac) {
    for(const FactionEntry& fe : factionData) {
      if((fe.fac == fac) && fe.yasb) {
	return fe.yasb->id;
      }
    }
    throw FactionNotFound(fac);
  }

  Fac GetFaction(std::string id) {
    for(const FactionEntry& fe : factionData) {
      if(fe.yasb && (fe.yasb->id == id)) {
	return fe.fac;
      }
    }
    throw FactionNotFound(id);
  }

  // pilots
  PilotNotFound::PilotNotFound(Plt     p) : runtime_error("Pilot not found (enum " + std::to_string((int)p) + ")") { }
  PilotNotFound::PilotNotFound(int32_t i) : runtime_error("Pilot not found: '" + std::to_string(i) + "'") { }

  int16_t GetPilot(Plt plt) {
    for(const PilotEntry& pe : pilotData) {
      if((pe.plt == plt) && pe.yasb) {
    	return pe.yasb->id;
      }
    }
    throw PilotNotFound(plt);
  }

  Plt GetPilot(int32_t id) {
    for(const PilotEntry& pe : pilotData) {
      if(pe.yasb && (pe.yasb->id == id)) {
	return pe.plt;
      }
    }
    throw PilotNotFound(id);
  }

  // upgrades
  UpgradeNotFound::UpgradeNotFound(Upg      u) : runtime_error("Upgrade not found (enum " + std::to_string((int)u) + ")") { }
  UpgradeNotFound::UpgradeNotFound(uint16_t i) : runtime_error("Upgrade not found: '" + std::to_string(i) + "'") { }

  uint16_t GetUpgrade(Upg upg) {
    for(const UpgradeEntry& ue : upgradeData) {
      if((ue.upg == upg) && ue.yasb) {
    	return ue.yasb->id;
      }
    }
    throw UpgradeNotFound(upg);
  }

  Upg GetUpgrade(uint16_t id) {
    for(const UpgradeEntry& ue : upgradeData) {
      if(ue.xws && (ue.yasb->id == id)) {
	return ue.upg;
      }
    }
    throw UpgradeNotFound(id);
  }

  // squad
  InvalidUrl::InvalidUrl(std::string f) : runtime_error("Invalid Url: '" + f + "'") { }

  std::string trim(std::string s) {
    // https://stackoverflow.com/q/25829143
    size_t first = s.find_first_not_of(' ');
    size_t last = s.find_last_not_of(' ');
    return s.substr(first, (last-first+1));
  }

  std::vector<std::string> tokenize(std::string s, const char DELIMITER) {
    std::vector<std::string> ret;
    size_t start = s.find_first_not_of(DELIMITER), end=start;
    while (start != std::string::npos){
      end = s.find(DELIMITER, start);
      ret.push_back(trim(s.substr(start, end-start)));
      start = s.find_first_not_of(DELIMITER, end);
    }
    return ret;
  }

  // http://www.zedwood.com/article/cpp-urlencode-function
  std::string UrlEncode(const std::string &s) {
    static const char lookup[]= "0123456789abcdef";
    std::stringstream e;
    for(int i=0, ix=s.length(); i<ix; i++)
      {
        const char& c = s[i];
        if ( (48 <= c && c <= 57) ||//0-9
             (65 <= c && c <= 90) ||//abc...xyz
             (97 <= c && c <= 122) || //ABC...XYZ
             (c=='-' || c=='_' || c=='.' || c=='~')
             )
          {
            e << c;
          }
        else
          {
            e << '%';
            e << lookup[ (c&0xF0)>>4 ];
            e << lookup[ (c&0x0F) ];
          }
      }
    return e.str();
  }

  std::string UrlDecode(std::string s) {
    std::string ret;
    char ch;
    int i, ii;
    for (i=0; i<s.length(); i++) {
      if (int(s[i])=='%') {
        sscanf(s.substr(i+1,2).c_str(), "%x", &ii);
        ch=static_cast<char>(ii);
        ret+=ch;
        i=i+2;
      } else {
        ret+=s[i];
      }
    }
    return (ret);
  }

  // ! is now Z, ; is now Y, : is now X, comma is now W

  Squad GetSquad(std::string url) {
    std::string start = "https://raithos.github.io/?";
    if(url.substr(0, start.size()) != start) {
      throw InvalidUrl(url);
    }
    Squad s;
    std::string data = url.substr(start.size());

    std::string squadStr = data;
    for(char& c : squadStr) {
      if(c == '!') { c = 'Z'; }
      if(c == ';') { c = 'Y'; }
      if(c == ':') { c = 'X'; }
      if(c == ',') { c = 'W'; }
    }

    printf("yasb: %s\n", squadStr.c_str());

    std::vector<std::string> tokens = tokenize(squadStr, '&');

    for(const std::string& kv : tokens) {
      int split = kv.find("=");
      std::string key = kv.substr(0, split);
      std::string val = kv.substr(split+1);

      if(key == "f") {
        s.SetFac(yasb::GetFaction(val));
      }
      else if(key == "sn") {
        s.SetName(UrlDecode(val));
      }
      else if(key == "d") {

        std::vector<std::string> d = tokenize(val, 'Z');
        // d[0] should be v5 - or v8 now
        // d[1] is the game type - im gonna ignore it
        // d[2] is points limit
        // d[3] is what we care about

        std::vector<std::string> shipstrings = tokenize(d[3], 'Y');
        for(const std::string& ss : shipstrings) {
          std::vector<std::string> sss = tokenize(ss, 'X');
          // sss[0] is the pilot
          // sss[1] is the upgrades
          Pilot p = Pilot::GetPilot(yasb::GetPilot(std::stoi(sss[0])));
          if(sss.size() > 1) {
            std::vector<std::string> us = tokenize(sss[1], 'W');
            for(const std::string& uss : us) {
              int32_t uid= std::stoi(uss);
              if(uid > -1) {
                Upgrade u = Upgrade::GetUpgrade(yasb::GetUpgrade((uint16_t)uid));
                p.ApplyUpgrade(u);
              }
            }
          }
          s.AddPilot(p);
        }
      }
      else if(key == "obs") {
        // dont care about obstacles
      }
      else {
        printf("Unhandled key '%s'\n", key.c_str());
      }
    }
    return s;
  }

  std::string GetSquad(Squad s) {
    std::stringstream ss;
    // url
    ss << "https://raithos.github.io/";
    // faction
    ss << "?f=" << (s.GetFac() ? yasb::GetFaction(*s.GetFac()) : "");
    // squad...
    ss << "&d=";
    // ...version
    ss << "v8Z";
    // ...standard squad (or something)
    ss << "sZ200Z";

    // ...the stuff
    for(const Pilot& p : s.GetPilots()) {
      ss << yasb::GetPilot(p.GetPlt()) << "X";
      std::list<Upgrade> upgs;
      {
        std::vector<Upgrade> tmp = p.GetAppliedUpgrades();
        std::copy(tmp.begin(), tmp.end(), std::back_inserter(upgs));
      }
      std::optional<UpgradeBar> bar = p.GetModUpgradeBar();
      if(bar) {
        bool isFirst = true;
        for(const UpgradeSlot& us : *bar) {
          if(isFirst) { isFirst = false; }
          else        { ss << "W"; }
          std::list<Upgrade>::iterator it = std::find_if(upgs.begin(), upgs.end(), [us](const Upgrade& u){ return us.CanEquip(u.GetUpT()); });
          if(it == upgs.end()) {
            //ss << -1;
          } else {
	    ss << yasb::GetUpgrade(it->GetUpg());
            upgs.erase(it);
          }
        }
        ss << "XY";
      }
    }
    // name
    ss << "&sn=" << UrlEncode(s.GetName());
    // obstacles (dont care)
    ss << "&obs=";
    return ss.str();
  }
}


//
// sanity checks
//
std::vector<Fac> GetMissingFactions() {
  std::vector<Fac> missing = Faction::GetAllFacs();
  for(const FactionEntry& fe : factionData) {
    missing.erase(std::remove(missing.begin(), missing.end(), fe.fac), missing.end());
  }
  return missing;
}

std::vector<Shp> GetMissingShips() {
  std::vector<Shp> missing = Ship::GetAllShps();
  for(const ShipEntry& se : shipData) {
    missing.erase(std::remove(missing.begin(), missing.end(), se.shp), missing.end());
  }
  return missing;
}

std::vector<Plt> GetMissingPilots() {
  std::vector<Plt> missing = Pilot::GetAllPlts();
  for(const PilotEntry& pe : pilotData) {
    missing.erase(std::remove(missing.begin(), missing.end(), pe.plt), missing.end());
  }
  return missing;
}

std::vector<UpT> GetMissingUpgradeTypes() {
  std::vector<UpT> missing = UpgradeType::GetAllUpTs();
  for(const UpgradeTypeEntry& ute : upgradeTypeData) {
    missing.erase(std::remove(missing.begin(), missing.end(), ute.upt), missing.end());
  }
  return missing;
}

std::vector<Upg> GetMissingUpgrades() {
  std::vector<Upg> missing = Upgrade::GetAllUpgs();
  for(const UpgradeEntry& ue : upgradeData) {
    missing.erase(std::remove(missing.begin(), missing.end(), ue.upg), missing.end());
  }
  return missing;
}
  /*
std::vector<Cnd> GetMissingConditions() {
  std::vector<Cnd> missing;
  for(const Condition& c : Condition::GetAllConditions()) {
    missing.push_back(c.GetCnd());
  }
  for(const ConditionEntry& ce : conditionData) {
    missing.erase(std::remove(missing.begin(), missing.end(), ce.cnd), missing.end());
  }
  return missing;
}
  */
}
}
