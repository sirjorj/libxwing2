#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

enum class Fac {
  //None       = 0x00,
  Rebel      = 0x01,
  Imperial   = 0x02,
  Scum       = 0x04,
  Resistance = 0x08,
  FirstOrder = 0x10,
  Republic   = 0x20,
  Separatist = 0x40,
  //All        = 0x7F,
};

class FactionNotFound : public std::runtime_error {
 public:
  FactionNotFound(Fac f);
  FactionNotFound(std::string xws);
};

class Faction {
 public:
  static Faction GetFaction(Fac s);
  static std::vector<Fac> GetAllFacs();
  Fac         GetFac()     const;
  std::string GetName()    const;
  std::string GetShort()   const;
  std::string Get3Letter() const;

 private:
  Fac         fac;
  std::string name;
  std::string shortName;

  static std::vector<Faction> factions;

  Faction(Fac         f,
          std::string n,
	  std::string s);
};

}
