#include "glyph.h"
#include <map>

namespace libxwing2 {



std::vector<ShipGlyph> ShipGlyph::shipGlyphs = {
  { Shp::Aggressor,        "aggressorassaultfighter",         "\u0069" },
  { Shp::AlphaClass,       "alphaclassstarwing",              "\u0026" },
  { Shp::ARC170,           "arc170starfighter",               "\u0063" },
  { Shp::ASF01BWing,       "asf01bwing",                      "\u0062" },
  { Shp::AttackShuttle,    "attackshuttle",                   "\u0067" },
  { Shp::Auzituck,         "auzituckgunship",                 "\u0040" },
  { Shp::Belbullab22,      "belbullab22starfighter",          "\u005b" },
  { Shp::BTANR2YWing,      "btanr2ywing",                     "\u007b" },
  { Shp::BTLA4YWing,       "btla4ywing",                      "\u0079" },
  { Shp::BTLBYWing,        "btlbywing",                       "\u003a" },
  { Shp::BTLS8KWing,       "btls8kwing",                      "\u006b" },
  { Shp::CR90,             "cr90corvette",                    "\u0032" },
  { Shp::CROC,             "croccruiser",                     "\u0035" },
  { Shp::CustYT1300,       "customizedyt1300lightfreighter",  "\u0057" },
  { Shp::Delta7,           "delta7aethersprite",              "\u005c" },
  { Shp::DroidTriFighter,  "droidtrifighter",                 "\u002b" },
  { Shp::EscCraft,         "escapecraft",                     "\u0058" },
  { Shp::Eta2Actis,        "eta2actis",                       "\u002d" },
  { Shp::EWing,            "ewing",                           "\u0065" },
  { Shp::FangFighter,      "fangfighter",                     "\u004d" },
  { Shp::Fireball,         "fireball",                        "\u0030" },
  { Shp::Firespray,        "firesprayclasspatrolcraft",       "\u0066" },
  { Shp::G1A,              "g1astarfighter",                  "\u006e" },
  { Shp::Gozanti,          "gozanticlasscruiser",             "\u0034" },
  { Shp::GR75,             "gr75mediumtransport",             "\u0031" },
  { Shp::HMP,              "hmpdroidgunship",                 "\u002e" },
  { Shp::HWK290,           "hwk290lightfreighter",            "\u0068" },
  { Shp::Hyena,            "hyenaclassdroidbomber",           "\u003d" },
  { Shp::JM5K,             "jumpmaster5000",                  "\u0070" },
  { Shp::Kihraxz,          "kihraxzfighter",                  "\u0072" },
  { Shp::LAAT,             "laatigunship",                    "\u002f" },
  { Shp::Lambda,           "lambdaclasst4ashuttle",           "\u006c" },
  { Shp::LancerClass,      "lancerclasspursuitcraft",         "\u004c" },
  { Shp::M12LKimogila,     "m12lkimogilafighter",             "\u004b" },
  { Shp::M3A,              "m3ainterceptor",                  "\u0073" },
  { Shp::MG100,            "mg100starfortresssf17",           "\u005a" },
  { Shp::ModifiedTIEln,    "modifiedtielnfighter",            "\u0043" },
  { Shp::ModYT1300,        "modifiedyt1300lightfreighter",    "\u006d" },
  { Shp::N1,               "nabooroyaln1starfighter",         "\u003c" },
  { Shp::Nantex,           "nantexclassstarfighter",          "\u003b" },
  { Shp::NimbusClass,      "nimbusclassvwing",                "\u002c" },
  { Shp::Quadjumper,       "quadrijettransferspacetug",       "\u0071" },
  { Shp::Raider,           "raiderclasscorvette",             "\u0033" },
  { Shp::ResTransport,     "resistancetransport",             "\u003e" },
  { Shp::ResTransportPod,  "resistancetransportpod",          "\u003f" },
  { Shp::RZ1AWing,         "rz1awing",                        "\u0061" },
  { Shp::RZ2AWing,         "rz2awing",                        "\u0045" },
  { Shp::ScaYT1300,        "scavengedyt1300",                 "\u0059" },
  { Shp::Scurrg,           "scurrgh6bomber",                  "\u0048" },
  { Shp::Sheathipede,      "sheathipedeclassshuttle",         "\u0025" },
  { Shp::SithInf,          "sithinfiltrator",                 "\u005d" },
  { Shp::StarViper,        "starviperclassattackplatform",    "\u0076" },
  { Shp::Syliure,          "syliureclasshyperspacering",      "\u002a" },
  { Shp::T65XWing,         "t65xwing",                        "\u0078" },
  { Shp::T70XWing,         "t70xwing",                        "\u0077" },
  { Shp::TIEAdvV1,         "tieadvancedv1",                   "\u0052" },
  { Shp::TIEAdvX1,         "tieadvancedx1",                   "\u0041" },
  { Shp::TIEagAggressor,   "tieagaggressor",                  "\u0060" },
  { Shp::TIEbaInterceptor, "tiebainterceptor",                "\u006a" },
  { Shp::TIEcaPunisher,    "tiecapunisher",                   "\u004e" },
  { Shp::TIEdDefender,     "tieddefender",                    "\u0044" },
  { Shp::TIEfoFighter,     "tiefofighter",                    "\u004f" },
  { Shp::TIEinInterceptor, "tieinterceptor",                  "\u0049" },
  { Shp::TIElnFighter,     "tielnfighter",                    "\u0046" },
  { Shp::TIEphPhantom,     "tiephphantom",                    "\u0050" },
  { Shp::TIErbHeavy,       "tierbheavy",                      "\u004a" },
  { Shp::TIEReaper,        "tiereaper",                       "\u0056" },
  { Shp::TIEsaBomber,      "tiesabomber",                     "\u0042" },
  { Shp::TIEseBomber,      "tiesebomber",                     "\u0021" },
  { Shp::TIEsfFighter,     "tiesffighter",                    "\u0053" },
  { Shp::TIEskStriker,     "tieskstriker",                    "\u0054" },
  { Shp::TIEvnSilencer,    "tiesilencer",                     "\u0024" },
  { Shp::TIEwiWhisperMod,  "tiewiwhispermodifiedinterceptor", "\u0023" },
  { Shp::Trident,          "tridentclassassaultship",         "\u0036" },
  { Shp::Upsilon,          "upsilonclassshuttle",             "\u0055" },
  { Shp::UWing,            "ut60duwing",                      "\u0075" },
  { Shp::V19,              "v19torrentstarfighter",           "\u005e" },
  { Shp::VCX100,           "vcx100lightfreighter",            "\u0047" },
  { Shp::VT49,             "vt49decimator",                   "\u0064" },
  { Shp::Vulture,          "vultureclassdroidfighter",        "\u005f" },
  { Shp::Xi,               "xiiclasslightshuttle",            "\u0051" },
  { Shp::YT2400,           "yt2400lightfreighter",            "\u006f" },
  { Shp::YV666,            "yv666lightfreighter",             "\u0074" },
  { Shp::Z95,              "z95af4headhunter",                "\u007a" },
};

ShipGlyphNotFound::ShipGlyphNotFound(Shp s) : runtime_error("Ship not found: (enum " + std::to_string((int)s) + ")") {  }

ShipGlyph ShipGlyph::GetShipGlyph(Shp shp) {
  for(const ShipGlyph &sg : ShipGlyph::shipGlyphs) {
    if(sg.GetShp() == shp) {
      return sg;
    }
  }
  throw ShipGlyphNotFound(shp);
}

std::string ShipGlyph::GetGlyph(Shp s) {
  try        { return ShipGlyph::GetShipGlyph(s).GetGlyph(); }
  catch(...) { return ""; }
}

  //std::vector<ShipGlyph> ShipGlyph::GetAllShipGlyphs() { return ShipGlyph::shipGlyphs; }

Shp         ShipGlyph::GetShp()      const { return this->shp; }
std::string ShipGlyph::GetScssName() const { return this->scssName; }
std::string ShipGlyph::GetGlyph()    const { return this->glyph; }

ShipGlyph::ShipGlyph(Shp t, std::string scss, std::string gly)
  : shp(t), scssName(scss), glyph(gly) { }





std::vector<IconGlyph> IconGlyph::iconGlyphs = {
  { Ico::agility,                               "agility",                               "",                    "\u005e" },
  { Ico::astromech,                             "astromech",                             "{ASTROMECH}",         "\u0041" },
  { Ico::attack,                                "attack",                                "",                    "\u0025" },
  { Ico::bankleft,                              "bankleft",                              "{LBANK}",             "\u0037" },
  { Ico::bankright,                             "bankright",                             "{RBANK}",             "\u0039" },
  { Ico::barrelroll,                            "barrelroll",                            "{BARRELROLL}",        "\u0072" },
  { Ico::base_all,                              "base-all",                              "",                    "\u00c0" },
  { Ico::base_small,                            "base-small",                            "",                    "\u00c1" },
  { Ico::base_medium,                           "base-medium",                           "",                    "\u00c2" },
  { Ico::base_large,                            "base-large",                            "",                    "\u00c3" },
  { Ico::boost,                                 "boost",                                 "{BOOST}",             "\u0062" },
  { Ico::bullseyearc,                           "bullseyearc",                           "{BULLSEYEARC}",       "\u007d" },
  { Ico::calculate,                             "calculate",                             "{CALCULATE}",         "\u0061" },
  { Ico::cannon,                                "cannon",                                "{CANNON}",            "\u0043" },
  { Ico::cargo,                                 "cargo",                                 "{CARGO}",             "\u0047" },
  { Ico::charge,                                "charge",                                "{CHARGE}",            "\u0067" },
  { Ico::cloak,                                 "cloak",                                 "{CLOAK}",             "\u006b" },
  { Ico::command,                               "command",                               "",                    "\u0056" },
  { Ico::condition_outline,                     "condition-outline",                     "",                    "\u00c6" },
  { Ico::condition,                             "condition",                             "",                    "\u00b0" },
  { Ico::config,                                "config",                                "{CONFIG}",            "\u006e" },
  { Ico::coordinate,                            "coordinate",                            "{COORDINATE}",        "\u006f" },
  { Ico::crew,                                  "crew",                                  "{CREW}",              "\u0057" },
  { Ico::crit,                                  "crit",                                  "{CRIT}",              "\u0063" },
  { Ico::dalan_bankleft,                        "dalan-bankleft",                        "",                    "\u005b" },
  { Ico::dalan_bankright,                       "dalan-bankright",                       "",                    "\u005d" },
  { Ico::device,                                "device",                                "{PAYLOAD}",           "\u0042" },
  { Ico::deplete,                               "deplete",                               "",                    "\u0044" },
  { Ico::doubleturretarc,                       "doubleturretarc",                       "{DOUBLETURRETARC}",   "\u0071" },
  { Ico::doublerecurring,                       "doublerecurring",                       "",                    "\u005f" },
  { Ico::empire,                                "empire",                                "",                    "\u0040" },
  { Ico::energy,                                "energy",                                "{ENERGY}",            "\u0028" },
  { Ico::epic,                                  "epic",                                  "",                    "\u0029" },
  { Ico::evade,                                 "evade",                                 "{EVADE}",             "\u0065" },
  { Ico::firstorder,                            "firstorder",                            "",                    "\u002b" },
  { Ico::focus,                                 "focus",                                 "{FOCUS}",             "\u0066" },
  { Ico::forcecharge,                           "forcecharge",                           "{FORCE}",             "\u0068" },
  { Ico::forcepower,                            "forcepower",                            "",                    "\u0046" },
  { Ico::frontarc,                              "frontarc",                              "{FRONTARC}",          "\u007b" },
  { Ico::fullfrontarc,                          "fullfrontarc",                          "{FULLFRONTARC}",      "\u007e" },
  { Ico::fullreararc,                           "fullreararc",                           "{FULLREARARC}",       "\u00a1" },
  { Ico::gunner,                                "gunner",                                "{GUNNER}",            "\u0059" },
  { Ico::hardpoint,                             "hardpoint",                             "{HARDPOINT}",         "\u0048" },
  { Ico::helmet_imperial,                       "helmet-imperial",                       "",                    "\u0079" },
  { Ico::helmet_rebel,                          "helmet-rebel",                          "",                    "\u0078" },
  { Ico::helmet_scum,                           "helmet-scum",                           "",                    "\u007a" },
  { Ico::hit,                                   "hit",                                   "{HIT}",               "\u0064" },
  { Ico::hull,                                  "hull",                                  "",                    "\u0026" },
  { Ico::ig88d_sloopleft,                       "ig88d-sloopleft",                       "",                    "\u0022" },
  { Ico::ig88d_sloopright,                      "ig88d-sloopright",                      "",                    "\u0027" },
  { Ico::illicit,                               "illicit",                               "{ILLICIT}",           "\u0049" },
  { Ico::jam,                                   "jam",                                   "{JAM}",               "\u006a" },
  { Ico::kturn,                                 "kturn",                                 "{KTURN}",             "\u0032" },
  { Ico::leftarc,                               "leftarc",                               "{LEFTARC}",           "\u00a3" },
  { Ico::linked,                                "linked",                                "{LINKED}",            "\u003e" },
  { Ico::lock,                                  "lock",                                  "{LOCK}",              "\u006c" },
  { Ico::missile,                               "missile",                               "{MISSILE}",           "\u004d" },
  { Ico::modification,                          "modification",                          "{MODIFICATION}",      "\u006d" },
  { Ico::negativerecurring,                     "negativerecurring",                     "",                    "\u0081" },
  { Ico::obstacle_core2asteroid0_outline,       "obstacle-core2asteroid0-outline",       "",                    "\u0109" },
  { Ico::obstacle_core2asteroid0,               "obstacle-core2asteroid0",               "",                    "\u0125" },
  { Ico::obstacle_core2asteroid1_outline,       "obstacle-core2asteroid1-outline",       "",                    "\u010c" },
  { Ico::obstacle_core2asteroid1,               "obstacle-core2asteroid1",               "",                    "\u0128" },
  { Ico::obstacle_core2asteroid2_outline,       "obstacle-core2asteroid2-outline",       "",                    "\u010a" },
  { Ico::obstacle_core2asteroid2,               "obstacle-core2asteroid2",               "",                    "\u0126" },
  { Ico::obstacle_core2asteroid3_outline,       "obstacle-core2asteroid3-outline",       "",                    "\u010b" },
  { Ico::obstacle_core2asteroid3,               "obstacle-core2asteroid3",               "",                    "\u0127" },
  { Ico::obstacle_core2asteroid4_outline,       "obstacle-core2asteroid4-outline",       "",                    "\u010d" },
  { Ico::obstacle_core2asteroid4,               "obstacle-core2asteroid4",               "",                    "\u0129" },
  { Ico::obstacle_core2asteroid5_outline,       "obstacle-core2asteroid5-outline",       "",                    "\u010e" },
  { Ico::obstacle_core2asteroid5,               "obstacle-core2asteroid5",               "",                    "\u012a" },
  { Ico::obstacle_coreasteroid0_outline,        "obstacle-coreasteroid0-outline",        "",                    "\u0102" },
  { Ico::obstacle_coreasteroid0,                "obstacle-coreasteroid0",                "",                    "\u011e" },
  { Ico::obstacle_coreasteroid1_outline,        "obstacle-coreasteroid1-outline",        "",                    "\u0103" },
  { Ico::obstacle_coreasteroid1,                "obstacle-coreasteroid1",                "",                    "\u011f" },
  { Ico::obstacle_coreasteroid2_outline,        "obstacle-coreasteroid2-outline",        "",                    "\u0101" },
  { Ico::obstacle_coreasteroid2,                "obstacle-coreasteroid2",                "",                    "\u011d" },
  { Ico::obstacle_coreasteroid3_outline,        "obstacle-coreasteroid3-outline",        "",                    "\u0105" },
  { Ico::obstacle_coreasteroid3,                "obstacle-coreasteroid3",                "",                    "\u0121" },
  { Ico::obstacle_coreasteroid4_outline,        "obstacle-coreasteroid4-outline",        "",                    "\u0104" },
  { Ico::obstacle_coreasteroid4,                "obstacle-coreasteroid4",                "",                    "\u0120" },
  { Ico::obstacle_coreasteroid5_outline,        "obstacle-coreasteroid5-outline",        "",                    "\u0100" },
  { Ico::obstacle_coreasteroid5,                "obstacle-coreasteroid5",                "",                    "\u011c" },
  { Ico::obstacle_vt49decimatordebris0_outline, "obstacle-vt49decimatordebris0-outline", "",                    "\u0110" },
  { Ico::obstacle_vt49decimatordebris0,         "obstacle-vt49decimatordebris0",         "",                    "\u012c" },
  { Ico::obstacle_vt49decimatordebris1_outline, "obstacle-vt49decimatordebris1-outline", "",                    "\u0111" },
  { Ico::obstacle_vt49decimatordebris1,         "obstacle-vt49decimatordebris1",         "",                    "\u012d" },
  { Ico::obstacle_vt49decimatordebris2_outline, "obstacle-vt49decimatordebris2-outline", "",                    "\u0107" },
  { Ico::obstacle_vt49decimatordebris2,         "obstacle-vt49decimatordebris2",         "",                    "\u0123" },
  { Ico::obstacle_yt2400debris0_outline,        "obstacle-yt2400debris0-outline",        "",                    "\u010f" },
  { Ico::obstacle_yt2400debris0,                "obstacle-yt2400debris0",                "",                    "\u012b" },
  { Ico::obstacle_yt2400debris1_outline,        "obstacle-yt2400debris1-outline",        "",                    "\u0108" },
  { Ico::obstacle_yt2400debris1,                "obstacle-yt2400debris1",                "",                    "\u0124" },
  { Ico::obstacle_yt2400debris2_outline,        "obstacle-yt2400debris2-outline",        "",                    "\u0106" },
  { Ico::obstacle_yt2400debris2,                "obstacle-yt2400debris2",                "",                    "\u0122" },
  { Ico::point,                                 "point",                                 "{COUNTER}",           "\u00cc" },
  { Ico::rangebonusindicator,                   "rangebonusindicator",                   "",                    "\u003f" },
  { Ico::reararc,                               "reararc",                               "{REARARC}",           "\u007c" },
  { Ico::rebel_outline,                         "rebel-outline",                         "",                    "\u002d" },
  { Ico::rebel,                                 "rebel",                                 "",                    "\u0021" },
  { Ico::recover,                               "recover",                               "",                    "\u0076" },
  { Ico::recurring,                             "recurring",                             "",                    "\u0060" },
  { Ico::reinforce,                             "reinforce",                             "{REINFORCE}",         "\u0069" },
  { Ico::reload,                                "reload",                                "{RELOAD}",            "\u003d" },
  { Ico::republic,                              "republic",                              "",                    "\u002f" },
  { Ico::reversebankleft,                       "reversebankleft",                       "",                    "\u004a" },
  { Ico::reversebankright,                      "reversebankright",                      "",                    "\u004c" },
  { Ico::reversestraight,                       "reversestraight",                       "{REVERSESTRAIGHT}",   "\u004b" },
  { Ico::rightarc,                              "rightarc",                              "{RIGHTARC}",          "\u00a2" },
  { Ico::rotatearc,                             "rotatearc",                             "{ROTATEARC}",         "\u0052" },
  { Ico::scum,                                  "scum",                                  "",                    "\u0023" },
  { Ico::separatists,                           "separatists",                           "",                    "\u002e" },
  { Ico::sensor,                                "sensor",                                "{SENSOR}",            "\u0053" },
  { Ico::shield,                                "shield",                                "{SHIELD}",            "\u002a" },
  { Ico::singleturretarc,                       "singleturretarc",                       "{SINGLETURRETARC}",   "\u0070" },
  { Ico::slam,                                  "slam",                                  "{SLAM}",              "\u0073" },
  { Ico::sloopleft,                             "sloopleft",                             "{LSLOOP}",            "\u0031" },
  { Ico::sloopright,                            "sloopright",                            "{RSLOOP}",            "\u0033" },
  { Ico::squad_point_cost,                      "squad-point-cost",                      "",                    "\u0030" },
  { Ico::stop,                                  "stop",                                  "{STATIONARY}",        "\u0035" },
  { Ico::straight,                              "straight",                              "{STRAIGHT}",          "\u0038" },
  { Ico::strain,                                "strain",                                "{STRAIN}",            "\u004e" },
  { Ico::tacticalrelay,                         "tactical-relay",                        "{TACTICALRELAY}",     "\u005a" },
  { Ico::talent,                                "talent",                                "{TALENT}",            "\u0045" },
  { Ico::team,                                  "team",                                  "{TEAM}",              "\u0054" },
  { Ico::tech,                                  "tech",                                  "{TECH}",              "\u0058" },
  { Ico::title,                                 "title",                                 "",                    "\u0074" },
  { Ico::token_calculate_outline,               "token-calculate-outline",               "",                    "\u00cb" },
  { Ico::token_calculate,                       "token-calculate",                       "",                    "\u00ca" },
  { Ico::token_charge_outline,                  "token-charge-outline",                  "",                    "\u00d3" },
  { Ico::token_charge,                          "token-charge",                          "",                    "\u00d2" },
  { Ico::token_crit_outline,                    "token-crit-outline",                    "",                    "\u00e8" },
  { Ico::token_crit,                            "token-crit",                            "",                    "\u00c7" },
  { Ico::token_cloak_outline,                   "token-cloak-outline",                   "",                    "\u00f4" },
  { Ico::token_cloak,                           "token-cloak",                           "",                    "\u00e5" },
  { Ico::token_deplete_outline,                 "token-deplete-outline",                 "",                    "\u00f4" },
  { Ico::token_deplete,                         "token-deplete",                         "",                    "\u00e5" },
  { Ico::token_evade_outline,                   "token-evade-outline",                   "",                    "\u00e9" },
  { Ico::token_evade,                           "token-evade",                           "",                    "\u00c5" },
  { Ico::token_focus_outline,                   "token-focus-outline",                   "",                    "\u00e7" },
  { Ico::token_focus,                           "token-focus",                           "",                    "\u00c4" },
  { Ico::token_force_outline,                   "token-force-outline",                   "",                    "\u00d5" },
  { Ico::token_force,                           "token-force",                           "",                    "\u00d4" },
  { Ico::token_disarm_outline,                  "token-disarm-outline",                  "",                    "\u00ed" },
  { Ico::token_disarm,                          "token-disarm",                          "",                    "\u00d6" },
  { Ico::token_ion_outline,                     "token-ion-outline",                     "",                    "\u00f3" },
  { Ico::token_ion,                             "token-ion",                             "",                    "\u00e4" },
  { Ico::token_jam_outline,                     "token-jam-outline",                     "",                    "\u00f0" },
  { Ico::token_jam,                             "token-jam",                             "",                    "\u00e1" },
  { Ico::token_lock_outline,                    "token-lock-outline",                    "",                    "\u00f2" },
  { Ico::token_lock,                            "token-lock",                            "",                    "\u00e3" },
  { Ico::token_point_1_outline,                 "token-point-1-outline",                 "",                    "\u00f4" },
  { Ico::token_point_1,                         "token-point-1",                         "",                    "\u00e5" },
  { Ico::token_point_3_outline,                 "token-point-3-outline",                 "",                    "\u00f4" },
  { Ico::token_point_3,                         "token-point-3",                         "",                    "\u00e5" },
  { Ico::token_reinforce_outline,               "token-reinforce-outline",               "",                    "\u00ec" },
  { Ico::token_reinforce,                       "token-reinforce",                       "",                    "\u00dc" },
  { Ico::token_reinforceaft_outline,            "token-reinforceaft-outline",            "",                    "\u00ee" },
  { Ico::token_reinforceaft,                    "token-reinforceaft",                    "",                    "\u00dd" },
  { Ico::token_shield_outline,                  "token-shield-outline",                  "",                    "\u00eb" },
  { Ico::token_shield,                          "token-shield",                          "",                    "\u00d1" },
  { Ico::token_strain_outline,                  "token-strain-outline",                  "",                    "\u00f4" },
  { Ico::token_strain,                          "token-strain",                          "",                    "\u00e5" },
  { Ico::token_stress_outline,                  "token-stress-outline",                  "",                    "\u00ea" },
  { Ico::token_stress,                          "token-stress",                          "",                    "\u00c9" },
  { Ico::token_tractor_outline,                 "token-tractor-outline",                 "",                    "\u00ef" },
  { Ico::token_tractor,                         "token-tractor",                         "",                    "\u00e0" },
  { Ico::torpedo,                               "torpedo",                               "{TORPEDO}",           "\u0050" },
  { Ico::triplerecurring,                       "triplerecurring",                       "",                    "\u0080" },
  { Ico::trollleft,                             "trollleft",                             "{LTROLL}",            "\u003a" },
  { Ico::trollright,                            "trollright",                            "{RTROLL}",            "\u003b" },
  { Ico::turnleft,                              "turnleft",                              "{LTURN}",             "\u0034" },
  { Ico::turnright,                             "turnright",                             "{RTURN}",             "\u0036" },
  { Ico::turret,                                "turret",                                "{TURRET}",            "\u0055" },
  { Ico::unique_outline,                        "unique-outline",                        "",                    "\u2022" },
  { Ico::unique,                                "unique",                                "",                    "\u0075" },
  // I added this as a way to return nothing without throwing an exception
  { Ico::blank,                                 "",                                                            "", ""       },
};

IconGlyphNotFound::IconGlyphNotFound(Ico i) : runtime_error("Icon not found: (enum " + std::to_string((int)i) + ")") {  }



Ico IconGlyph::ActToIco(Act a) {
  switch(a) {
  case Act::Focus:      return Ico::focus;
  case Act::Lock:       return Ico::lock;
  case Act::BarrelRoll: return Ico::barrelroll;
  case Act::Reload:     return Ico::reload;
  case Act::Reinforce:  return Ico::reinforce;
  case Act::Boost:      return Ico::boost;
  case Act::Coordinate: return Ico::coordinate;
  case Act::Jam:        return Ico::jam;
  case Act::Evade:      return Ico::evade;
  case Act::Cloak:      return Ico::cloak;
  case Act::RotateArc:  return Ico::rotatearc;
  case Act::Calculate:  return Ico::calculate;
  case Act::SLAM:       return Ico::slam;
  default:              return Ico::blank;
  }
}

Ico IconGlyph::FacToIco(Fac f) {
  switch(f) {
  case Fac::Rebel:          return Ico::rebel;
  case Fac::Imperial:       return Ico::empire;
  case Fac::Scum:           return Ico::scum;
  case Fac::Resistance:     return Ico::rebel_outline;
  case Fac::FirstOrder:     return Ico::firstorder;
  case Fac::Separatist:     return Ico::separatists;
  case Fac::Republic:       return Ico::republic;
  default:                  return Ico::blank;
  }
}

Ico IconGlyph::UpgToIco(UpT u) {
  switch(u) {
  case UpT::Astro:     return Ico::astromech;
  case UpT::Cannon:    return Ico::cannon;
  case UpT::Cargo:     return Ico::cargo;
  case UpT::Command:   return Ico::command;
  case UpT::Config:    return Ico::config;
  case UpT::Crew:      return Ico::crew;
  case UpT::Force:     return Ico::forcepower;
  case UpT::Gunner:    return Ico::gunner;
  case UpT::Hardpoint: return Ico::hardpoint;
  case UpT::Illicit:   return Ico::illicit;
  case UpT::Missile:   return Ico::missile;
  case UpT::Mod:       return Ico::modification;
  case UpT::Payload:   return Ico::device;
  case UpT::Sensor:    return Ico::sensor;
  case UpT::TactRel:   return Ico::tacticalrelay;
  case UpT::Talent:    return Ico::talent;
  case UpT::Team:      return Ico::team;
  case UpT::Tech:      return Ico::tech;
  case UpT::Title:     return Ico::title;
  case UpT::Torpedo:   return Ico::torpedo;
  case UpT::Turret:    return Ico::turret;
  default:             return Ico::blank;
  }
}

Ico IconGlyph::AtDToIco(AtD a) {
  switch(a) {
  case AtD::Blank: return Ico::blank;
  case AtD::Focus: return Ico::focus;
  case AtD::Hit:   return Ico::hit;
  case AtD::Crit:  return Ico::crit;
  default:         return Ico::blank;
  }
}

Ico IconGlyph::DeDToIco(DeD d) {
  switch(d) {
  case DeD::Blank: return Ico::blank;
  case DeD::Focus: return Ico::focus;
  case DeD::Evade: return Ico::evade;
  default:          return Ico::blank;
  }
}

Ico IconGlyph::ArcToIco(Arc a) {
  switch(a) {
  case Arc::None:         return Ico::blank;
  case Arc::Front:        return Ico::frontarc;
  case Arc::Rear:         return Ico::reararc;
  case Arc::Left:         return Ico::leftarc;
  case Arc::Right:        return Ico::rightarc;
  case Arc::Bullseye:     return Ico::bullseyearc;
  case Arc::FullFront:    return Ico::fullfrontarc;
  case Arc::FullRear:     return Ico::fullreararc;
  case Arc::SingleTurret: return Ico::singleturretarc;
  case Arc::DoubleTurret: return Ico::doubleturretarc;
  default:                return Ico::blank;
  }
}

Ico IconGlyph::BrnToIco(Brn b) {
  switch(b) {
  case Brn::LTurn:       return Ico::turnleft;
  case Brn::LBank:       return Ico::bankleft;
  case Brn::Straight:    return Ico::straight;
  case Brn::RBank:       return Ico::bankright;
  case Brn::RTurn:       return Ico::turnright;
  case Brn::KTurn:       return Ico::kturn;
  case Brn::Stationary:  return Ico::stop;
  case Brn::LSloop:      return Ico::sloopleft;
  case Brn::RSloop:      return Ico::sloopright;
  case Brn::LTroll:      return Ico::trollleft;
  case Brn::RTroll:      return Ico::trollright;
  case Brn::RevLBank:    return Ico::reversebankleft;
  case Brn::RevStraight: return Ico::reversestraight;
  case Brn::RevRBank:    return Ico::reversebankright;
  default:               return Ico::blank;
  }
}



IconGlyph   IconGlyph::GetIconGlyph(Ico ico) {
  for(const IconGlyph &ig : IconGlyph::iconGlyphs) {
    if(ig.GetIco() == ico) {
      return ig;
    }
  }
  throw IconGlyphNotFound(ico);
}

std::string IconGlyph::GetGlyph(Ico i) {
  try        { return IconGlyph::GetIconGlyph(i).GetGlyph(); }
  catch(...) { return ""; }
}

std::string IconGlyph::GetGlyph(std::string ctph) {
  if(ctph == "") { return ""; }
  for(IconGlyph &ig : IconGlyph::iconGlyphs) {
    if(ig.GetCardTextPlaceholder() == ctph) {
      return ig.GetGlyph();
    }
  }
  return ctph;
}

IconGlyph IconGlyph::GetIconGlyph(Act a) { return IconGlyph::GetIconGlyph(ActToIco(a)); }
std::string IconGlyph::GetGlyph(Act a)   { return IconGlyph::GetGlyph(ActToIco(a));     }

IconGlyph IconGlyph::GetIconGlyph(Fac f) { return IconGlyph::GetIconGlyph(FacToIco(f)); }
std::string IconGlyph::GetGlyph(Fac f)   { return IconGlyph::GetGlyph(FacToIco(f));     }

IconGlyph IconGlyph::GetIconGlyph(UpT u) { return IconGlyph::GetIconGlyph(UpgToIco(u)); }
std::string IconGlyph::GetGlyph(UpT u)   { return IconGlyph::GetGlyph(UpgToIco(u));     }

//IconGlyph IconGlyph::GetIconGlyphEx(UpT u) { return IconGlyph::GetIconGlyph(ExToIco(u)); }
//std::string IconGlyph::GetGlyphEx(UpT u)   { return IconGlyph::GetGlyph(ExToIco(u));     }

IconGlyph    IconGlyph::GetIconGlyph(AtD a) { return IconGlyph::GetIconGlyph(AtDToIco(a)); }
std::string  IconGlyph::GetGlyph(AtD a)     { return IconGlyph::GetGlyph(AtDToIco(a));     }
IconGlyph    IconGlyph::GetIconGlyph(DeD d) { return IconGlyph::GetIconGlyph(DeDToIco(d)); }
std::string  IconGlyph::GetGlyph(DeD d)     { return IconGlyph::GetGlyph(DeDToIco(d));     }

IconGlyph IconGlyph::GetIconGlyph(Arc a) { return IconGlyph::GetIconGlyph(ArcToIco(a)); }
std::string IconGlyph::GetGlyph(Arc a)   { return IconGlyph::GetGlyph(ArcToIco(a));     }

IconGlyph IconGlyph::GetIconGlyph(Brn b) { return IconGlyph::GetIconGlyph(BrnToIco(b)); }
std::string IconGlyph::GetGlyph(Brn b)   { return IconGlyph::GetGlyph(BrnToIco(b));     }

std::vector<Ico> IconGlyph::GetAllIcos() {
  std::vector<Ico> ret;
  for(const IconGlyph& iconGlyph : IconGlyph::iconGlyphs) {
    ret.push_back(iconGlyph.GetIco());
  }
  return ret;
}

Ico         IconGlyph::GetIco()                 const { return this->ico; }
std::string IconGlyph::GetScssName()            const { return this->scssName; }
std::string IconGlyph::GetCardTextPlaceholder() const { return this->cardTextPlaceholder; }
std::string IconGlyph::GetGlyph()               const { return this->glyph; }

IconGlyph::IconGlyph(Ico i, std::string scss, std::string ctph, std::string gly)
  : ico(i), scssName(scss), cardTextPlaceholder(ctph), glyph(gly) { }

}
