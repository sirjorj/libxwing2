#include "basesize.h"

namespace libxwing2 {

// Base Size
std::vector<BaseSize> BaseSize::baseSizes = {
  { BSz::None,   "None"   },
  { BSz::Small,  "Small"  },
  { BSz::Medium, "Medium" },
  { BSz::Large,  "Large"  },
  { BSz::Huge,   "Huge"   },
  { BSz::All,    "All"    },
};
  /*
BSz operator|(BSz a, BSz b) {
  return static_cast<BSz>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

BSz operator&(BSz a, BSz b) {
  return static_cast<BSz>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}
  */
BaseSizeNotFound::BaseSizeNotFound(BSz b) : runtime_error("Base Size not found (enum " + std::to_string((int)b) + ")") { }

BaseSize BaseSize::GetBaseSize(BSz bsz) {
  for(const BaseSize& basesize : BaseSize::baseSizes) {
    if(basesize.GetBSz() == bsz) {
      return basesize;
    }
  }
  throw BaseSizeNotFound(bsz);
}

std::vector<BSz> BaseSize::GetAllBSzs() {
  std::vector<BSz> ret;
  for(const BaseSize& basesize : BaseSize::baseSizes) {
    ret.push_back(basesize.GetBSz());
  }
  return ret;
}

BSz         BaseSize::GetBSz()  const { return this->bsz; }
std::string BaseSize::GetName() const { return this->name; }

BaseSize::BaseSize(BSz         b,
                   std::string n)
  : bsz(b), name(n) { }

}
