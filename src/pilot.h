#pragma once
#include "action.h"
#include "attack.h"
#include "chargeable.h"
#include "faction.h"
#include "force.h"
#include "plt.h"
#include "ship.h"
#include "shipability.h"
#include "text.h"
#include "upgrade.h"
#include <list>
#include <map>
#include <vector>

namespace libxwing2 {

class PilotNotFound : public std::runtime_error {
 public:
  PilotNotFound(Plt plt);
  PilotNotFound(std::string pltstr);
};

class Pilot {
 public:
  // factories
  static Pilot            GetPilot(Plt pilot);
  static Pilot            GetPilot(std::string pltstr);
  static std::vector<Plt> GetAllPlts(std::vector<Fac> facs={});
  static std::vector<Plt> FindPlt(std::string);
  static std::vector<Plt> FindPlt(std::string, std::vector<Plt> src);
  static std::vector<Plt> Search(std::string s);

  // maintenance
  static void SanityCheck();

  // plt/shp/fac
  Plt         GetPlt()        const;
  std::string GetPltStr()     const;
  Fac         GetFac()        const;
  Faction     GetFaction()    const;
  Shp         GetShp()        const;
  Ship        GetShip()       const;

  // stats
  int8_t      GetNatInitiative()       const;
  int8_t      GetModInitiative()       const;
  int8_t      GetNatEngagement()       const;
  int8_t      GetModEngagement()       const;
  uint8_t     GetLimited()             const;
  std::string GetName()                const;
  std::string GetShortName()           const;
  std::string GetSubtitle()            const;
  PriAttacks  GetNatAttacks()          const;
  PriAttacks  GetModAttacks()          const;
  int8_t      GetNatAgility()          const;
  int8_t      GetModAgility()          const;
  int8_t      GetNatHull()             const;
  int8_t      GetModHull()             const;
  Chargeable  GetNatShield()           const;
  Chargeable  GetModShield()           const;
  Chargeable  GetNatEnergy()           const;
  Chargeable  GetModEnergy()           const;
  Chargeable  GetNatCharge()           const;
  Chargeable  GetModCharge()           const;
  Force       GetNatForce()            const;
  Force       GetModForce()            const;
  std::optional<SAb>         GetSAb()  const;
  std::optional<ShipAbility> GetShipAbility() const;
  ActionBar   GetNatActions()          const;
  ActionBar   GetModActions()          const;
  bool        HasAbility()             const;
  Text GetText() const;

  // adjustables
  std::optional<int16_t> GetNatCost(PtL ptl=PtL::Current)          const;
  std::optional<int16_t> GetModCost(PtL ptl=PtL::Current)          const;
  std::optional<int16_t> GetUpgCost(Upg upg, PtL ptl=PtL::Current) const;
  std::optional<bool>    GetHyperspace(PtL ptl=PtL::Current)       const;
  std::optional<UpgradeBar> GetNatUpgradeBar(PtL ptl=PtL::Current) const;
  std::optional<UpgradeBar> GetModUpgradeBar(PtL ptl=PtL::Current) const;
  std::optional<KeywordList> GetKeywordList(PtL ptl=PtL::Current)  const;

  // other
  bool       IsUnreleased()    const;
  Maneuvers  GetNatManeuvers() const;
  Maneuvers  GetModManeuvers() const;

  std::vector<Upgrade>& GetAppliedUpgrades();
  std::vector<Upgrade> GetAppliedUpgrades() const;
  //std::map<UpT, std::vector<Upgrade>> GetOrganizedUpgrades() const;
  void ApplyUpgrade(Upgrade u);

 private:
  // stats
  Plt     plt;
  Fac     fac;
  Shp     shp;
  uint8_t     initiative;
  uint8_t     engagement;
  uint8_t     limited;
  std::string pilotName;
  std::string pilotSubtitle;
  PriAttacks  attacks;
  int8_t      agility;
  int8_t      hull;
  Chargeable  shield;
  Chargeable  energy;
  Chargeable  charge;
  Force       force;
  std::optional<SAb> sab;
  ActionBar   actions;
  bool        hasAbility;
  std::string text;

  // state
  std::vector<Upgrade> appliedUpgrades;

  // db
  static std::vector<Pilot> pilots;

  // ctor
  Pilot(Plt         p,
	Fac         f,
	Shp         s,
        int8_t      init,
        uint8_t     lim,
	std::string name,
	std::string subt,
	PriAttacks  att,
	int8_t      agi,
	int8_t      hul,
	int8_t      shi,
	Chargeable  chr,
	Force       frc,
	std::optional<SAb> sa,
	ActionBar   act,
	bool        abi,
	std::string txt
      );

  Pilot(Plt         p,
	Fac         f,
	Shp         s,
        int8_t      init,
	int8_t      enga,
	std::string name,
	PriAttacks  att,
	int8_t      agi,
	int8_t      hul,
	Chargeable  shi,
	Chargeable  enr,
	Chargeable  chr,
	Force       frc,
	std::optional<SAb> sa,
	ActionBar   act,
	bool        abi,
	std::string txt
      );



  // The (now optional) GameState Object
  class GameState {
  public:
    GameState(Pilot *p);

    bool IsEnabled() const;
    void Enable();
    void Disable();
    int8_t GetCurShield() const;
    int8_t GetShieldHits() const;
    void ShieldUp();
    void ShieldDn();
    int8_t GetCurHull() const;
    int8_t GetHullHits() const;
    void HullUp();
    void HullDn();
    int8_t GetCurCharge() const;
    void ChargeUp();
    void ChargeDn();
    int8_t GetCurForce() const;
    void ForceUp();
    void ForceDn();

    // crit stuff...

  private:
    Pilot *pilot;
    bool isEnabled;
    int8_t shieldHits;
    int8_t hullHits;
    int8_t charge;
    int8_t force;
  };

  std::optional<GameState> gameState;

 public:
  GameState& GS();

};

}
