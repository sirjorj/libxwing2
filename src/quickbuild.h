#pragma once
#include "qb.h"
#include "pilot.h"
#include "upgrade.h"
#include <stdint.h>
#include <vector>

namespace libxwing2 {

enum class QBCT {
  PDF,
  Exp,
  Promo
};

struct QBCard {
  QBC qbc;
  std::vector<QB> qbs;
  QBCT qbct;
  std::string qbctData;
};

class QuickBuildNotFound : public std::runtime_error {
 public:
  QuickBuildNotFound(QB qb);
};

class QuickBuildCardNotFound : public std::runtime_error {
 public:
  QuickBuildCardNotFound(QBC qbc);
};

struct QBPlt {
  Plt plt;
  std::vector<Upg> upgs;
};

struct QBPilot {
  Pilot pilot;
  std::vector<Upgrade> upgrades;
};

class QuickBuild {
 public:
  static QuickBuild       GetQuickBuild(QB qb);
  static std::vector<QB>  GetQBs(Plt t);
  static std::vector<QB>  GetAllQBs();
  static QBCard           GetQBCard(QBC);
  static std::vector<QBC> GetAllQBCs();
  //static void Check();

  QB                   GetQB()       const;
  std::string          GetName()     const;
  uint8_t              GetThreat()   const;
  std::vector<QBPlt>   GetQBPlts()   const;
  std::vector<QBPilot> GetQBPilots() const;

 private:
  QB qb;
  std::string name;
  uint8_t threat;
  std::vector<QBPlt> qbps;

  static std::vector<QuickBuild> quickBuilds;
  static std::vector<QBCard>     qbCards;

  QuickBuild(QB                qb,
	     std::string        n,
	     uint8_t            t,
	     std::vector<QBPlt> q);
};

}
