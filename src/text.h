#pragma once
#include <string>
#include <vector>



namespace libxwing2 {

class Text {
 private:
  const std::string text;

 public:
  Text(std::string t);

  const std::string GetCleanText() const;
  const std::string GetMarkedText() const;
  const std::vector<std::string> GetCleanSegments() const;
  const std::vector<std::string> GetMarkedSegments() const;
};

}
