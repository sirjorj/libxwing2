#include "ship.h"
#include "helper.h"
#include "shared.h"
#include <string>

namespace libxwing2 {

// Ship
ShipNotFound::ShipNotFound(Shp shp)            : runtime_error("Ship not found (enum " + std::to_string((int)shp) + ")") { }
ShipNotFound::ShipNotFound(std::string shpstr) : runtime_error("Ship not found '" + shpstr + "'") { }

Ship Ship::GetShip(Shp shp) {
  for(const Ship& ship : Ship::ships) {
    if(ship.GetShp() == shp) {
      return ship;
    }
  }
  throw ShipNotFound(shp);
}

Ship Ship::GetShip(std::string shpstr) {
  for(const Ship& ship : Ship::ships) {
    if(ship.GetShpStr() == shpstr) {
      return ship;
    }
  }
  throw ShipNotFound(shpstr);
}

std::vector<Shp> Ship::GetAllShps() {
  std::vector<Shp> ret;
  for(const Ship& ship : Ship::ships) {
    ret.push_back(ship.GetShp());
  }
  return ret;
}

std::vector<Shp> Ship::FindShp(std::string s) {
  std::vector<Shp> ret;
  std::string ss = ToLower(s); // searchString
  for(const Ship& ship : Ship::ships) {
    if((ToLower(ship.GetName()).find(ss)      != std::string::npos) ||
       (ToLower(ship.GetShortName()).find(ss) != std::string::npos)
       ) {
      ret.push_back(ship.GetShp());
    }
  }
  return ret;
}

std::vector<Shp> Ship::Search(std::string s) {
  std::vector<Shp> ret;
  std::string ss = ToLower(s);
  for(Shp shp : Ship::GetAllShps()) {
    Ship ship = Ship::GetShip(shp);
    if((ToLower(ship.GetShpStr()).find(ss)    != std::string::npos) ||
       (ToLower(ship.GetName()).find(ss)      != std::string::npos) ||
       (ToLower(ship.GetShortName()).find(ss) != std::string::npos) ||
       (ToLower(ship.GetDialId()).find(ss)    != std::string::npos)
       ) {
      ret.push_back(ship.GetShp());
    }
  }
  return ret;
}

void Ship::SanityCheck() {
  std::vector<std::string> entries;
  std::vector<std::string> dupes;
  int counter=0;
  printf("Checking Ships");
  for(const Ship& s : Ship::ships) {
    counter++;
    std::string newOne = s.GetShpStr();
    if(std::find(entries.begin(), entries.end(), newOne) == entries.end()) {
      printf("."); fflush(stdout);
    } else {
      dupes.push_back(newOne);
      printf("X"); fflush(stdout);
    }
    entries.push_back(newOne);
  }
  printf("%zu\n", entries.size());
  if(dupes.size()) {
    printf("Dupes: %zu\n", dupes.size());
    for(const std::string& d : dupes) {
      printf("  %s\n", d.c_str());
    }
  }
}

Shp         Ship::GetShp()        const { return this->shp; }
std::string Ship::GetShpStr()     const { return ToStringHelper(this->dialId);}
std::string Ship::GetName()       const { return this->name; }
std::string Ship::GetShortName()  const { return this->shortName; }
std::string Ship::GetDialId()     const { return this->dialId; }
  BSz         Ship::GetBSz()        const { return this->bsz; }
BaseSize    Ship::GetBaseSize()   const { return BaseSize::GetBaseSize(this->bsz); }
Maneuvers   Ship::GetManeuvers()  const { return this->maneuvers; }

Ship::Ship(Shp         t,
           std::string n,
           std::string s,
	   std::string d,
           BSz         b,
           Maneuvers   m)
  : shp(t), name(n), shortName(s), dialId(d), bsz(b), maneuvers(m) { }

}
