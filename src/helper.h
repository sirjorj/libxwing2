#pragma once
#include <algorithm>
#include <string>

static inline std::string ToStringHelper(std::string input) {
  std::string ret = input;
  ret.erase(std::remove(ret.begin(), ret.end(), ' '),  ret.end());
  ret.erase(std::remove(ret.begin(), ret.end(), '?'),  ret.end());
  ret.erase(std::remove(ret.begin(), ret.end(), '\''), ret.end());
  ret.erase(std::remove(ret.begin(), ret.end(), '-'),  ret.end());
  return ret;
}

static inline std::string ReplaceString(std::string str, std::string rem, std::string rep) {
  size_t p;
  while((p=str.find(rem)) != std::string::npos) {
    str.replace(p, rem.size(), rep);
  }
  return str;
}
