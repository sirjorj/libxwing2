#include "quickbuild.h"

namespace libxwing2 {



QuickBuildNotFound::QuickBuildNotFound(QB qb) : runtime_error("QuickBuild not found: (enum " + std::to_string((int)qb) + ")") { }
QuickBuildCardNotFound::QuickBuildCardNotFound(QBC qbc) : runtime_error("QuickBuildCard not found: (enum " + std::to_string((int)qbc) + ")") { }

QuickBuild QuickBuild::GetQuickBuild(QB qb) {
  for(const QuickBuild& quickBuild : QuickBuild::quickBuilds) {
    if(quickBuild.GetQB() == qb) {
      return quickBuild;
    }
  }
  throw QuickBuildNotFound(qb);
}

std::vector<QB> QuickBuild::GetQBs(Plt plt) {
  std::vector<QB> ret;
  for(const QuickBuild& quickBuild : QuickBuild::quickBuilds) {
    for(const QBPlt& qbp : quickBuild.GetQBPlts()) {
      if(qbp.plt == plt) {
	ret.push_back(quickBuild.GetQB());
	break;
      }
    }
  }
  return ret;
}

std::vector<QB> QuickBuild::GetAllQBs() {
  std::vector<QB> ret;
  for(const QuickBuild& quickBuild : QuickBuild::quickBuilds) {
    ret.push_back(quickBuild.GetQB());
  }
  return ret;
}

QBCard QuickBuild::GetQBCard(QBC qbc) {
  for(const QBCard &qbCard : QuickBuild::qbCards) {
    if(qbCard.qbc == qbc) {
      return qbCard;
    }
  }
  throw QuickBuildCardNotFound(qbc);
}

std::vector<QBC> QuickBuild::GetAllQBCs() {
  std::vector<QBC> ret;
  for(const QBCard &qbCard : QuickBuild::qbCards) {
    ret.push_back(qbCard.qbc);
  }
  return ret;
}




  /*
void QuickBuild::Check() {
  std::map<QB, int> counter;
  for(const QuickBuild& qb : QuickBuild::GetAllQuickBuilds()) {
    counter[qb.GetQB()]++;
  }
  for(const std::pair<QB, int>& p : counter) {
    if(p.second > 1) {
      printf("%dx - %d (%s)\n", p.second, p.first, QuickBuild::GetQuickBuild(p.first).GetQBPilots()[0].pilot.GetName().c_str());
    }
  }
}
  */
QB                   QuickBuild::GetQB()       const { return this->qb; }
std::string          QuickBuild::GetName()     const { return this->name; }
uint8_t              QuickBuild::GetThreat()   const { return this->threat; }
std::vector<QBPlt>   QuickBuild::GetQBPlts()   const { return this->qbps; }
std::vector<QBPilot> QuickBuild::GetQBPilots() const {
  std::vector<QBPilot> ret;
  for(const QBPlt& q : this->qbps) {
    QBPilot n = { Pilot::GetPilot(q.plt),
		  std::vector<Upgrade>() };
    for(Upg u : q.upgs) {
      n.upgrades.push_back(Upgrade::GetUpgrade(u));
    }
    ret.push_back(n);
  }
  return ret;
}

QuickBuild::QuickBuild(QB                qb,
		       std::string        n,
		       uint8_t            t,
		       std::vector<QBPlt> q)
  : qb(qb), name(n), threat(t), qbps(q) { }

}
