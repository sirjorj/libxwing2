#pragma once
#include "faction.h"
#include "text.h"
#include <optional>
#include <string>

namespace libxwing2 {

enum class Rem {
  BuzzDroids,
  DRK1,
  Satellite,
  SensBuoyBlue,
  SensBuoyRed,
  Shuttle,
  TrackingTrp,
  Turret,
};

class RemoteNotFound : public std::runtime_error {
 public:
  RemoteNotFound(Rem r);
};



class Remote {
 public:
  // factories
  static Remote GetRemote(Rem r);
  static std::vector<Rem> GetAllRems();

  Rem                GetRem()        const;
  uint8_t            GetInitiative() const;
  std::string        GetName()       const;
  std::optional<Fac> GetFac()        const;
  uint8_t            GetAgility()    const;
  uint8_t            GetHull()       const;
  Text               GetText()       const;

 private:
  const Rem                rem;
  const uint8_t            initiative;
  const std::string        name;
  const std::optional<Fac> fac;
  const uint8_t            agility;
  const uint8_t            hull;
  const std::string        text;

  static std::vector<Remote> remotes;

  Remote(Rem r, uint8_t i, std::string n, std::optional<Fac> f, uint8_t a, uint8_t h, std::string t);
};

}
