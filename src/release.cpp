#include "release.h"
#include <ctime>

namespace libxwing2 {

ReleaseNotFound::ReleaseNotFound(Rel r)         : runtime_error("Release not found (enum " + std::to_string((int)r) + ")") { }
ReleaseNotFound::ReleaseNotFound(std::string r) : runtime_error("Release not found (" + r + ")") { }

bool operator==(const RelDate& a, const RelDate& b) {
  return (a.year  == b.year)  &&
         (a.month == b.month) &&
         (a.date  == b.date);
}
bool operator!=(const RelDate& a, const RelDate& b) {
  return !(a==b);
}
bool operator>(const RelDate& a, const RelDate& b) {
  if(a.year > b.year) { return true; }
  else if(a.year == b.year) {
    if(a.month > b.month) { return true; }
    else if(a.month == b.month) {
      if(a.date > b.date) { return true; }
    }
  }
  return false;
}
bool operator<(const RelDate& a, const RelDate& b) {
  return b > a;
}
bool operator>=(const RelDate& a, const RelDate& b) {
  return (a>b) || (a==b);
}
bool operator<=(const RelDate& a, const RelDate& b) {
  return (a<b) || (a==b);
}


int GetDaysBetween(const RelDate rdA, const RelDate rdB) {
  struct std::tm a = {0,0,0,rdA.date, rdA.month-1, rdA.year-1900};
  struct std::tm b = {0,0,0,rdB.date, rdB.month-1, rdB.year-1900};
  std::time_t x = std::mktime(&a);
  std::time_t y = std::mktime(&b);
  return std::difftime(y, x) / (60 * 60 * 24);
}



Release Release::GetRelease(Rel rel) {
  for(const Release& release : Release::releases) {
    if(release.GetRel() == rel) {
      return release;
    }
  }
  throw ReleaseNotFound(rel);
}

Release Release::GetRelease(std::string sku) {
  for(const Release& r : Release::releases) {
    if(r.GetSku() == sku) {
      return r;
    }
  }
  throw ReleaseNotFound(sku);
}

std::vector<Rel> Release::GetByPilot(Plt p) {
  std::vector<Rel> ret;
  for(const Release& release : Release::releases) {
    for(Plt ps : release.pilots) {
      if(ps == p) {
        ret.push_back(release.GetRel());
        break;
      }
    }
  }
  return ret;
}

std::vector<Rel> Release::GetByUpgrade(Upg u) {
  std::vector<Rel> ret;
  for(const Release& release : Release::releases) {
    for(Upg us : release.upgrades) {
      if(us == u) {
        ret.push_back(release.GetRel());
        break;
      }
    }
  }
  return ret;
}

std::vector<Rel> Release::GetAllRels() {
  std::vector<Rel> ret;
  for(const Release& release : Release::releases) {
    ret.push_back(release.GetRel());
  }
  return ret;
}

Rel                  Release::GetRel()                 const { return this->rel; }
std::string          Release::GetSku()                 const { return this->sku;  }
std::string          Release::GetIsbn()                const { return this->isbn; }
std::string          Release::GetName()                const { return this->name; }
RelGroup             Release::GetGroup()               const { return this->group; }
RelDate              Release::GetAnnounceDate()        const { return this->announceDate; }
RelDate              Release::GetReleaseDate()         const { return this->releaseDate; }
Article              Release::GetAnnouncementArticle() const { return this->articles.announcement; }
std::vector<Article> Release::GetPreviewArticles()     const { return this->articles.previews; }
Article              Release::GetReleaseArticle()      const { return this->articles.release; }
std::string          Release::GetNotes()               const { return this->notes; }
std::vector<RelShip> Release::GetShips()               const { return this->ships; }
std::vector<Plt>     Release::GetPilots()              const { return this->pilots; }
std::vector<Upg>     Release::GetUpgrades()            const { return this->upgrades; }
std::vector<Cnd>     Release::GetConditions()          const { return this->conditions; }
std::vector<Env>     Release::GetEnvironments()        const { return this->environments; }
TokenCollection      Release::GetTokens()              const { return this->tokens; }
bool                 Release::IsUnreleased()           const {
  if(this->GetReleaseDate().year == 0
     && this->GetReleaseDate().month == 0
     && this->GetReleaseDate().date == 0) {
    return true;
  }
  time_t now = std::time(0);
  time_t rel;
  {
    std::tm tm = {0};
    tm.tm_year = this->releaseDate.year - 1900;
    tm.tm_mon  = this->releaseDate.month - 1;
    tm.tm_mday = this->releaseDate.date;
    rel = mktime(&tm);
  }
  return rel > now;
}

Release::Release(Rel                  r,
		 std::string          sku,
                 std::string          isbn,
                 std::string          nam,
                 RelGroup             grp,
                 RelDate              ad,
                 RelDate              rd,
                 RelArticles          art,
		 std::string          note,
                 std::vector<RelShip> shps,
                 std::vector<Plt>     plts,
                 std::vector<Upg>     upgs,
                 std::vector<Cnd>     cond,
                 std::vector<Env>     env,
                 TokenCollection      tok)
: rel(r), sku(sku), isbn(isbn), name(nam), group(grp), announceDate(ad), releaseDate(rd), articles(art), notes(note),
  ships(shps), pilots(plts), upgrades(upgs), conditions(cond), environments(env), tokens(tok) { }

}
