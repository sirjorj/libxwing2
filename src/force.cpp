#include "force.h"


namespace libxwing2 {

// Base Size
std::vector<ForceAffiliation> ForceAffiliation::forceAffiliations = {
  { FAf::None,  "None"  },
  { FAf::Dark,  "Dark"  },
  { FAf::Light, "Light" }
};

FAf operator|(FAf a, FAf b) {
  return static_cast<FAf>(static_cast<uint32_t>(a) | static_cast<uint32_t>(b));
}

FAf operator&(FAf a, FAf b) {
  return static_cast<FAf>(static_cast<uint32_t>(a) & static_cast<uint32_t>(b));
}

ForceAffiliationNotFound::ForceAffiliationNotFound(FAf f) : runtime_error("Force Affiliation not found (enum " + std::to_string((int)f) + ")") { }

ForceAffiliation ForceAffiliation::GetForceAffiliation(FAf faf) {
  for(const ForceAffiliation& fa : ForceAffiliation::forceAffiliations) {
    if(fa.GetFAf() == faf) {
      return fa;
    }
  }
  throw ForceAffiliationNotFound(faf);
}

FAf         ForceAffiliation::GetFAf()  const { return this->faf; }
std::string ForceAffiliation::GetName() const { return this->name; }

ForceAffiliation::ForceAffiliation(FAf f,
				   std::string n)
  : faf(f), name(n) { }



Force::Force(int8_t cap,
	     int8_t recur,
	     FAf af)
  : Chargeable(cap, recur), faf(af) {}

FAf Force::GetFAf() const { return this->faf; }
  ForceAffiliation Force::GetForceAffiliation() const { return ForceAffiliation::GetForceAffiliation(this->faf); }

}

