#pragma once
#include "attack.h"
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// deck
enum class Dck {
  CoreSet,
  HugeShip,
};

class DeckNotFound : public std::runtime_error {
 public:
  DeckNotFound(Dck d);
  DeckNotFound(std::string d);
};

class Deck {
 public:
  static Deck             GetDeck(Dck dck);
  static Deck             GetDeck(std::string dckstr);
  static std::vector<Dck> GetAllDcks();
  Dck         GetDck()    const;
  std::string GetDckStr() const;
  std::string GetName()   const;

 private:
  Dck dck;
  std::string name;

  static std::vector<Deck> deck;

  Deck(Dck         d,
       std::string n);
};



// Precision shot
enum class PrS {
  BridgeHit,
  ControlsDisrupted,
  DriveDamaged,
  TurretDisabled,
};

class PrecisionShotNotFound : public std::runtime_error {
 public:
  PrecisionShotNotFound(PrS p);
};

class PrecisionShot {
 public:
  static PrecisionShot GetPrecisionShot(PrS p);

  PrS GetPrS() const;
  std::string GetName() const;
  std::vector<Arc> GetArcs() const;
  std::string GetText() const;

 private:
  PrS prs;
  std::string name;
  std::vector<Arc> arcs;
  std::string text;

  static std::vector<PrecisionShot> precisionShots;

  PrecisionShot(PrS p,
		std::string n,
		std::vector<Arc> a,
		std::string t);
};



// trait
enum class Trt {
  Crew,
  Pilot,
  Ship,
};

class TraitNotFound : public std::runtime_error {
 public:
  TraitNotFound(Trt d);
};

class Trait {
 public:
  static Trait GetTrait(Trt t);
  Trt          GetTrt()  const;
  std::string  GetName() const;

 private:
  Trt trt;
  std::string name;

  static std::vector<Trait> trait;

  Trait(Trt         t,
        std::string n);
};



// Damage Condition
enum class Dmg {
  // Core set
  PanickedPilot,
  BlindedPilot,
  WoundedPilot,
  StunnedPilot,
  ConsoleFire,
  DamagedEngine,
  WeaponsFailure,
  HullBreach,
  StructuralDamage,
  DamagedSensorArray,
  LooseStabilizer,
  DisabledPowerRegulator,
  FuelLeak,
  DirectHit,
  // Huge Ship
  DamagedCircuitry,
  ScrambledOrders,
  HullRupture,
  StabilizersDamaged,
  PowerSurge,
  EngineCoolantLeak,
  PanickedCrew,
  ExplodingColsoles,
  CrewShaken,
  AtmosphereLeak,
};

typedef std::pair<Dmg,Dck> DmgCrd;

// damage deck
class DamageCardNotFound : public std::runtime_error {
 public:
  DamageCardNotFound(DmgCrd dc);
};

class DamageCard {
 public:
  static DamageCard       GetDamageCard(DmgCrd dc);
  static std::vector<Dmg> GetAllDmgs(Dck d);

  Dmg                          GetDmg()           const;
  std::vector<uint8_t>         GetIds()           const;
  uint8_t                      GetCount()         const;
  Dck                          GetDck()           const;
  Deck                         GetDeck()          const;
  Trt                          GetTrt()           const;
  Trait                        GetTrait()         const;
  std::optional<PrS>           GetPrS()           const;
  std::optional<PrecisionShot> GetPrecisionShot() const;
  std::string                  GetName()          const;
  std::string                  GetText()          const;

 private:
  Dmg                  dmg;
  std::vector<uint8_t> ids;
  uint8_t              count;
  Dck                  dck;
  Trt                  trt;
  std::optional<PrS>   precShot;
  std::string          name;
  std::string          text;

  static std::vector<DamageCard> damageCards;

  DamageCard(Dmg                  dm,
	     std::vector<uint8_t> id,
             uint8_t              c,
     	     Dck                  dc,
	     Trt                  tr,
	     std::optional<PrS>   ps,
             std::string          na,
             std::string          tx);
};

}
