#pragma once
#include <stdexcept>
#include <string>
#include <vector>

namespace libxwing2 {

// difficulty
enum class Dif {
  Blue   = 0x01,
  White  = 0x02,
  Red    = 0x04,
  Purple = 0x08,
  All    = 0x0F
};

// prefix
Dif& operator++(Dif&);
Dif& operator--(Dif&);
// postfix
Dif operator++(Dif&,int);
Dif operator--(Dif&,int);

class DifficultyNotFound : public std::runtime_error {
 public:
  DifficultyNotFound(Dif d);
};

class Difficulty {
 public:
  static Difficulty GetDifficulty(Dif d);
  static std::vector<Dif> GetAllDifs();
  Dif         GetDif()  const;
  std::string GetName() const;

 private:
  Dif dif;
  std::string name;

  static std::vector<Difficulty> difficulties;

  Difficulty(Dif         d,
	     std::string n);
};

}
