#pragma once
#include "action.h"
#include "attack.h"
#include "faction.h"
#include "force.h"
#include "shared.h"
#include "ship.h"
#include "shipability.h"
#include "upgradetype.h"

namespace libxwing2 {

class Pilot;
class Upgrade;

typedef std::vector<Shp> Ships;

enum class Plt;
enum class Upg;
typedef std::vector<Arc> Arcs;
typedef std::function<std::vector<std::string>(const Pilot&, const std::vector<Pilot>&)> RestrictionCheck;
typedef struct { std::vector<Fac> f; std::string i;} FacIncluding;

class Restriction {
 public:
  Restriction();
  Restriction(std::string t);

  Restriction& ByAct(SAct a);
  Restriction& ByAgi(int a);
  Restriction& ByArc(Arc a);
  Restriction& ByBase(std::vector<BSz> b);
  Restriction& ByEnergyMin(int8_t e);
  Restriction& ByEquippedUpg(UpT u);
  Restriction& ByFac(std::vector<Fac> f);
  Restriction& ByFacInc(std::vector<Fac> f, std::string i);
  Restriction& ByForce(FAf f);
  Restriction& ByInitMax(int8_t i);
  Restriction& ByInitMin(int8_t i);
  Restriction& ByLimit(int8_t l);
  Restriction& ByShipAbility(SAb s);
  Restriction& ByShips(std::vector<Shp> s);
  Restriction& ByShieldMin(int8_t s);
  Restriction& IsSolitary();
  Restriction& IsStandardized();

  std::string        GetText() const;
  std::vector<Fac>   GetFactions() const;
  std::vector<Fac>   GetAllowedFactions() const;
  std::vector<BSz>   GetBaseSizes() const;
  std::optional<UpT> GetEquippedUpgrade() const;
  FAf                GetFAf() const;
  std::vector<std::string> CheckRestrictions(const Pilot&, const Upgrade&, const std::vector<Pilot>&) const;
  bool GetSolitary() const;
  bool IsHugeShipOnly() const;

 private:
  std::optional<std::string>      _text;
  std::optional<SAct>             _act;
  std::optional<int8_t>           _agi;
  std::optional<Arc>              _arc;
  std::optional<std::vector<BSz>> _baseSizes;
  std::optional<int8_t>           _energyMin;
  std::optional<UpT>              _equippedUpgrade;
  std::optional<std::vector<Fac>> _factions;
  std::optional<FacIncluding>     _factionsIncluding;
  std::optional<FAf>              _forceAf;
  std::optional<int8_t>           _initMax;
  std::optional<int8_t>           _initMin;
  std::optional<int8_t>           _limit;
  std::optional<SAb>              _sab;
  std::optional<std::vector<Shp>> _ships;
  std::optional<int8_t>           _shieldMin;
  std::optional<bool>             _solitary;
  std::optional<bool>             _standardized;
};

}
